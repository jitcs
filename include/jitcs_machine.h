//===-- jitcs_machine.h -----------------------------------------*- C++ -*-===//
// Target machine info
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// IMachineInfo provides helpers to create calling convention objects and 
// function. Particular architectures (e.g. x86) are defined in different 
// header files and will fill in especially IMachineDetails. IMachineDetails
// contains all information necessary for the compiler component, but is of no
// interest to clients.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_MACHINE_H_
#define _JITCS_MACHINE_H_

#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include "jitcs_cpu.h"
#include "jitcs_fnctypeinfo.h"
#include "jitcs_ids.h"
#include <memory>

#ifdef __GNUC__
#define INLINEIT inline __attribute__((always_inline))
#else
#define INLINEIT inline
#endif

namespace jitcs {
struct Instruction;
struct VirtualRegister;
//class BBlock;
//struct CCInfo;
class BasicBlock;
class CallingConvention;
class CodeGenHelper;
class Function;
class IDumper;
class TempAllocator;

class IMachineDetails;

class IMachineInfo {
protected:
  IMachineInfo(const CPUInfo&);
  virtual ~IMachineInfo() = default;
private:
  IMachineInfo() = delete;
  IMachineInfo(const IMachineInfo&) = delete;
  IMachineInfo& operator =(const IMachineInfo&) = delete;
public:
  CPUInfo cpu;
  
  virtual Ref<const IMachineDetails> details() const = 0;
  virtual std::shared_ptr<const CallingConvention>
    getCC(FTSubType, Slice<FTTypeId> results, Slice<FTTypeId> params) = 0;

  std::unique_ptr<Function>
    createFnc(RefCounter<TempAllocator>, std::shared_ptr<const CallingConvention>);

  template <typename T>
  INLINEIT std::shared_ptr<const CallingConvention> getCC() {
    return _getCC<typename GetFTSubType<T>::FType>(GetFTSubType<T>::Value);
  }
  template <typename T>
  INLINEIT std::unique_ptr<Function> createFnc(RefCounter<TempAllocator> tmp) {
    return createFnc(tmp, _getCC<typename GetFTSubType<T>::FType>
               (GetFTSubType<T>::Value));
  }

protected:
  template <typename T>
  std::shared_ptr<const CallingConvention> _getCC(FTSubType sub) {
    FTTypeId res[1];
    FTTypeId par[10];
    res[0] = GetFTResultId<T>::Value;
    static const int N = GetFTParamCount<T>::Value;
    static_assert(N <= 10, "too many parameters in function type");
    _fillFTParams<T, N>(par);
    return getCC(sub, Slice<FTTypeId>(res, 1), Slice<FTTypeId>(par, N));
  }

private:
  template <typename T, uint N, bool B> struct _FTParamsFiller_;
  template <typename T, uint N>
  struct _FTParamsFiller_<T, N, true> {
    static inline void run(FTTypeId* arr) {
      static_assert(GetFTParamId<T, N>::OK, "unsupported parameter type");
      arr[N] = GetFTParamId<T, N>::Value;
      _FTParamsFiller_<T, N + 1, (N + 1 < GetFTParamCount<T>::Value)>::run(arr);
    }
  };
  template <typename T, uint N>
  struct _FTParamsFiller_<T, N, false> {
    static inline void run(FTTypeId* arr) {}
  };
  template <typename T, uint N>
  void _fillFTParams(FTTypeId* arr) {
    _FTParamsFiller_<T, 0, (0 < GetFTParamCount<T>::Value)>::run(arr);
  }

private:
  // called from RefCounter
  friend class RefCounter<IMachineInfo>;
  void _incRef() { ++_refCnt; }
  void _decRef() { if (!--_refCnt) _release(); }
  void _release();

private:
  size_t _refCnt;
};

} // end namespace jitcs

#endif 
// _JITCS_MACHINE_H_
