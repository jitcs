//===-- jitcs_bblock.h ------------------------------------------*- C++ -*-===//
// The public representation of a BasicBlock as used in JITCS.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Enumeration of predecessors, successors and instructions is possible, but 
// slow.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_BBLOCK_H_
#define _JITCS_BBLOCK_H_

#include "jitcs_adt_enumerator.h"
#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"

namespace jitcs {
class BasicBlockImpl;
class MachineDumper;
struct Instruction;

class BasicBlock {
protected:
  BasicBlock() {}
  // there should not EVER be a chance for the destructor being called, but
  // g++ insists.
  ~BasicBlock() = default;

public:
  BasicBlock(const BasicBlock &) = delete;
  void operator=(const BasicBlock &) = delete;
  
  virtual BasicBlockImpl& impl() = 0;
  virtual const BasicBlockImpl& impl() const = 0;
  
public:
  virtual BBId id() const = 0;
  
  Enumerator<const BasicBlock>  predecessors();
  Enumerator<const BasicBlock>  successors();
  Enumerator<const Instruction> instructions();

  virtual void dump(MachineDumper&) const = 0;

public:
  // append one or more instructions contained within the passed instruction stream
  // the passed stream must be writable and available for the entire life time of the basic block.
  // global memory should not be passed in if it contains virtual register references or similar,
  // as instructions might be modified in place for certain optimizations.
  // the same holds for memory references inside instructions.
  // returns the number of detected instructions, or 0 if an error occurred.
  virtual size_t append(Slice<iptr>) = 0;
  size_t append(iptr* p, size_t N) { return append(Slice<iptr>(p, N)); }
  template<unsigned N> inline size_t append(iptr p[N]) { 
    return append(Slice<iptr>(p, N));
  }
  // same as append, but copy the data to storage available for the life time of the basic
  // block. this can be used to pass in instruction generated on stack, or stored in global
  // memory.
  // returns the number of detected instructions, or 0 if an error occurred.
  // caution: this will NOT create copies of memory references inside instructions.
  virtual size_t append_copy(Slice<const iptr>) = 0;
  size_t append_copy(iptr const *p, size_t N) { return append_copy(Slice<const iptr>(p, N)); }
  template<unsigned N> inline size_t append_copy(iptr const p[N]) { 
    return append_copy(Slice<const iptr>(p, N));
  }
};

} // end of namespace jitcs

#endif
// _JITCS_BBLOCK_H_
