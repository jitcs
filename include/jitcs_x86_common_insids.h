//===-- jitcs_x86_common_insids.h C++ -----------------------------------*-===//
//
// Register definitions for 32bit x86 code.
//
//===----------------------------------------------------------------------===//

#ifndef _JITCS_X86_COMMON_INSIDS_H_
#define _JITCS_X86_COMMON_INSIDS_H_

namespace jitcs
{
namespace x86_common
{
  enum CondCodeId {
    CC_O  =  0,
    CC_NO =  1,
    CC_B  =  2,
    CC_AE =  3,
    CC_E  =  4,
    CC_NE =  5,
    CC_BE =  6,
    CC_A  =  7,
    CC_S  =  8,
    CC_NS =  9,
    CC_PE = 10,
    CC_PO = 11,
    CC_L  = 12,
    CC_GE = 13,
    CC_LE = 14,
    CC_G  = 15,

    CC_C  = CC_B,
    CC_NAE= CC_B,
    CC_NB = CC_AE,
    CC_NC = CC_AE,
    CC_Z  = CC_E,
    CC_NZ = CC_NE,
    CC_NA = CC_BE,
    CC_NBE= CC_A,
    CC_P  = CC_PE,
    CC_NP = CC_PO,
    CC_NGE= CC_L,
    CC_NL = CC_GE,
    CC_NG = CC_LE,
    CC_NLE= CC_G,
  };
   
} // end of namespace jitcs::x86_common
} // end of namespace jitcs
#endif
// _JITCS_X86_COMMON_INSIDS_H_
