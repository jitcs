//===-- jitcs_x64.h C++ -------------------------------------------------*-===//
//
// Header file for 64bit x86 platforms.
//
//===----------------------------------------------------------------------===//

#ifndef _JITCS_X86_64_H_
#define _JITCS_X86_64_H_

#include "jitcs_x86_64_insids.h"
#include "jitcs_x86_64_regs.h"
#include "jitcs_x86_64_cons.h"
#include "jitcs_x86_64_machine.h"

#endif
// _JITCS_X86_64_H_
