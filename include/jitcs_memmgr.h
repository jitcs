//===-- jitcs_memmgr.h ------------------------------------------*- C++ -*-===//
// A memory manager for code and data
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The manager is an allocator based on Doug Leah's malloc. Memory is allocated
// from the system with execution permission.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_MEMMGR_H_
#define _JITCS_MEMMGR_H_

#include "jitcs_base.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_adt_slice.h"
#include <memory>

namespace jitcs {
class MemoryMgrImpl;

class MemoryMgr {
public:
  struct CodeAndData {
    Slice<u8> data, code;
  };
public:
  static RefCounter<MemoryMgr> Create();
protected:
  MemoryMgr();
  ~MemoryMgr();

public:
  /* todo
   * allocate area for function relevant constants (aligned to 16 byte)
   * allocate area for code (aligned to cache line size, currently 64 byte)
   * allocated areas are stored in a function object, function objects can be forced to free allocated storage
   */
  CodeAndData allocate(size_t data, size_t code);
  CodeAndData shortenAllocation(CodeAndData, size_t data, size_t code);
  void deallocate(CodeAndData);

  bool check();
  size_t getTotalSpace();
  size_t getAllocatedSpace();
  size_t getFreeSpace();
  size_t getReachableFreeSpace();
  bool shrink();
  
private:
  // called from RefCounter
  friend class RefCounter<MemoryMgr>;
  void _incRef() { ++_refCnt; }
  void _decRef() { if (!--_refCnt) _release(); }
  void _release();

private:
  std::unique_ptr<MemoryMgrImpl> _mgr; 
  size_t _refCnt;
};

} // end of namespace jitcs

#endif
// _JITCS_MEMMGR_H_
