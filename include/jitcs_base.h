//===-- jitcs_base.h --------------------------------------------*- C++ -*-===//
// Definition of basic types and basic macros.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Almost all other source files of JITCS include this header.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_BASE_H_
#define _JITCS_BASE_H_

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#ifdef _MSC_VER
// import nullptr, offsetof
#include <stddef.h>
#endif

// TODO: check the existence of nullptr

// first a few checks if the types are correctly defined, 
// the architecture is 32 or 64 bit
static_assert(sizeof(int8_t) == 1 && sizeof(uint8_t) == 1,
              "wrong size of int8_t");
static_assert(sizeof(int16_t) == 2 && sizeof(uint16_t) == 2,
              "wrong size of int16_t");
static_assert(sizeof(int32_t) == 4 && sizeof(uint32_t) == 4,
              "wrong size of int32_t");
static_assert(sizeof(int64_t) == 8 && sizeof(uint64_t) == 8,
              "wrong size of int64_t");

static_assert(sizeof(float) == 4 && sizeof(double) == 8,
              "wrong size of float or double");

static_assert(sizeof(intptr_t) == sizeof(void*), "wrong size of intptr_t");
static_assert(sizeof(uintptr_t) == sizeof(void*), "wrong size of uintptr_t");

static_assert(sizeof(int) == 4 || sizeof(int) == 8, "wrong size of int");
static_assert(sizeof(void*) == 4 || sizeof(void*) == 8, "wrong size of void*");
static_assert(sizeof(int) <= sizeof(void*), "wrong size of void* < int");


namespace jitcs {
// type shortcut alias
typedef unsigned int       uint;

// type constructors for use in templates
template <unsigned N> struct Int {};
template <> struct Int<8>  { typedef int8_t  Type; };
template <> struct Int<16> { typedef int16_t Type; };
template <> struct Int<32> { typedef int32_t Type; };
template <> struct Int<64> { typedef int64_t Type; };

template <unsigned N> struct UInt {};
template <> struct UInt<8>  { typedef uint8_t  Type; };
template <> struct UInt<16> { typedef uint16_t Type; };
template <> struct UInt<32> { typedef uint32_t Type; };
template <> struct UInt<64> { typedef uint64_t Type; };

template <unsigned N> struct Float {};
template <> struct Float<32> { typedef float  Type; };
template <> struct Float<64> { typedef double Type; };

// more type aliases
typedef int8_t     i8;
typedef int16_t    i16;
typedef int32_t    i32;
typedef int64_t    i64;
typedef uint8_t    u8;
typedef uint16_t   u16;
typedef uint32_t   u32;
typedef uint64_t   u64;

typedef intptr_t   iptr;
typedef uintptr_t  uptr;


} // end of namespace jitcs

// ------------------------------
// debug macros
#define _JITCS_ALWAYS_CHECK_(X) do { if (!(X)) throw ("Assertion failed in " __FILE__ ": " #X); } while (0)

#if defined(_DEBUG) && !defined(JITCS_DEBUG)
#define JITCS_DEBUG
#endif

#if defined(_NDEBUG) && defined(JITCS_DEBUG)
#undef JITCS_DEBUG
#endif


#ifdef JITCS_DEBUG
#define _JITCS_DBG_CHECK_(X) _JITCS_ALWAYS_CHECK_(X)
#define _JITCS_DBG_EXPR_(X)  X
#define _JITCS_DBG_PARAM_(X) ,X
#else
#define _JITCS_DBG_CHECK_(X)
#define _JITCS_DBG_EXPR_(X)
#define _JITCS_DBG_PARAM_(X)
#endif


#ifdef _MSC_VER
#if defined(_M_IX86)
#define JITCS_X86
#define JITCS_X86_32
#define JITCS_32
#endif
#if defined(_M_X64)
#define JITCS_X86
#define JITCS_X86_64
#define JITCS_64
#endif
#if defined(_M_ARM)
#define JITCS_ARM
#endif
#if defined(_M_MIPS)
#define JITCS_MIPS
#endif
#endif
#ifdef __GNUC__
#if defined(__i386__)
#define JITCS_X86
#define JITCS_X86_32
#define JITCS_32
#endif
#if defined(__x86_64__)
#define JITCS_X86
#define JITCS_X86_64
#define JITCS_64
#endif
#if defined(__CC_ARM) || defined(__ARMCC__) \
    || defined(__arm__) || defined(__thumb__)
#define JITCS_ARM
#endif
#endif

#if defined(_MSC_VER) || defined(_WIN32) || defined(_WIN64)
#define JITCS_WINDOWS
#else
#define JITCS_POSIX
#endif

#endif
// _JITCS_BASE_H_
