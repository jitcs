//===-- jitcs_x86.h C++ -------------------------------------------------*-===//
//
// Header file for 32bit x86 platforms.
//
//===----------------------------------------------------------------------===//

#ifndef _JITCS_X86_32_H_
#define _JITCS_X86_32_H_

#include "jitcs_x86_32_insids.h"
#include "jitcs_x86_32_regs.h"
#include "jitcs_x86_32_cons.h"
#include "jitcs_x86_32_machine.h"

#endif
// _JITCS_X86_32_H_
