//===-- jitcs_ids.h ---------------------------------------------*- C++ -*-===//
// ID types for instructions et al.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Defining these types here avoids certain include-order problems.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_IDS_H_
#define _JITCS_IDS_H_

#include "jitcs_base.h"

namespace jitcs {

enum RegId : u32 {
  R_None = 0, 
  R_HardwareLimit = 0x1000, 
};
enum ResId : u32 { 
  RES_None = ~0u, 
};
enum RegClassId : u8 { 
  RCL_None = 0xff, 
};
enum ResClassId : u8 { 
  RESCL_None = 0xff, 
};

enum InsId : uptr { 
  I_None = 0, 
  I_Count,
  I_CoreMax = I_Count,
};

enum BBId : uptr { 
  BB_None = 0, 
};

// used in virtual register placement and memory reference
enum MModeEnum { 
  MMODE_Bits = 4,
  MMODE_Mask = (1 << MMODE_Bits) - 1,
  MMODE_FPCount =      8,
};
enum MMode { 
  MMODE_None =         0, // address undefined
  MMODE_Base =         1, // base + scale * index + offset
  //MMODE_Sym  =         2, // symbol + offset - the symbol may be global or local(SPREL/FPREL)
  MMODE_Global=        3, // global pointer + offset (in X64 encoded as RIP-relative)
  MMODE_VRegSpillSlot= 4, // memory address of a VReg spill slot
  MMODE_SPRel=         8, // stack (aka FP0) pointer + offset
  MMODE_FP1Rel= MMODE_SPRel+1, // frame pointer 1 + offset
  MMODE_FP2Rel= MMODE_SPRel+2, // frame pointer 2 + offset
  MMODE_FP3Rel= MMODE_SPRel+3, // frame pointer 3 + offset
  MMODE_FP4Rel= MMODE_SPRel+4, // frame pointer 4 + offset
  MMODE_FP5Rel= MMODE_SPRel+5, // frame pointer 5 + offset
  MMODE_FP6Rel= MMODE_SPRel+6, // frame pointer 6 + offset
  MMODE_FP7Rel= MMODE_SPRel+7, // frame pointer 7 + offset
};


} // end of namespace jitcs

#endif
// _JITCS_IDS_H_
