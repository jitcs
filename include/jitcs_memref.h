//===-- jitcs_memref.h ------------------------------------------*- C++ -*-===//
// Memory operands in instructions
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A MemoryReference contains all data necessary to describe the memory 
// operand of a single hardware instruction. If a MemoryReference contains
// uses of a non-fixed virtual register, and is used in more than one
// instruction, copies might be created at register allocation time.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_MEMREF_H_
#define _JITCS_MEMREF_H_

#include "jitcs_base.h"
#include "jitcs_ids.h"

namespace jitcs {
struct VirtualRegister;

template <int N> struct MemDataImpl {};
template <> struct MemDataImpl<4> {
  union {
    struct { VirtualRegister const *base, *index; } regs;
    struct { u8* ptr; } global;
  };  
  int offset;
  int modeAndScale;
};
template <> struct MemDataImpl<8> {
  union {
    struct { VirtualRegister const *base, *index; } regs;
    struct { u8* ptr; } global;
  };  
  int offset;
  int modeAndScale;
  iptr _padding;
};

typedef MemDataImpl<sizeof(void*)> MemData;
static_assert(sizeof(MemData) == 16 || sizeof(MemData) == 32, 
              "wrong size of MemData");

struct MemoryReference : MemData {
  void setVRegSpillSlot(Ref<const VirtualRegister> id) { 
    regs.base = id._ptr;
    regs.index = nullptr;
    offset = 0;
    modeAndScale = MMODE_VRegSpillSlot;
  }
  void setBaseAndOffset(Ref<const VirtualRegister> id, int o) { 
    regs.base = id._ptr;
    regs.index = nullptr;
    offset = o;
    modeAndScale = MMODE_Base;
  }
  void setBaseIndexAndOffset(Ref<const VirtualRegister> id, 
                             Ref<const VirtualRegister> id2, int s, int o) { 
    regs.base = id._ptr;
    regs.index = id2._ptr;
    offset = o;
    modeAndScale = MMODE_Base + (s << MMODE_Bits);
  }
  void setFPRelative(uint fp, int o) { 
    assert(fp >= 0 && fp < MMODE_FPCount);
    offset = o;
    modeAndScale = MMODE_SPRel + fp;
  }
  inline void setSPRelative(int o) { 
    setFPRelative(0, o);
  }
  void setGlobal(u8* p, int o) { 
    global.ptr = p;
    offset = o;
    modeAndScale = MMODE_Global;
  }

  MMode getMode() const { return static_cast<MMode>(modeAndScale &  MMODE_Mask); }
  int getScale() const { return modeAndScale >> MMODE_Bits; }
  int getOffset() const { return offset; }
  RefOrNull<const VirtualRegister> getBaseReg() const  { return regs.base; }
  RefOrNull<const VirtualRegister> getIndexReg() const { return regs.index; }
  RefOrNull<const VirtualRegister> getConstBaseReg() const  { return regs.base; }
  RefOrNull<const VirtualRegister> getConstIndexReg() const { return regs.index; }
  const u8* getGlobal() const { return global.ptr; }

  void setBaseReg(RefOrNull<const VirtualRegister> r) { regs.base = r._ptr; }
  void setIndexReg(RefOrNull<const VirtualRegister> r) { regs.index = r._ptr; }
};

} // end of namespace jitcs

#endif
// _JITCS_MEMREF_H_
