//===-- jitcs_spillslotinfo.h -----------------------------------*- C++ -*-===//
// Description of the spill slot position for virtual registers.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// SpillSlotInfo can be used for all virtual registers, whose spill slot
// is at a memory location relative to one of the frame pointer registers,
// e.g. the stack pointer. For emulators it will be possible, to define 
// virtual registers being spilled to and reloaded from the virtual machine 
// state.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_SPILLSLOTINFO_H_
#define _JITCS_SPILLSLOTINFO_H_

#include "jitcs_base.h"
#include "jitcs_ids.h"

namespace jitcs {
class MachineDumper;

enum NoSpillEnum { NO_SPILL };

struct SpillSlotInfo {
  int aggregateData;
  
  enum { 
    SSI_SizeBits = 3, // spill slots can be at most 2^(2^3-1) = 2^7 = 128 bytes each
    SSI_SizeMask = (1 << SSI_SizeBits) - 1,
  };
  
  SpillSlotInfo() = default;
  SpillSlotInfo(const SpillSlotInfo&) = default;
  SpillSlotInfo& operator =(const SpillSlotInfo&) = default;
  
  static_assert(MMODE_None == 0, "assumption failed");
  static_assert(MMODE_SPRel + MMODE_FPCount <= (1 << MMODE_Bits), "assumption failed");
  static_assert(MMODE_Bits + SSI_SizeBits < sizeof(aggregateData) * 8, "assumption failed");
  
  SpillSlotInfo(NoSpillEnum) : aggregateData(0) {}
  SpillSlotInfo(uint fpmode, int ofs, uint logSize) : aggregateData(0) { place(fpmode, ofs, logSize); }
  
  bool isPlaced() const { return aggregateData != 0; }
  bool isMem() const { return isPlaced(); }
  
  void unplace() { aggregateData = 0; }
  void place(uint fpmode, int ofs, uint logSize) {
    assert(fpmode >= MMODE_SPRel && fpmode < MMODE_SPRel + MMODE_FPCount);
    assert((ofs << MMODE_Bits) >> MMODE_Bits == ofs);
    assert(logSize < (1 << SSI_SizeBits));
    aggregateData = (ofs << (MMODE_Bits + SSI_SizeBits)) | (logSize << MMODE_Bits) | (fpmode);
  }
  bool isSPRel() const { return (aggregateData & MMODE_Mask) == MMODE_SPRel; }
  uint getFP() const { assert(isPlaced()); return (aggregateData & MMODE_Mask) - MMODE_SPRel; }
  int getOffset() const { assert(isPlaced()); return aggregateData >> (MMODE_Bits + SSI_SizeBits); }
  uint getLogSize() const { assert(isPlaced()); return (aggregateData >> MMODE_Bits) & SSI_SizeMask; }
  uint getSize() const { return 1 << getLogSize(); }

  void dump(MachineDumper& o) const;
};

} // End jitcs namespace

#endif
// _JITCS_SPILLSLOTINFO_H_
