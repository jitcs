//===-- jitcs_cpu.h ---------------------------------------------*- C++ -*-===//
// CPU feature description and detection.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Sets up an architecture and relevant feature flags. Has the architecture
// A_Host, which will run a feature detection of the host running the 
// applicaton.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_CPU_H_
#define _JITCS_CPU_H_

#include "jitcs_base.h"
#include "jitcs_adt_bitstore.h"

namespace jitcs {
struct CPUInfo {
public:
  enum ArchitectureId {
    A_Host,
    A_X86_32 = 0,
    A_X86_64,
    A_Count,
  };
  enum FeatId {
    // only for 32bit x86
    F_X86CMOV = 0, F_X86SSE, F_X86SSE2,
    // only for 64bit x86
    F_X86LSAHF64,
    // all x86
    F_X86RDTSC,
    F_X86RDTSCP, F_X86CLFLUSH, 
    F_X86SSE3, F_X86SSSE3, F_X86SSE41, F_X86SSE42, 
    F_X86AVX, F_X86AVX2,
    F_X86AES, F_X86PCLMULQDQ, 
    F_X86POPCNT, F_X86LZCNT,
    F_X86BMI1, F_X86BMI2,
    F_X86TSX, F_X86FMA3, F_X86F16C, // intel features
    //F_MONITOR, <- only ring 0
    F_X86CMPXCHG8B, F_X86CMPXCHG16B, 
    //F_MISALIGNEDSSE <- all avx allow unaligned access (except movapX),
    F_X86RDRAND, 

    F_COUNT
  };

public:
  CPUInfo(ArchitectureId);
  CPUInfo() = delete;
  CPUInfo(const CPUInfo&) = default;
  CPUInfo& operator =(const CPUInfo&) = default;
  
public:
  inline uint   getPrefAlign()     const { return _sizeOfPreferredAlignment; }
  inline uint   getCachelineSize() const { return _sizeOfCacheline; }
  inline uint   getCoreCount()     const { return _countOfLogCores; }
  inline uint   getPhysCoreCount() const { return _countOfPhysCores; }
  inline uint   getLogCoreCount()  const { return _countOfLogCores; }

  inline bool       isX86()        const { return (_arch == A_X86_32) || (_arch == A_X86_64); }
  inline bool       isX86_32()     const { return (_arch == A_X86_32); }
  inline bool       isX86_64()     const { return (_arch == A_X86_64); }

  inline bool       hasX86CMOV()   const { return _testFeat(F_X86CMOV); }
  inline bool       hasX86SSE()    const { return _testFeat(F_X86SSE); }
  inline bool       hasX86SSE2()   const { return _testFeat(F_X86SSE2); }
  inline bool       hasX86LSAHF()  const { return _testFeat(F_X86LSAHF64); }
  inline bool       hasX86SSE3()   const { return _testFeat(F_X86SSE3); }
  inline bool       hasX86SSSE3()  const { return _testFeat(F_X86SSSE3); }
  inline bool       hasX86SSE41()  const { return _testFeat(F_X86SSE41); }
  inline bool       hasX86SSE42()  const { return _testFeat(F_X86SSE42); }
  inline bool       hasX86AVX()    const { return _testFeat(F_X86AVX); }
  inline bool       hasX86AVX2()   const { return _testFeat(F_X86AVX2); }
  inline bool       hasX86FMA3()   const { return _testFeat(F_X86FMA3); }
  inline bool       hasX86F16C()   const { return _testFeat(F_X86F16C); }
  inline bool       hasX86AES()    const { return _testFeat(F_X86AES); }
  inline bool       hasX86PCLMULQDQ()const { return _testFeat(F_X86PCLMULQDQ); }
  inline bool       hasX86POPCNT() const { return _testFeat(F_X86POPCNT); }
  inline bool       hasX86LZCNT()  const { return _testFeat(F_X86LZCNT); }
  inline bool       hasX86BMI1()   const { return _testFeat(F_X86BMI1); }
  inline bool       hasX86BMI2()   const { return _testFeat(F_X86BMI2); }
  inline bool       hasX86TSX()    const { return _testFeat(F_X86TSX); }
  inline bool       hasX86RDTSC()  const { return _testFeat(F_X86RDTSC); }
  inline bool       hasX86RDTSCP() const { return _testFeat(F_X86RDTSCP); }
  inline bool       hasX86CLFLUSH()const { return _testFeat(F_X86CLFLUSH); }
  inline bool       hasX86CMPXCHG8B()const { return _testFeat(F_X86CMPXCHG8B); }
  inline bool       hasX86CMPXCHG16B()const { return _testFeat(F_X86CMPXCHG16B); }
  inline bool       hasX86RDRAND() const { return _testFeat(F_X86RDRAND); }

public:
  static const CPUInfo& GetHost() { return _hostCPUInfo; }

  void enableFeat(FeatId f);
  void disableFeat(FeatId f);
  void setPreferredAlignment(uint);
  void setCachelineSize(uint);
  void setCoreCount(uint phys, uint log);

private:
  inline bool _testFeat(FeatId f) const { return _setOfFeats.testBit(f); }
  void _initializeFromHost();
  void _setFeat(FeatId f, bool b);

private:
  ArchitectureId _arch;
  BitStore<F_COUNT> _setOfFeats;
  unsigned      _sizeOfPreferredAlignment;
  unsigned      _sizeOfCacheline;
  unsigned      _countOfPhysCores, _countOfLogCores;
  static CPUInfo _hostCPUInfo;
};

} // end namespace jitcs

#endif 
// _JITCS_CPU_H_
