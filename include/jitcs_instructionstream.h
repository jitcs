//===-- jitcs_instructionstream.h -------------------------------*- C++ -*-===//
// Helper to construct instructions en bloc.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Used by e.g. jitcs_{arch}_cons.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INSTRUCTIONSTREAM_H_
#define _JITCS_INSTRUCTIONSTREAM_H_

#include "jitcs_adt_ref.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"
#include "jitcs_instruction.h"
#include "jitcs_bblock.h"

namespace jitcs {
class BasicBlock;
struct MemoryReference;
struct VirtualRegister;

struct InstructionStream {
  Ref<Instruction> alloc(u32 d) { 
    if (d > space) _flush();
    iptr* t = cur;
    cur += d; space -= d; 
    return reinterpret_cast<Instruction*>(t);
  }

protected:
  void init(iptr* dp, u32 spc) { cur = dp; space = spc; }
  virtual void _flush() = 0;

protected:
  iptr *cur;
  u32 space;
};

template <uint INSSZ>
struct InstructionStreamBuffer : InstructionStream {
  enum { INSSZ2 = (INSSZ + 31) & (~31) };
  RefOrNull<BasicBlock> bb;
  iptr originalBuffer[INSSZ2];
  
  InstructionStreamBuffer() = default;
  void start(RefOrNull<BasicBlock> block) { bb = block; init(&originalBuffer[0], INSSZ2); }
  void start(Ref<BasicBlock> block) { bb._ptr = block._ptr; init(&originalBuffer[0], INSSZ2); }
  void end() { _flush(); bb = nullptr; }
    
protected:
  virtual void _flush() {
    if (!bb.isNull())
      bb.removeNullType()->append_copy(&originalBuffer[0], INSSZ2 - space);
    init(&originalBuffer[0], INSSZ2);
  }
};

} // end of namespace jitcs

#endif
// _JITCS_INSTRUCTIONSTREAM_H_
