//===-- jitcs_function.h ----------------------------------------*- C++ -*-===//
// The public representation of a Function as used in JITCS.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Enumeration of basic blocks and virtual registers is possible, but slow.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_FUNCTION_H_
#define _JITCS_FUNCTION_H_

#include "jitcs_adt_enumerator.h"
#include "jitcs_adt_ref.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"
#include "jitcs_memmgr.h"

namespace jitcs {
class BasicBlock;
class CallingConvention;
class MachineDumper;
struct VirtualRegister;
class FunctionImpl;

class Function {
public:
  enum Strategy {
    Direct_Code_Gen,
    One_Pass_Linear_Scan,
    Two_Pass_Linear_Scan,
  };
protected:
  Function() {}
public:
  virtual ~Function() {}
  Function(const Function &) = delete;
  void operator=(const Function &) = delete;
  
  virtual FunctionImpl& impl() = 0;
  virtual const FunctionImpl& impl() const = 0;
  
public:
  Enumerator<const BasicBlock>      enumerateBasicBlocks();
  //Enumerator<const VirtualRegister> enumerateVirtualRegisters();
  virtual std::shared_ptr<const CallingConvention> getCallingConvention() const = 0;
  virtual Ref<const VirtualRegister>        getArgumentRegister(uint n, RegClassId) = 0;
  virtual Ref<const VirtualRegister>        getResultRegister(uint n, RegClassId) = 0;
  virtual Ref<const VirtualRegister>        createRegister(RegClassId, int logSz = 0) = 0;

  //Ref<VirtualRegister> allocateTemporary(Ref<VirtualRegister> compatibleTo);
  //void deallocateTemporary(Ref<VirtualRegister> id);

public:
  //void check();
  //void clean();
  virtual void dump(MachineDumper&) const = 0;

  Ref<BasicBlock>  getStartBlock();
  Ref<BasicBlock>  createBasicBlock();

public:
  virtual MemoryMgr::CodeAndData generate(RefCounter<MemoryMgr>, Strategy) = 0;
};

} // End jitcs namespace

#endif
// _JITCS_FUNCTION_H_
