//===-- jitcs_adt_ref.h -----------------------------------------*- C++ -*-===//
// An attributed pointer class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A Ref or RefOrNull pointer object NEVER owns the object pointed to.
// A Ref pointer can never contain a null pointer. A RefOrNull object cannot
// directly use the -> operator. You can check for isNull(), and 
// removeNullType() if it is not.
// (The principle of this is taken from the language Ceylon which, according 
// to my understanding, handles null pointers quite nicely.)
//===----------------------------------------------------------------------===//

#ifndef _JITCS_ADT_REF_H_
#define _JITCS_ADT_REF_H_

#include <assert.h>
#include <type_traits>

namespace jitcs {
enum {
  REF_Any = 0,
  REF_Nullable = 1,
  REF_Const = 2,
};

template <bool b, typename = typename std::enable_if<b,int>::type>
void guarantee(const char *) {}

// _Ref is an attributed pointer type. 
// Currently it supports the const-attribute of its data, and
// the nonnull-attribute of the pointer.

template <typename T, unsigned FLAGS>
struct _Ref {
  typedef typename std::add_const<T>::type CType;
  typedef T NCType;
  static const bool IsConstRef = (FLAGS & REF_Const) != 0;
  typedef typename std::conditional<IsConstRef, CType, NCType>::type Type;
  typedef Type* PtrType;
  typedef Type& RefType;

  _Ref() {
    guarantee<(FLAGS & REF_Nullable) != 0>
      ("Default constructor only for nullable refs");
    _ptr = nullptr;
    _isValid = false;
  }
  _Ref(const _Ref&) = default;
  _Ref& operator = (const _Ref&) = default;
  PtrType ptr() {
    assert(_isValid);
    return _ptr;
  }
  PtrType operator ->() { 
    guarantee<(FLAGS & REF_Nullable) == 0>
      ("dereferencing only for non-nullable refs, remove nullable first");
    return ptr();
  }
  RefType operator *() { 
    guarantee<(FLAGS & REF_Nullable) == 0>
      ("dereferencing only for non-nullable refs, remove nullable first");
    return *ptr();
  }

  template <typename T2, unsigned FLAGS2>
  _Ref(_Ref<T2, FLAGS2> o) {
    static_assert((~FLAGS & FLAGS2) == 0, "incompatible _Ref flags");
    //static_assert(std::is_base_of<T, T2>::value, "incompatible _Ref types");
    _ptr = o._ptr;
    _isValid = true;
  }
  template <typename T2>
  _Ref(T2* o) {
    typedef typename std::remove_const<T2>::type T2B;
    //static_assert(std::is_base_of<T, T2B>::value, "incompatible _Ref types");
    if ((FLAGS & REF_Nullable) == 0)
      assert(o != nullptr);
    _ptr = o;
    _isValid = true;
  }
  _Ref(std::nullptr_t) {
    guarantee<(FLAGS & REF_Nullable) != 0>("nullptr not allowed here");
    _ptr = nullptr;
    _isValid = true;
  }

  template <typename T2, unsigned FLAGS2>
  _Ref& operator =(_Ref<T2, FLAGS2> o) {
    static_assert((~FLAGS & FLAGS2) == 0, "incompatible _Ref flags");
    //static_assert(std::is_base_of<T, T2>::value, "incompatible _Ref types");
    _ptr = o._ptr;
    _isValid = o._isValid;
  }
  template <typename T2>
  _Ref& operator =(T2* o) {
    typedef typename std::remove_const<T2>::type T2B;
    //static_assert(std::is_base_of<T, T2B>::value, "incompatible _Ref types");
    if ((FLAGS & REF_Nullable) == 0)
      assert(o != nullptr);
    _ptr = o;
    _isValid = true;
  }
  _Ref& operator =(std::nullptr_t) {
    guarantee<(FLAGS & REF_Nullable) != 0>("nullptr not allowed here");
    _ptr = nullptr;
    _isValid = true;
  }


  bool isNull() const { 
    if ((FLAGS & REF_Nullable) == 0)
      return false;
    return _ptr == nullptr;
  }
  _Ref<T,FLAGS & ~REF_Nullable> removeNullType() const {
    if ((FLAGS & REF_Nullable) == 0)
      return _ptr;
    assert(_ptr != nullptr);
    return _ptr;
  }

  PtrType _ptr;
  bool _isValid;
};

// Ref represents a nonnull pointer to some object. 

template <typename T>
using Ref = _Ref<typename std::remove_const<T>::type,
                 (std::is_const<T>::value ? REF_Const : REF_Any)>;

// RefOrNull represents a normal pointer to some object. Accessing the
// data is achieved thru removeNullType, after which the "->" operator
// is accessible. Calling removeNullType without checking isNull first
// is considered a bug.
// The concept is taken from the Ceylon language which handles null
// pointers very nicely thru a clever type system.

template <typename T>
using RefOrNull = _Ref<typename std::remove_const<T>::type,
                       (std::is_const<T>::value ? REF_Const : REF_Any)
                        + REF_Nullable>;
} // end of namespace jitcs

#endif 
// _JITCS_ADT_REF_H_
