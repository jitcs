//===-- jitcs.h -------------------------------------------------*- C++ -*-===//
/*
Copyright (C) 2013-2014 Dirk Steinke. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[ MIT license: http://www.opensource.org/licenses/mit-license.php ]
*/
//===----------------------------------------------------------------------===//
// Glue for JITCS.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Clients may simply include this file if all they want is building functions
// for the host architecture. Register names and instructions will be available
// in the jitcs::host namespace.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_H_
#define _JITCS_H_

#include "jitcs_base.h"
#include "jitcs_cpu.h"
#include "jitcs_memmgr.h"
#include "jitcs_function.h"
#include "jitcs_machine.h"
#include "jitcs_tmpalloc.h"

#ifdef JITCS_X86_32
#include "jitcs_x86_32_machine.h"
#endif
#ifdef JITCS_X86_64
#include "jitcs_x86_64_machine.h"
#endif

namespace jitcs {
namespace host {
RefCounter<IMachineInfo> GetMachineInfo();
#ifdef JITCS_X86_32
using namespace jitcs::x86_32;
#endif
#ifdef JITCS_X86_64
using namespace jitcs::x86_64;
#endif
} // end of namespace jitcs::host
} // end of namespace jitcs

#endif
// _JITCS_H_
