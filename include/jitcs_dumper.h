//===-- jitcs_dumper.h ------------------------------------------*- C++ -*-===//
// Helper for printing information for debug purposes.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A lot of classes of JITCS accept an IDumper interface for writing
// information, which e.g. PrintFDumper implements. MachineDumper dumper
// provides target specific dumping support. Using it will pull in some big 
// data tables into the application, so it should be avoided except for
// debug purposes.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_DUMPER_H_
#define _JITCS_DUMPER_H_

#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"
#include <memory>
#include <stdarg.h>

namespace jitcs {
struct Instruction;
struct MemoryReference;
struct VirtualRegister;
class IMachineInfo;

class IDumper {
protected:
  IDumper() = default;
private:
  IDumper(const IDumper&) = delete;
  IDumper& operator =(const IDumper&) = delete;
public:
  void write(const char*);
  void writef(const char*, ...);
  virtual void writev(const char*, va_list) = 0;
};
class PrintFDumper : public IDumper {
public:
  PrintFDumper() = default;
private:
  PrintFDumper(const PrintFDumper&) = delete;
  PrintFDumper& operator =(const PrintFDumper&) = delete;
public:
  /*[[override]]*/ virtual void writev(const char*, va_list);
};
class StringDumper : public IDumper {
public:
  StringDumper() = default;
private:
  StringDumper(const StringDumper&) = delete;
  StringDumper& operator =(const StringDumper&) = delete;
public:
  /*[[override]]*/ virtual void writev(const char*, va_list);
public:
  std::string takeResult();
private:
  std::string _accumulator;
};

class IMachineDumper {
public:
  virtual void write(IDumper&, Ref<const Instruction>) const;
  virtual void write(IDumper&, Ref<const VirtualRegister>) const;

  virtual void write(IDumper&, InsId) const;

  void write(IDumper&, Ref<const MemoryReference>, bool vsib) const;
};

class MachineDumper : public IDumper {
public:
  MachineDumper(IDumper&, const IMachineInfo&);
private:
  MachineDumper() = delete;
  MachineDumper(const MachineDumper&) = delete;
  MachineDumper& operator =(const MachineDumper&) = delete;
public:
  using IDumper::write;
  void write(Ref<const Instruction>);
  void write(Ref<const VirtualRegister>);

  void write(InsId);
  
  /*[[override]]*/ virtual void writev(const char*, va_list);
private:
  IDumper& _out;
  std::unique_ptr<IMachineDumper> _md;
};

} // end namespace jitcs

#endif 
// _JITCS_DUMPER_H_
