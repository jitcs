//===-- jitcs_callingconvention.h -------------------------------*- C++ -*-===//
// Public interface to a function ABI object.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The calling convention object defines all relevant information necessary
// for the code generators to generate functions of the given type, and calls
// to functions of the given type.
// Calling convention objects are currently created only thru the MachineInfo
// objects.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_CALLINGCONVENTION_H_
#define _JITCS_CALLINGCONVENTION_H_

#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include "jitcs_fnctypeinfo.h"
#include "jitcs_spillslotinfo.h"
#include "jitcs_ids.h"
#include <memory>

namespace jitcs {
class MachineDumper;
class IMachineInfo;
struct VirtualRegister;

enum CallerCalleeStrategyId {
  CCS_Scratch,
  CCS_ByCaller,
  CCS_ByCallee,
};

class CallingConvention {
public:
  CallingConvention() = default;
  virtual ~CallingConvention() = default;

public:
  void dump(MachineDumper&) const;

public:
  virtual RefCounter<IMachineInfo> getMachineInfo() const = 0;
  virtual size_t getResultCount() const = 0;
  virtual size_t getParamCount() const = 0;
  virtual size_t getParamAreaSize() const = 0;
  virtual SpillSlotInfo getResultSpillSlotInfo(size_t r) const = 0;
  virtual RefOrNull<const VirtualRegister> getResultRegister(size_t r) const = 0;
  virtual SpillSlotInfo getParamSpillSlotInfo(size_t p) const = 0;
  virtual RefOrNull<const VirtualRegister> getParamRegister(size_t p) const = 0;
  virtual size_t getRequiredStackAlignment() const = 0;
  virtual size_t getRequiredStackAlignmentOffset() const = 0;
  virtual CallerCalleeStrategyId getStackCleaningStrategy() const = 0;

  virtual FTSubType getSubType() const = 0;
  virtual FTTypeId getResultType(size_t r) const = 0;
  virtual FTTypeId getParamType(size_t p) const = 0;

  virtual u32 getScratchResources(ResClassId) const = 0;
  virtual u32 getReservedResources(ResClassId) const = 0;

public:
  virtual void setTypeInfo(FTSubType,Slice<FTTypeId> res,Slice<FTTypeId> par) = 0;
  virtual void addResult(const VirtualRegister*, SpillSlotInfo) = 0;
  virtual void addParam(const VirtualRegister*, SpillSlotInfo) = 0;
  virtual void setStackAlignment(size_t align, size_t ofs) = 0;
  virtual void setParamAreaSize(size_t) = 0;
  virtual void setCleanStack(CallerCalleeStrategyId) = 0;
  virtual void setResClassCount(size_t resNo) = 0;
  virtual void setScratchResources(ResClassId, u32) = 0;
  virtual void setReservedResources(ResClassId, u32) = 0;
};

} // end of namespace jitcs

#endif
// _JITCS_CALLINGCONVENTION_H_
