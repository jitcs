//===-- jitcs_fnctypeinfo.h - -----------------------------------*- C++ -*-===//
// Function type deconstructor.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The helper functions in this header are used by the code constructing
// default calling convention objects.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_FNCTYPEINFO_H_
#define _JITCS_FNCTYPEINFO_H_

#include "jitcs_base.h"

namespace jitcs {

enum FTSubType {
  FTS_CDecl,
  FTS_FastCall,
  FTS_StdCall,
};
enum FTTypeId {
  FTT_Void,
  FTT_Int,
  FTT_PtrInt,
  FTT_Ptr,
};
template <typename T> struct GetFTSubType {};
template <typename R, typename ...Args>
struct GetFTSubType<R (__cdecl *)(Args...)> { 
  static const FTSubType Value = FTS_CDecl; 
  typedef R (*FType)(Args...);
};
template <typename R, typename ...Args>
struct GetFTSubType<R (__fastcall *)(Args...)> { 
  static const FTSubType Value = FTS_FastCall; 
  typedef R (*FType)(Args...);
};
template <typename R, typename ...Args>
struct GetFTSubType<R (__stdcall *)(Args...)> { 
  static const FTSubType Value = FTS_StdCall; 
  typedef R (*FType)(Args...);
};

template <typename T> 
struct GetFTTypeId { 
  static const bool OK = false; 
  static const FTTypeId Value = FTT_Void; 
};
template <>
struct GetFTTypeId<void> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Void; 
};
template <>
struct GetFTTypeId<i32> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Int; 
};
template <>
struct GetFTTypeId<u32> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Int; 
};
template <>
struct GetFTTypeId<i64> { 
  static const bool OK = sizeof(i64) <= sizeof(void*); 
  static const FTTypeId Value = sizeof(i64) == sizeof(void*) ? FTT_PtrInt : FTT_Int; 
};
template <>
struct GetFTTypeId<u64> { 
  static const bool OK = sizeof(i64) <= sizeof(void*); 
  static const FTTypeId Value = sizeof(u64) == sizeof(void*) ? FTT_PtrInt : FTT_Int; 
};
template <typename T> 
struct GetFTTypeId<T*> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Ptr; 
};
template <typename T> 
struct GetFTTypeId<T const*> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Ptr; 
};
template <typename T> 
struct GetFTTypeId<T&> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Ptr; 
};
template <typename T> 
struct GetFTTypeId<T const&> { 
  static const bool OK = true; 
  static const FTTypeId Value = FTT_Ptr; 
};


template <typename T> struct _GetFTResultId {};
template <typename R, typename ...Args>
struct _GetFTResultId<R (*)(Args...)> { 
  static const bool OK = GetFTTypeId<R>::OK;
  static const FTTypeId Value = GetFTTypeId<R>::Value;
};
template <typename T> struct GetFTResultId : _GetFTResultId<typename GetFTSubType<T>::FType> {};
template <typename T> struct _GetFTParamCount {};
template <typename R>
struct _GetFTParamCount<R (*)()> { 
  static const uint Value = 0;
};
template <typename R, typename Arg, typename ...Args>
struct _GetFTParamCount<R (*)(Arg, Args...)> { 
  static const uint Value = _GetFTParamCount<R (*)(Args...)>::Value + 1;
};
template <typename T> struct GetFTParamCount : _GetFTParamCount<typename GetFTSubType<T>::FType> {};
template <typename T, uint N> struct _GetFTParamId {};
template <typename R, typename Arg, typename ...Args>
struct _GetFTParamId<R (*)(Arg, Args...), 0> { 
  static const bool OK = GetFTTypeId<Arg>::OK;
  static const FTTypeId Value = GetFTTypeId<Arg>::Value;
};
template <typename R, uint N>
struct _GetFTParamId<R (*)(), N> { 
  static const bool OK = false;
  static const FTTypeId Value = FTT_Void;
};
template <uint N, typename R, typename Arg, typename ...Args>
struct _GetFTParamId<R (*)(Arg, Args...), N> { 
  static const bool OK = _GetFTParamId<R (*)(Args...), N - 1>::OK;
  static const FTTypeId Value = _GetFTParamId<R (*)(Args...), N - 1>::Value;
};
template <typename T, uint N> struct GetFTParamId : _GetFTParamId<typename GetFTSubType<T>::FType, N> {};
} // end of namespace jitcs

#endif
// _JITCS_FNCTYPEINFO_H_
