//===-- jitcs_tmpalloc.h ----------------------------------------*- C++ -*-===//
// Never-free allocator classes.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// TempAllocator allocates data en bloc, and releases it all at once.
// No destructors are called on release.
// StreamAllocator speeds up allocating multiple objects with enhanced 
// alignment requirements.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_TMPALLOC_H_
#define _JITCS_TMPALLOC_H_

#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include <list>
#include <vector>

namespace jitcs {

class TempAllocator {
public:
  enum { MINIMUM_BLOCK_SIZE= 4096 };

public:
  TempAllocator();
  ~TempAllocator();

public:
  template <typename T>
  T* allocTyped() { 
    return reinterpret_cast<T*>(_alloc(sizeof(T), alignof(T)));
  }
  template <typename T>
  Slice<T> allocTypedArray(size_t sz) { 
    return Slice<T>(reinterpret_cast<T*>(_alloc(sizeof(T) * sz, alignof(T))),
                    sz);
  }

private:
  // called from RefCounter
  friend class RefCounter<TempAllocator>;
  void _incRef() { ++_refCnt; }
  void _decRef() { if (!--_refCnt) _clean(true); }

private:
  u8* _alloc(size_t sz, size_t alignment);
  // drop all current allocations to this allocator, and start from scratch
  void _clean(bool keepbest);

private:
  typedef std::list< std::vector<u8> > BlockList;
  BlockList _blocks;
  BlockList::iterator _curblock;
  u8* _cur;
  size_t _rem;
  size_t _allocatedSpace;
  size_t _refCnt;
};

// allocator helper object to allocate quickly from a TempAllocator
template <typename T>
class StreamAllocator {
public:
  StreamAllocator(TempAllocator& a, size_t n)
    : _allocator(a)
    , _ptr(nullptr)
    , _end(nullptr)
    , _allocN(n) {
    _JITCS_ALWAYS_CHECK_(_allocN > 0);
  }

public:
  inline Ref<T> alloc() {
    if (_ptr != _end)
      return _ptr++; 
    return _alloc();
  }

private:
  Ref<T> _alloc() {
    _ptr = _allocator.allocTypedArray<T>(_allocN).ptr();
    _end = _ptr + _allocN;
    return _ptr++;
  }

private:
  TempAllocator &_allocator;
  T *_ptr, *_end;
  size_t _allocN;
};

} // end of namespace jitcs

#endif
// _JITCS_TMPALLOC_H_
