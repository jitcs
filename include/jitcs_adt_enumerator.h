//===-- jitcs_adt_enumerator.h ----------------------------------*- C++ -*-===//
// An abstract enumeration helper for object lists.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The point of an enumerator is to provide the means to enumerate lists of 
// objects, while hiding the implementation detail on the exact structure of
// the list. The enumeration is done like in the D Language input range.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_ADT_ENUMERATOR_H_
#define _JITCS_ADT_ENUMERATOR_H_

#include "jitcs_base.h"
#include <memory>

namespace jitcs {
template <typename T>
class IEnumerator {
public:
  typedef T ItemType;
  virtual bool empty() const = 0;
  virtual T& front() = 0;
  virtual void popFront() = 0;
};
template <typename T>
class Enumerator {
public:
  Enumerator() = delete;
  Enumerator(const Enumerator&) = delete;
  Enumerator& operator =(const Enumerator&) = delete;
  Enumerator(Enumerator&&) = default;
  Enumerator& operator =(Enumerator&&) = default;
  Enumerator(std::unique_ptr<IEnumerator<T>> && e) : _e(std::move(e)) {}

  bool empty() const { return _e->empty(); }
  T& front() { return _e->front(); }
  void popFront() { return _e->popFront(); }
private:
  std::unique_ptr<IEnumerator<T>> _e;
};
} // end namespace jitcs

#endif 
// _JITCS_ADT_ENUMERATOR_H_
