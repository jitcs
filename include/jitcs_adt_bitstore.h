//===-- jitcs_adt_bitstore.h ------------------------------------*- C++ -*-===//
// A fixed-size bit array class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A BitStore contains a fixed number of bits, and uses either an array
// of bytes or a single integer variable for storage.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_ADT_BITSTORE_H_
#define _JITCS_ADT_BITSTORE_H_

#include "jitcs_base.h"

namespace jitcs {
// structure to contain a small, fixed amount of bits
// will either use a single integer value or an array of bytes
template <unsigned N, bool SMALL> struct _BitStore {
  static_assert(N > 0, "BitStore doesn't allow zero bits");
  u8 data[(N + 7) >> 3];
    
  void setBit(u32 f, bool val) { 
    u32 ix = f >> 3; 
    u8 msk = 1 << (f & 7); 
    data[ix] = val ? (data[ix] | msk) : (data[ix] & ~msk); 
  }
  bool testBit(u32 f) const { 
    u32 ix = f >> 3; 
    u8 msk = 1 << (f & 7); 
    return (data[ix] & msk) != 0;
  }
  void clear() { 
    memset(&data, 0, sizeof(data));
  }
};
template <unsigned N> struct _BitStore<N, true> {
  static_assert(N > 0 && N <= 64, 
                "_BitStore<N,true> doesn't support more than 64 bits");
  enum { 
    N1 = N - 1, 
    N2 = N1 | (N1 >> 1),
    N3 = N2 | (N2 >> 2),
    N4 = N3 | (N3 >> 4),
    N5 = N4 <= 7 ? 7 : N4,
  };
  typedef typename UInt<N5 + 1>::Type DataType;
  DataType data;
    
  void setBit(u32 f, bool val) {
    DataType msk = DataType(1) << f; 
    data = val ? (data | msk) : (data & ~msk);
  }
  bool testBit(u32 f) const {
    DataType msk = DataType(1) << f; 
    return (data & msk) != 0;
  }
  void clear() {
    data = 0;
  }
};
template <unsigned N> struct BitStore : _BitStore<N, (sizeof(unsigned) <= sizeof(iptr))> {};
} // end of namespace jitcs

#endif 
// _JITCS_ADT_BITSTORE_H_
