//===-- jitcs_adt_range.h ---------------------------------------*- C++ -*-===//
// A not so abstract enumeration helper for object lists.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The interface of Range is based on D Language input range. In principle, it
// is just a pair of iterators.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_ADT_RANGE_H_
#define _JITCS_ADT_RANGE_H_

#include "jitcs_base.h"

namespace jitcs {
// simple range for array-like slices
// instead of keeping a size around, it is only using 
// a start and end pointer, decreasing the number of
// changed memory positions per traversal step
// size calculation is more costly though

  template <typename T> struct OpDerefType {
    typedef typename T::value_type value_type;
  };
  template <typename T> struct OpDerefType<T*> {
    typedef T value_type;
  };


  template <typename T>
  struct Range {
  public:
    typedef typename OpDerefType<T>::value_type value_type;
  public:
    T itStart;
    T itEnd;
    
  public:
    Range() = delete;
    Range(const Range&) = default;
    Range& operator =(const Range&) = default;

    Range(T start, T end) : itStart(start), itEnd(end) {}

  public:
    // range interface
    bool isEmpty() const { return itStart == itEnd; }

    value_type& getFront()             { _JITCS_DBG_CHECK_(!isEmpty()); return *itStart; }
    value_type const& getFront() const { _JITCS_DBG_CHECK_(!isEmpty()); return *itStart; }
    value_type& getBack()              { _JITCS_DBG_CHECK_(!isEmpty()); T it = itEnd; --it; return *it; }
    value_type const& getBack() const  { _JITCS_DBG_CHECK_(!isEmpty()); T it = itEnd; --it; return *it; }

    void popFront() { _JITCS_DBG_CHECK_(!isEmpty()); ++itStart; }
    void popBack()  { _JITCS_DBG_CHECK_(!isEmpty()); --itEnd; }

    bool operator !() const              { return isEmpty(); }
    value_type& operator *()             { return getFront(); }
    value_type const& operator *() const { return getFront(); }
    value_type* operator ->()             { return &getFront(); }
    value_type const* operator ->() const { return &getFront(); }
    void operator ++()                   { popFront(); }
  };

  template <typename T>
  struct ConstRange {
  public:
    typedef typename OpDerefType<T>::value_type value_type;
  public:
    T itStart;
    T itEnd;
    
  public:
    ConstRange() = delete;
    ConstRange(const ConstRange&) = default;
    ConstRange& operator =(const ConstRange&) = default;

    ConstRange(T start, T end)
      : itStart(start)
      , itEnd(end) {
    }

  public:
    // range interface
    bool isEmpty() const { return itStart == itEnd; }

    value_type const& getFront() const { _JITCS_DBG_CHECK_(!isEmpty()); return *itStart; }
    value_type const& getBack() const  { _JITCS_DBG_CHECK_(!isEmpty()); T it = itEnd; --it; return *it; }

    void popFront() { _JITCS_DBG_CHECK_(!isEmpty()); ++itStart; }
    void popBack()  { _JITCS_DBG_CHECK_(!isEmpty()); --itEnd; }

    bool operator !() const              { return isEmpty(); }
    value_type const& operator *() const { return getFront(); }
    value_type const* operator ->() const { return &getFront(); }
    void operator ++()                   { popFront(); }
  };

  template <typename U>
  inline Range<typename U::const_iterator> getRange(U const& u) {
    return Range<typename U::const_iterator>(u.begin(), u.end());
  }
  template <typename U>
  inline ConstRange<typename U::const_iterator> getConstRange(U const& u) {
    return ConstRange<typename U::const_iterator>(u.begin(), u.end());
  }
  template <typename U>
  inline Range<typename U::iterator> getRange(U& u) {
    return Range<typename U::iterator>(u.begin(), u.end());
  }
} // end of namespace jitcs

#endif
// _JITCS_ADT_RANGE_H_
