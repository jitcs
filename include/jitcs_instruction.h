//===-- jitcs_instruction.h -------------------------------------*- C++ -*-===//
// Interface to a single instruction object.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Actually, multiple instructions are stored as an array of iptr, and 
// Instruction is only used as a typecast from a pointer to those iptr.
// Instructions are never hand-constructed by the application, but the
// application uses helper functions in jitcs_{arch}_cons.h.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INSTRUCTION_H_
#define _JITCS_INSTRUCTION_H_

#include "jitcs_adt_ref.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"

namespace jitcs {
class BasicBlock;
class BasicBlockImpl;
struct MemoryReference;
struct VirtualRegister;

struct Instruction {
  iptr _ptr[32];
  
  template <typename ...T>
  void setAll(InsId id, T... args) { setInsId(id); _setAll(1, args...); }
  
  void setInsId(InsId id)                          { _ptr[0] = id; }
  void setRRefOp(size_t i, Ref<const VirtualRegister> r) { _setOpRef<const VirtualRegister>(i, r); }
  void setMRefOp(size_t i, Ref<MemoryReference> m) { _setOpRef<MemoryReference>(i, m); }
  void setBBOp  (size_t i, Ref<BasicBlock> bb)     { _setOpRef<BasicBlock>(i, bb); }
  void setImmOp (size_t i, iptr imm)               { _ptr[i] = imm; }

  InsId                 getInsId()          const { return static_cast<InsId>(_ptr[0]); }

  Ref<const VirtualRegister>  getRRefOp(size_t i) const { 
    return _getOpRef<const VirtualRegister>(i);
  }
  Ref<const VirtualRegister>  getConstRRefOp(size_t i) const { 
    return _getConstOpRef<VirtualRegister>(i);
  }

  Ref<MemoryReference>  getMRefOp(size_t i) const { return _getOpRef<MemoryReference>(i); }
  Ref<BasicBlock>       getBBOp  (size_t i) const { return _getOpRef<BasicBlock>(i); }
  Ref<BasicBlockImpl>   getBBIOp (size_t i) const { return _getOpRef<BasicBlockImpl>(i); }
  iptr                  getImmOp (size_t i) const { return _ptr[i]; } 

  Ref<MemoryReference>  getMRefOp(size_t i)       { return _getOpRef<MemoryReference>(i); } 
  Ref<BasicBlock>       getBBOp  (size_t i)       { return _getOpRef<BasicBlock>(i); } 
  Ref<BasicBlockImpl>   getBBIOp (size_t i)       { return _getOpRef<BasicBlockImpl>(i); } 

  Ref<const MemoryReference>  getConstMRefOp(size_t i) const { return _getConstOpRef<MemoryReference>(i); }
  Ref<const BasicBlock>       getConstBBOp  (size_t i) const { return _getConstOpRef<BasicBlock>(i); }
  Ref<const BasicBlockImpl>   getConstBBIOp (size_t i) const { return _getConstOpRef<BasicBlockImpl>(i); }
  
private:
  template <typename T>
  inline void _setOp(int i, T* p) { _ptr[i] = reinterpret_cast<intptr_t>(p); } 
  template <typename T>
  inline void _setOpRef(int i, Ref<T> p) { _setOp(i, p._ptr); }
  template <typename T>
  inline T _getOp(int i) const { return reinterpret_cast<T>(_ptr[i]); } 
  template <typename T>
  inline Ref<T> _getOpRef(int i) const { return Ref<T>(_getOp<T*>(i)); }
  template <typename T>
  inline Ref<const T> _getConstOpRef(int i) const { return Ref<const T>(_getOp<T*>(i)); }

  inline void _set(int i, Ref<const VirtualRegister> r) { setRRefOp(i, r); }
  inline void _set(int i, Ref<MemoryReference> m) { setMRefOp(i, m); }
  inline void _set(int i, Ref<BasicBlock> bb)     { setBBOp(i, bb); }
  inline void _set(int i, iptr imm)               { setImmOp(i, imm); }
  inline void _setAll(int i) {}
  template <typename U, typename ...T>
  inline void _setAll(int i, U first, T... args) { _set(i, first); _setAll(i + 1, args...); }
};

} // end of namespace jitcs

#endif
// _JITCS_INSTRUCTION_H_
