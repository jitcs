//===-- jitcs_adt_slice.h ---------------------------------------*- C++ -*-===//
// A reference to a array of objects.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A Slice is a pure reference type. It NEVER owns the memory it points to.
// A Slice is pair of pointer and size/count, and is based on the D Language
// array slice.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_ADT_SLICE_H_
#define _JITCS_ADT_SLICE_H_

#include "jitcs_base.h"
#include "jitcs_adt_ref.h"

namespace jitcs {

template <typename T, unsigned FLAGS>
struct _Slice {
  typedef typename std::add_const<T>::type CType;
  typedef T NCType;
  static const bool IsConstRef = (FLAGS & REF_Const) != 0;
  typedef typename std::conditional<IsConstRef, CType, NCType>::type Type;
  typedef Type* PtrType;
  typedef Type& RefType;

  typedef Type* iterator;
  typedef Type const* const_iterator;


  _Slice() {
    _ptr = nullptr;
    _size = 0;
  }
  _Slice(const _Slice&) = default;
  _Slice& operator = (const _Slice&) = default;
//  RefType operator *() { 
//    guarantee<(FLAGS & REF_Nullable) == 0>
//      ("dereferencing only for non-nullable refs, remove nullable first");
//    return *ptr();
//  }

  template <typename T2, unsigned FLAGS2>
  _Slice(_Slice<T2, FLAGS2> o) {
    static_assert((~FLAGS & FLAGS2) == 0, "incompatible _Slice flags");
    //static_assert(std::is_base_of<T, T2>::value, "incompatible _Slice types");
    _ptr = o._ptr;
  }
  template <typename T2>
  _Slice(T2* o, size_t n) {
    typedef typename std::remove_const<T2>::type T2B;
    //static_assert(std::is_base_of<T, T2B>::value, "incompatible _Slice types");
    //if ((FLAGS & REF_Nullable) == 0)
    //  assert(o != nullptr);
    _ptr = o;
    _size = n;
  }

  template <typename T2, unsigned FLAGS2>
  _Slice& operator =(_Slice<T2, FLAGS2> o) {
    static_assert((~FLAGS & FLAGS2) == 0, "incompatible _Slice flags");
    //static_assert(std::is_base_of<T, T2>::value, "incompatible _Ref types");
    _ptr = o._ptr;
    _size = o._size;
  }

//  Type* ptr() { return _ptr; }
//  Type const* ptr() const { return _ptr; }
  PtrType ptr() const { return _ptr; }
  size_t size() const { return _size; }

  bool empty() const { return _size == 0; }
  bool isEmpty() const { return _size == 0; }

  bool isValidIndex(size_t r) const { return r >= 0 && r < size(); }
  
  RefType operator [](size_t i) const { 
    assert(isValidIndex(i)); 
    return _ptr[i];
  }
  void popFront(size_t n) {
    _JITCS_DBG_CHECK_(n <= _size);
    _ptr += n;
    _size -= n;
  }
  void popBack(size_t n) {
    _JITCS_DBG_CHECK_(n <= _size);
    _size -= n;
  }
  void fill(const NCType& v) {
    guarantee<!IsConstRef>("mutation disabled on const container");
    for (size_t i = 0, n = _size; i < n; ++i)
      _ptr[i] = v;
  }

  iterator begin() { return _ptr; }
  iterator end() { return _ptr + _size; }
  const_iterator begin() const { return _ptr; }
  const_iterator end() const { return _ptr + _size; }
  const_iterator cbegin() const { return _ptr; }
  const_iterator cend() const { return _ptr + _size; }

  PtrType _ptr;
  size_t _size; 
};

template <typename T>
using Slice = _Slice<typename std::remove_const<T>::type,
                     (std::is_const<T>::value ? REF_Const : REF_Any)>;

} // End jitcs namespace

#endif
// _JITCS_ADT_SLICE_H_
