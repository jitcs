//===-- jitcs_adt_refcounter.h ----------------------------------*- C++ -*-===//
// A reference-counted, object owning, not-thread-safe pointer class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Do NOT share a RefCounter handled object across threads.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_ADT_REFCOUNTER_H_
#define _JITCS_ADT_REFCOUNTER_H_

#include <assert.h>

namespace jitcs {
// Ref represents a nonnull pointer to some object. 
template <typename T> struct RefCounter {
  T* _ptr;
  
  RefCounter() = delete;
  RefCounter& operator =(const RefCounter&) = delete;
  RefCounter(const RefCounter& r) { _ptr = r._ptr; _ptr->_incRef(); }
  RefCounter(T* d) { assert(d != nullptr); _ptr = d; _ptr->_incRef(); }
  ~RefCounter() { _ptr->_decRef(); }
  
  T* operator ->() { return _ptr; }
  T& operator *() { return *_ptr; }
};
}

#endif 
// _JITCS_ADT_REFCOUNTER_H_
