//===-- tests/test_asm.h ----------------------------------------*- C++ -*-===//
// Class of a simple assembler for testing purposes.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The assembler is used with the _bin/{arch}/*.as files.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_TESTS_TESTASM_H_
#define _JITCS_TESTS_TESTASM_H_

#include "jitcs_base.h"
#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_instructionstream.h"
#include "unittest.h"
#include "jitcs_tmpalloc.h"
#include "jitcs_memref.h"
#include <vector>
#include <string>
#include <map>
#include <memory>
#include <unordered_set>

namespace jitcs {
  class IMachineInfo;
  class MemoryMgr;
  class Function;  
  class BasicBlock;
  class TempAllocator;
  struct VirtualRegister;
}

struct TestAssembler {
  bool setup;
  std::string filename, funcname;
  size_t lineno;
  std::vector<jitcs::u8> outbytes;
  jitcs::RefCounter<jitcs::IMachineInfo> mi;
  jitcs::RefCounter<jitcs::MemoryMgr> mem;
  jitcs::RefCounter<jitcs::TempAllocator> alloc;
  std::unique_ptr<jitcs::Function> fnc;
  jitcs::RefOrNull<jitcs::BasicBlock> curbb;
  jitcs::InstructionStreamBuffer<256> ibuf;
  std::map<std::string, jitcs::BasicBlock*> bbnames;
  std::map<std::string, const jitcs::VirtualRegister*> vrnames;
  jitcs::StreamAllocator<jitcs::MemoryReference>* memstream;

  std::map<std::string, jitcs::u32> constinsnames;
  std::map<std::string, jitcs::u32> constrcnames;
  std::map<std::string, const jitcs::VirtualRegister*> constvrnames;
  
  TestAssembler(jitcs::RefCounter<jitcs::MemoryMgr> m, 
                jitcs::RefCounter<jitcs::TempAllocator> a,
                jitcs::RefCounter<jitcs::IMachineInfo> mi);
  virtual ~TestAssembler();
  
  void clear();
  const jitcs::VirtualRegister* findReg(std::string const& n);
  void checkFile(jitcs::UnitTest&, std::string const& filename, 
                 std::unordered_set<jitcs::u32>* used_insids = nullptr,
                 std::string const& outputfile = "");

  virtual void setupInstructionNames(std::map<std::string, jitcs::u32>&) = 0;
  virtual void setupRegisterClassNames(std::map<std::string, jitcs::u32>&) = 0;
  virtual void setupFixedRegisterNames(std::map<std::string, const jitcs::VirtualRegister*>&) = 0;
  virtual const char* addInstruction(jitcs::u32 insid, std::vector<std::string> const& parms) = 0;

protected:
  typedef std::vector<size_t> DFAnnotationInstruction;
  typedef std::vector<DFAnnotationInstruction> DFAnnotationBlock;
  typedef std::map<size_t, DFAnnotationBlock> DFAnnotationFunction;
  
protected:
  void _setupInstructionDFAnnotation
      (jitcs::UnitTest& t, DFAnnotationInstruction& ann, const std::string& text);
  bool _runDFTest(jitcs::UnitTest& t, DFAnnotationFunction& fnc);
  bool _runTest(jitcs::UnitTest& t, std::string const& outputfile);
  void _error(jitcs::UnitTest& t, std::string const& msg);
  static std::string toLower(std::string const& z);
  static void split(std::string const& line, std::string const& splitter, std::string& pin, std::string& pout);
  static void split(std::string const& line, std::string const& splitter, std::vector<std::string>& parts);
};
#endif
//  _JITCS_TESTS_TESTASM_H_
