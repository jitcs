//===-- tests/test_adt_smallheap.cpp ----------------------------*- C++ -*-===//
// Test to check SmallTopHeap class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_adt_smallheap.h"
#include "unittest.h"

#include <stdio.h>

using namespace jitcs;

template <unsigned N, bool B>
void dump(const char* msg, SmallTopHeap<u32, N, B>& th) {
  printf("%s: SmallTopHeap:\n", msg);
  printf("  heap:");
  for (size_t i = 0; i < th.size; ++i) {
    if (B) {
      printf(" %d:%d(%d)", i, th.heap[i], th.values[i]);
    } else {
      printf(" %d:%d", i, th.heap[i]);
    }
  }
  printf("\n");
  printf("  hpos:");
  for (size_t i = 0; i < th.NUM; ++i) {
    if (th.hpos[i] >= th.size) continue;
    if (th.heap[th.hpos[i]] != i) continue;
    if (B) {
      printf(" %d->%d", i, th.hpos[i]);
    } else {
      printf(" %d(%d)->%d", i, th.values[i], th.hpos[i]);
    }
  }
  printf("\n");
}

static void test(UnitTest& t) {
  SmallTopHeap<u32, 32, false> h1;
  t.check("unordered val/empty", h1.isEmpty());
  h1.push(5, 1000);
  h1.push(1, 2000);
  h1.push(7, 1500);
  h1.push(3, 1800);
  h1.push(9,  800);
  h1.push(13, 900);
  h1.push(14, 900);
  h1.push(15, 900);
  t.check("unordered val/check 1", h1.check());
  t.check("unordered val/top", 
          h1.peekTop() == 1 && h1.peekTopValue() == 2000);
  h1.dropTop();
  t.check("unordered val/check after droptop", h1.check());
  t.check("unordered val/top 2", 
          h1.peekTop() == 3 && h1.peekTopValue() == 1800);
  h1.drop(7);
  t.check("unordered val/check after drop", h1.check());
  t.check("unordered val/top 3", 
          h1.peekTop() == 3 && h1.peekTopValue() == 1800);
  h1.dropTop();
  h1.dropTop();
  h1.dropTop();
  h1.dropTop();
  h1.dropTop();
  t.check("unordered val/check after drop 5", h1.check());
  t.check("unordered val/top last", 
          h1.peekTop() == 9 && h1.peekTopValue() == 800);
  h1.dropTop();
  t.check("unordered val/check empty", h1.isEmpty());
  
  SmallTopHeap<u32, 32, true> h2;
  t.check("ordered val/empty", h2.isEmpty());
  h2.push(5, 1000);
  h2.push(1, 2000);
  h2.push(7, 1500);
  h2.push(3, 1800);
  h2.push(9,  800);
  h2.push(13, 900);
  h2.push(14, 900);
  h2.push(15, 900);
  t.check("ordered val/check 1", h2.check());
  t.check("ordered val/top", 
          h2.peekTop() == 1 && h2.peekTopValue() == 2000);
  h2.dropTop();
  t.check("ordered val/check after droptop", h2.check());
  t.check("ordered val/top 2", 
          h2.peekTop() == 3 && h2.peekTopValue() == 1800);
  h2.drop(7);
  t.check("ordered val/check after drop", h2.check());
  t.check("ordered val/top 3", 
          h2.peekTop() == 3 && h2.peekTopValue() == 1800);
  h2.dropTop();
  h2.dropTop();
  h2.dropTop();
  h2.dropTop();
  h2.dropTop();
  t.check("ordered val/check after drop 5", h2.check());
  t.check("ordered val/top last", 
          h2.peekTop() == 9 && h2.peekTopValue() == 800);
  h2.dropTop();
  t.check("ordered val/check empty", h2.isEmpty());
}

static UnitTestRun _1("ADT/SmallTopHeap", test);
