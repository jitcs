//===-- tests/test_adt_slice.cpp --------------------------------*- C++ -*-===//
// Test to check Slice class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_adt_slice.h"
#include "unittest.h"

using namespace jitcs;

static void test(UnitTest& t) {
  size_t data[5] = {0x12345678, 0xdeadbeef, 0x1, ~(size_t)0, 0};
  Slice<size_t> arr;
  t.check("Slice/0", arr.size() == 0);
  Slice<size_t>::iterator b = arr.begin(), e = arr.end();
  t.check("Slice/1", b == e);

  arr = Slice<size_t>(data, 5);
  t.check("Slice/2", arr.size() == 5 && arr._ptr == data);
  t.check("Slice/3", arr.isValidIndex(4) && !arr.isValidIndex(5));
  t.check("Slice/4", arr[1] == 0xdeadbeef);
  
  b = arr.begin(), e = arr.end();
  t.check("Slice/5", b != e);
  ++b;
  t.check("Slice/6", *b == 0xdeadbeef);
  ++b;
  ++b;
  ++b;
  t.check("Slice/7", b != e);
  ++b;
  t.check("Slice/8", b == e);
}

static UnitTestRun _("ADT/Slice", test);
