//===-- tests/test_fnctypeinfo.cpp ------------------------------*- C++ -*-===//
// Test of functions deconstructing function types.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_fnctypeinfo.h"
#include "unittest.h"

using namespace jitcs;

static bool isIntOrPtrInt(FTTypeId id) {
  return id == FTT_Int || id == FTT_PtrInt;
}


static void test(UnitTest& t) {
  typedef void (__cdecl *F0)();
  typedef int  (__stdcall *F2)(int, int*);
  typedef int* (__fastcall *F8)(int, uint, iptr, uptr, char*, const short*,
                     long&, const int&);
  t.check("GetFTSubType/1", GetFTSubType<F0>::Value == FTS_CDecl);
  t.check("GetFTSubType/2", GetFTSubType<F2>::Value == FTS_StdCall);
  t.check("GetFTSubType/3", GetFTSubType<F8>::Value == FTS_FastCall);

  t.check("GetFTParamCount/1", GetFTParamCount<F0>::Value == 0);
  t.check("GetFTParamCount/2", GetFTParamCount<F2>::Value == 2);
  t.check("GetFTParamCount/3", GetFTParamCount<F8>::Value == 8);

  t.check("GetFTResultId/1", GetFTResultId<F0>::Value == FTT_Void);
  t.check("GetFTResultId/2", isIntOrPtrInt(GetFTResultId<F2>::Value));
  t.check("GetFTResultId/3", GetFTResultId<F8>::Value == FTT_Ptr);

  t.check("GetFTParamId/1", GetFTParamId<F0, 0>::Value == FTT_Void);
  t.check("GetFTParamId/2", isIntOrPtrInt(GetFTParamId<F2, 0>::Value));
  t.check("GetFTParamId/3", GetFTParamId<F2, 1>::Value == FTT_Ptr);
  t.check("GetFTParamId/4", isIntOrPtrInt(GetFTParamId<F8, 0>::Value));
  t.check("GetFTParamId/5", isIntOrPtrInt(GetFTParamId<F8, 1>::Value));
  t.check("GetFTParamId/6", isIntOrPtrInt(GetFTParamId<F8, 2>::Value));
  t.check("GetFTParamId/7", isIntOrPtrInt(GetFTParamId<F8, 3>::Value));
  t.check("GetFTParamId/8", GetFTParamId<F8, 4>::Value == FTT_Ptr);
  t.check("GetFTParamId/9", GetFTParamId<F8, 5>::Value == FTT_Ptr);
  t.check("GetFTParamId/10", GetFTParamId<F8, 6>::Value == FTT_Ptr);
  t.check("GetFTParamId/11", GetFTParamId<F8, 7>::Value == FTT_Ptr);
}

static UnitTestRun _("FncTypeInfo", test);
