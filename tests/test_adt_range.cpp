//===-- tests/test_adt_range.cpp --------------------------------*- C++ -*-===//
// Test to check Range class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_adt_range.h"
#include "unittest.h"
#include <vector>

using namespace jitcs;

static void test(UnitTest& t) {
  std::vector<int> test1;
  Range<std::vector<int>::iterator> r1 = getRange(test1);
  t.check("Range/0", r1.isEmpty() && !r1);
  
  test1.push_back(0xdeadbeef);
  r1 = getRange(test1);
  t.check("Range/1", !r1.isEmpty() && !!r1);
  t.check("Range/2", *r1 == 0xdeadbeef);
  ++r1;
  t.check("Range/3", r1.isEmpty() && !r1);
}

static UnitTestRun _("ADT/Range", test);
