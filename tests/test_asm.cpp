//===-- tests/test_asm.cpp --------------------------------------*- C++ -*-===//
// Class of a simple assembler for testing purposes.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The assembler is used with the _bin/{arch}/*.as files.
//===----------------------------------------------------------------------===//

#include "test_asm.h"
#include "jitcs_callingconvention.h"
#include "jitcs_machine.h"
#include "jitcs_tmpalloc.h"
#include "jitcs_memmgr.h"
#include "jitcs_function.h"
#include "jitcs_int_dfg_analysis.h"
#include "jitcs_int_machine.h"
#include "jitcs_int_adt_dynslice.h"
#include "jitcs_int_bblock_impl.h"
#include "jitcs_dumper.h"
#include <stdio.h>

static bool readln(FILE* f, std::string& line, size_t& linecount) {
  char c;
  line.clear();
  while (fread(&c, 1, 1, f) == 1) {
    if (c == '\r') return true;
    if (c == '\n') { linecount++; return true; }
    line.append(1, c);
  }
  return (line.size() > 0);
}
static void trim(std::string& line) {
  {
    std::string::iterator i = line.begin(), e = line.end();
    while (i != e && *i == ' ') ++i;
    line.erase(line.begin(), i);
  }
  {
    std::string::iterator i = line.begin(), e = line.end();
    while (i != e && *(e - 1) == ' ') --e;
    line.erase(e, line.end());
  }
}
void TestAssembler::split(std::string const& line, std::string const& splitter, std::string& pin, std::string& pout) {
  std::string::size_type pos = line.find(splitter);
  if (pos != std::string::npos) {
    pin = line.substr(0, pos);
    pout = line.substr(pos + splitter.size());
    trim(pin);
    trim(pout);
  } else {
    pin = line;
    trim(pin);
    pout.clear();
  }
}
void TestAssembler::split(std::string const& line, std::string const& splitter, std::vector<std::string>& parts) {
  std::string worker = line;
  std::string::size_type pos;
  while ((pos = worker.find(splitter)) != std::string::npos) {
    std::string pin = worker.substr(0, pos);
    trim(pin);
    parts.push_back(pin);
    worker = worker.substr(pos + splitter.size());
  }
  trim(worker);
  parts.push_back(worker);
}
static bool read_outbytes(std::string const& data, std::vector<jitcs::u8>& out) {
  jitcs::u8 d = 0;
  bool second = false;
  for (std::string::const_iterator i = data.begin(), e = data.end(); i != e; ++i) {
    char c = *i;
    if (c == ' ') continue;
    if (c >= '0' && c <= '9') {
      d = d * 16 + (c - '0');
      second = !second;
      if (!second) out.push_back(d);
      continue;
    }
    c = c | 0x20;
    if (c >= 'a' && c <= 'f') {
      d = d * 16 + 10 + (c - 'a');
      second = !second;
      if (!second) out.push_back(d);
      continue;
    }
    return false;
  }
  return !second;
}
std::string TestAssembler::toLower(std::string const& z) {
  std::string x = z;
  for (std::string::iterator i = x.begin(), e = x.end(); i != e; ++i) {
    if (*i >= 'A' && *i <= 'Z') *i |= 0x20;
  }
  return x;
}

TestAssembler::TestAssembler(jitcs::RefCounter<jitcs::MemoryMgr> m, 
                             jitcs::RefCounter<jitcs::TempAllocator> a,
                             jitcs::RefCounter<jitcs::IMachineInfo> mi_) 
  : mem(m)
  , alloc(a)
  , mi(mi_) {
  setup = false;
  clear();
}
TestAssembler::~TestAssembler() {
}

void TestAssembler::clear() {
  outbytes.clear();
  bbnames.clear();
  vrnames.clear();
  fnc.reset();
  curbb = nullptr;
}
const jitcs::VirtualRegister* TestAssembler::findReg(std::string const& n) {
  std::string n2 = toLower(n);
  if (constvrnames.find(n2) != constvrnames.end())
    return constvrnames[n2];
  if (vrnames.find(n2) != vrnames.end())
    return vrnames[n2];
  return nullptr;
}
//void printBytes(std::vector<u8> const& v) {
//  for (std::vector<u8>::const_iterator i = v.begin(), e = v.end(); i != e; ++i)
//    printf("%02X", *i);
//}
bool TestAssembler::_runTest(jitcs::UnitTest& t, std::string const& outputfile) {
  std::vector<jitcs::u8> expected = outbytes, result;
  jitcs::MemoryMgr::CodeAndData cda = fnc->generate(mem, jitcs::Function::Direct_Code_Gen);
  result.insert(result.begin(), cda.code._ptr, cda.code._ptr + cda.code.size());
  mem->deallocate(cda);

  bool ok = true;
  size_t i, n;
  if (expected.size() > result.size()) ok = false;
  if (ok) {
    for (i = 0, n = expected.size(); i < n; ++i) {
      if (expected[i] != result[i]) {
        ok = false; break;
      }
    }
  }
  
  if (outputfile.size() > 0) {
    FILE* f = fopen(outputfile.c_str(), "wb");
    if (result.size() > 0)
      fwrite(&result[0], result.size(), 1, f);
    fclose(f);
  }
  
  if (!ok) {
    char buffer[100];
    sprintf(buffer, "byte test failed at byte %d", i);
    _error(t, buffer);
  }
  clear();
  return ok;
}
void TestAssembler::_error(jitcs::UnitTest& t, std::string const& text) {
  std::string msg;
  if (filename.size() > 0) {
    msg += "file ";
    msg += filename;
    msg += ":";
  }
  if (funcname.size() > 0) {
    msg += "fnc ";
    msg += funcname;
    msg += ":";
  }
  if (lineno >= 1) {
    char buf[20];
    sprintf(buf, "line %d: ", lineno);
    msg += buf;
  }
  msg += text;
  t.check(msg.c_str(), false);
}
static void _check(jitcs::UnitTest& t, std::string const& text, bool condition) {
  t.check(text.c_str(), condition);
}

void TestAssembler::_setupInstructionDFAnnotation
    (jitcs::UnitTest& t, DFAnnotationInstruction& ann, const std::string& text) {
  jitcs::Ref<const jitcs::IMachineDetails> det = mi->details();
  jitcs::FunctionImpl& f = reinterpret_cast<jitcs::FunctionImpl&>(*fnc);
  // allocate space for 2 bits for all resources
  size_t rcnt = det->getResCount() + f.vregs_size();
  ann.resize(jitcs::DivRoundedUpByPowerOf2<jitcs::BitSlice::E_BitsPerWord>(rcnt) * 2);
  jitcs::BitSlice bits_used(&ann[0], rcnt);
  jitcs::BitSlice bits_defined(&ann[ann.size() / 2], rcnt);
  // clear def/use bits
  bits_used.clearAll();
  bits_defined.clearAll();

  std::vector<std::string> regvec;
  split(toLower(text), ",", regvec);
  for (size_t i = 0, n = regvec.size(); i < n; ++i) {
    if (regvec[i] == "") continue;
    std::string reg, mode;
    split(regvec[i], "/", reg, mode);
    jitcs::ResId res;
    if (constvrnames.find(reg) != constvrnames.end()) {
      res = constvrnames.find(reg)->second->res;
    } else if (vrnames.find(reg) != vrnames.end()) {
      res = vrnames.find(reg)->second->res;
    } else {
      _error(t, "DF Annotation failed for unknown register " + reg);
      continue;
    }
    if (res >= rcnt) {
      _error(t, "DF Annotation: wrong resource for register " + reg);
      continue;
    }
    if (mode == "r") {
      bits_used.mark(res);
    } else if (mode == "w") {
      bits_defined.mark(res);
    } else if (mode == "rw") {
      bits_used.mark(res);
      bits_defined.mark(res);
    } else {
      _error(t, "DF Annotation: wrong mode " + mode);
    }
  }
}
bool TestAssembler::_runDFTest(jitcs::UnitTest& t, DFAnnotationFunction& ann) {
  if (ann.size() == 0) return true;
  jitcs::Ref<const jitcs::IMachineDetails> det = mi->details();
  jitcs::FunctionImpl& f = reinterpret_cast<jitcs::FunctionImpl&>(*fnc);
  size_t rcnt = det->getResCount() + f.vregs_size();

  jitcs::DynSlice<jitcs::u32,8> touchedFixed(det->getResClassCount());
  jitcs::DFGAnalysis a;
  a.init(f, jitcs::DFGAnalysis::M_Local, touchedFixed);
  
  jitcs::DynBitSlice<128> used(rcnt, false), defined(rcnt, false);
  
  bool result = true;
  for (jitcs::FunctionImpl::bb_range r = f.bbs_range(); !!r; ++r) {
    jitcs::BasicBlockImpl* bb = *r;
    if (bb->instr_size() > 0 && ann.find(bb->id()) == ann.end()) {
      char buffer[100];
      sprintf(buffer, "missing DF annotations for bblock %d", bb->id());
      _error(t, buffer);
      result = false;
      continue;
    }
    DFAnnotationBlock& bann = ann[bb->id()];
    if (bann.size() != bb->instr_size()) {
      char buffer[100];
      sprintf(buffer, "length mismatch for DF annotations of bblock %d", bb->id());
      _error(t, buffer);
      result = false;
      continue;
    }
    for (size_t j = 0, m = bb->instr_size(); j < m; ++j) {
      used.clearAll();
      defined.clearAll();
      a.extractUseDef(bb, j, used, defined);
      jitcs::BitSlice comp_used(&bann[j][0], rcnt);
      jitcs::BitSlice comp_defined(&bann[j][bann[j].size() / 2], rcnt);
      if (!comp_used.equals(used)
          || !comp_defined.equals(defined)) {
        char buffer[100];
        sprintf(buffer, "df mismatch in bblock %d, instructon %d", bb->id(), j);
        _error(t, buffer);
        jitcs::PrintFDumper dumper;
        printf("comp used ");
        comp_used.dump(dumper);
        printf("\n");
        printf("comp define ");
        comp_defined.dump(dumper);
        printf("\n");
        printf("use ");
        used.dump(dumper);
        printf("\n");
        printf("def ");
        defined.dump(dumper);
        printf("\n");
        result = false;
      }
    }
  }
  return result;
}

void TestAssembler::checkFile(jitcs::UnitTest& t, std::string const& filename_,
                              std::unordered_set<jitcs::u32>* used_insids,
                              std::string const& outputfile) {
  DFAnnotationFunction ann;
  
  if (!setup) {
    setupInstructionNames(constinsnames);
    setupRegisterClassNames(constrcnames);
    setupFixedRegisterNames(constvrnames);
    setup = true;
  }
  filename = filename_;
  funcname = "";
  lineno = 0;
  clear();

  jitcs::StreamAllocator<jitcs::MemoryReference> memstreamer(*alloc, 32);
  memstream = &memstreamer;
  
  FILE* f = fopen(filename.c_str(), "r");
  if (!f) {
    _error(t, "cannot open");
    return;
  }
  bool dump = false;
  bool empty = true;
  bool inserror = false;
  std::string line, pin, pout;
  while (readln(f, line, lineno)) {
    split(line, "==>", pin, pout);
    
    if (pin.substr(0,3) == "<==") {
      if (!empty) { 
        ibuf.end();
        if (inserror) {
          _error(t, "did not compile function due to previous errors");
          clear();
          ann.clear();
        } else {
          _runDFTest(t, ann);
          if (!_runTest(t, outputfile)) return; 
        }
      }
      empty = true;
      inserror = false;
      funcname = pin.substr(3);
      trim(funcname);
    } else if (pin.substr(0,5) == "@fun:") {
      jitcs::FTSubType sub = jitcs::FTS_CDecl;
      std::vector<jitcs::FTTypeId> restypes, partypes;
      std::string parms, result;
      std::vector<std::string> parmvec, resvec;
      split(pin.substr(5), "->", parms, result);
      split(parms, ",", parmvec);
      split(result, ",", resvec);
      for (size_t i = 0, n = parmvec.size(); i < n; ++i) {
        std::string type, varname;
        split(parmvec[i], " ", type, varname);
        if (type == "int") {
          partypes.push_back(jitcs::FTT_Int);
        } else if (type == "void*") {
          partypes.push_back(jitcs::FTT_Ptr);
        } else if (type == "ptrdiff_t") {
          partypes.push_back(jitcs::FTT_PtrInt);
        } else if (type == "void") {
          partypes.push_back(jitcs::FTT_Void);
        } else {
          _error(t, "invalid param type "+type);
          return;
        }
      }
      if (partypes.size() == 1 && partypes[0] == jitcs::FTT_Void) {
        partypes.clear();
      }
      for (size_t i = 0, n = resvec.size(); i < n; ++i) {
        std::string type, varname;
        split(resvec[i], " ", type, varname);
        if (type == "int") {
          restypes.push_back(jitcs::FTT_Int);
        } else if (type == "void*") {
          restypes.push_back(jitcs::FTT_Ptr);
        } else if (type == "ptrdiff_t") {
          restypes.push_back(jitcs::FTT_PtrInt);
        } else if (type == "void") {
          restypes.push_back(jitcs::FTT_Void);
        } else {
          _error(t, "invalid result type "+type);
          return;
        }
      }
      if (restypes.size() == 1 && restypes[0] == jitcs::FTT_Void) {
        restypes.clear();
      }
      if (resvec.size() != 1) {
        _error(t, "more than one result value");
        return;
      }
      jitcs::Slice<jitcs::FTTypeId> resslice = restypes.size() > 0 
                                        ? jitcs::Slice<jitcs::FTTypeId>(&restypes[0], restypes.size())
                                        : jitcs::Slice<jitcs::FTTypeId>();
      jitcs::Slice<jitcs::FTTypeId> parslice = partypes.size() > 0 
                                        ? jitcs::Slice<jitcs::FTTypeId>(&partypes[0], partypes.size())
                                        : jitcs::Slice<jitcs::FTTypeId>();
      std::shared_ptr<const jitcs::CallingConvention> cc
        = mi->getCC(sub, resslice, parslice);
      fnc.reset();
      fnc = std::move(mi->createFnc(alloc, cc));
      
      //for (size_t i = 0, n = parmvec.size(); i < n; ++i) {
      //  std::string type, varname;
      //  split(parmvec[i], " ", type, varname);
      //  if (varname.size() > 0) vrnames[toLower(varname)] = fnc->arg(i);
      //}
      //for (size_t i = 0, n = resvec.size(); i < n; ++i) {
      //  std::string type, varname;
      //  split(resvec[i], " ", type, varname);
      //  if (varname.size() > 0) vrnames[toLower(varname)] = fnc->result(i);
      //}
      empty = false;
      curbb = fnc->getStartBlock();
      ibuf.start(curbb);
    } else if (pin.substr(0,1) == ":") {
      if (empty) {
        _error(t, "cannot add basic block to empty function");
        return;
      }
      std::string n2 = toLower(pin.substr(1));
      jitcs::RefOrNull<jitcs::BasicBlock> bb = bbnames.find(n2) != bbnames.end() ? jitcs::Ref<jitcs::BasicBlock>(bbnames[n2]) : fnc->createBasicBlock();
      ibuf.end();
      curbb = bb;
      ibuf.start(curbb);
      bbnames[pin.substr(1)] = bb.ptr();
    } else if (pin.substr(0,5) == "@var:") {
      if (empty) {
        _error(t, "cannot add var to empty function");
        return;
      }
      std::string z = toLower(pin.substr(5));
      trim(z);
      std::string type, varname;
      split(z, " ", type, varname);
      if (varname == "") {
        _error(t, "unnamed var");
        return;
      }
      if (constrcnames.find(type) == constrcnames.end()) {
        _error(t, "invalid register class for var " + varname);
        return;
      }
      vrnames[toLower(varname)] = fnc->createRegister(static_cast<jitcs::RegClassId>(constrcnames[type]))._ptr;
    } else if (pin.substr(0,10) == "@strategy:") {
      if (empty) {
        _error(t, "cannot set flags of empty function");
        return;
      }
    } else if (pin == "") {
    } else {
      std::string pin1, pin2;
      split(pin, "|", pin1, pin2);
      pin = pin1;
      // 
      std::string ins, parms;
      std::vector<std::string> parmvec;
      split(toLower(pin), " ", ins, parms);
      if (parms != "")
        split(parms, ",", parmvec);
      if (constinsnames.find(ins) == constinsnames.end()) {
        inserror = true;
        _error(t, "invalid instruction " + ins);
      } else {
        if (used_insids)
          used_insids->insert(constinsnames[ins]);
        const char* x = addInstruction(constinsnames[ins], parmvec);
        if (x != nullptr) {
          inserror = true;
          _error(t, "error in instruction '" + pin + "': " + x);
        } else if (!curbb.isNull()) {
          DFAnnotationBlock& dfab = ann[curbb.removeNullType()->id()];
          dfab.push_back(DFAnnotationInstruction());
          _setupInstructionDFAnnotation(t, dfab[dfab.size() - 1], pin2);
        }
      }
    }
    
    if (!read_outbytes(pout, outbytes)) {
      _error(t, "invalid outbytes");
      return;
    }
  }
  if (!empty) { 
    ibuf.end();
    if (!inserror) {
      _runDFTest(t, ann);
      if (!_runTest(t, outputfile)) return; 
    }
  }
  return;
}
