//===-- tests/test_callingconvention.cpp ------------------------*- C++ -*-===//
// Test of calling conventions of several platforms.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_callingconvention.h"
#include "unittest.h"
#include "jitcs_x86_32_machine.h"
#include "jitcs_x86_64_machine.h"
#include "jitcs_int_virtualregister.h"
#include "x86/jitcs_int_x86_32_regs.h"
#include "x86/jitcs_int_x86_64_regs.h"

#include <stdio.h>

using namespace jitcs;

static bool CheckReg(RefOrNull<const VirtualRegister> found, Ref<const VirtualRegister> expected) {
  return found._ptr == expected._ptr;
}
static bool CheckSSI(SpillSlotInfo ssi, int offset, int logSz) {
  return ssi.getFP() == 1 && ssi.getOffset() == offset && ssi.getLogSize() == logSz;
}

static void test(UnitTest& t) {
  typedef void (__cdecl *FT0_c)();
  typedef void (__fastcall *FT0_f)();
  typedef void (__stdcall *FT0_s)();
  typedef int (__cdecl *FT8_c)(int,void*,int,void*,int,void*,int,void*);
  typedef int (__fastcall *FT8_f)(int,void*,int,void*,int,void*,int,void*);
  typedef int (__stdcall *FT8_s)(int,void*,int,void*,int,void*,int,void*);
  {
    RefCounter<IMachineInfo> mi = GetX86_32WinMachineInfo();
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_c>();
      t.check("Win32/CDecl/0 Params/a", cc->getResultCount() == 0);
      t.check("Win32/CDecl/0 Params/b", cc->getParamCount() == 0);
      t.check("Win32/CDecl/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Win32/CDecl/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_f>();
      t.check("Win32/FastCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Win32/FastCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Win32/FastCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Win32/FastCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCallee);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_s>();
      t.check("Win32/StdCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Win32/StdCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Win32/StdCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Win32/StdCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCallee);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT8_c>();
      t.check("Win32/CDecl/8 Params/a", cc->getResultCount() == 1);
      t.check("Win32/CDecl/8 Params/b", cc->getParamCount() == 8);
      t.check("Win32/CDecl/8 Params/c", cc->getParamAreaSize() == 32);
      t.check("Win32/CDecl/8 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Win32/CDecl/8 Params/e", CheckReg(cc->getResultRegister(0), x86_32::EAX));
      t.check("Win32/CDecl/8 Params/f", CheckSSI(cc->getParamSpillSlotInfo(0), 0, 2));
      t.check("Win32/CDecl/8 Params/g", CheckSSI(cc->getParamSpillSlotInfo(7), 28, 2));
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT8_f>();
      t.check("Win32/FastCall/8 Params/a", cc->getResultCount() == 1);
      t.check("Win32/FastCall/8 Params/b", cc->getParamCount() == 8);
      t.check("Win32/FastCall/8 Params/c", cc->getParamAreaSize() == 24);
      t.check("Win32/FastCall/8 Params/d", cc->getStackCleaningStrategy() == CCS_ByCallee);
      t.check("Win32/FastCall/8 Params/e", CheckReg(cc->getResultRegister(0), x86_32::EAX));
      t.check("Win32/FastCall/8 Params/f", CheckReg(cc->getParamRegister(0), x86_32::ECX));
      t.check("Win32/FastCall/8 Params/g", CheckReg(cc->getParamRegister(1), x86_32::EDX));
      t.check("Win32/FastCall/8 Params/h", CheckSSI(cc->getParamSpillSlotInfo(2), 0, 2));
      t.check("Win32/FastCall/8 Params/i", CheckSSI(cc->getParamSpillSlotInfo(7), 20, 2));
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT8_s>();
      t.check("Win32/StdCall/8 Params/a", cc->getResultCount() == 1);
      t.check("Win32/StdCall/8 Params/b", cc->getParamCount() == 8);
      t.check("Win32/StdCall/8 Params/c", cc->getParamAreaSize() == 32);
      t.check("Win32/StdCall/8 Params/d", cc->getStackCleaningStrategy() == CCS_ByCallee);
      t.check("Win32/StdCall/8 Params/e", CheckReg(cc->getResultRegister(0), x86_32::EAX));
      t.check("Win32/StdCall/8 Params/f", CheckSSI(cc->getParamSpillSlotInfo(0), 0, 2));
      t.check("Win32/StdCall/8 Params/g", CheckSSI(cc->getParamSpillSlotInfo(7), 28, 2));
    }
  }
  {
    RefCounter<IMachineInfo> mi = GetX86_32PosixMachineInfo();
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_c>();
      t.check("Posix32/CDecl/0 Params/a", cc->getResultCount() == 0);
      t.check("Posix32/CDecl/0 Params/b", cc->getParamCount() == 0);
      t.check("Posix32/CDecl/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Posix32/CDecl/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_f>();
      t.check("Posix32/FastCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Posix32/FastCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Posix32/FastCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Posix32/FastCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Posix32/FastCall/0 Params/e", cc->getSubType() == FTS_CDecl);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_s>();
      t.check("Posix32/StdCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Posix32/StdCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Posix32/StdCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Posix32/StdCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Posix32/StdCall/0 Params/e", cc->getSubType() == FTS_CDecl);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT8_c>();
      t.check("Posix32/CDecl/8 Params/a", cc->getResultCount() == 1);
      t.check("Posix32/CDecl/8 Params/b", cc->getParamCount() == 8);
      t.check("Posix32/CDecl/8 Params/c", cc->getParamAreaSize() == 32);
      t.check("Posix32/CDecl/8 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Posix32/CDecl/8 Params/e", CheckReg(cc->getResultRegister(0), x86_32::EAX));
      t.check("Posix32/CDecl/8 Params/f", CheckSSI(cc->getParamSpillSlotInfo(0), 0, 2));
      t.check("Posix32/CDecl/8 Params/g", CheckSSI(cc->getParamSpillSlotInfo(7), 28, 2));
    }
  }
  {
    RefCounter<IMachineInfo> mi = GetX86_64WinMachineInfo();
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_c>();
      t.check("Win64/CDecl/0 Params/a", cc->getResultCount() == 0);
      t.check("Win64/CDecl/0 Params/b", cc->getParamCount() == 0);
      t.check("Win64/CDecl/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Win64/CDecl/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_f>();
      t.check("Win64/FastCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Win64/FastCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Win64/FastCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Win64/FastCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Win64/FastCall/0 Params/e", cc->getSubType() == FTS_CDecl);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_s>();
      t.check("Win64/StdCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Win64/StdCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Win64/StdCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Win64/StdCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Win64/StdCall/0 Params/e", cc->getSubType() == FTS_CDecl);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT8_c>();
      /*
      switch (cc->getSubType()) {
      case FTS_CDecl: printf("cdecl"); break;
      case FTS_FastCall: printf("cdecl"); break;
      case FTS_StdCall: printf("cdecl"); break;
      default: printf("other"); break;
      }
      printf(" ");
      for (size_t i = 0; i < cc->getResultCount(); ++i) {
        if (i>0) printf(",");
        switch (cc->getResultType(i)) {
        case FTT_Void: printf("void"); break;
        case FTT_Int: printf("int"); break;
        case FTT_PtrInt: printf("ptrint"); break;
        case FTT_Ptr: printf("ptr"); break;
        default: printf("other"); break;
        }
      }
      printf("<-");
      for (size_t i = 0; i < cc->getParamCount(); ++i) {
        if (i>0) printf(",");
        switch (cc->getParamType(i)) {
        case FTT_Void: printf("void"); break;
        case FTT_Int: printf("int"); break;
        case FTT_PtrInt: printf("ptrint"); break;
        case FTT_Ptr: printf("ptr"); break;
        default: printf("other"); break;
        }
      }
      printf("\n");
      */
      
      t.check("Win64/CDecl/8 Params/a", cc->getResultCount() == 1);
      t.check("Win64/CDecl/8 Params/b", cc->getParamCount() == 8);
      t.check("Win64/CDecl/8 Params/c", cc->getParamAreaSize() == 64);
      t.check("Win64/CDecl/8 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Win64/CDecl/8 Params/e", CheckReg(cc->getResultRegister(0), x86_64::RAX));
      t.check("Win64/CDecl/8 Params/f", CheckReg(cc->getParamRegister(0), x86_64::RCX));
      t.check("Win64/CDecl/8 Params/g", CheckReg(cc->getParamRegister(1), x86_64::RDX));
      t.check("Win64/CDecl/8 Params/h", CheckReg(cc->getParamRegister(2), x86_64::R8));
      t.check("Win64/CDecl/8 Params/i", CheckReg(cc->getParamRegister(3), x86_64::R9));
      t.check("Win64/CDecl/8 Params/j", CheckSSI(cc->getParamSpillSlotInfo(4), 32, 2));
      t.check("Win64/CDecl/8 Params/k", CheckSSI(cc->getParamSpillSlotInfo(7), 56, 3));
    }
  }
  {
    RefCounter<IMachineInfo> mi = GetX86_64PosixMachineInfo();
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_c>();
      t.check("Posix64/CDecl/0 Params/a", cc->getResultCount() == 0);
      t.check("Posix64/CDecl/0 Params/b", cc->getParamCount() == 0);
      t.check("Posix64/CDecl/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Posix64/CDecl/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_f>();
      t.check("Posix64/FastCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Posix64/FastCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Posix64/FastCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Posix64/FastCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Posix64/FastCall/0 Params/e", cc->getSubType() == FTS_CDecl);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT0_s>();
      t.check("Posix64/StdCall/0 Params/a", cc->getResultCount() == 0);
      t.check("Posix64/StdCall/0 Params/b", cc->getParamCount() == 0);
      t.check("Posix64/StdCall/0 Params/c", cc->getParamAreaSize() == 0);
      t.check("Posix64/StdCall/0 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Posix64/StdCall/0 Params/e", cc->getSubType() == FTS_CDecl);
    }
    {
      std::shared_ptr<const CallingConvention> cc = mi->getCC<FT8_c>();
      t.check("Posix64/CDecl/8 Params/a", cc->getResultCount() == 1);
      t.check("Posix64/CDecl/8 Params/b", cc->getParamCount() == 8);
      t.check("Posix64/CDecl/8 Params/c", cc->getParamAreaSize() == 16);
      t.check("Posix64/CDecl/8 Params/d", cc->getStackCleaningStrategy() == CCS_ByCaller);
      t.check("Posix64/CDecl/8 Params/e", CheckReg(cc->getResultRegister(0), x86_64::RAX));
      t.check("Posix64/CDecl/8 Params/f", CheckReg(cc->getParamRegister(0), x86_64::RDI));
      t.check("Posix64/CDecl/8 Params/g", CheckReg(cc->getParamRegister(1), x86_64::RSI));
      t.check("Posix64/CDecl/8 Params/h", CheckReg(cc->getParamRegister(2), x86_64::RDX));
      t.check("Posix64/CDecl/8 Params/i", CheckReg(cc->getParamRegister(3), x86_64::RCX));
      t.check("Posix64/CDecl/8 Params/j", CheckReg(cc->getParamRegister(4), x86_64::R8));
      t.check("Posix64/CDecl/8 Params/k", CheckReg(cc->getParamRegister(5), x86_64::R9));
      t.check("Posix64/CDecl/8 Params/l", CheckSSI(cc->getParamSpillSlotInfo(6), 0, 2));
      t.check("Posix64/CDecl/8 Params/m", CheckSSI(cc->getParamSpillSlotInfo(7), 8, 3));
    }
  }
}

static UnitTestRun _("CallingConvention", test);
