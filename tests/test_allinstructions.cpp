//===-- tests/test_allinstructions.cpp --------------------------*- C++ -*-===//
// Test all instruction encodings provided by JITCS.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Will report wrong encodings, and instructions which have not been encoded
// at all. For that purpose, we pull in several *.as files from the _bin/{arch}
// directory. These *.as files have been created from *.ins files in 
// tests/{arch}, enriched by the target encodings as used by NASM.
//===----------------------------------------------------------------------===//

#include "jitcs.h"
#include "unittest.h"
#include "jitcs_instructionstream.h"
#include "jitcs_dumper.h"
#include "jitcs_memmgr.h"
#include "jitcs_x86_32.h"
#include "jitcs_x86_64.h"
#include <unordered_set>
#include "jitcs_int_machine.h"
#include "jitcs_int_bitfuncs.h"
#include "test_asm_x86_32.h"
#include "test_asm_x86_64.h"

#include <stdio.h>

using namespace jitcs;

static void test(UnitTest& t) {
  typedef void (__cdecl *FT0_c)();
  RefCounter<TempAllocator> alloc(new TempAllocator);
  RefCounter<MemoryMgr> mgr = MemoryMgr::Create();
  
  #ifdef EVM_WINDOWS
  std::string path = "data\\";
  #else
  std::string path = "data/";
  #endif
  {
    std::vector<std::string> x86as_files;
    x86as_files.push_back("mov_set");
    x86as_files.push_back("arith");
    x86as_files.push_back("other");
    x86as_files.push_back("other_vex");
    x86as_files.push_back("simd_mov");
    x86as_files.push_back("simd_arith");
    x86as_files.push_back("simd_blend");
    x86as_files.push_back("simd_cvt");
    x86as_files.push_back("simd_insext");
    x86as_files.push_back("simd_shuffle");
    x86as_files.push_back("simd_mov_vex");
    x86as_files.push_back("simd_arith_vex");
    x86as_files.push_back("simd_blend_vex");
    x86as_files.push_back("simd_cvt_vex");
    x86as_files.push_back("simd_insext_vex");
    x86as_files.push_back("simd_shuffle_vex");
    x86as_files.push_back("cf");
    x86as_files.push_back("addrmode");

    std::unordered_set<u32> allins;
    std::unordered_set<u32> allins_compressed;
    {
      std::string prefix = path + "x86_32_";
    
      RefCounter<IMachineInfo> mi = GetX86_32WinMachineInfo();
      TestAssemblerX86_32 asm_win32(mgr, alloc, mi);
      allins.clear();
      allins_compressed.clear();

      for (auto fn: x86as_files)
        asm_win32.checkFile(t, prefix + fn + ".as", &allins, 
                            prefix + fn + ".bin");

      for (auto v: allins) {
        u32 vv = v & 0x7fffffff;
        allins_compressed.insert(((vv >> x86_32::ICL_NonMainBits) << x86_32::ICL_SubBits) 
                                + (vv & x86_32::ICL_SubMask));
      }    
      
      size_t n = 0;
      for (u32 i = x86_32::ICL_First; i < x86_32::ICL_Last; ++i) {
        n += popcnt(x86_32::ToValidMask(static_cast<x86_32::InsClassId>(i)));
      }
      char buffer[128];
      sprintf(buffer, "x86_32/completeness: %d ins available, %d ins handled", n, allins_compressed.size());
      t.check(buffer, n == allins_compressed.size());
      if (n > allins_compressed.size()) {
        PrintFDumper dd;
        MachineDumper md(dd, *mi);
        md.write("unhandled:");
        for (u32 i = x86_32::ICL_First; i < x86_32::ICL_Last; ++i) {
          u32 mask = x86_32::ToValidMask(static_cast<x86_32::InsClassId>(i));
          for (size_t i2 = 0; i2 < 32; ++i2) {
            if ((mask & (1 << i2)) == 0) continue;
            u32 a = (i << x86_32::ICL_NonMainBits) + i2;
            u32 b = (i << x86_32::ICL_SubBits) + i2;
            if (allins_compressed.find(b) == allins_compressed.end()) {
              md.write(" ");
              md.write(static_cast<InsId>(a));
            }
          }
        }
        md.write("\n");
      }
    }

    {
      std::string prefix = path + "x86_64_";
    
      RefCounter<IMachineInfo> mi = GetX86_64WinMachineInfo();
      TestAssemblerX86_64 asm_win64(mgr, alloc, mi);
      allins.clear();
      allins_compressed.clear();

      for (auto fn: x86as_files)
        asm_win64.checkFile(t, prefix + fn + ".as", &allins, 
                            prefix + fn + ".bin");

      for (auto v: allins) {
        u32 vv = v & 0x7fffffff;
        allins_compressed.insert
            (((vv >> x86_64::ICL_NonMainBits) << x86_64::ICL_SubBits) 
             + (vv & x86_64::ICL_SubMask));
      }    
      
      size_t n = 0;
      for (u32 i = x86_64::ICL_First; i < x86_64::ICL_Last; ++i) {
        n += popcnt(x86_64::ToValidMask(static_cast<x86_64::InsClassId>(i)));
      }
      char buffer[128];
      sprintf(buffer, "x86_64/completeness: %d ins available, %d ins handled", n, allins_compressed.size());
      t.check(buffer, n == allins_compressed.size());
      if (n > allins_compressed.size()) {
        PrintFDumper dd;
        MachineDumper md(dd, *mi);
        md.write("unhandled:");
        for (u32 i = x86_64::ICL_First; i < x86_64::ICL_Last; ++i) {
          u32 mask = x86_64::ToValidMask(static_cast<x86_64::InsClassId>(i));
          for (size_t i2 = 0; i2 < 32; ++i2) {
            if ((mask & (1 << i2)) == 0) continue;
            u32 a = (i << x86_64::ICL_NonMainBits) + i2;
            u32 b = (i << x86_64::ICL_SubBits) + i2;
            if (allins_compressed.find(b) == allins_compressed.end()) {
              md.write(" ");
              md.write(static_cast<InsId>(a));
            }
          }
        }
        md.write("\n");
      }
    }
  }
}

static UnitTestRun _("AllInstructions", test);
