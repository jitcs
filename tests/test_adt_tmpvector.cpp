//===-- tests/test_adt_tmpvector.cpp ----------------------------*- C++ -*-===//
// Test to check TmpVector class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_adt_tmpvector.h"
#include "jitcs_tmpalloc.h"
#include "unittest.h"

#include <stdio.h>
using namespace jitcs;

static void test(UnitTest& t) {
  TempAllocator alloc;
  TmpVector<size_t,2> tmp(alloc);
  t.check("empty", tmp.size() == 0 && tmp.isLocalData());
  tmp.push_back(0x12345678);
  t.check("push_back/1", tmp.size() == 1 && tmp.isLocalData() && tmp[0] == 0x12345678);
  tmp.push_back(0xdeadbeef);
  t.check("push_back/2", tmp.size() == 2 && tmp.isLocalData() && tmp[1] == 0xdeadbeef);
  tmp.push_back(0x1);
  t.check("push_back/3", tmp.size() == 3 && !tmp.isLocalData() && tmp[1] == 0xdeadbeef && tmp[2] == 1);
}

static UnitTestRun _("ADT/TmpVector", test);
