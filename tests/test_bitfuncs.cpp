//===-- tests/test_bitfuncs.cpp ---------------------------------*- C++ -*-===//
// Test of the trailing-zero/one-count and population count.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_bitfuncs.h"
#include "unittest.h"

using namespace jitcs;

static void test(UnitTest& t) {
  t.check("TrailingZeroCnt/LowBit.32", t0cntnz((u32)1) == 0 && t0cntnz(~(u32)0) == 0);
  t.check("TrailingZeroCnt/LowBit.64", t0cntnz((u64)1) == 0 && t0cntnz(~(u64)0) == 0);
  t.check("TrailingZeroCnt/HighBit.32", t0cntnz(((u32)1) << 31) == 31);
  t.check("TrailingZeroCnt/HighBit.64", t0cntnz(((u64)1) << 63) == 63);
  t.check("TrailingZeroCnt/Shift", t0cntnz((u32)19, 2) == 4);
  t.check("TrailingOneCnt/LowBit.32", t1cntnz(~1) == 0 && t1cntnz(0) == 0);
  t.check("TrailingOneCnt/LowBit.64", t1cntnz(~(u64)1) == 0 && t1cntnz((u64)0) == 0);
  t.check("TrailingOneCnt/HighBit.32", t1cntnz(~(((u32)1) << 31)) == 31);
  t.check("TrailingOneCnt/HighBit.64", t1cntnz(~(((u64)1) << 63)) == 63);
  t.check("TrailingOneCnt/Shift", t0cntnz((u64)19, 2) == 4);
  t.check("PopCnt/Zero", popcnt(0) == 0);
  t.check("PopCnt/AllBits.32", popcnt(~(u32)0) == 32);
  t.check("PopCnt/AllBits.64", popcnt(~(u64)0) == 64);
}

static UnitTestRun _1("ADT/BitFuncs", test);
