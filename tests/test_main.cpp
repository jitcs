//===-- tests/test_main.cpp -------------------------------------*- C++ -*-===//
// Unit test program.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// All tests register themselves when linked in, so the main function only
// needs to start running them.
//===----------------------------------------------------------------------===//

#include "unittest.h"
#include <stdio.h>

using namespace jitcs;

int main() {
  UnitTest t;

  UnitTestRegistry::get()->runTests(t);

  printf("total number of successful tests: %d\n", t.getNumberOfSuccessfulTests());
  printf("total number of failed tests: %d\n", t.getNumberOfFailedTests());
  return t.getNumberOfFailedTests();
}
