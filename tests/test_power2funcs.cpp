//===-- tests/test_power2funcs.cpp ------------------------------*- C++ -*-===//
// Test of certain compile-time/run-time functions relating to powers of 2.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_power2funcs.h"
#include "unittest.h"

using namespace jitcs;

static void test(UnitTest& t) {
  t.check("IsPowerOf2OrZeroCT/0", IsPowerOf2OrZeroCT<0>::Val == true);
  t.check("IsPowerOf2OrZeroCT/3", IsPowerOf2OrZeroCT<3>::Val == false);
  t.check("IsPowerOf2OrZeroCT/4", IsPowerOf2OrZeroCT<4>::Val == true);
  t.check("IsPowerOf2CT/0", IsPowerOf2CT<0>::Val == false);
  t.check("IsPowerOf2CT/3", IsPowerOf2CT<3>::Val == false);
  t.check("IsPowerOf2CT/4", IsPowerOf2CT<4>::Val == true);
  t.check("Log2CT/1", Log2CT<1>::Val == 0);
  t.check("Log2CT/4", Log2CT<4>::Val == 2);
  t.check("Log2CT/5", Log2CT<5>::Val == 2);
  t.check("RoundUpToPowerOf2CT/16,16", RoundUpToPowerOf2CT<16,16>::Val == 16);
  t.check("RoundUpToPowerOf2CT/20,16", RoundUpToPowerOf2CT<20,16>::Val == 32);

  t.check("IsPowerOf2OrZero/0", IsPowerOf2OrZero(0) == true);
  t.check("IsPowerOf2OrZero/3", IsPowerOf2OrZero(3) == false);
  t.check("IsPowerOf2OrZero/4", IsPowerOf2OrZero(4) == true);
  t.check("IsPowerOf2/0", IsPowerOf2(0) == false);
  t.check("IsPowerOf2/3", IsPowerOf2(3) == false);
  t.check("IsPowerOf2/4", IsPowerOf2(4) == true);
  t.check("RoundUpToPowerOf2/16,T16", RoundUpToPowerOf2<16>(16) == 16);
  t.check("RoundUpToPowerOf2/20,T16", RoundUpToPowerOf2<16>(20) == 32);
  t.check("RoundUpToPowerOf2/16,16", RoundUpToPowerOf2(16,16) == 16);
  t.check("RoundUpToPowerOf2/20,16", RoundUpToPowerOf2(20,16) == 32);
  t.check("DivRoundedUpByPowerOf2/16,T16", DivRoundedUpByPowerOf2<16>(16) == 1);
  t.check("DivRoundedUpByPowerOf2/20,T16", DivRoundedUpByPowerOf2<16>(20) == 2);
}

static UnitTestRun _1("ADT/PowerOf2Funcs", test);
