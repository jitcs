//===-- tests/test_adt_bitmap.cpp -------------------------------*- C++ -*-===//
// Test to check the BitSlice class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_adt_bitmap.h"
#include "unittest.h"

#include <stdio.h>

using namespace jitcs;

static void test(UnitTest& t) {
  ConstBitSlice bmp;
  BitSlice bmp2;
  t.check("empty/size", bmp.bitSize() == 0 && bmp2.bitSize() == 0);

  const size_t data[5] = {0x12345678, 0xdeadbeef, 0x1, ~(size_t)0, 0};
  const size_t bitcount = ConstBitSlice::E_BitsPerWord * 4 + 15;
  ConstBitSlice bmp3(data, bitcount);
  t.check("5 words/size&ptr", bmp3.bitSize() == bitcount && bmp3.ptr() == data
              && bmp3.wordSize() == 5);
  t.check("isValidIndex", bmp3.isValidIndex(0) && bmp3.isValidIndex(bitcount - 1)
              && !bmp3.isValidIndex(bitcount));
  t.check("getWordForIndex", bmp3.getWordForIndex(ConstBitSlice::E_BitsPerWord) == 0xdeadbeef);
  t.check("bit test", bmp3.test(ConstBitSlice::E_BitsPerWord * 3 + 12) == true
                              && bmp3.test(ConstBitSlice::E_BitsPerWord * 4 + 12) == false);

  size_t data2[5] = {0, 0, 0, 0, 0};
  BitSlice bmp4 = BitSlice(data2, bitcount);
  bmp4.copyFrom(bmp3);
  t.check("copyFrom", bmp4.equals(bmp3));
  bmp4.mark(bitcount - 1);
  t.check("isSubsetOf", bmp3.isSubsetOf(bmp3)
                           && bmp3.isSubsetOf(bmp4) && !bmp4.isSubsetOf(bmp3));

  bool oldvalue = bmp4.test(0);
  bmp4.mark(0);
  t.check("bit set", bmp4.test(0));
  bmp4.clear(0);
  t.check("bit clear", !bmp4.test(0));
  t.check("testAndMark", !bmp4.testAndMark(0) && bmp4.test(0));
  t.check("testAndClear", bmp4.testAndClear(0) && !bmp4.test(0));
  t.check("testAndComplement", !bmp4.testAndComplement(0) && bmp4.test(0));
  t.check("testAndComplement2", bmp4.testAndComplement(0) && !bmp4.test(0));
  bmp4.clearAll();
  t.check("clearAll", bmp4.countSetBits() == 0);
  bmp4.setAll();
  t.check("setAll", bmp4.countSetBits() == bitcount);

  size_t data3[5] = {0xabba, 0xdeaf, 0xbeeb0b, 0xbabee, 0xcaca0};
  BitSlice bmp5(data3, bitcount);
  bmp4.copyFrom(bmp3);
  bmp4.intersectWith(bmp5);
  t.check("intersectWith", bmp4.getWordForIndex(0) == (0xabba & 0x12345678));
  bmp4.copyFrom(bmp3);
  bmp4.uniteWith(bmp5);
  t.check("uniteWith", bmp4.getWordForIndex(0) == (0xabba | 0x12345678));
  bmp4.copyFrom(bmp3);
  bmp4.clearFrom(bmp5);
  t.check("clearFrom", bmp4.getWordForIndex(0) == (~0xabba & 0x12345678));

  BitSlice bmpit;
  BitSlice::BitEnumerator it0(bmpit.enumBits());
  t.check("BitmapIterator/empty", it0.isEmpty());

  size_t data4[5] = {0, 0, 0, 0, 0};
  BitSlice bmpit2(data4, bitcount);
  BitSlice::BitEnumerator it1(bmpit2.enumBits());
  t.check("BitmapIterator/zero bitmap", it1.isEmpty());

  bmpit2.mark(0);
  bmpit2.mark(31);
  bmpit2.mark(bitcount - 1);
  BitSlice::BitEnumerator it2(bmpit2.enumBits());
  t.check("BitmapIterator/iteration1", !it2.isEmpty() && *it2 == 0);
  ++it2;
  t.check("BitmapIterator/iteration2", !it2.isEmpty() && *it2 == 31);
  ++it2;
  t.check("BitmapIterator/iteration3", !it2.isEmpty() && *it2 == bitcount - 1);
  ++it2;
  t.check("BitmapIterator/iteration4", it2.isEmpty());
  
}

static UnitTestRun _1("ADT/Bitmap", test);
