//===-- tests/test_simplefunction.cpp ---------------------------*- C++ -*-===//
// A fixed-size bit array class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs.h"
#include "unittest.h"
#include "jitcs_instructionstream.h"
#include "jitcs_dumper.h"

#include <stdio.h>

using namespace jitcs;

static void test(UnitTest& t) {
  typedef int (__cdecl *FT0_c)(int, int);
  RefCounter<IMachineInfo> mi = host::GetMachineInfo();
  RefCounter<TempAllocator> alloc(new TempAllocator);
  {
    std::unique_ptr<Function> fn = mi->createFnc<FT0_c>(alloc);
    InstructionStreamBuffer<256> ibuf;

    Ref<const VirtualRegister> param0 = fn->getArgumentRegister(0, host::RCL_GR);
    Ref<const VirtualRegister> param1 = fn->getArgumentRegister(1, host::RCL_GR);
    Ref<const VirtualRegister> result0 = fn->getResultRegister(0, host::RCL_GR);

    Ref<BasicBlock> bb1 = fn->getStartBlock();
    Ref<BasicBlock> bb2 = fn->createBasicBlock();
    Ref<BasicBlock> bb3 = fn->createBasicBlock();

    ibuf.start(bb1);
    host::CMP_RR(ibuf, param0, param1);
    host::JGE_BB_FT(ibuf, bb2, bb3);
    ibuf.end();
    
    ibuf.start(bb2);
    host::MOV_RR(ibuf, result0, param0);
    host::RET(ibuf);
    ibuf.end();
    
    ibuf.start(bb3);
    host::MOV_RR(ibuf, result0, param1);
    host::RET(ibuf);
    ibuf.end();

    {
      Enumerator<const BasicBlock> enumbb = fn->enumerateBasicBlocks();
      size_t c = 0, c1 = 0, c2 = 0, c3 = 0;
      while (!enumbb.empty()) {
        const BasicBlock& bb = enumbb.front();
        ++c;
        if (&bb == bb1._ptr) ++c1;
        if (&bb == bb2._ptr) ++c2;
        if (&bb == bb3._ptr) ++c3;
        enumbb.popFront();
      }
      t.check("Build/Enumerate basic blocks", c1 == 1 && c2 == 1 && c3 == 1 && c == 3);
    }
    {
      Enumerator<const Instruction> enumins = bb1->instructions();
      bool ok = true;
      ok = !enumins.empty();
      if (ok) {
        const Instruction& ins = enumins.front();
        ok = ok && (ins.getInsId() == host::I_CMP_RR);
        enumins.popFront();
      }
      t.check("Build/Enumerate instructions on BB1/1", ok);
      ok = ok && !enumins.empty();
      if (ok) {
        const Instruction& ins = enumins.front();
        ok = ok && (ins.getInsId() == host::I_JGE_BB_FT);
        enumins.popFront();
      }
      t.check("Build/Enumerate instructions on BB1/2", ok);
      ok = ok && enumins.empty();
      t.check("Build/Enumerate instructions on BB1/3", ok);
    }
    {
      Enumerator<const BasicBlock> predbb = bb1->predecessors();
      Enumerator<const BasicBlock> succbb = bb1->successors();
      size_t c = 0, c1 = 0, c2 = 0, c3 = 0;
      while (!succbb.empty()) {
        const BasicBlock& bb = succbb.front();
        ++c;
        if (&bb == bb1._ptr) ++c1;
        if (&bb == bb2._ptr) ++c2;
        if (&bb == bb3._ptr) ++c3;
        succbb.popFront();
      }
      t.check("Build/Pred+succ", 
              predbb.empty() && c == 2 && c2 == 1 && c3 == 1);
    }
    {
      StringDumper dumper;
      MachineDumper mdumper(dumper, *mi);
      fn->dump(mdumper);
      std::string dumpresult = dumper.takeResult();

      std::string expected;
      if (mi->cpu.isX86_32()) expected =
"Function dump: CC{FP1+0(4B)/-, FP1+4(4B)/- -> -/eax}\n"
"  param/result vregs: VR4096_GR32, VR4097_GR32 -> VR4098_GR32\n"
"BB(0): OUT(BB(1),BB(2))\n"
"  cmp_ww VR4097_GR32/r VR4096_GR32/r\n"
"  jge_bb_ft BB1 BB2\n"
"BB(1): IN(BB(0))\n"
"  mov_ww VR4096_GR32/r VR4098_GR32/w\n"
"  ret\n"
"BB(2): IN(BB(0))\n"
"  mov_ww VR4097_GR32/r VR4098_GR32/w\n"
"  ret\n";
      t.check("Build/Dump", dumpresult == expected);
    }
    
    t.check("Generating code not implemented", false);
    t.check("Running function not implemented", false);
  }
}

static UnitTestRun _("SimpleFunction", test);
