//===-- tests/test_dfg_analysis.cpp -----------------------------*- C++ -*-===//
// Test of the dataflow analysis of a not totally simple function.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int.h"
#include "unittest.h"
#include "jitcs_instructionstream.h"
#include "jitcs_dumper.h"
#include "jitcs_int_dfg_analysis.h"
#include "jitcs_int_adt_dynslice.h"

using namespace jitcs;

static void test(UnitTest& t) {
  typedef void (__cdecl *FT0_c)();
  RefCounter<IMachineInfo> mi = host::GetMachineInfo();
  RefCounter<TempAllocator> alloc(new TempAllocator);
  {
    std::unique_ptr<Function> fn = mi->createFnc<FT0_c>(alloc);
    InstructionStreamBuffer<256> ibuf;

    Ref<BasicBlock> bb1 = fn->getStartBlock();
    Ref<BasicBlock> bb2 = fn->createBasicBlock();
    Ref<BasicBlock> bb3 = fn->createBasicBlock();
    Ref<BasicBlock> bb4 = fn->createBasicBlock();
    Ref<BasicBlock> bb5 = fn->createBasicBlock();
    Ref<BasicBlock> bb6 = fn->createBasicBlock();
    Ref<const VirtualRegister> v1 = fn->createRegister(host::RCL_GR);
    Ref<const VirtualRegister> v2 = fn->createRegister(host::RCL_GR);
    Ref<const VirtualRegister> v3 = fn->createRegister(host::RCL_GR);
    Ref<const VirtualRegister> v4 = fn->createRegister(host::RCL_GR);

    //   BB1(entry)
    //    |
    //    v
    //   BB2 <------.
    //    |---.     |
    //    v   v     |
    //   BB3  BB4   |
    //    |---.     |
    //    v         |
    //   BB5        |
    //    |---------.
    //    v
    //   BB6
    //    |
    //    v
    //   BB7 (exit)
    //
    // v1: induction variable, defined in BB1, used in BB5, 
    //     live thru-out the loop
    // v2: defined in BB2, used in BB3, not live in any other BB
    // v3: defined in BB1, used in BB6, live thru-out the loop
    // v3: defined and used in BB4, not live anywhere else
    
    ibuf.start(bb1);
    host::MOV_RI32U(ibuf, v1, 0);
    host::MOV_RI32U(ibuf, v3, 4);
    host::JMP_FT(ibuf, bb2);
    ibuf.end();
    
    ibuf.start(bb2);
    host::MOV_RI32U(ibuf, v2, 1);
    host::CMP_RI(ibuf, v1, 5);
    host::JGE_BB_FT(ibuf, bb4, bb3);
    ibuf.end();
    
    ibuf.start(bb3);
    host::ADD_RR(ibuf, v1, v2);
    host::JMP_BB(ibuf, bb5);
    ibuf.end();

    ibuf.start(bb4);
    host::MOV_RI32U(ibuf, v4, 2);
    host::ADD_RR(ibuf, v1, v4);
    host::JMP_FT(ibuf, bb5);
    ibuf.end();

    ibuf.start(bb5);
    host::ADD_RI(ibuf, v1, 1);
    host::CMP_RI(ibuf, v1, 100);
    host::JB_BB_FT(ibuf, bb2, bb6);
    ibuf.end();

    ibuf.start(bb6);
    host::MOV_RR(ibuf, host::EAX, v3);
    host::RET(ibuf);
    ibuf.end();

    DynSlice<u32,8> touchedFixed(mi->details()->getResClassCount());
    DFGAnalysis a;
    a.init(reinterpret_cast<FunctionImpl&>(*fn), DFGAnalysis::M_Global, 
           touchedFixed);
           
    t.check("v1/bb1/use", !a.isUsed(bb1, v1));
    t.check("v1/bb1/def", a.isDefined(bb1, v1));
    t.check("v1/bb1/live-in", !a.isLiveIn(bb1, v1));
    t.check("v1/bb1/live-out", a.isLiveOut(bb1, v1));

    t.check("v1/bb2/use", a.isUsed(bb2, v1));
    t.check("v1/bb2/def", !a.isDefined(bb2, v1));
    t.check("v1/bb2/live-in", a.isLiveIn(bb2, v1));
    t.check("v1/bb2/live-out", a.isLiveOut(bb2, v1));

    t.check("v1/bb3/use", a.isUsed(bb3, v1));
    t.check("v1/bb3/def", a.isDefined(bb3, v1));
    t.check("v1/bb3/live-in", a.isLiveIn(bb3, v1));
    t.check("v1/bb3/live-out", a.isLiveOut(bb3, v1));

    t.check("v1/bb4/use", a.isUsed(bb4, v1));
    t.check("v1/bb4/def", a.isDefined(bb4, v1));
    t.check("v1/bb4/live-in", a.isLiveIn(bb4, v1));
    t.check("v1/bb4/live-out", a.isLiveOut(bb4, v1));

    t.check("v1/bb5/use", a.isUsed(bb5, v1));
    t.check("v1/bb5/def", a.isDefined(bb5, v1));
    t.check("v1/bb5/live-in", a.isLiveIn(bb5, v1));
    t.check("v1/bb5/live-out", a.isLiveOut(bb5, v1));

    t.check("v1/bb6/use", !a.isUsed(bb6, v1));
    t.check("v1/bb6/def", !a.isDefined(bb6, v1));
    t.check("v1/bb6/live-in", !a.isLiveIn(bb6, v1));
    t.check("v1/bb6/live-out", !a.isLiveOut(bb6, v1));
    
    t.check("v2/bb2/use", !a.isUsed(bb2, v2));
    t.check("v2/bb2/def", a.isDefined(bb2, v2));
    t.check("v2/bb2/live-in", !a.isLiveIn(bb2, v2));
    t.check("v2/bb2/live-out", a.isLiveOut(bb2, v2));

    t.check("v2/bb3/use", a.isUsed(bb3, v2));
    t.check("v2/bb3/def", !a.isDefined(bb3, v2));
    t.check("v2/bb3/live-in", a.isLiveIn(bb3, v2));
    t.check("v2/bb3/live-out", !a.isLiveOut(bb3, v2));
           
    t.check("v2/bb4/use", !a.isUsed(bb4, v2));
    t.check("v2/bb4/def", !a.isDefined(bb4, v2));
    t.check("v2/bb4/live-in", !a.isLiveIn(bb4, v2));
    t.check("v2/bb4/live-out", !a.isLiveOut(bb4, v2));

    t.check("v2/bb5/use", !a.isUsed(bb5, v2));
    t.check("v2/bb5/def", !a.isDefined(bb5, v2));
    t.check("v2/bb5/live-in", !a.isLiveIn(bb5, v2));
    t.check("v2/bb5/live-out", !a.isLiveOut(bb5, v2));

    t.check("v3/bb1/use", !a.isUsed(bb1, v3));
    t.check("v3/bb1/def", a.isDefined(bb1, v3));
    t.check("v3/bb1/live-in", !a.isLiveIn(bb1, v3));
    t.check("v3/bb1/live-out", a.isLiveOut(bb1, v3));

    t.check("v3/bb2/use", !a.isUsed(bb2, v3));
    t.check("v3/bb2/def", !a.isDefined(bb2, v3));
    t.check("v3/bb2/live-in", a.isLiveIn(bb2, v3));
    t.check("v3/bb2/live-out", a.isLiveOut(bb2, v3));

    t.check("v3/bb5/use", !a.isUsed(bb5, v3));
    t.check("v3/bb5/def", !a.isDefined(bb5, v3));
    t.check("v3/bb5/live-in", a.isLiveIn(bb5, v3));
    t.check("v3/bb5/live-out", a.isLiveOut(bb5, v3));

    t.check("v3/bb6/use", a.isUsed(bb6, v3));
    t.check("v3/bb6/def", !a.isDefined(bb6, v3));
    t.check("v3/bb6/live-in", a.isLiveIn(bb6, v3));
    t.check("v3/bb6/live-out", !a.isLiveOut(bb6, v3));

    t.check("v4/bb4/use", !a.isUsed(bb4, v4));
    t.check("v4/bb4/def", a.isDefined(bb4, v4));
    t.check("v4/bb4/live-in", !a.isLiveIn(bb4, v4));
    t.check("v4/bb4/live-out", !a.isLiveOut(bb4, v4));

    u32 d = host::ResClassInfoCT<host::RESCL_GRRSC>::RES_Idx;
    u32 tf_gr = touchedFixed[host::RESCL_GRRSC];
    t.check("fixed/eax", (tf_gr & (1 << (host::RegInfoCT<host::R_EAX>::RES_Id - d))) != 0);
    t.check("fixed/edx", (tf_gr & (1 << (host::RegInfoCT<host::R_EDX>::RES_Id - d))) == 0);
  }
}

static UnitTestRun _("DFG Analysis", test);
