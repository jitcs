//===-- tests/test_unittest.cpp ---------------------------------*- C++ -*-===//
// A tiny unit test framework.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "unittest.h"
#include <stdio.h>

using namespace jitcs;

UnitTest::UnitTest() 
  : _numberOfSuccessfulTests(0)
  , _numberOfFailedTests(0)
  , _nameOfCurrentTest() {
}

void UnitTest::check(const char* subtestName, bool success) {
  if (success) {
    _numberOfSuccessfulTests++;
    return;
  }
  _numberOfFailedTests++;
  if (_nameOfCurrentTest.size() == 0)
    printf("FAILED unnamed test/%s\n", subtestName);
  else
    printf("FAILED: test %s/%s\n", _nameOfCurrentTest.c_str(), subtestName);
}
void UnitTest::run(std::string const &testName, void (*testFnc)(UnitTest &)) {
printf("test started: %s\n", testName.c_str());
  _nameOfCurrentTest = testName;
  try {
    testFnc(*this);  
  } catch (const char* msg) {
    _numberOfFailedTests++;
    printf("FAILED: test %s crashed: %s\n", _nameOfCurrentTest.c_str(), msg);
  } catch (...) {
    _numberOfFailedTests++;
    printf("FAILED: test %s crashed\n", _nameOfCurrentTest.c_str());
  }
printf("test ended: %s\n", testName.c_str());
}
// --------------------------------
UnitTestRegistry* UnitTestRegistry::get() {
  static UnitTestRegistry* instance = nullptr;
  // TODO: this isn't thread-safe. do we care?
  if (instance == nullptr) {
    instance = new UnitTestRegistry;
  }
  return instance;
}

UnitTestRegistry::UnitTestRegistry() {
}
    
void UnitTestRegistry::registerTest(std::string const &testName, void (*testFnc)(UnitTest &)) {
  // TODO: this isn't thread-safe. do we care?
  _tests.push_back(std::make_pair(testName, testFnc));
}
void UnitTestRegistry::runTests(UnitTest & t) {
printf("tests start\n");
  for (listType::iterator i = _tests.begin(), e = _tests.end(); i != e; ++i) {
    try {
      t.run(i->first, i->second);
    } catch (...) {
    }
  }
printf("tests finished\n");
}
UnitTestRun::UnitTestRun(std::string const &testName, void (*testFnc)(UnitTest &)) {
  UnitTestRegistry::get()->registerTest(testName, testFnc);
}
