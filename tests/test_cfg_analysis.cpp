//===-- tests/test_cfg_analysis.cpp -----------------------------*- C++ -*-===//
// Test of the loop depth detection.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs.h"
#include "unittest.h"
#include "jitcs_instructionstream.h"
#include "jitcs_dumper.h"
#include "jitcs_int_cfg_analysis.h"

#include <stdio.h>

using namespace jitcs;

static void test(UnitTest& t) {
  typedef void (__cdecl *FT0_c)();
  RefCounter<IMachineInfo> mi = host::GetMachineInfo();
  RefCounter<TempAllocator> alloc(new TempAllocator);
  {
    std::unique_ptr<Function> fn = mi->createFnc<FT0_c>(alloc);
    InstructionStreamBuffer<256> ibuf;

    Ref<BasicBlock> bb1 = fn->getStartBlock();
    Ref<BasicBlock> bb2 = fn->createBasicBlock();
    Ref<BasicBlock> bb3 = fn->createBasicBlock();
    Ref<BasicBlock> bb4 = fn->createBasicBlock();
    Ref<BasicBlock> bb5 = fn->createBasicBlock();
    Ref<BasicBlock> bb6 = fn->createBasicBlock();

    ibuf.start(bb1);
    host::JGE_BB_FT(ibuf, bb2, bb3);
    ibuf.end();
    
    ibuf.start(bb2);
    host::RET(ibuf);
    ibuf.end();
    
    ibuf.start(bb3);
    host::JGE_BB_FT(ibuf, bb4, bb5);
    ibuf.end();

    ibuf.start(bb4);
    host::JMP_BB(ibuf, bb6);
    ibuf.end();

    ibuf.start(bb5);
    host::JMP_BB(ibuf, bb6);
    ibuf.end();

    ibuf.start(bb6);
    host::JGE_BB_FT(ibuf, bb3, bb2);
    ibuf.end();

    CFGAnalysis a;
    a.init(reinterpret_cast<FunctionImpl&>(*fn), CFGAnalysis::DEPTH_ANALYSIS);
    t.check("V1/blocks", a.getBlockCount() == 6);
    t.check("V1/order integrity", a.checkIntegrity());
    t.check("V1/total depth", a.getMaxBBlockDepth() == 1);
    t.check("V1/individual depth 1", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb1._ptr)) == 0);
    t.check("V1/individual depth 2", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb2._ptr)) == 0);
    t.check("V1/individual depth 3", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb3._ptr)) == 1);
    t.check("V1/individual depth 4", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb4._ptr)) == 1);
    t.check("V1/individual depth 5", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb5._ptr)) == 1);
    t.check("V1/individual depth 6", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb6._ptr)) == 1);
  }
  {
    std::unique_ptr<Function> fn = mi->createFnc<FT0_c>(alloc);
    InstructionStreamBuffer<256> ibuf;

    Ref<BasicBlock> bb1 = fn->getStartBlock();
    Ref<BasicBlock> bb2 = fn->createBasicBlock();
    Ref<BasicBlock> bb3 = fn->createBasicBlock();
    Ref<BasicBlock> bb4 = fn->createBasicBlock();
    Ref<BasicBlock> bb5 = fn->createBasicBlock();
    Ref<BasicBlock> bb6 = fn->createBasicBlock();

    ibuf.start(bb1);
    host::JGE_BB_FT(ibuf, bb2, bb3);
    ibuf.end();
    
    ibuf.start(bb2);
    host::JMP_BB(ibuf, bb3);
    ibuf.end();
    
    ibuf.start(bb3);
    host::JGE_BB_FT(ibuf, bb4, bb6);
    ibuf.end();

    ibuf.start(bb4);
    host::JMP_BB(ibuf, bb5);
    ibuf.end();

    ibuf.start(bb5);
    host::JL_BB_FT(ibuf, bb2, bb6);
    ibuf.end();

    ibuf.start(bb6);
    host::RET(ibuf);
    ibuf.end();

    CFGAnalysis a;
    a.init(reinterpret_cast<FunctionImpl&>(*fn), CFGAnalysis::DEPTH_ANALYSIS);
    t.check("V2/blocks", a.getBlockCount() == 6);
    t.check("V2/order integrity", a.checkIntegrity());
    t.check("V2/total depth", a.getMaxBBlockDepth() == 1);
    t.check("V2/individual depth 1", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb1._ptr)) == 0);
    t.check("V2/individual depth 2", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb2._ptr)) == 1);
    t.check("V2/individual depth 3", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb3._ptr)) == 1);
    t.check("V2/individual depth 4", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb4._ptr)) == 1);
    t.check("V2/individual depth 5", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb5._ptr)) == 1);
    t.check("V2/individual depth 6", a.getDepth(reinterpret_cast<BasicBlockImpl*>(bb6._ptr)) == 0);
  }
}

static UnitTestRun _("CFG Analysis", test);
