//===-- tests/cpuinfo.cpp ---------------------------------------*- C++ -*-===//
// Simple program to detect the host's feature set.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// It is not really much of a test.
//===----------------------------------------------------------------------===//

#include "jitcs_cpu.h"
#include <stdio.h>

int main() {
  jitcs::CPUInfo h(jitcs::CPUInfo::A_Host);
  
  printf("%s %s", h.isX86() ? "Intel" : "Unknown",
                  h.isX86_32() 
                  ? "X86 32bit" 
                  : (h.isX86_64() ? "X86 64bit" : "???"));
  printf("(%d byte cacheline, %d phys core(s), %d log core(s))",
         h.getCachelineSize(), h.getPhysCoreCount(), h.getLogCoreCount());
  printf("(preferred alignment: %d bytes)", h.getPrefAlign());
  if (h.isX86()) {
    if (h.hasX86CMOV()) printf(" cmov");
    if (h.hasX86SSE()) printf(" sse");
    if (h.hasX86SSE2()) printf(" sse2");
    if (h.hasX86SSE3()) printf(" sse3");
    if (h.hasX86SSSE3()) printf(" ssse3");
    if (h.hasX86SSE41()) printf(" sse41");
    if (h.hasX86SSE42()) printf(" sse42");
    if (h.hasX86AVX()) printf(" avx");
    if (h.hasX86AVX2()) printf(" avx2");
    if (h.hasX86LSAHF()) printf(" lsahf");
    if (h.hasX86FMA3()) printf(" fma3");
    if (h.hasX86F16C()) printf(" f16c");
    if (h.hasX86AES()) printf(" aes");
    if (h.hasX86PCLMULQDQ()) printf(" pclmulqdq");
    if (h.hasX86POPCNT()) printf(" popcnt");
    if (h.hasX86LZCNT()) printf(" lzcnt");
    if (h.hasX86BMI1()) printf(" bmi1");
    if (h.hasX86BMI2()) printf(" bmi2");
    if (h.hasX86TSX()) printf(" tsx");
    if (h.hasX86RDTSC()) printf(" rdtsc");
    if (h.hasX86RDTSCP()) printf(" rdtscp");
    if (h.hasX86CLFLUSH()) printf(" clflush");
    if (h.hasX86CMPXCHG8B()) printf(" cmpxchg8b");
    if (h.hasX86CMPXCHG16B()) printf(" cmpxchg16b");
    if (h.hasX86RDRAND()) printf(" rdrand");
  }
  puts("");
  return 0;
}
