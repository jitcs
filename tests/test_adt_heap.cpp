//===-- tests/test_adt_heap.cpp ---------------------------------*- C++ -*-===//
// Test to check the TopHeap class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_adt_heap.h"
#include "unittest.h"

#include <stdio.h>

using namespace jitcs;

struct HeapTest {
  HeapTest() = default;
  HeapTest(const HeapTest&) = default;
  HeapTest(size_t id_, size_t v) : id(id_), value(v), heappos(0) {};

  size_t id;
  size_t heappos;
  size_t value;
};
void dump(TopHeap<HeapTest>& th) {
  printf("TopHeap:");
  for (size_t i = 0; i < th.data.size(); ++i) {
    printf(" %d:%d(%d,%d)", 
           i, th.data[i].id, th.data[i].value, th.data[i].heappos);
  }
  printf("\n");
}

bool operator ==(const HeapTest& h1, const HeapTest& h2) {
  return h1.id == h2.id;
}
bool operator !=(const HeapTest& h1, const HeapTest& h2) {
  return h1.id != h2.id;
}

static void test(UnitTest& t) {
  TopHeap<HeapTest> h1;
  t.check("Heap/empty", h1.isEmpty());
  for (size_t i = 0; i < 10; ++i) {
    h1.push(HeapTest(i * 2 + 0, i * 2 + 0));
    t.check("Heap/push", h1.check());
  }
  for (size_t i = 0; i < 10; ++i) {
    h1.push(HeapTest(i * 2 + 1, i * 2 + 1));
    t.check("Heap/push", h1.check());
  }
  t.check("Heap/top", h1.peekTopValue() == 19 && h1.peekTop().id == 19);
  h1.drop(h1.data[3]);
  t.check("Heap/drop", h1.check() 
                       && h1.peekTopValue() == 19 && h1.peekTop().id == 19);
  h1.dropTop();
  t.check("Heap/dropTop", h1.check() 
                       && h1.peekTopValue() == 18 && h1.peekTop().id == 18);
  while (!h1.isEmpty()) {
    h1.dropTop();
    t.check("Heap/drop", h1.check());
  }
}

static UnitTestRun _1("ADT/Heap", test);
