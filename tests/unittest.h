//===-- tests/unittest.h ----------------------------------------*- C++ -*-===//
// A tiny unit test framework.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_TESTS_UNITTEST_H_
#define _JITCS_TESTS_UNITTEST_H_

#include "jitcs_base.h"
#include <string>
#include <list>

namespace jitcs {

class UnitTest {
public:
  UnitTest();

  void run(std::string const &testName, void (*testFnc)(UnitTest &));
  void check(const char* subtestName, bool success);
    
  size_t getNumberOfSuccessfulTests() const { return _numberOfSuccessfulTests; }
  size_t getNumberOfFailedTests() const { return _numberOfFailedTests; }
    
private:
  size_t _numberOfSuccessfulTests;
  size_t _numberOfFailedTests;
  std::string _nameOfCurrentTest;
};
class UnitTestRegistry {
public:
  static UnitTestRegistry* get();
    
  void registerTest(std::string const &testName, void (*testFnc)(UnitTest &));
  void runTests(UnitTest &);
    
private:
  UnitTestRegistry();
    
private:
  typedef std::pair< std::string, void (*)(UnitTest &) > entryType;
  typedef std::list< entryType > listType;
  listType _tests;
};
  
struct UnitTestRun {
  UnitTestRun(std::string const &testName, void (*testFnc)(UnitTest &));
};
} // end of namespace jitcs

#endif
// _JITCS_TESTS_UNITTEST_H_
