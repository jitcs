//===-- tests/test_simplecodegen.cpp ----------------------------*- C++ -*-===//
// A fixed-size bit array class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs.h"
#include "unittest.h"
#include "jitcs_instructionstream.h"
#include "jitcs_dumper.h"
#include "jitcs_memmgr.h"
#include "jitcs_x86_32.h"

using namespace jitcs;

void Dump(IDumper& d, Slice<u8> mem) {
  size_t n = mem.size();
  while (n > 0 && mem[n - 1] == 0) --n;
  for (size_t i = 0; i < n; ++i) {
    d.writef("%02x", mem[i]);
  }
  if (n < mem.size())
    d.writef("%s00 * %d", n > 0 ? " + " : "", mem.size() - n);
}
void Dump(IDumper& d, MemoryMgr::CodeAndData code) {
  d.writef("Data %db: ", code.data.size());
  Dump(d, code.data);
  d.write("\n");
  d.writef("Code %db: ", code.code.size());
  Dump(d, code.code);
  d.write("\n");
}

static void test(UnitTest& t) {
  typedef int (__cdecl *FT0_c)(int, int);
  RefCounter<IMachineInfo> mi = GetX86_32WinMachineInfo();
  RefCounter<TempAllocator> alloc(new TempAllocator);
  RefCounter<MemoryMgr> mgr = MemoryMgr::Create();
  {
    std::unique_ptr<Function> fn = mi->createFnc<FT0_c>(alloc);
    InstructionStreamBuffer<256> ibuf;

    Ref<BasicBlock> bb1 = fn->getStartBlock();

    ibuf.start(bb1);
    x86_32::MOV_WI(ibuf, x86_32::EAX, 0);
    x86_32::ADD_WI(ibuf, x86_32::EAX, 1);
    x86_32::RET(ibuf);
    ibuf.end();

    MemoryMgr::CodeAndData code = fn->generate(mgr, Function::Direct_Code_Gen);

    StringDumper dumper;
    Dump(dumper, code);
    std::string dumpresult = dumper.takeResult();

    std::string expected = 
"Data 48b: 00 * 48\n"
"Code 64b: b80000000083c001c3 + 00 * 55\n";

    t.check("x86_32/1", dumpresult == expected);
  }
}

static UnitTestRun _("SimpleCodeGen", test);
