//===-- src/jitcs_int_adt_dynslice.h ----------------------------*- C++ -*-===//
// A data container attached to a Slice view.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A DynSlice provides the data array for the Slice. It will use a
// fixed size array, or allocate the space from the heap.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_ADT_DYNSLICE_H_
#define _JITCS_INT_ADT_DYNSLICE_H_

#include "jitcs_base.h"
#include "jitcs_adt_slice.h"

namespace jitcs {
class TempAllocator;

template <typename T, unsigned N>
struct DynSlice : public Slice<T> {
  T _short_data[N];

  DynSlice(size_t n) 
    : Slice<T>(n <= N ? _short_data : new T[n], n)
  {}
  ~DynSlice() {
    if (!isLocalData()) delete[] this->_ptr;
  }
  void reinit(size_t n) {
    if (!isLocalData()) delete[] this->_ptr;
    this->_ptr = n <= N ? _short_data : new T[n];
    this->_size = n;
  }
  bool isLocalData() const { return this->_ptr == _short_data; }
};

} // end of namespace jitcs

#endif
// _JITCS_INT_ADT_DYNSLICE_H_
