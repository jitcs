//===-- src/jitcs_int.h -----------------------------------------*- C++ -*-===//
// More glue for JITCS.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// This adds more target specific types, enums, etc. into the 
// jitcs::host namespace.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_H_
#define _JITCS_INT_H_

#include "jitcs.h"

#ifdef JITCS_X86_32
#include "x86/jitcs_int_x86_32_machine.h"
#include "x86/jitcs_int_x86_32_regs.h"
#endif
#ifdef JITCS_X86_64
#include "x86/jitcs_int_x86_64_machine.h"
#include "x86/jitcs_int_x86_64_regs.h"
#endif

namespace jitcs {
namespace host {
#ifdef JITCS_X86_32
using namespace jitcs::x86_32;
#endif
#ifdef JITCS_X86_64
using namespace jitcs::x86_64;
#endif
} // end of namespace jitcs::host
} // end of namespace jitcs

#endif
// _JITCS_INT_H_
