//===-- src/jitcs_int_dfg_analysis.h ----------------------------*- C++ -*-===//
// Data flow graph analysis.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The DFG has a local and a global phase. The local phase has O(I+B*R), with
// I the number of instructions, B the number of basic blocks and R the number
// of virtual registers. It collects all def-use information from all 
// instructions in all basic blocks. It results in a set of used and defined
// registers for each basic blocks. (B*R is the result of clearing all bits
// at the start of each basic blocks analysis.)
// The global phase has O(B*R*R) worstcase runtime. (R is for the set of bits
// handled for each iteration, and B*R is the upper limit for increasing the
// number of set bits for each basic block.) The phase calculates the
// live-in bitset as live-out minus defined plus uses.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_DFGANALYSIS_H_
#define _JITCS_INT_DFGANALYSIS_H_

#include "jitcs_base.h"
#include "jitcs_ids.h"
#include "jitcs_adt_ref.h"
#include "jitcs_adt_slice.h"
#include "jitcs_int_adt_bitmap.h"
#include "jitcs_int_virtualregister.h"
#include "jitcs_int_function_impl.h"

namespace jitcs {
class IDumper;
class BasicBlockImpl;
class FunctionImpl;
class TempAllocator;

class DFGAnalysis {
public:
  enum AMode {
    M_Local,
    M_Global
  };
  struct ResourceData {
    enum { E_UseDefBits = 2, E_UseDefMask = (1 << E_UseDefBits) - 1,
           E_UseDefShift = 0, E_Use = 1, E_Def = 2, E_UseDef = E_Use | E_Def,
           //E_SizeBits = 3, E_SizeMask = (1 << E_SizeBits) - 1, 
           //E_SizeShift = E_UseDefShift + E_UseDefBits,
           //E_ResClassBits = 3, E_ResClassMask = (1 << E_ResClassBits) - 1, 
           //E_ResClassShift = E_SizeShift + E_SizeBits,
           //E_ResClassShift = E_UseDefShift + E_UseDefBits,
           E_ResShift = E_UseDefShift + E_UseDefBits };
    u32 resInfo;
    u32 allowedMask;
    u32 nextUse;
    u32 hintMask;
    
    ResId getResId() { return static_cast<ResId>(resInfo >> E_ResShift); }
    //ResClassId getResClassId() { 
    //  return static_cast<ResClassId>
    //    ((resInfo >> E_ResClassShift) & E_ResClassMask);
    //}
    //u32 getResLogSize() { return ((resInfo >> E_SizeShift) & E_SizeMask); }
    u32 getNext(BitSlice liveOut) {
      return nextUse == ~1 ? (~0 ^ liveOut.test(getResId())) : nextUse;
    }
    u32 getUseDef() { return ((resInfo >> E_UseDefShift) & E_UseDefMask); }
    bool isUse() { return (resInfo & E_Use) != 0; }
    bool isDef() { return (resInfo & E_Def) != 0; }
    bool isUseOnly() { return (resInfo & E_UseDefMask) == E_Use; }
    bool isDefOnly() { return (resInfo & E_UseDefMask) == E_Def; }
    bool isUseDef() { return (resInfo & E_UseDefMask) == E_UseDef; }
  };
  struct InstructionData {
    ResourceData* ptr;
    u8 resClassEnd[8];

    // for each resource class, the range of relevant resources can be
    // extracted as ptr[RCL == 0 ? 0 : resClassEnd[RCL-1] .. resClassEnd[RCL]]
    // - each range of resources starts with fixed register resources
    // - the non-fixed register resources are ordered by increasing number of
    // bits in the mask field
    // - the bit of fixed resources is removed from non-fixed resources
    // with overlapping life-range (concerning the instruction)
    void check();
  };
  struct NextData {
    u32 next, last;
  };
  struct BlockData {
    BasicBlockImpl* bb;
    Slice<InstructionData> instructions;
    Slice<NextData> useRangeData;
    BitSlice useRangeIsValid;
    BitSlice liveIn;
    BitSlice liveOut;
    BitSlice used;
    BitSlice defined;
  };

public:
  DFGAnalysis();
  void init(FunctionImpl& f, AMode m, Slice<u32> touchedFixed);
  void dump(IDumper& o) const;

  bool isUsed(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const;
  bool isDefined(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const;
  bool isLiveIn(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const;
  bool isLiveOut(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const;
  
  bool isUsed(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const;
  bool isDefined(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const;
  bool isLiveIn(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const;
  bool isLiveOut(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const;

  void extractUseDef(Ref<BasicBlockImpl> bb, size_t ins, 
                     BitSlice used, BitSlice defined) const;

private:
  
private:
  Slice<BlockData> _blocks;
};

struct GatherResourceList {
  enum { NUM = 15 };
  struct Entry {
    u32 resInfo;
    u32 mask;
  };
  
  struct ClassWise {
    Entry touchedResources[NUM];
    u32 numFixed, numNonFixed;
  };
  ClassWise byClass[8];
  u8 classInit;
  // --------------------
  void init() {
    classInit = 0;
  }
  // --------------------
  void addNonFixedDef(Ref<const VirtualRegister> r, u32 mask) {
    _addNonFixed(r->resclass, _combine(r), mask, DFGAnalysis::ResourceData::E_Def);
  }
  void addNonFixedUseDef(Ref<const VirtualRegister> r, u32 mask) {
    _addNonFixed(r->resclass, _combine(r), mask, DFGAnalysis::ResourceData::E_UseDef);
  }
  void addNonFixedUse(Ref<const VirtualRegister> r, u32 mask) {
    _addNonFixed(r->resclass, _combine(r), mask, DFGAnalysis::ResourceData::E_Use);
  }
  // --------------------
  void addFixedDef(Ref<const VirtualRegister> r, u32 mask) {
    _addFixed(r->resclass, _combine(r), mask, DFGAnalysis::ResourceData::E_Def);
  }
  void addFixedUseDef(Ref<const VirtualRegister> r, u32 mask) {
    _addFixed(r->resclass, _combine(r), mask, DFGAnalysis::ResourceData::E_UseDef);
  }
  void addFixedUse(Ref<const VirtualRegister> r, u32 mask) {
    _addFixed(r->resclass, _combine(r), mask, DFGAnalysis::ResourceData::E_Use);
  }
  // --------------------
  void addFixedDef(ResId res, ResClassId rescl, u32 mask) {
    _addFixed(rescl, _combine(res), mask, DFGAnalysis::ResourceData::E_Def);
  }
  void addFixedUseDef(ResId res, ResClassId rescl, u32 mask) {
    _addFixed(rescl, _combine(res), mask, DFGAnalysis::ResourceData::E_UseDef);
  }
  void addFixedUse(ResId res, ResClassId rescl, u32 mask) {
    _addFixed(rescl, _combine(res), mask, DFGAnalysis::ResourceData::E_Use);
  }

private:
  u32 _combine(ResId res) const {
    return (res << DFGAnalysis::ResourceData::E_ResShift);
  }
  u32 _combine(Ref<const VirtualRegister> r) const {
    return _combine(r->res);
  }
  void _addNonFixed(ResClassId, u32 id, u32 mask, u32 usedef);
  void _addFixed(ResClassId, u32 id, u32 mask, u32 usedef);
};

class SetupBlockResourceInfo {
public:
  SetupBlockResourceInfo(FunctionImpl& f);
  SetupBlockResourceInfo() = delete;
  SetupBlockResourceInfo(const SetupBlockResourceInfo&) = delete;
  SetupBlockResourceInfo& operator =(const SetupBlockResourceInfo&) = delete;

  void init(Slice<DFGAnalysis::InstructionData> ins,
            BitSlice def, BitSlice use,
            BitSlice nextValid, Slice<DFGAnalysis::NextData> next,
            Slice<u32> touchedFixed);
  void push_front(GatherResourceList&);

  RefOrNull<const VirtualRegister> getFrameRegister(size_t fr) {
    return _f.getFrameRegisterInfo(fr).reg;
  }

private:
  Slice<DFGAnalysis::InstructionData> _ins;
  BitSlice _def, _use, _nextValid;
  Slice<u32> _touchedFixed;
  Slice<DFGAnalysis::NextData> _next;
  size_t _idx;
  FunctionImpl& _f;
  TempAllocator &_alloc;
  Slice<DFGAnalysis::ResourceData> _remaining;
  u32 _rclCnt;
};

} // end of namespace jitcs

#endif
// _JITCS_INT_DFGANALYSIS_H_
