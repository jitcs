//===-- src/jitcs_int_bitfuncs.h --------------------------------*- C++ -*-===//
// Trailing-Zero/One-Count and population count.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Trailing-Zero-Count is used to find bit positions in bitmasks, and
// population count is probably used somewhere too.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_ADT_BITFUNCS_H_
#define _JITCS_INT_ADT_BITFUNCS_H_

#include "jitcs_base.h"

#ifdef _MSC_VER
#include <intrin.h>
#pragma intrinsic(_BitScanForward)
#ifdef JITCS_64
#pragma intrinsic(_BitScanForward64)
#endif
#endif
#if __GNUC__
#include "x86intrin.h"
#endif

namespace jitcs {

inline uint t0cnt32nz_(u32 v, uint o) {
  assert(v != 0);
#if __GNUC__
  int index = __bsfd(v);
#endif
#ifdef _MSC_VER
  unsigned long index;
  u8 ok = _BitScanForward(&index, v);
  assert(ok);
#endif
  return index + o;
}
#ifdef JITCS_64
inline uint t0cnt64nz_(u64 v, uint o) {
  assert(v != 0);
#if __GNUC__
  int index = __bsfq(v);
#endif
#ifdef _MSC_VER
  unsigned long index;
  u8 ok = _BitScanForward64(&index, v);
  assert(ok);
#endif
  return index + o;
}
#else
inline uint t0cnt64nz_(u64 v, uint o) {
  assert(v != 0);
  return t0cnt32nz_(((u32)v) == 0 ? (u32)(v >> 32) : (u32)v, 
                    o + (((u32)v) == 0 ? 32 : 0));
}
#endif

inline uint t0cnt32nz(u32 v, uint o = 0) { return t0cnt32nz_(v >> o, o); }
inline uint t0cnt64nz(u64 v, uint o = 0) { return t0cnt64nz_(v >> o, o); }

inline uint t1cnt32nz(u32 v, uint o = 0) { return t0cnt32nz(~v, o); }
inline uint t1cnt64nz(u64 v, uint o = 0) { return t0cnt64nz(~v, o); }

// --------------------------

inline uint popcnt32(u32 v) {
  enum {
    M01 = (~(u32)0) / 255,
    M55 = 0x55 * M01,
    M33 = 0x33 * M01,
    M0F = 0x0f * M01,
  };
  v = (v & M55) + ((v >> 1) & M55);
  v = (v & M33) + ((v >> 2) & M33);
  v = (v & M0F) + ((v >> 4) & M0F);
  return (v * M01) >> 24;
}
#ifdef JITCS_64
inline uint popcnt64(u64 v) {
  enum {
    M01 = (~(u64)0) / 255,
    M55 = 0x55 * M01,
    M33 = 0x33 * M01,
    M0F = 0x0f * M01,
  };
  v = (v & M55) + ((v >> 1) & M55);
  v = (v & M33) + ((v >> 2) & M33);
  v = (v & M0F) + ((v >> 4) & M0F);
  return (v * M01) >> 24;
}
#else
inline uint popcnt64(u64 v) { 
  return popcnt32((u32)v) + popcnt32((u32)(v >> 32));
}
#endif

// --------------------------

inline uint t0cntnz(i32 v, uint o = 0) { return t0cnt32nz((u32)v, o); }
inline uint t0cntnz(u32 v, uint o = 0) { return t0cnt32nz(v, o); }
inline uint t0cntnz(u64 v, uint o = 0) { return t0cnt64nz(v, o); }

inline uint t1cntnz(i32 v, uint o = 0) { return t1cnt32nz((u32)v, o); }
inline uint t1cntnz(u32 v, uint o = 0) { return t1cnt32nz(v, o); }
inline uint t1cntnz(u64 v, uint o = 0) { return t1cnt64nz(v, o); }

inline uint popcnt(i32 v) { return popcnt32((u32)v); }
inline uint popcnt(u32 v) { return popcnt32(v); }
inline uint popcnt(u64 v) { return popcnt64(v); }

} // end of namespace evm
#endif
// _JITCS_INT_ADT_BITFUNCS_H_
