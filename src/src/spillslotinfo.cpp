//===-- src/src/spillslotinfo.cpp -------------------------------*- C++ -*-===//
// Description of the spill slot position for virtual registers.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// SpillSlotInfo can be used for all virtual registers, whose spill slot
// is at a memory location relative to one of the frame pointer registers,
// e.g. the stack pointer. For emulators it will be possible, to define 
// virtual registers being spilled to and reloaded from the virtual machine 
// state.
//===----------------------------------------------------------------------===//

#include "jitcs_spillslotinfo.h"
#include "jitcs_dumper.h"

void jitcs::SpillSlotInfo::dump(MachineDumper& o) const {
  if (!isPlaced()) {
    o.write("-");
    return;
  }
  if (isSPRel())
    o.write("SP");
  else
    o.writef("FP%d", getFP());
  int ofs = getOffset();
  if (ofs >= 0)
    o.writef("+%d",ofs);
  else
    o.writef("-%d",-ofs);
  o.writef("(%dB)", getSize());
}
