//===-- src/src/jitcs_int_codegenerator.cpp ---------------------*- C++ -*-===//
// Code generation helper.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_codegenerator.h"
#include "jitcs_adt_range.h"
#include "jitcs_int_cfg_analysis.h"
#include "jitcs_int_function_impl.h"
#include "jitcs_int_machine.h"
#include "jitcs_int_virtualregister.h"
#include "jitcs_int_adt_dynslice.h"

jitcs::CodeGenerator::CodeGenerator()
{}

void jitcs::CodeGenerator::_setEstimatedPosAndClearChain(Ref<const BasicBlockImpl> bb, size_t x) { 
  BBlockInfo& bbi = _bblocks[bb->id()];
  bbi.relocChain = nullptr;
  bbi.posAndFlags = (x << BBLOC_SHIFT) | BBLOC_ESTIMATED; 
}
void jitcs::CodeGenerator::_setFinalPos(Ref<const BasicBlockImpl> bb, size_t x, Ref<u8> y) { 
  BBlockInfo& bbi = _bblocks[bb->id()];
  bbi.posAndFlags = (x << BBLOC_SHIFT) | BBLOC_FINAL; 
  
  // fix relocations
  RefOrNull<RelocInfo> chain = bbi.relocChain;
  while (!chain.isNull()) {
    Ref<RelocInfo> data = chain.removeNullType();
    switch (data->type) {
    case REL_PCREL8_TAIL:
      *(data->tgt._ptr - 1) = (i16)(y._ptr - data->tgt._ptr);
      break;
    case REL_PCREL16_TAIL:
      { i16* x16= (i16*)(data->tgt._ptr - 2); *x16= (i16)(y._ptr - data->tgt._ptr); }
      break;
    case REL_PCREL32_TAIL:
      { i32* x32= (i32*)(data->tgt._ptr - 4); *x32= (i32)(y._ptr - data->tgt._ptr); }
      break;
    default:
      assert(0);
      break;
    }
    chain = data->next;
  }
}
void jitcs::CodeGenerator::addRelocation(Ref<const BasicBlockImpl> bb, Ref<u8> x, RelocType rel) {
  BBlockInfo& bbi = _bblocks[bb->id()];
  Ref<RelocInfo> ri = _relocalloc->alloc();
  ri->next = bbi.relocChain;
  ri->tgt = x._ptr;
  ri->type = rel;
  bbi.relocChain = ri._ptr;
}

jitcs::MemoryMgr::CodeAndData jitcs::CodeGenerator::generate
    (FunctionImpl& f, CFGAnalysis const& ana, MemoryMgr& vm, Slice<u8> data) {
  // first, estimate the size of each basic block and get a roundabout position for them
  Ref<const IMachineDetails> mid = f.getMachineDetails();

  for (size_t i = 0; i < MMODE_FPCount; ++i) {
    FunctionImpl::FrameRegisterInfo fri = f.getFrameRegisterInfo(i);
    _frameRegisterInfo[i].reg = fri.reg.isNull() ? R_None : fri.reg.removeNullType()->id;
    _frameRegisterInfo[i].ofs = fri.ofs;
  }
  
  size_t bbn = ana.getBlockCount();
  DynSlice<BBlockInfo, 16> bblocks(bbn);
  StreamAllocator<RelocInfo> relocalloc(f.getAllocator(), 32);
  _bblocks = bblocks;
  _relocalloc = &relocalloc;

  size_t bbpos = 0;
  for (size_t i = 0; i < bbn; ++i) {
    Ref<const BasicBlockImpl> bb = ana.cgetOrderedBlock(i);
    _setEstimatedPosAndClearChain(bb, bbpos);
    bbpos += mid->estimateCodeSize(bb->cinsns(), bb, *this);
  }
  MemoryMgr::CodeAndData mem = vm.allocate(data.size(), bbpos);
  std::copy(data.begin(), data.end(), mem.data.begin());

  _codespace = mem.code;
  Slice<u8> remainingCodeSpace = mem.code;
  for (size_t i = 0; i < bbn; ++i) {
    Ref<const BasicBlockImpl> bb = ana.cgetOrderedBlock(i);
    _setFinalPos(bb, remainingCodeSpace._ptr - _codespace._ptr, remainingCodeSpace._ptr);
    remainingCodeSpace = mid->generateCode(remainingCodeSpace, bb->cinsns(), bb, *this);
  }
  return vm.shortenAllocation(mem, data.size(), remainingCodeSpace._ptr - _codespace._ptr);
}
