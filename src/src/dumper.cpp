//===-- src/src/dumper.cpp --------------------------------------*- C++ -*-===//
// Helper for printing information for debug purposes.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A lot of classes of JITCS accept an IDumper interface for writing
// information, which e.g. PrintFDumper implements. MachineDumper dumper
// provides target specific dumping support. Using it will pull in some big 
// data tables into the application, so it should be avoided except for
// debug purposes.
//===----------------------------------------------------------------------===//

#include "jitcs_adt_ref.h"
#include "jitcs_base.h"
#include "jitcs_dumper.h"
#include "jitcs_machine.h"
#include "x86/jitcs_int_x86_32_dumper.h"
#include "x86/jitcs_int_x86_64_dumper.h"
#include "jitcs_int_virtualregister.h"
#include "jitcs_instruction.h"
#include "jitcs_memref.h"
#include <stdarg.h>
#include <stdio.h>

void jitcs::IDumper::write(const char* txt) {
  writef("%s", txt);
}
void jitcs::IDumper::writef(const char* fmt, ...) {
  va_list argptr;
  va_start(argptr, fmt);
  writev(fmt, argptr);
}
void jitcs::PrintFDumper::writev(const char* fmt, va_list va) {
  vprintf(fmt, va);
}
void jitcs::StringDumper::writev(const char* fmt, va_list va) {
  char buffer[1024];
  buffer[1023] = 0;
  vsnprintf(buffer, 1023, fmt, va);
  _accumulator.append(buffer);
}
std::string jitcs::StringDumper::takeResult() {
  return std::move(_accumulator);
}


jitcs::MachineDumper::MachineDumper(IDumper &o, const IMachineInfo& mi)
  : _out(o) {
  if (mi.cpu.isX86_32())
    _md.reset(new x86_32::X86_32MachineDumper());
  else if (mi.cpu.isX86_64())
    _md.reset(new x86_64::X86_64MachineDumper());
  else
    assert(false);
}
void jitcs::MachineDumper::writev(const char* fmt, va_list va) {
  _out.writev(fmt, va);
}
void jitcs::MachineDumper::write(Ref<const Instruction> ri) {
  _md->write(_out, ri);
}
void jitcs::MachineDumper::write(InsId id) {
  _md->write(_out, id);
}
void jitcs::MachineDumper::write(Ref<const VirtualRegister> rv) {
  _md->write(_out, rv);
}

void jitcs::IMachineDumper::write(IDumper& d, InsId id) const {
  // right now, we didn't define any instruction which are not hardware
  d.writef("UNK InsId 0x%04x", id);
}
void jitcs::IMachineDumper::write(IDumper& d, Ref<const Instruction> ins) const {
  // right now, we didn't define any instruction which are not hardware
  d.writef("UNK InsId 0x%04x", ins->getInsId());
}
void jitcs::IMachineDumper::write(IDumper& d, Ref<const VirtualRegister> ins) const {
  // we should actually never end up here
  if (ins->id >= R_HardwareLimit) {
    d.writef("VR%04d", ins->id - R_HardwareLimit);
  } else {
    d.writef("VR_HW%02x", ins->id);
  }
}
void jitcs::IMachineDumper::write(IDumper& d, Ref<const MemoryReference> mp, bool vsib) const {
  switch (mp->getMode()) {
  case jitcs::MMODE_Base: // base + scale * index + offset
    d.write("[");
    {
      RefOrNull<const VirtualRegister> base = mp->getConstBaseReg();
      if (base.isNull())
        d.write("NOBASE!");
      else
        write(d, base.removeNullType());
    }
    {
      RefOrNull<const VirtualRegister> index = mp->getConstIndexReg();
      if (!index.isNull()) {
        d.write(" + ");
        write(d, index.removeNullType());
        if (mp->getScale() > 1) d.writef(" * %d", mp->getScale()); 
        if (vsib) d.write("/VSIB");
      }
    }
    break;
  case jitcs::MMODE_FP1Rel:
  case jitcs::MMODE_FP2Rel:
  case jitcs::MMODE_FP3Rel:
  case jitcs::MMODE_FP4Rel:
  case jitcs::MMODE_FP5Rel:
  case jitcs::MMODE_FP6Rel:
  case jitcs::MMODE_FP7Rel:
    d.writef("[FP%d", mp->getMode()&7);
    break;
  case jitcs::MMODE_SPRel:
    d.write("[SP");
    break;
  case jitcs::MMODE_Global:
    d.writef("[GLOBAL(%p)", mp->getGlobal());
    break;
  //case jitcs::MMODE_Sym:
  //  o.writef("[SYM(%d)", mp->getSymbol().id);
  //  break;
  case jitcs::MMODE_VRegSpillSlot:
    d.write("[&");
    {
      RefOrNull<const VirtualRegister> base = mp->getConstBaseReg();
      if (base.isNull())
        d.write("NOREG!");
      else
        write(d, base.removeNullType());
    }
    break;
  default:
    d.write("[ILLEGAL]");
    return;
  }
  d.writef(" %s 0x%x]", mp->getOffset() < 0 ? "-" : "+", abs(mp->getOffset()));
}
