//===-- src/src/adt_bitmap.h ------------------------------------*- C++ -*-===//
// BitSlice class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A BitSlice is a view to an array of words, and allows checking and setting
// single bits. A BitSlice does NOT own the data it points to.
// A DynBitSlice provides the data array for the BitSlice. It will use a
// fixed size array, or allocate the space from the heap.
//===----------------------------------------------------------------------===//

#include "jitcs_int_adt_bitmap.h"
#include "jitcs_dumper.h"

void jitcs::DumpBitmap(IDumper& o, const size_t* _data, size_t _nbits) {
  o.writef("%d bits, set:", _nbits);
  for (size_t i = 0, n = _nbits; i * sizeof(size_t) * 8 < n; ++i) {
    if (_data[i] == 0) continue;
    for (size_t j = 0, m = sizeof(size_t) * 8; j < m; ++j) {
      if (_data[i] & (1 << j)) {
        o.writef(" %d", j);
      }
    }
  }
}
