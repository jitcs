//===-- src/src/bblock_impl.cpp ---------------------------------*- C++ -*-===//
// BasicBlock implementation class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// BasicBlockImpl objects are allocated from a TempAllocator, and their
// destructor is never called.
//===----------------------------------------------------------------------===//

#include "jitcs_int_bblock_impl.h"
#include "jitcs_int_function_impl.h"
#include "jitcs_dumper.h"
#include "jitcs_tmpalloc.h"
#include "jitcs_machine.h"
#include "jitcs_int_machine.h"
#include "jitcs_callingconvention.h"
#include <algorithm>
#include <memory.h>

#include <stdio.h>

jitcs::BasicBlockImpl::BasicBlockImpl(FunctionImpl& f, BBId id) 
  : _bbId(id)
  , _fnc(f)
  , _bbFallThruFrom(nullptr)
  , _bbFallThruTo(nullptr)
  , _alloc(f.getAllocator())
  , _hasCFIns(false)
  , _successors(f.getAllocator())
  , _predecessors(f.getAllocator())
  , _listOfIns(f.getAllocator()) {
}
void jitcs::BasicBlockImpl::BuildEdge(Ref<BasicBlockImpl> fromBB, Ref<BasicBlockImpl> toBB, bool isFallthru) {
  if (std::find(toBB->_predecessors.begin(), toBB->_predecessors.end(), fromBB._ptr) 
      == toBB->_predecessors.end())
    toBB->_predecessors.push_back(fromBB._ptr);
  if (std::find(fromBB->_successors.begin(), fromBB->_successors.end(), toBB._ptr) 
      == fromBB->_successors.end())
    fromBB->_successors.push_back(toBB._ptr);
  if (isFallthru) {
    fromBB->_bbFallThruTo = toBB._ptr;
    toBB->_bbFallThruFrom = fromBB._ptr;
  }
}
void jitcs::BasicBlockImpl::SetFallthru(Ref<BasicBlockImpl> fromBB, Ref<BasicBlockImpl> toBB) {
  fromBB->_bbFallThruTo = toBB._ptr;
  toBB->_bbFallThruFrom = fromBB._ptr;
}

void jitcs::BasicBlockImpl::dump(MachineDumper& o) const {
  o.writef("BB(%d):", _bbId);
  if (!_predecessors.isEmpty()) {
    o.write(" IN(");
    bool first = true;
    for (const_bb_range r = pred_crange(); !!r; ++r) {
      if (!first) o.write(",");
      o.writef("BB(%d)", (*r)->id());
      first = false;
    }
    o.write(")");
  }
  if (!_successors.isEmpty()) {
    o.write(" OUT(");
    bool first = true;
    for (const_bb_range r = succ_crange(); !!r; ++r) {
      if (!first) o.write(",");
      o.writef("BB(%d)", (*r)->id());
      first = false;
    }
    o.write(")");
  }
  o.write("\n");
  
  for (const_ins_range r = instr_crange(); !!r; ++r) {
    o.write("  ");
    o.write(Ref<const Instruction>(*r));
    o.write("\n");
  }
}
size_t jitcs::BasicBlockImpl::append_copy(Slice<const iptr> s) {
  Slice<iptr> data = _alloc.allocTypedArray<iptr>(s.size());
  memcpy(data.ptr(), s.ptr(), sizeof(iptr) * s.size());
  return append(data);
}
size_t jitcs::BasicBlockImpl::append(Slice<iptr> s) {
  const size_t INSN = 16;
  Instruction* insblock[INSN];
  size_t inscnt = 0;
  size_t instotal = 0;
  Ref<const IMachineDetails> mid = _fnc.getMachineDetails();
  while (!s.empty()) {
    _JITCS_ALWAYS_CHECK_(!_hasCFIns);
    size_t result = mid->getInsIdInfo(static_cast<InsId>(s[0]));
    _JITCS_ALWAYS_CHECK_((result & 1) == 1);
    // TODO: consider variable length instructions!
    size_t n = result >> 2;
    _JITCS_DBG_CHECK_(n <= s.size());
    _hasCFIns = (result & 2) != 0;
    if (inscnt == INSN) {
      _listOfIns.append(Slice<Instruction*>(insblock, INSN));
      instotal += INSN;
      inscnt = 0;
    }
    insblock[inscnt++] = reinterpret_cast<Instruction*>(s.ptr());
    s.popFront(n);
  }
  if (inscnt > 0) {
    _listOfIns.append(Slice<Instruction*>(insblock, inscnt));
    instotal += inscnt;
  }
  if (_hasCFIns) {
    mid->buildCtrlFlow(_listOfIns[_listOfIns.size() - 1], this);
  }
  return instotal;
}
