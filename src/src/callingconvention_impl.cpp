//===-- src/src/callingconvention_impl.cpp ----------------------*- C++ -*-===//
// Implementation class to handle calling convention object setup.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_callingconvention_impl.h"
#include "jitcs_machine.h"

jitcs::CallingConventionImpl::CallingConventionImpl(const RefCounter<IMachineInfo>& m) 
  : _machine(m)
  , _alignment(sizeof(size_t))
  , _alignmentOffset(0)
  , _stackCleaning(CCS_ByCallee) {
}

void jitcs::CallingConventionImpl::addResult(const VirtualRegister* r, SpillSlotInfo p) {
  IOParamInfo pi = {r, p};
  _results.push_back(pi);
}
void jitcs::CallingConventionImpl::addParam(const VirtualRegister* r, SpillSlotInfo p) {
  IOParamInfo pi = {r, p};
  _params.push_back(pi);
}
void jitcs::CallingConventionImpl::setStackAlignment(size_t align, size_t ofs) {
  assert(align > 0 && (align & (align - 1)) == 0 && ofs >= 0 && ofs < align);
  _alignment = align;
  _alignmentOffset = ofs;
}
void jitcs::CallingConventionImpl::setParamAreaSize(size_t a) {
  _paramArea = a;
}
void jitcs::CallingConventionImpl::setCleanStack(CallerCalleeStrategyId c) {
  _stackCleaning = c;
}
/*void jitcs::CallingConventionImpl::setResource(u8* p, ConstBitmapRef caller, ConstBitmapRef callee) {
  assert(caller._data != 0 && callee._data != 0 && caller._nbits == callee._nbits);
  _resourceData = p;
  _callerSave = caller;
  _calleeSave = callee;
  _callerSaveList.clear();
  _calleeSaveList.clear();
  _callerSaveList.resize(evm::platform::GetResClassCount());
  _calleeSaveList.resize(evm::platform::GetResClassCount());
  for (size_t i = 0, n = caller._nbits; i < n; ++i) {
    bool caller = _callerSave.test(i), callee = _calleeSave.test(i);
    assert(!caller || !callee);
    if (caller || callee) {
      ResClassId rescl = evm::platform::GetResClassOfRes(ResId::Make(i));
      assert(rescl.id < evm::platform::GetResClassCount());
      if (caller) _callerSaveList[rescl.id].push_back(i);
      if (callee) _calleeSaveList[rescl.id].push_back(i);
    }
  }
  for (size_t i = 0, n = evm::platform::GetResClassCount(); i < n; ++i) {
    _callerSaveList[i].push_back(~0);
    _calleeSaveList[i].push_back(~0);
  }
}
*/
void jitcs::CallingConventionImpl::setTypeInfo
    (FTSubType sub,
     Slice<FTTypeId> results,
     Slice<FTTypeId> params) {
  _subtype = sub;
  _outtypes.clear();
  _intypes.clear();
  _outtypes.insert(_outtypes.end(), results.begin(), results.end());
  _intypes.insert(_intypes.end(), params.begin(), params.end());
}
jitcs::u32 jitcs::CallingConventionImpl::getScratchResources
    (ResClassId rclid) const {
  return _scratchResources[rclid];
}
jitcs::u32 jitcs::CallingConventionImpl::getReservedResources
    (ResClassId rclid) const {
  return _reservedResources[rclid];
}
void jitcs::CallingConventionImpl::setResClassCount(size_t resNo) {
  _scratchResources.resize(resNo);
  _reservedResources.resize(resNo);
}
void jitcs::CallingConventionImpl::setScratchResources(ResClassId rcl, u32 v) {
  _scratchResources[rcl] = v;
}
void jitcs::CallingConventionImpl::setReservedResources(ResClassId rcl, u32 v) {
  _reservedResources[rcl] = v;
}
