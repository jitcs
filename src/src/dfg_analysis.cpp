//===-- src/src/dfg_analysis.cpp --------------------------------*- C++ -*-===//
// Data flow graph analysis.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The DFG has a local and a global phase. The local phase has O(I+B*R), with
// I the number of instructions, B the number of basic blocks and R the number
// of virtual registers. It collects all def-use information from all 
// instructions in all basic blocks. It results in a set of used and defined
// registers for each basic blocks. (B*R is the result of clearing all bits
// at the start of each basic blocks analysis.)
// The global phase has O(B*R*R) worstcase runtime. (R is for the set of bits
// handled for each iteration, and B*R is the upper limit for increasing the
// number of set bits for each basic block.) The phase calculates the
// live-in bitset as live-out minus defined plus uses.
//===----------------------------------------------------------------------===//

#include "jitcs_int_dfg_analysis.h"
#include "jitcs_adt_range.h"
#include "jitcs_int_function_impl.h"
#include "jitcs_int_bblock_impl.h"
#include "jitcs_int_bitfuncs.h"
#include "jitcs_dumper.h"
#include "jitcs_int_adt_dynslice.h"
#include "jitcs_callingconvention.h"
#include "jitcs_int_machine.h"

//#include <stdio.h>

jitcs::DFGAnalysis::DFGAnalysis() 
{}

void jitcs::DFGAnalysis::init(FunctionImpl& f, AMode m, Slice<u32> touchedFixed) {
  size_t bbn = f.bbs_size();
  TempAllocator &alloc = f.getAllocator();
  _blocks = alloc.allocTypedArray<BlockData>(bbn);
  Ref<const IMachineDetails> details = f.getMachineDetails();
  _JITCS_DBG_CHECK_(touchedFixed.size() >= details->getResClassCount());
  size_t rn = details->getResCount() + f.vregs_size();
  size_t rn2 = DivRoundedUpByPowerOf2<sizeof(BitSlice::WordType) * 8>(rn);
  Slice<BitSlice::WordType> data = alloc.allocTypedArray<BitSlice::WordType>(bbn * 4 * rn2);
  Slice<BitSlice::WordType> nextbits = alloc.allocTypedArray<BitSlice::WordType>(bbn * rn2);
  Slice<NextData> nextdata = alloc.allocTypedArray<NextData>(bbn * rn);

//printf("dfa init/0\n");
//PrintFDumper dump;
  
  data.fill(0);
  nextbits.fill(0);
  
  SetupBlockResourceInfo sbri(f);
  size_t bbi = 0;
  for (FunctionImpl::bb_range r = f.bbs_range(); !!r; ++r) {
    BasicBlockImpl* bb = *r;
    _JITCS_DBG_CHECK_(bb->id() == bbi);
    BlockData &bd = _blocks[bbi];
    ++bbi;
    bd.bb = bb;
    bd.liveIn = BitSlice(data.ptr(), rn);
    data.popFront(rn2);
    bd.liveOut = BitSlice(data.ptr(), rn);
    data.popFront(rn2);
    bd.used = BitSlice(data.ptr(), rn);
    data.popFront(rn2);
    bd.defined = BitSlice(data.ptr(), rn);
    data.popFront(rn2);
    bd.useRangeData = Slice<NextData>(nextdata.ptr(), rn);
    nextdata.popFront(rn);
    bd.useRangeIsValid = BitSlice(nextbits.ptr(), rn);
    nextbits.popFront(rn2);
    bd.instructions = alloc.allocTypedArray<InstructionData>(bb->instr_size());
//printf("bb %d: sbri init %p\n", bb->id(), bd.instructions.ptr());
    sbri.init(bd.instructions, bd.defined, bd.used, 
              bd.useRangeIsValid, bd.useRangeData,
              touchedFixed);
//printf("bb %d: before extract\n", bb->id());
    details->extractDefUse(bb->cinsns(), bb, sbri);
//printf("bb %d: after extract\n", bb->id());
    
    if (m == M_Local) {
      bd.liveOut.setAll();
      bd.liveIn.copyFrom(bd.liveOut);
      bd.liveIn.clearFrom(bd.defined);
      bd.liveIn.setFrom(bd.used);
    }
  }

  if (m == M_Global) {
    // this is the classical approach to calculate live-in and live-out of all
    // basic blocks by using a worklist and picking any block that changed without
    // preference, starting from exit nodes.
    // TODO: check if a priority list on (1) the number of successors that have already
    // been handled, (2) the number of changed bits on live-out, (3) the depth of the
    // block in question, might improve average runtime 
    DynSlice<BasicBlockImpl const*, 64> worklist(bbn);
    DynBitSlice<64> inworklist(bbn, true);
    DynBitSlice<64> handled(bbn, true);
    size_t worklistn = 0;

//printf("dfa global\n");

    {
      std::shared_ptr<const CallingConvention> cc = f.getCallingConvention();
      for (FunctionImpl::const_bb_range r = f.bbs_crange(); !!r; ++r) {
        BasicBlockImpl const* bb = *r;
        BlockData& bbi = _blocks[bb->id()];
        _JITCS_DBG_CHECK_(bbi.liveIn.isAllZeroes());
        if (!!bb->succ_range()) continue;
        for (size_t i = 0, n = cc->getResultCount(); i < n; ++i) {
          RefOrNull<const VirtualRegister> rr = cc->getResultRegister(i);
          if (!rr.isNull())
            bbi.liveOut.mark(rr.removeNullType()->res);
        }
        worklist[worklistn++] = bb;
        inworklist.mark(bb->id());
      }
    }
//printf("dfa after exit nodes\n");
    while (worklistn > 0) {
      BasicBlockImpl const* bb = worklist[--worklistn];
      size_t bbid = bb->id();
//printf("worklist: bb %d\n", bb->id());
      BlockData& bbi = _blocks[bbid];
      inworklist.clear(bbid);
      handled.mark(bbid);
      // update live out from live in of successors
      for (BasicBlockImpl::const_bb_range r = bb->succ_range(); !!r; ++r)
        bbi.liveOut.uniteWith(_blocks[(*r)->id()].liveIn);
//printf("liveout: ");
//bbi.liveOut.dump(dump);
//printf("\n");
//printf("use: ");
//bbi.used.dump(dump);
//printf("\n");
//printf("def: ");
//bbi.defined.dump(dump);
//printf("\n");
//printf("livein: ");
//bbi.liveIn.dump(dump);
//printf("\n");
      // calculate live in from use, def and live out
      bool changed = false;
      size_t nout = bbi.liveIn.wordSize();
      BitSlice::PtrType pin = bbi.liveIn.ptr(), pdef = bbi.defined.ptr(),
                        puse = bbi.used.ptr(), pout = bbi.liveOut.ptr();
      for (size_t i = 0; i < nout; ++i) {
        size_t newval = (pout[i] & ~pdef[i]) | puse[i];
        _JITCS_DBG_CHECK_((pin[i] & ~newval) == 0);
        changed |= pin[i] != newval;
        pin[i] = newval;
      }
//printf("new livein (%s): ", changed ? "changed" : "not changed");
//bbi.liveIn.dump(dump);
//printf("\n");
      // put predecessors into worklist
      for (BasicBlockImpl::const_bb_range r = bb->pred_crange(); !!r; ++r) {
        size_t predid = (*r)->id();
        if (changed || !handled.test(predid)) {
          //handled.clear(predid);
          if (!inworklist.testAndMark(predid)) {
//printf("add bb %d to worklist\n", predid);
            worklist[worklistn++] = *r;
          }
        }
      }
    }
  }
//printf("dfa done\n");
}

#if 0
void jitcs::DFGAnalysis::dump(IDumper& o) const {
  o.write("--- Data Flow:\n");
  for (size_t i = 0, n = _blocks.size(); i < n; ++i) {
    BBlockInfo const& bbi = _blocks[i];
    o.writef("  BB(%d)\n", bbi.bb->id().id);
    size_t ix = 0;
    for (BBlock::ins_range r = bbi.bb->instr_range(); !!r; ++r) {
      evm::platform::Dump(o, *r, f);
      if (bbi.nextidx.size() == bbi.bb->instr_size()) {
        o.writef("    pos %d, next:", bbi.inspos[ix]);
        size_t n1 = bbi.nextidx[ix];
        size_t n2 = ix + 1 < bbi.nextidx.size() ? bbi.nextidx[ix+1] : bbi.nexts.size();
        for (size_t n = n1; n < n2; ++n)
          bbi.nexts[n].dump(o);
        o.write("\n");
      }
      ++ix;
    }
    o.write("    BB Defined: ");
    bbi.bitsDefd.dump(o);
    o.write("\n");
    o.write("    BB Used: ");
    bbi.bitsUsed.dump(o);
    o.write("\n");
    o.write("    BB Live In: ");
    bbi.bitsLiveIn.dump(o);
    o.write("\n");
    o.write("    BB Live Out: ");
    bbi.bitsLiveOut.dump(o);
    o.write("\n");
  }

  for (Slice<BasicBlockImpl*>::const_iterator b = _order.begin(), e = _order.end(), i = b; i != e; ++i) {
    if (i != b)
      o.write(", ");
    if (*i == nullptr) {
      o.write("ERROR(null)");
      continue;
    }
    BBId id = (*i)->id();
    if (id < 0 || id >= _blocks.size()) {
      o.writef("ERROR(id=%d)", id);
      continue;
    }
    o.writef("BB %d(D=%d)", id, _blocks[id].depth);
  }
  o.write("\n");
}
#endif

void jitcs::GatherResourceList::_addFixed(ResClassId rclid, u32 id, u32 mask, u32 usedef) {
//printf("addfixed: %d, %d, %d, %d\n", rclid, id, mask, usedef);
  _JITCS_DBG_CHECK_(IsPowerOf2(mask));
  ClassWise& cw = byClass[rclid];
  if (!(classInit & (1 << rclid))) {
    cw.numFixed = cw.numNonFixed = 0;
    classInit |= 1 << rclid;
  }
  for (uint i = NUM - cw.numFixed; i < NUM; ++i) {
    if ((cw.touchedResources[i].resInfo >> DFGAnalysis::ResourceData::E_ResShift) 
         == (id >> DFGAnalysis::ResourceData::E_ResShift)) { 
      cw.touchedResources[i].mask &= mask;
      cw.touchedResources[i].resInfo |= usedef;
      return;
    }
  }
  cw.touchedResources[NUM - (++cw.numFixed)] = {id | usedef, mask};
  _JITCS_DBG_CHECK_(cw.numFixed + cw.numNonFixed <= NUM);
}
void jitcs::GatherResourceList::_addNonFixed(ResClassId rclid, u32 id, u32 mask, u32 usedef) {
//printf("addnonfixed: %d, %d, %d, %d\n", rclid, id, mask, usedef);
  _JITCS_DBG_CHECK_(mask != 0);
  ClassWise& cw = byClass[rclid];
  if (!(classInit & (1 << rclid))) {
    cw.numFixed = cw.numNonFixed = 0;
    classInit |= 1 << rclid;
  }
  for (uint i = 0; i < cw.numNonFixed; ++i) {
    if ((cw.touchedResources[i].resInfo >> DFGAnalysis::ResourceData::E_ResShift) 
        == (id >> DFGAnalysis::ResourceData::E_ResShift)) { 
      cw.touchedResources[i].mask &= mask;
      cw.touchedResources[i].resInfo |= usedef;
      return;
    }
  }
  cw.touchedResources[cw.numNonFixed++] = {id | usedef, mask};
  _JITCS_DBG_CHECK_(cw.numFixed + cw.numNonFixed <= NUM);
}

jitcs::SetupBlockResourceInfo::SetupBlockResourceInfo(FunctionImpl& f) 
  : _alloc(f.getAllocator())
  , _f(f)
  , _rclCnt(f.getMachineDetails()->getResClassCount())  {
}
void jitcs::SetupBlockResourceInfo::init(Slice<DFGAnalysis::InstructionData> ins,
          BitSlice def, BitSlice use,
          BitSlice nextValid, Slice<DFGAnalysis::NextData> next,
          Slice<u32> touchedFixed) {
  // TODO: def and use must be filled with zeros...
  _def = def;
  _use = use;
  _nextValid = nextValid;
  _next = next;
  _JITCS_DBG_CHECK_(def.isAllZeroes() && use.isAllZeroes());
  _JITCS_DBG_CHECK_(nextValid.isAllZeroes());
  _ins = ins;
  _idx = ins.size();
  _touchedFixed = touchedFixed;
}

static void _SortNonFixed(jitcs::Slice<jitcs::GatherResourceList::Entry> d) {
  _JITCS_DBG_CHECK_(d.size() >= 2);
  jitcs::u32 bitcnt[jitcs::GatherResourceList::NUM];
  for (size_t i = 0; i < d.size(); ++i)
    bitcnt[i] = jitcs::popcnt(d[i].mask);
  // sort non-fixed resources
  jitcs::GatherResourceList::Entry* p = d.ptr();
  switch (d.size()) {
  case 2:
    if (bitcnt[0] > bitcnt[1]) {
      std::swap(p[0], p[1]);
    }
    break;
  case 3:
    if (bitcnt[0] <= bitcnt[1]) {
      if (bitcnt[1] <= bitcnt[2]) { 
        // 0 <= 1 <= 2 => do nothing
      } else {
        if (bitcnt[0] <= bitcnt[2]) {
          // 0 <= 2 < 1 => swap 1 and 2
          std::swap(p[1], p[2]);
        } else {
          // 1 >= 0 > 2 => rotate
          jitcs::GatherResourceList::Entry tmp = p[2];
          p[2] = p[1];
          p[1] = p[0];
          p[0] = tmp;
        }
      }
    } else {
      if (bitcnt[1] > bitcnt[2]) { 
        // 0 > 1 > 2 => swap 0 and 2
        std::swap(p[0], p[2]);
      } else {
        if (bitcnt[0] <= bitcnt[2]) {
          // 2 >= 0 > 1 => swap 0 and 1
          std::swap(p[0], p[1]);
        } else {
          // 0 > 2 >= 1 => rotate
          jitcs::GatherResourceList::Entry tmp = p[0];
          p[0] = p[1];
          p[1] = p[2];
          p[2] = tmp;
        }
      }
    }
    break;
  case 4:
    if (bitcnt[0] > bitcnt[1]) {
      std::swap(bitcnt[0], bitcnt[1]);
      std::swap(p[0], p[1]);
    }
    if (bitcnt[2] > bitcnt[3]) {
      std::swap(bitcnt[2], bitcnt[3]);
      std::swap(p[2], p[3]);
    }
    if (bitcnt[0] > bitcnt[2]) {
      std::swap(bitcnt[0], bitcnt[2]);
      std::swap(p[0], p[2]);
    }
    if (bitcnt[1] > bitcnt[3]) {
      std::swap(bitcnt[1], bitcnt[3]);
      std::swap(p[1], p[3]);
    }
    if (bitcnt[1] > bitcnt[2]) {
      std::swap(bitcnt[1], bitcnt[2]);
      std::swap(p[1], p[2]);
    }
    break;
  default:
    for (size_t i = 1, n = d.size(); i < n; ++i) {
      for (size_t j = n; j > i; --j) {
        std::swap(bitcnt[j - 2], bitcnt[j - 1]);
        std::swap(p[j - 2], p[j - 1]);
      }
    }
    break;
  }
}


void jitcs::SetupBlockResourceInfo::push_front(GatherResourceList & grl) {
//printf("pushfront -> %d\n", _idx - 1);
  _JITCS_DBG_CHECK_(_idx > 0);
  --_idx;
  DFGAnalysis::InstructionData& id = _ins[_idx];
//printf("pushfront: ins data: ix %d, (%p)\n", _idx, &id);
  size_t num = 0;
  u32 ci = grl.classInit;
  if (ci == 0) {
    id.ptr = nullptr;
    Slice<u8>(id.resClassEnd, 8).fill(0);
    return;
  }
  u32 rclid = 0, currclid = 0;
  u8 rclids[8], rclnum = 0;
  while (ci != 0) {
    u32 dci = t0cntnz(ci);
    ci >>= dci;
    ci >>= 1;
    rclid += dci;
//printf("  prehandle class %d\n", rclid);
    GatherResourceList::ClassWise& cw = grl.byClass[rclid];
    _JITCS_DBG_CHECK_(cw.numFixed + cw.numNonFixed > 0 && cw.numFixed + cw.numNonFixed <= 15);
    while (currclid < rclid) {
//printf("set resclass %d end to %d\n", currclid, num);
      id.resClassEnd[currclid++] = num;
    }
    u32 resMaskUse = 0;
    u32 resMaskDef = 0;
    u32 bitcnt[GatherResourceList::NUM];

//printf("  prehandle fixed\n");
    // set use/def masks for all fixed registers/resources
    for (size_t i = 0; i < cw.numFixed; ++i) {
      size_t ix = GatherResourceList::NUM - cw.numFixed + i;
      const GatherResourceList::Entry& e = cw.touchedResources[ix];
      if (e.resInfo & DFGAnalysis::ResourceData::E_Use)
        resMaskUse |= e.mask;
      if (e.resInfo & DFGAnalysis::ResourceData::E_Def)
        resMaskDef |= e.mask;
    }
    _touchedFixed[rclid] |= resMaskUse | resMaskDef;
//printf("  prehandle nonfixed\n");
    // clear bits for overlapping fixed registers
    for (size_t i = 0; i < cw.numNonFixed; ++i) {
      GatherResourceList::Entry& e = cw.touchedResources[i];
      if (e.resInfo & DFGAnalysis::ResourceData::E_Use)
        e.mask &= ~resMaskUse;
      if (e.resInfo & DFGAnalysis::ResourceData::E_Def)
        e.mask &= ~resMaskDef;
      _JITCS_ALWAYS_CHECK_(e.mask != 0);
    }
    // sort non fixed by number of mask bits
    // (fixed only have a single bit set anyway)
    if (cw.numNonFixed >= 2) {
//printf("  prehandle sort nonfixed\n");
      _SortNonFixed(jitcs::Slice<jitcs::GatherResourceList::Entry>
                      (cw.touchedResources, cw.numNonFixed));
    }

    num += cw.numFixed + cw.numNonFixed;
    rclids[rclnum++] = rclid;
    ++rclid;
//printf("  prehandle: cur num = %d\n", num);
  }
  while (currclid < _rclCnt) {
//printf("set resclass %d end to %d\n", currclid, num);
    id.resClassEnd[currclid++] = num;
  }
  // TODO: use constant ident instead of hardcoded value
  id.resClassEnd[7] = num;
  
  if (num > _remaining.size()) {
    _remaining = _alloc.allocTypedArray<DFGAnalysis::ResourceData>(256);
  }
  id.ptr = _remaining.ptr();
  _remaining.popFront(num);
//printf("  handle data ptr: %p\n", id.ptr);
  DFGAnalysis::ResourceData *p = id.ptr;
  for (size_t j = 0; j < rclnum; ++j) {
    GatherResourceList::ClassWise& cw = grl.byClass[rclids[j]];
//printf("  handle class %d\n", rclids[j]);
    for (size_t i = 0, n = cw.numFixed + cw.numNonFixed; i < n; ++i) {
      size_t ix = i < cw.numFixed ? GatherResourceList::NUM - cw.numFixed + i 
                                   : i - cw.numFixed;
      const GatherResourceList::Entry& e = cw.touchedResources[ix];
      u32 res = e.resInfo;
      p->resInfo = res;
      p->allowedMask = e.mask;
      p->hintMask = 0;
//printf("  pushfront: reg %p: GRLIx: %d -> RCLIx: %d, resInfo %d\n", p, ix, i, res);
      // remember next use
      u32 r = res >> DFGAnalysis::ResourceData::E_ResShift;
      if (_nextValid.testAndMark(r)) {
        p->nextUse = _next[r].next;
      } else {
        p->nextUse = ~1;
        _next[r].last = 2 * _idx;
      }
      _next[r].next = (res & DFGAnalysis::ResourceData::E_Use) ? (2 * _idx) : ~0;
      // mark def/use
//printf("  handle reg %d: mark %s\n", i, 
//       (res & 3) == 1 ? "use" :
//       (res & 3) == 2 ? "def" :
//       (res & 3) == 3 ? "use/def" : "error");
      if (res & DFGAnalysis::ResourceData::E_Def) _def.mark(r);
      if (res & DFGAnalysis::ResourceData::E_Use) _use.mark(r); else _use.clear(r);
      ++p;
//printf("  handle reg %d done\n", i);
    }
  }
//printf("  pushfront end\n");
}
bool jitcs::DFGAnalysis::isUsed(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const {
  return _blocks[bb->id()].used.test(v->res);
}
bool jitcs::DFGAnalysis::isDefined(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const {
  return _blocks[bb->id()].defined.test(v->res);
}
bool jitcs::DFGAnalysis::isLiveIn(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const {
  return _blocks[bb->id()].liveIn.test(v->res);
}
bool jitcs::DFGAnalysis::isLiveOut(Ref<BasicBlockImpl> bb, Ref<const VirtualRegister> v) const {
  return _blocks[bb->id()].liveOut.test(v->res);
}
bool jitcs::DFGAnalysis::isUsed(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const {
  return isUsed(Ref<BasicBlockImpl>(reinterpret_cast<BasicBlockImpl*>(bb.ptr())), v);
}
bool jitcs::DFGAnalysis::isDefined(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const {
  return isDefined(Ref<BasicBlockImpl>(reinterpret_cast<BasicBlockImpl*>(bb.ptr())), v);
}
bool jitcs::DFGAnalysis::isLiveIn(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const {
  return isLiveIn(Ref<BasicBlockImpl>(reinterpret_cast<BasicBlockImpl*>(bb.ptr())), v);
}
bool jitcs::DFGAnalysis::isLiveOut(Ref<BasicBlock> bb, Ref<const VirtualRegister> v) const {
  return isLiveOut(Ref<BasicBlockImpl>(reinterpret_cast<BasicBlockImpl*>(bb.ptr())), v);
}

void jitcs::DFGAnalysis::extractUseDef(Ref<BasicBlockImpl> bb, size_t j, 
                                       BitSlice used, BitSlice defined) const {
  _JITCS_DBG_CHECK_(bb->id() < _blocks.size() 
                    && j < _blocks[bb->id()].instructions.size());
  const InstructionData& id = _blocks[bb->id()].instructions[j];
//printf("DFA/extract: %p: count %d %d %d %d %d %d %d %d - %p\n", 
//       &id,
//       id.resClassEnd[0], id.resClassEnd[1], id.resClassEnd[2], id.resClassEnd[3],
//       id.resClassEnd[4], id.resClassEnd[5], id.resClassEnd[6], id.resClassEnd[7],
//       id.ptr);
  for (size_t j = 0; j < id.resClassEnd[7]; ++j) {
    ResourceData &r = id.ptr[j];
//printf("DFA/extract: %d -> %d\n", j, r.resInfo);
    if (r.isUse()) used.mark(r.getResId());
    if (r.isDef()) defined.mark(r.getResId());
  }
}
