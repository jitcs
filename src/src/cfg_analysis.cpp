//===-- src/src/cfg_analysis.cpp --------------------------------*- C++ -*-===//
// Control flow graph analysis.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The point of CFG analysis is twofold: it will generate an order of basic
// blocks for code generation, trying to keep any "fallthru" hint encountered.
// Secondly, it can, on request, try to determine to loop depth for each and
// every basic block. The used algorithm is probably slow for large sets
// of basic blocks with many loops.
//===----------------------------------------------------------------------===//

#include "jitcs_int_cfg_analysis.h"
#include "jitcs_adt_range.h"
#include "jitcs_int_function_impl.h"
#include "jitcs_int_bblock_impl.h"
#include "jitcs_dumper.h"
#include "jitcs_int_adt_dynslice.h"

jitcs::CFGAnalysis::CFGAnalysis() 
{}

void jitcs::CFGAnalysis::init(FunctionImpl& f, AMode m) {
  size_t bbn = f.bbs_size();
  TempAllocator &alloc = f.getAllocator();
  _order = alloc.allocTypedArray<BasicBlockImpl*>(bbn);
  _bblocks = alloc.allocTypedArray<BBlockInfo>(bbn);
  _maxDepth = 0;

  // get ordering: just put the basic blocks in the order they were
  // created, but with keeping fall-thru edges intact if possible
  size_t ordersize = 0;
  size_t bbi = 0;
  DynBitSlice<64> handled(bbn, true);
  for (FunctionImpl::bb_range r = f.bbs_range(); !!r; ++r) {
    BasicBlockImpl* bb = *r;
    _JITCS_DBG_CHECK_(bb->id() == bbi);
    _bblocks[bbi].bb = bb;
    _bblocks[bbi].depth = 0;
    ++bbi;

    if (handled.testAndMark(bb->id()))
      continue;

    // add basic blocks from a fall thru, unless they are already in the list
    do {
      if (ordersize > 0)
        BasicBlockImpl::SetFallthru(_order[ordersize - 1], bb);
      _order[ordersize++] = bb;
      bb = bb->getFallThruTo()._ptr;
    } while (bb != nullptr && !handled.testAndMark(bb->id()));
  }
  _JITCS_ALWAYS_CHECK_(ordersize == bbn && _order[0] == f.getStartBlock()._ptr);

  if (m == SIMPLE) return;
  // start depth analysis
  
  DynSlice<size_t, 64> worklist(bbn);
  size_t worklistn = 0;
  handled.clearAll();
  DynBitSlice<64> candidates(bbn, false);
  DynBitSlice<64> inworklist(bbn, true);
  candidates.setAll();
  _maxDepth = _calcDepth(0, 0, handled, candidates, worklist, inworklist);

  assert(handled.countSetBits() == bbn);
}
// depth: supposed depth of the current set of basic blocks
// headbb: probable head node of the loop (or the entry block of the function)
// handled: ???
// candidates: this set restricts the basic blocks which are considered for handling
// worklist: ???
// inworklist: 
size_t jitcs::CFGAnalysis::_calcDepth(size_t depth, size_t headbb, 
                                      BitSlice handled, BitSlice candidates, 
                                      Slice<size_t> worklist, BitSlice inworklist) {
  // start with a fresh working list
  size_t worklistn = 0;
  inworklist.clearAll();
  size_t mdepth = depth;
  worklist[worklistn++] = headbb;
  inworklist.mark(headbb);
  do {
    // this depth analysis consists of a repeating process of two steps: the forward pass
    // and the backward pass.
    // in the forward pass, only basic blocks are handled which are reachable only from the
    // headnode. or, a node N can only be handled, if all its predecessors have been handled.
    // (with the headnode automatically in the set).
    // once we reach the backward pass, the working list will contain only nodes which are reachable
    // by backedges. for each node N in the working list, we will collect the set C ("candidates") of 
    // all basic blocks which are reachable thru back edges from N. for each N, we assume it is 
    // a loop header, and recursively call _calcDepth for it. by restricting the set of 
    // of a loop 
    // we will 
    // 
    size_t outn = worklistn;
    do {
      worklistn = outn;
      outn = 0;
      for (size_t i = 0; i < worklistn; ++i) {
        size_t bbi = worklist[i];
        assert(candidates.test(bbi) && !handled.test(bbi) && inworklist.test(bbi));
        BasicBlockImpl* bb = _bblocks[bbi].bb;
        bool pred_handled = true;
        // check if all predecessors of the current block have been handles/all back edges
        // lie in the block sub graph already handled
        // the headnode (which might be a loop header) ignores its back edges
        if (bbi != headbb) {
          for (BasicBlockImpl::const_bb_range r = bb->pred_crange(); !!r; ++r) {
            size_t predid = (*r)->id();
            if (!handled.test(predid)) {
              pred_handled = false;
              break;
            }
          }
        }
        // if at least one predecessor/backedge must be handled first, put the block back on the
        // working list
        if (!pred_handled) {
          worklist[outn++] = bbi;
          continue;
        }
        // otherwise, mark the block as handled, and write down its depth!
        // then, add their successors to the worklist
        handled.mark(bbi);
        _bblocks[bbi].depth = depth;
        inworklist.clear(bbi);
        for (BasicBlockImpl::const_bb_range r = bb->succ_crange(); !!r; ++r) {
          size_t succid = (*r)->id();
          if (!candidates.test(succid) || handled.test(succid) || inworklist.test(succid))
            continue;
          worklist[worklistn++] = succid;
          inworklist.mark(succid);
        }
      }
      // we managed to handle the whole working list in the forward pass.
      // all reachable blocks are not part of a loop.
      if (outn == 0) return mdepth;
      // we checked all blocks in the working list. if we can remove at least
      // one entry per iteration, we cannot be sure, that the remaining blocks
      // cannot be removed in the forward pass.
    } while (outn < worklistn);

    // backward pass: for each basic block still in the worklist, create the set 
    // of basic blocks which are considered part of the loop (reachable thru back edges
    // without entering already handled basic blocks)
    size_t bbn = worklist.size();
    DynSlice<size_t, 64> worklistloop(bbn);
    DynBitSlice<64> allcandidatesloop(bbn, true);
    DynBitSlice<64> candidatesloop(bbn, false);
    DynBitSlice<64> inworklistloop(bbn, false);
    outn = 0;
    for (size_t i = 0; i < worklistn; ++i) {
      size_t bbi = worklist[i];
      assert(candidates.test(bbi) && inworklist.test(bbi));
      // maybe we handled this block in a previous iteration
      // this can happen for unnatural loops
      if (handled.test(bbi)) continue;
      // collect all unhandled predecessors of current basic block
      candidatesloop.clearAll();
      size_t tempn = 0;
      worklistloop[tempn++] = bbi;
      candidatesloop.mark(bbi);
      bool isloop = false;
      while (tempn > 0) {
        BasicBlockImpl* bb = _bblocks[worklistloop[--tempn]].bb;
        for (BasicBlockImpl::const_bb_range r = bb->pred_crange(); !!r; ++r) {
          size_t predid = (*r)->id();
          if (predid == bbi) isloop = true;
          if (handled.test(predid) || !candidates.test(predid)) continue;
          if (candidatesloop.testAndMark(predid)) continue;
          worklistloop[tempn++] = predid;
        }
      }
      if (isloop) {
        // calculate depth for assumed loop
        // this will not necessarily handle all blocks in the candidate list
        size_t newmdepth = _calcDepth(depth + 1, bbi, handled, candidatesloop,
                                      worklistloop, inworklistloop);
        // update the maximum depth
        mdepth = std::max(mdepth, newmdepth);
        allcandidatesloop.uniteWith(candidatesloop);
        inworklist.clear(bbi);
      } else {
        // this is not actually a loop, maybe it's waiting for the loop handling of
        // another block? we could treat the block like a loop, but then the block 
        // would get a greater undeserved loop depth
        worklist[outn++] = bbi;
      }
    }
    // add successors from loop to work list
    // (allcandidatesloop contains all previously unhandled blocks, which might
    // have been part of a loop).
    worklistn = outn;
    for (BitSlice::BitEnumerator r(allcandidatesloop.enumBits()); !!r; ++r) {
      size_t bbi = *r;
      BasicBlockImpl* bb = _bblocks[bbi].bb;
      for (BasicBlockImpl::const_bb_range r = bb->succ_crange(); !!r; ++r) {
        size_t succid = (*r)->id();
        if (!candidates.test(succid) || handled.test(succid) || inworklist.test(succid))
          continue;
        worklist[worklistn++] = succid;
        inworklist.mark(succid);
      }
    }
  } while (worklistn > 0);
  return mdepth;
}

void jitcs::CFGAnalysis::clear() {
  _order = Slice<BasicBlockImpl*>();
  _bblocks = Slice<BBlockInfo>();
  _maxDepth = 0;
}

bool jitcs::CFGAnalysis::checkIntegrity() const {
  if (_order.size() != _bblocks.size()) return false;
  if (_order.size() > 0) {
    DynBitSlice<64> handled(_order.size(), true);
    // check if _order contains all BBlocks from 0 to size-1
    for (Slice<BasicBlockImpl*>::const_iterator i = _order.begin(), e = _order.end(); i != e; ++i) {
      if (*i == nullptr) return false;
      BBId id = (*i)->id();
      if (id < 0 || id >= _order.size()) return false;
      if (handled.testAndMark(id)) return false;
    }
    // check that the maxdepth is bounding the bblocks depths
    for (Slice<BBlockInfo>::const_iterator i = _bblocks.begin(), e = _bblocks.end(); i != e; ++i) {
      if ((*i).depth < 0 || (*i).depth > _maxDepth) return false;
    }
  }
  return true;
}

void jitcs::CFGAnalysis::dump(IDumper& o) const {
  o.write("--- BB order: ");

  for (Slice<BasicBlockImpl*>::const_iterator b = _order.begin(), e = _order.end(), i = b; i != e; ++i) {
    if (i != b)
      o.write(", ");
    if (*i == nullptr) {
      o.write("ERROR(null)");
      continue;
    }
    BBId id = (*i)->id();
    if (id < 0 || id >= _bblocks.size()) {
      o.writef("ERROR(id=%d)", id);
      continue;
    }
    o.writef("BB %d(D=%d)", id, _bblocks[id].depth);
  }
  o.write("\n");
}
