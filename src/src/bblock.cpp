//===-- src/src/bblock.cpp ---------------------------------*- C++ -*-===//
// Public BasicBlock interface class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_bblock.h"
#include "jitcs_int_bblock_impl.h"

namespace {
class BBEnum : public jitcs::IEnumerator<const jitcs::BasicBlock> {
public:
  typedef jitcs::BasicBlockImpl::BasicBlockList::const_iterator it_type;

  BBEnum(it_type b, it_type e) : _b(b), _e(e) {}
  virtual bool empty() const { return _b == _e; }
  virtual ItemType& front() { assert(!empty()); return *(*_b); }
  virtual void popFront() { assert(!empty()); ++_b; }
  
private:
  it_type _b, _e;
};
class InsEnum : public jitcs::IEnumerator<const jitcs::Instruction> {
public:
  typedef jitcs::BasicBlockImpl::InstructionList::const_iterator it_type;

  InsEnum(it_type b, it_type e) : _b(b), _e(e) {}
  virtual bool empty() const { return _b == _e; }
  virtual ItemType& front() { assert(!empty()); return **_b; }
  virtual void popFront() { assert(!empty()); ++_b; }
  
private:
  it_type _b, _e;
};
}
  
jitcs::Enumerator<const jitcs::BasicBlock>  jitcs::BasicBlock::predecessors() {
  const BasicBlockImpl& imp = this->impl();
  return std::unique_ptr<jitcs::IEnumerator<const jitcs::BasicBlock>>(
    new BBEnum(imp.pred_begin(), imp.pred_end()));
}
jitcs::Enumerator<const jitcs::BasicBlock>  jitcs::BasicBlock::successors() {
  const BasicBlockImpl& imp = this->impl();
  return std::unique_ptr<jitcs::IEnumerator<const jitcs::BasicBlock>>(
    new BBEnum(imp.succ_cbegin(), imp.succ_cend()));
}
jitcs::Enumerator<const jitcs::Instruction> jitcs::BasicBlock::instructions() {
  const BasicBlockImpl& imp = this->impl();
  return std::unique_ptr<jitcs::IEnumerator<const jitcs::Instruction>>(
    new InsEnum(imp.instr_begin(), imp.instr_end()));
}
