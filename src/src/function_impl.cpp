//===-- src/src/function_impl.cpp -------------------------------*- C++ -*-===//
// Function implementation class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_int_function_impl.h"
#include "jitcs_function.h"
#include "jitcs_int_bblock_impl.h"
#include "jitcs_callingconvention.h"
#include "jitcs_dumper.h"
#include "jitcs_machine.h"
#include "jitcs_tmpalloc.h"
#include "jitcs_int_virtualregister.h"
#include "jitcs_int_cfg_analysis.h"
#include "jitcs_int_codegenerator.h"
#include "jitcs_int_machine.h"

#include <stdio.h>

jitcs::FunctionImpl::FunctionImpl
    (RefCounter<TempAllocator>& ins, 
     const std::shared_ptr<const CallingConvention>& cc) 
  : _allocator(ins)
  //, _m(M_Construct)
  //, _frameInfoCnt(2)
  , _cc(cc)
  , _mi(cc->getMachineInfo())
  , _bblocks(*ins)
  , _parregs(*ins)
  , _vregs(*ins) {

  // create vregs for parameters and results
//  ins.setAlignment(16);
  
/*  size_t rn = _cc.getResultCount(), pn = _cc.getParamCount();
  for (size_t i = 0; i < rn; ++i) {
    VReg* vr = createVReg(_cc.getResultRegisterClass(i));
    assert(vr->id.id == R_HardwareLimit + i);
    vr->spillplacement = _cc.getResultSpillPlacement(i);
  }
  for (size_t i = 0; i < pn; ++i) {
    VReg* vr = createVReg(_cc.getParamRegisterClass(i));
    assert(vr->id.id == R_HardwareLimit + pn + i);
    vr->spillplacement = _cc.getParamSpillPlacement(i);
  }*/
  Ref<const IMachineDetails> mid = getMachineDetails();
  for (size_t i = 0, n = MMODE_FPCount; i < n; ++i) {
    _frameRegisterInfo[i].reg = i == 0 ? mid->stackFrameRegister()._ptr : nullptr;
    _frameRegisterInfo[i].ofs = 0;
  }
}
jitcs::FunctionImpl::~FunctionImpl() {
//  clean();
}

void jitcs::FunctionImpl::dump(MachineDumper& o) const {
  o.writef("Function dump: ");
  _cc->dump(o);
  o.write("\n");
  size_t pc = _cc->getParamCount(), rc = _cc->getResultCount();
  o.writef("  param/result vregs: ");
  for (size_t i = 0; i < pc; ++i) {
    if (i > 0) o.write(", ");
    RefOrNull<const VirtualRegister> cell = _parregs[rc + i];
    if (cell.isNull())
      o.write("-");
    else
      o.write(cell.removeNullType());
  }
  o.write(" -> ");
  for (size_t i = 0; i < rc; ++i) {
    if (i > 0) o.write(", ");
    RefOrNull<const VirtualRegister> cell = _parregs[i];
    if (cell.isNull())
      o.write("-");
    else
      o.write(cell.removeNullType());
  }
  o.write("\n");
  for (const_bb_range r = bbs_crange(); !!r; ++r) {
    (*r)->dump(o);
  }
  //TODO: clusters, maxdepth
  //TODO: vregs
  //o.write("--vregs\n");
  //for (size_t i = 0, n = _vregs.size(); i < n; ++i) {
  //  o.write("  ");
  //  _vregs[i]->dump(o);
  //  o.write("\n");
  //}
  //TODO: frameinfos
}
jitcs::Ref<jitcs::BasicBlockImpl> jitcs::FunctionImpl::getStartBlock() {
  if (!_bblocks.size()) {
    return createBasicBlock();
  }
  return _bblocks[0];
}

jitcs::Ref<jitcs::BasicBlockImpl> jitcs::FunctionImpl::createBasicBlock() {
  // TODO: allocate from TempAllocator
  BasicBlockImpl* bb = _allocator->allocTyped<BasicBlockImpl>();
  new (bb) BasicBlockImpl(*this, BBId(_bblocks.size()));
  //BasicBlockImpl* bb = new BasicBlockImpl(*this, BBId(_bblocks.size()));
  _bblocks.push_back(bb);
  return bb;
}
void jitcs::FunctionImpl::_prepareArgumentAndResultRegisterSpace() {
  _parregs.initSizeAndClear((_cc->getResultCount() + _cc->getParamCount())|1, nullptr);
}

jitcs::Ref<const jitcs::VirtualRegister> 
  jitcs::FunctionImpl::getArgumentRegister(uint n, RegClassId rc) {
  assert(_parregs.size() == 0 || _parregs.size() >= (_cc->getResultCount() + _cc->getParamCount()));
  assert(n < _cc->getParamCount());
  if (_parregs.size() == 0)
    _prepareArgumentAndResultRegisterSpace();
  const VirtualRegister* &cell = _parregs[_cc->getResultCount() + n];
  if (cell == nullptr) {
    cell = createRegister(rc)._ptr;
  }
  assert(cell->regclass == rc);
  return cell;
}
jitcs::Ref<const jitcs::VirtualRegister> 
  jitcs::FunctionImpl::getResultRegister(uint n, RegClassId rc) {
  assert(_parregs.size() == 0 || _parregs.size() >= (_cc->getResultCount() + _cc->getParamCount()));
  assert(n < _cc->getResultCount());
  if (_parregs.size() == 0)
    _prepareArgumentAndResultRegisterSpace();
  const VirtualRegister* &cell = _parregs[n];
  if (cell == nullptr) {
    cell = createRegister(rc)._ptr;
  }
  assert(cell->regclass == rc);
  return cell;
}
jitcs::Ref<const jitcs::VirtualRegister> 
  jitcs::FunctionImpl::createRegister(RegClassId rc, int logSz) {
  // TODO: allocate registers blockwise...
  VirtualRegister* r = _allocator->allocTyped<VirtualRegister>();
  r->setupDynamic(*_mi, _vregs.size(), rc, logSz);
  _vregs.push_back(r);
  return r;
}

jitcs::MemoryMgr::CodeAndData jitcs::FunctionImpl::generate
  (RefCounter<MemoryMgr> vm, Function::Strategy s) {
  assert(s == Function::Direct_Code_Gen);
  CFGAnalysis ana;
  ana.init(*this, 
           s == Function::Direct_Code_Gen || s == Function::One_Pass_Linear_Scan
           ? CFGAnalysis::SIMPLE
           : CFGAnalysis::DEPTH_ANALYSIS);

  /*
  BBlockDataFlowMap dfa;
  dfa.init(*this, _allocator, _cc);
  BBlockRegAlloc ra;
  ra.runRegAlloc1P(*this, _cc, dfa);
  */
  CodeGenerator cg;
  jitcs::MemoryMgr::CodeAndData result;
  result = cg.generate(*this, ana, *vm, Slice<u8>());
  return result;
}

/*
void evm::Function::check() {
  for (size_t i = 0, n = _bblocks.size(); i < n; ++i) {
  }
  for (size_t i = 0, n = _vregs.size(); i < n; ++i) {
    assert(_vregs[i]->id.id == R_HardwareLimit + i);
  }
  assert(_frameInfoCnt < MMODE_FPCount);
  for (size_t i = 0, n = _frameInfoCnt; i < n; ++i) {
//    assert(_frameInfo[i].reg.id < R_HardwareLimit);
  }
}

void evm::Function::clean() {
  for (size_t i = 0; i < _bblocks.size(); ++i) {
    delete _bblocks[i];
    _bblocks[i] = nullptr;
  }
  _bblocks.clear();
  _vregs.clear();
}*/

//u8* evm::Function::getBBlockEstimatedAddress(BBId bb) {
//  return (bb.id > 0 && bb.id <= _bblocks.size()) ? _zero + _bblocks[bb.id-1]->getEstimatedOrFinalPos() : nullptr;
//}
/*
void evm::Function::_setupRegClassInfo(uint rclassn, uint resn, const size_t* rclass2res, uint rclass2reswn) {
  _rclassn = rclassn;
  _resn = resn,
  _rclass2res = rclass2res;
  _rclass2reswn = rclass2reswn;
}
evm::RegId evm::Function::allocVReg(uint regclass, uint valuesize) {
  u32 ofs = alloc(16);
  VRegPtr vr = VRegPtr::Make(_allocator.getPtr(ofs));
  vr.setup(regclass, valuesize, MMODE_NONE, 0);
  return addVReg(ofs);
}
evm::RegId evm::Function::addVReg(u32 ofs) {
  _vregs.push_back(ofs);
  return RegId::Make(R_HW + _vregs.size() - 1);
}*/

/*
evm::InsRef evm::Function::createFreeIns(u32 sz) { 
//  assert(_allocator.isAligned(16) && sz > 0 && (sz & 15) == 0);
  return InsRef::Make(_allocator.alloc(RoundUpToP2<16>(sz * sizeof(IOpType))));
}
evm::InsPtr evm::Function::createIns(BBlock* bb, u32 sz) { 
  InsRef indip = createFreeIns(sz);
  bb->addIns(indip);
  return indip;
}
evm::InsRef evm::Function::createFreeCopyIns(u32 cnt) {
  InsRef res = createFreeIns(evm::core::GetRCopyInsSize(cnt));
  core::CopyInsPtr cp = core::CopyInsPtr::Make(res);
  cp.setUntypedInsId(InsId::Make(core::I_RCopy));
  cp.setCount(cnt);
  return res;
}
evm::core::CopyInsPtr evm::Function::createCopyIns(BBlock* bb, u32 cnt) {
  InsRef indip = createFreeCopyIns(cnt);
  bb->addIns(indip);
  return core::CopyInsPtr::Make(indip);
}
*/
/*
evm::core::KillInsPtr evm::Function::createKillIns(BBlock* bb, u32 cnt) {
  u32 ofs = alloc(evm::core::getRKillInsSize(cnt));
  bb->addIns(IndInsPtr::Make(ofs));
  core::KillInsPtr cp = core::KillInsPtr::Make(ofs2ptr(ofs));
  cp.setUntypedInsId(InsId::Make(core::I_RKILL));
  cp.setCount(cnt);
  return cp;
}
*/
/*
void evm::Function::allocateVRegMemRefs() {
//  assert(_allocator.isAligned(16));
  u8* ptr = _allocator.alloc(MemPtr::SIZE * _vregs.size());
  for (size_t i = 0, n = _vregs.size(); i < n; ++i) {
    _vregs[i]->memref = MemRef::Make(ptr + i * MemPtr::SIZE);
    _vregs[i]->memref.toMem().setVReg(VRegRef::Make(_vregs[i]));
  }
}
evm::VReg const * evm::Function::allocCompatibleTemporaryVReg(VRegRef id) {
  RegClassId rc = id.getRegClass();
  assert(_tempvregs.size() == 0 || _tempvregs.size() == platform::GetRegClassCount());
  if (_tempvregs.empty()) _tempvregs.resize(platform::GetRegClassCount());
  if (!_tempvregs[rc.id].empty()) {
    VReg const* vr = _tempvregs[rc.id].back();
    _tempvregs[rc.id].pop_back();
    return vr;
  }
  return createVReg(rc);
}
void evm::Function::deallocTemporaryVReg(VRegRef id) {
  VReg const* vr = id;
  assert(_tempvregs.size() == 0 || _tempvregs.size() == platform::GetRegClassCount());
  if (_tempvregs.empty()) _tempvregs.resize(platform::GetRegClassCount());
  _tempvregs[vr->regclass.id].push_back(vr);
}
*/

/*
bool evm::Function::needsPossiblyFramePointer() const {
  u32 sz = _cc.getRequiredStackAlignment();
  for (ConstRange<std::vector<VReg*>::const_iterator> r = getConstRange(_vregs); !!r; ++r) {
    if ((*r)->sz > sz) return true;
  }
  // TODO: handle function calls which might increase alignment requirements
  return false;
}
*/
