//===-- src/src/host.cpp ----------------------------------------*- C++ -*-===//
// Helper functions concerning the jitcs::host namespace.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs.h"

#ifdef JITCS_X86_32
#include "jitcs_x86_32_machine.h"
#endif
#ifdef JITCS_X86_64
#include "jitcs_x86_64_machine.h"
#endif

jitcs::RefCounter<jitcs::IMachineInfo> jitcs::host::GetMachineInfo() {
#ifdef JITCS_X86_32
#  ifdef EVM_Windows
  return GetX86_32WinMachineInfo(true);
#  else
  return GetX86_32PosixMachineInfo(true);
#  endif
#endif
#ifdef JITCS_X86_64
#  ifdef EVM_Windows
  return GetX86_64WinMachineInfo(true);
#  else
  return GetX86_64PosixMachineInfo(true);
#  endif
#endif
}
