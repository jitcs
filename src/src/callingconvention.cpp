//===-- src/src/callingconvention.cpp ----------------------*- C++ -*-===//
// Public interface to calling convention object.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_callingconvention.h"
#include "jitcs_dumper.h"

void jitcs::CallingConvention::dump(MachineDumper& o) const {
  o.write("CC{");
  for (size_t i = 0, n = getParamCount(); i < n; ++i) {
    if (i > 0) o.write(", ");
    getParamSpillSlotInfo(i).dump(o);
    o.write("/");
    auto r = getParamRegister(i);
    if (r.isNull())
      o.write("-");
    else
      o.write(r.removeNullType());
  }
  o.write(" -> ");
  for (size_t i = 0, n = getResultCount(); i < n; ++i) {
    if (i > 0) o.write(", ");
    getResultSpillSlotInfo(i).dump(o);
    o.write("/");
    auto r = getResultRegister(i);
    if (r.isNull())
      o.write("-");
    else
      o.write(r.removeNullType());
  }
  o.write("}");
}
