//===-- src/src/function.cpp ------------------------------------*- C++ -*-===//
// Public Function interface class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#include "jitcs_function.h"
#include "jitcs_int_function_impl.h"
#include "jitcs_int_bblock_impl.h"

namespace {
class BBEnum : public jitcs::IEnumerator<const jitcs::BasicBlock> {
public:
  typedef jitcs::FunctionImpl::BasicBlockList::const_iterator it_type;

  BBEnum(it_type b, it_type e) : _b(b), _e(e) {}
  virtual bool empty() const { return _b == _e; }
  virtual ItemType& front() { assert(!empty()); return *(*_b); }
  virtual void popFront() { assert(!empty()); ++_b; }
  
private:
  it_type _b, _e;
};
}

jitcs::Enumerator<const jitcs::BasicBlock> jitcs::Function::enumerateBasicBlocks() {
  const FunctionImpl& imp = this->impl();
  return std::unique_ptr<jitcs::IEnumerator<const jitcs::BasicBlock>>(
    new BBEnum(imp.bbs_cbegin(), imp.bbs_cend()));
}
jitcs::Ref<jitcs::BasicBlock> jitcs::Function::getStartBlock() {
  return this->impl().getStartBlock();
}
jitcs::Ref<jitcs::BasicBlock> jitcs::Function::createBasicBlock() {
  return this->impl().createBasicBlock();
}
