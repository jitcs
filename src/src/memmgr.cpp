//===-- src/src/memmgr.cpp --------------------------------------*- C++ -*-===//
// A memory manager for code and data
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The manager is an allocator based on Doug Leah's malloc. Memory is allocated
// from the system with execution permission.
//===----------------------------------------------------------------------===//

// this allocator is based on Doug Leah's malloc. it contains slight 
// modifications such as:
// - the returned memory is executable. so don't used it for normal allocations
// - allocated blocks always fill whole cache lines
// - allocations are for data and code. data and code do not share cache lines,
//   but pages.
// TODO:
// the code lacks comments

#include "jitcs_memmgr.h"
#include "jitcs_cpu.h"

#if defined(JITCS_WINDOWS)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#if defined(JITCS_POSIX)
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
// MacOS uses MAP_ANON instead of MAP_ANONYMOUS
#ifndef MAP_ANONYMOUS
# define MAP_ANONYMOUS MAP_ANON
#endif
#endif

jitcs::RefCounter<jitcs::MemoryMgr> jitcs::MemoryMgr::Create() {
  return RefCounter<MemoryMgr>(new MemoryMgr);
}

namespace jitcs {
class MemoryMgrImpl {
public:
  typedef Slice<u8> MemoryArea;
  typedef MemoryMgr::CodeAndData CodeDataArea;
public:
  MemoryMgrImpl();
  ~MemoryMgrImpl();

public:
  /* todo
   * allocate area for function relevant constants (aligned to 16 byte)
   * allocate area for code (aligned to cache line size, currently 64 byte)
   * allocated areas are stored in a function object, function objects can be forced to free allocated storage
   */
  CodeDataArea allocate(size_t data, size_t code);
  CodeDataArea shortenAllocation(CodeDataArea, size_t data, size_t code);
  void deallocate(CodeDataArea);

  bool check();
  size_t getTotalSpace();
  size_t getAllocatedSpace();
  size_t getFreeSpace();
  size_t getReachableFreeSpace();
  bool shrink();
  
private:
  template <class T, class U> static T* _diffcast(U* org, ptrdiff_t diff) { return reinterpret_cast<T*>(reinterpret_cast<u8*>(org) + diff); }
  template <class T, class U> static T* _cast(U* org) { return _diffcast<T,U>(org, 0); }

private:
  enum {
    ARENA_SHIFT = 17,
    SUM_ARENA_SHIFT = 22,
    LIST_BIN_SHIFT = 4,
    TREE_BIN_SHIFT = 8,
    MAX_BIN_SHIFT = ARENA_SHIFT - 1,
    MAX_LIST_CHUNK_SIZE = 1 << TREE_BIN_SHIFT,
    MAX_REQUEST_SHIFT = 15,
    LIST_BIN_COUNT = 1 << (TREE_BIN_SHIFT - LIST_BIN_SHIFT),
    TREE_BIN_COUNT = 2 * (ARENA_SHIFT - TREE_BIN_SHIFT),
    CBIT = 1,
    FBIT = 4,
    ALLBITS = 7,
  };
  
  struct _ListNode;
  struct _TreeNode;
  struct _SysNode;
  struct _BlockNode {
    size_t size, prevsize;
    _BlockNode* getPrev() { return _diffcast<_BlockNode>(this, -(ptrdiff_t)prevsize); }
    _BlockNode* getNext() { return _diffcast<_BlockNode>(this, getSize()); }
    bool knowsPrev() { return prevsize > 0; }
    bool isInUse() { return (size & CBIT) != 0; }
    void setFooterSize(ptrdiff_t sz) { getNext()->prevsize = sz; }
    void setSize(ptrdiff_t sz) { size = (size & ALLBITS) + sz; }
    void setCBitFalse() { size &= ~CBIT; }
    void setCBitTrue() { size |= CBIT; }
    void init() { size = 16 | CBIT; }
    size_t getSize() { return size & ~ALLBITS; }

    _ListNode* toList() { return _diffcast<_ListNode>(this, 0); }
    _TreeNode* toTree() { return _diffcast<_TreeNode>(this, 0); }
    _SysNode* toSys() { return _diffcast<_SysNode>(this, 0); }
    MemoryArea payload() { return MemoryArea(_diffcast<u8>(this, 16), getSize()-16); }
    CodeDataArea payload(size_t dsz) { CodeDataArea cda; 
                                       cda.data = MemoryArea(_diffcast<u8>(this, 16), dsz-16);
                                       cda.code = MemoryArea(_diffcast<u8>(this, dsz), getSize()-dsz);
                                       return cda; }
  };
  struct _ListNode : public _BlockNode {
    _ListNode *prev, *next;
    bool isLast() { return next == this; }
    void init() { prev = next = this; }
    void linkInAfter(_ListNode* n) { next = n->next; prev = n; n->next = this; next->prev = this; }
    void linkInBefore(_ListNode* n) { next = n; prev = n->prev; n->prev = this; prev->next = this; }
    void linkOut() { prev->next = next; next->prev = prev; }
  };
  struct _TreeNode : public _BlockNode {
    _TreeNode *prev, *next, *children[2], *parent;
    size_t binno;
    bool isLast() { return next == this; }
    void init() { prev = next = this; }
    void linkInAfter(_TreeNode* n) { next = n->next; prev = n; n->next = this; next->prev = this; }
    void linkInBefore(_TreeNode* n) { next = n; prev = n->prev; n->prev = this; prev->next = this; }
    void linkOut() { prev->next = next; next->prev = prev; }
  };
  struct _SysNode : public _BlockNode {
    _SysNode *prev, *next;
    bool touched;
    bool isLast() { return next == this; }
    void init() { prev = next = this; }
    void linkInAfter(_SysNode* n) { next = n->next; prev = n; n->next = this; next->prev = this; }
    void linkInBefore(_SysNode* n) { next = n; prev = n->prev; n->prev = this; prev->next = this; }
    void linkOut() { prev->next = next; next->prev = prev; }
  };
  
private:
  MemoryArea _allocSystemPages(size_t);
  void _freeSystemPages(MemoryArea);

  void _allocArena();

  static size_t _getListIndex(size_t S);
  static size_t _getTreeIndex(size_t S);
  static _TreeNode* _getLeaf(_TreeNode* n);
  static _TreeNode* _getSmallestSh(_TreeNode* chunk, size_t sh);
  static _TreeNode* _getSmallest(_TreeNode* chunk, size_t binno);
  static _TreeNode* _getBest(_TreeNode* chunk, size_t binno, size_t sz);
  static size_t _getFreeListSize(_ListNode* sentinel);
  static size_t _getFreeTreeSize(_TreeNode* sentinel);

  void _moveTreeNode(_TreeNode* next, _TreeNode* org);
  void _unlinkTreeNode(_TreeNode* chunk);
  void _linkTreeNode(_TreeNode* chunk);
  void _unlinkListNode(_ListNode* chunk);
  void _linkListNode(_ListNode* chunk);
  void _unlinkNode(_BlockNode* chunk);
  void _linkNode(_BlockNode* chunk);
  void _fuseAndLinkNode(_BlockNode* chunk);

  void _replaceVictim(_BlockNode* chunk);
  _BlockNode* _allocVictim(size_t);
  _BlockNode* _allocTop(size_t);
  
private:
  size_t _sizeOfPage, _alignmentOfPage, _cacheLineSize;

  // bins: 16, 32, 48, 64, ... (2^n, 2^n+2^(n-1), 2^(n+1), ...)
  u32 _listBinMap, _treeBinMap;
  _ListNode _listBins[LIST_BIN_COUNT];
  _TreeNode* _treeBins[TREE_BIN_COUNT];
  _TreeNode _root;
  _BlockNode* _victim, *_top;
  _SysNode _pageList;
};
} // End jitcs namespace

static size_t _IsZeroOrPowerOf2(size_t base) {
  return ((base & (base - 1)) == 0);
}
static size_t _RoundUpToPowerOf2(size_t base) {
  if (_IsZeroOrPowerOf2(base)) return base;
  --base;
  base = base | (base >> 1);
  base = base | (base >> 2);
  base = base | (base >> 4);
  base = base | (base >> 8);
  base = base | (base >> 16);
  if (sizeof(base) > 4)
    base = base | (base >> (sizeof(base) > 4 ? 32 : 0));
  return ++base;
}
static size_t _GetLowBitIndex(size_t X) {
#if defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__))
  unsigned int J = __builtin_ctz(X);
#elif defined (__INTEL_COMPILER)
  unsigned int J = _bit_scan_forward (X);
#elif defined(_MSC_VER) && _MSC_VER>=1300
  unsigned int J;
  _BitScanForward((DWORD *) &J, X);
#elif USE_BUILTIN_FFS
  unsigned int J = ffs(X)-1;
#else
  unsigned int Y = X - 1;
  unsigned int K = Y >> (16-4) & 16;
  unsigned int N = K;        Y >>= K;
  N += K = Y >> (8-3) &  8;  Y >>= K;
  N += K = Y >> (4-2) &  4;  Y >>= K;
  N += K = Y >> (2-1) &  2;  Y >>= K;
  N += K = Y >> (1-0) &  1;  Y >>= K;
  unsigned int J = (N + Y);
#endif
  return J;
}

jitcs::MemoryMgrImpl::MemoryMgrImpl() {
#if defined(JITCS_WINDOWS)
  SYSTEM_INFO info;
  GetSystemInfo(&info);

  _alignmentOfPage = info.dwAllocationGranularity;
  _sizeOfPage = _RoundUpToPowerOf2(info.dwPageSize);
#endif
#if defined(JITCS_POSIX)
  _alignmentOfPage = _sizeOfPage = getpagesize();
#endif
  _cacheLineSize = CPUInfo::GetHost().getCachelineSize();
  _listBinMap = 0;
  _treeBinMap = 0;
  int i;
  for (i = 0; i < LIST_BIN_COUNT; ++i)
    _listBins[i].init();
  for (i = 0; i < TREE_BIN_COUNT; ++i)
    _treeBins[i] = 0;
  _victim = 0;
  _top = 0;
  _pageList.init();
}
jitcs::MemoryMgrImpl::~MemoryMgrImpl() {
  while (!_pageList.isLast()) {
    _SysNode* sys = _pageList.next;
    sys->linkOut();
    _freeSystemPages(MemoryArea(_cast<u8>(sys), sys->getSize()));
  }
}

jitcs::MemoryMgrImpl::MemoryArea jitcs::MemoryMgrImpl::_allocSystemPages(size_t sz) {
  size_t msize = (sz + _sizeOfPage - 1) & -(ptrdiff_t)_sizeOfPage;
#if defined(JITCS_WINDOWS)
  WORD protect = PAGE_EXECUTE_READWRITE;
  void* mbase = VirtualAllocEx(GetCurrentProcess(), NULL, msize, MEM_COMMIT | MEM_RESERVE, protect);
  if (mbase == NULL) return MemoryArea();
#endif
#if defined(JITCS_POSIX)
  int protection = PROT_READ | PROT_WRITE | (executable ? PROT_EXEC : 0);
  void* mbase = mmap(NULL, msize, protection, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (mbase == MAP_FAILED) return _makeMemoryArea();
#endif
  return MemoryArea(reinterpret_cast<u8*>(mbase), msize);
}

void jitcs::MemoryMgrImpl::_freeSystemPages(MemoryArea ps) {
#if defined(JITCS_WINDOWS)
  VirtualFreeEx(GetCurrentProcess(), ps._ptr, 0, MEM_RELEASE);
#endif
#if defined(JITCS_POSIX)
  munmap(ps._ptr, ps.size());
#endif
}

void jitcs::MemoryMgrImpl::_allocArena() {
  MemoryArea ma = _allocSystemPages(1 << ARENA_SHIFT);

  _SysNode* sys = _diffcast<_SysNode>(ma._ptr,0);
  sys->prevsize = 0;
  sys->size = ma.size() | CBIT;
  sys->touched = true;
  sys->linkInBefore(&_pageList);
  _BlockNode* first = _diffcast<_BlockNode>(sys, _cacheLineSize);
  _BlockNode* last = _diffcast<_BlockNode>(sys, ma.size() - _cacheLineSize);
  first->prevsize = _cacheLineSize;
  first->size = ma.size() - 2 * _cacheLineSize | CBIT | FBIT;
  last->prevsize = 0;
  last->size = _cacheLineSize | CBIT;
  if (_top) {
    _fuseAndLinkNode(_top);
  }
  _top = first;
}

/* -------------------------- public routines ---------------------------- */

/* assign tree index for size S to variable I. Use x86 asm if possible  */
size_t jitcs::MemoryMgrImpl::_getListIndex(size_t S) {
  return (S >> LIST_BIN_SHIFT) - 1;
}
size_t jitcs::MemoryMgrImpl::_getTreeIndex(size_t S) {
  size_t X = S >> TREE_BIN_SHIFT;
  if (X == 0) return 0;
  if (X >= 1 << (TREE_BIN_COUNT/2)) return TREE_BIN_COUNT - 1;
#if defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__))
  unsigned int K = (unsigned) sizeof(X)*__CHAR_BIT__ - 1 - (unsigned) __builtin_clz(X);
#elif defined (__INTEL_COMPILER)
  unsigned int K = _bit_scan_reverse (X);
#elif defined(_MSC_VER) && _MSC_VER>=1300
  unsigned int K;
  _BitScanReverse((DWORD *) &K, (DWORD) X);
#else /* GNUC */
  unsigned int Y = (unsigned int)X;
  unsigned int N = ((Y - 0x100) >> 16) & 8;
  unsigned int K = (((Y <<= N) - 0x1000) >> 16) & 4;
  N += K;
  N += K = (((Y <<= K) - 0x4000) >> 16) & 2;
  K = 14 - N + ((Y <<= K) >> 15);
#endif
  return (K << 1) + ((S >> (K + (TREE_BIN_SHIFT-1)) & 1));
}
jitcs::MemoryMgrImpl::CodeDataArea jitcs::MemoryMgrImpl::allocate(size_t data, size_t code) {
  size_t dsz = (16 + data + _cacheLineSize - 1) & -(ptrdiff_t)_cacheLineSize;
  size_t csz = (code + _cacheLineSize - 1) & -(ptrdiff_t)_cacheLineSize;
  size_t sz = dsz + csz;
  /*
     Basic algorithm:
     If a small request (< 256 bytes minus per-chunk overhead):
       1. If one exists, use a remainderless chunk in associated smallbin.
          (Remainderless means that there are too few excess bytes to
          represent as a chunk.)
       2. If it is big enough, use the dv chunk, which is normally the
          chunk adjacent to the one used for the most recent small request.
       3. If one exists, split the smallest available chunk in a bin,
          saving remainder in dv.
       4. If it is big enough, use the top chunk.
       5. If available, get memory from system and use it
     Otherwise, for a large request:
       1. Find the smallest available binned chunk that fits, and use it
          if it is better fitting than dv chunk, splitting if necessary.
       2. If better fitting than any binned chunk, use the dv chunk.
       3. If it is big enough, use the top chunk.
       4. If request size >= mmap threshold, try to directly mmap this chunk.
       5. If available, get memory from system and use it

     The ugly goto's here ensure that postaction occurs along all paths.
  */

  if (sz <= MAX_LIST_CHUNK_SIZE) {
    size_t index = _getListIndex(sz);
    u32 map = _listBinMap >> index;
    if ((map & 3) != 0) {
      index += ~map & 1;
      _ListNode* chunk = _listBins[index].next;
      chunk->linkOut();
      if (_listBins[index].isLast())
        _listBinMap &= ~(1 << index);
      chunk->setCBitTrue();
      return chunk->payload(dsz);
    }
    if (_victim && sz <= _victim->getSize()) {
      _BlockNode* newchunk = _allocVictim(sz);
      return newchunk->payload(dsz);
    }
    if (map != 0) { /* Use chunk in next nonempty smallbin */
      index += _GetLowBitIndex(map);
      _ListNode* chunk = _listBins[index].next;
      chunk->linkOut();
      if (_listBins[index].isLast())
        _listBinMap &= ~(1 << index);
      _replaceVictim(chunk);
      _BlockNode* newchunk = _allocVictim(sz);
      return newchunk->payload(dsz);
    }
    if (_treeBinMap != 0) {
      index = _GetLowBitIndex(_treeBinMap);
      _TreeNode* chunk = _getSmallest(_treeBins[index], index)->next;
      _unlinkTreeNode(chunk);
      _replaceVictim(chunk);
      _BlockNode* newchunk = _allocVictim(sz);
      return newchunk->payload(dsz);
    }
    if (!_top || _top->getSize() < sz)
      _allocArena();
    _BlockNode* newchunk = _allocTop(sz);
    return newchunk->payload(dsz);
  } else {
    // todo: handle large requests...
    size_t index = _getTreeIndex(sz);
    u32 map = _treeBinMap >> index;
    _TreeNode* chunk = 0;
    if ((map & 1) != 0) {
      chunk = _getBest(_treeBins[index], index, sz);
      assert(!chunk || chunk->getSize() >= sz);
    }
    if (!chunk && map >= 2) {
      index += _GetLowBitIndex(map & ~1);
      chunk = _getSmallest(_treeBins[index], index);
    }
    if (chunk && (!_victim || _victim->getSize() <= sz || chunk->getSize() < _victim->getSize())) {
      chunk = chunk->next;
      _unlinkTreeNode(chunk);
      _replaceVictim(chunk);
      _BlockNode* newchunk = _allocVictim(sz);
      return newchunk->payload(dsz);
    } else if (_victim && sz <= _victim->getSize()) {
      _BlockNode* newchunk = _allocVictim(sz);
      return newchunk->payload(dsz);
    }
    if (!_top || _top->getSize() < sz)
      _allocArena();
    _BlockNode* newchunk = _allocTop(sz);
    return newchunk->payload(dsz);
  }
}

void jitcs::MemoryMgrImpl::_fuseAndLinkNode(_BlockNode* chunk) {
  size_t sz = chunk->getSize();
  _BlockNode* next = chunk->getNext();
  if (!next->isInUse()) {
    _unlinkNode(next);
    sz += next->getSize();
  }
  if (chunk->knowsPrev() && !chunk->getPrev()->isInUse()) {
    chunk = chunk->getPrev();
    sz += chunk->getSize();
    _unlinkNode(chunk);
  }
  chunk->setSize(sz);
  chunk->setCBitFalse();
  chunk->setFooterSize(sz);
  _linkNode(chunk);
}

jitcs::MemoryMgrImpl::CodeDataArea jitcs::MemoryMgrImpl::shortenAllocation
  (CodeDataArea mem, size_t data, size_t code) {
  return mem;
}

void jitcs::MemoryMgrImpl::deallocate(CodeDataArea x) {
  if (!x.data._ptr || !x.data.size() || x.data._ptr + x.data.size() != x.code._ptr) return;

  _BlockNode* chunk = _diffcast<_BlockNode>(x.data._ptr, -16);
  if ((chunk->size & CBIT) == 0) return;
  size_t sz = 16 + x.data.size() + x.code.size();
  if ((chunk->size & ~7) != sz) return;

  _fuseAndLinkNode(chunk);
}

jitcs::MemoryMgrImpl::_TreeNode* jitcs::MemoryMgrImpl::_getLeaf(_TreeNode* n) {
  if (n->children[0]) return _getLeaf(n->children[0]);
  if (n->children[1]) return _getLeaf(n->children[1]);
  return n;
}

void jitcs::MemoryMgrImpl::_moveTreeNode(_TreeNode* next, _TreeNode* org) {
  next->children[0] = org->children[0];
  if (next->children[0]) next->children[0]->parent = next;
  next->children[1] = org->children[1];
  if (next->children[1]) next->children[1]->parent = next;
  next->parent = org->parent;
  if (next->parent) {
    if (next->parent == &_root)
      _treeBins[org->binno] = next;
    else
      next->parent->children[next->parent->children[1] == org] = next;
  }
}

void jitcs::MemoryMgrImpl::_unlinkTreeNode(_TreeNode* chunk) {
  if (chunk->size & FBIT) {
    if (chunk->getSize() + 2 * _cacheLineSize == _diffcast<_SysNode>(chunk, -(ptrdiff_t)_cacheLineSize)->getSize()) {
      _diffcast<_SysNode>(chunk, -(ptrdiff_t)_cacheLineSize)->touched = true;
    }
  }
  if (!chunk->isLast()) {
    chunk->linkOut();
    if (chunk->parent != 0)
      _moveTreeNode(chunk->next, chunk);
    return;
  }
  assert(chunk->parent != 0);
  _TreeNode* leaf = _getLeaf(chunk);
  if (leaf == chunk) {
    if (_cast<_TreeNode>(leaf->parent) == &_root) {
      _treeBinMap &= ~(1 << chunk->binno);
      _treeBins[chunk->binno] = 0;
    } else
      chunk->parent->children[chunk->parent->children[1] == chunk] = 0;
  } else {
    leaf->parent->children[leaf->parent->children[1] == leaf] = 0;
    _moveTreeNode(leaf, chunk);
  }
}

void jitcs::MemoryMgrImpl::_linkTreeNode(_TreeNode* chunk) {
  size_t binno = _getTreeIndex(chunk->size);
  if ((_treeBinMap & (1 << binno)) == 0) {
    _treeBinMap |= (1 << binno);
    _treeBins[binno] = chunk;
    chunk->parent = &_root;
    chunk->children[0] = chunk->children[1] = 0;
    chunk->binno = binno;
    chunk->prev = chunk->next = chunk;
    return;
  }
  size_t sz = chunk->size;
  _TreeNode** r = &_treeBins[binno], *p = &_root;
  size_t sh = (binno >> 1) + TREE_BIN_SHIFT - 2;
  while (*r) {
    if ((*r)->size == sz) {
      chunk->linkInBefore(*r);
      chunk->parent = 0;
      chunk->binno = binno;
      return;
    }
    p = *r;
    r = &(*r)->children[(sz >> sh) & 1];
    sh--;
  }
  *r = chunk;
  chunk->parent = p;
  chunk->children[0] = chunk->children[1] = 0;
  chunk->binno = binno;
  chunk->prev = chunk->next = chunk;
}

jitcs::MemoryMgrImpl::_TreeNode* jitcs::MemoryMgrImpl::_getSmallestSh(_TreeNode* chunk, size_t sh) {
  _TreeNode* r = chunk, *best = r;
  size_t bestsize = ~0;
  while (r) {
    if (r->getSize() < bestsize) {
      best = r;
      bestsize = r->getSize();
    }
    if (r->children[0] != 0)
      r = r->children[0];
    else if (r->children[1] != 0 /*&& ((bestsize >> sh) & 1) != 0*/)
      r = r->children[1];
    else
      break;
//    sh--;
  }
  return best;
}
jitcs::MemoryMgrImpl::_TreeNode* jitcs::MemoryMgrImpl::_getSmallest(_TreeNode* chunk, size_t binno) {
  return _getSmallestSh(chunk, (binno >> 1) + TREE_BIN_SHIFT - 2);
}
jitcs::MemoryMgrImpl::_TreeNode* jitcs::MemoryMgrImpl::_getBest(_TreeNode* chunk, size_t binno, size_t sz) {
  _TreeNode* r = chunk, *best = 0, *reset = 0, *rt;
  size_t bestsize = ~0;
  size_t sh = (binno >> 1) + TREE_BIN_SHIFT - 2, sh2 = sh;
  while (r) {
    if (r->getSize() >= sz && r->getSize() < bestsize) {
      best = r;
      bestsize = r->getSize();
      if (bestsize == sz) return best;
    }
    rt = r->children[1];
    r = r->children[(sz >> sh) & 1];
    if (rt && rt != r) { reset = rt; sh2 = sh-1; }
    if (!r) {
      if (reset) {
        rt = _getSmallestSh(reset, sh2);
        if (rt->getSize() >= sz && rt->getSize() < bestsize)
          return rt;
      }
    }
  }
  return best;
}


void jitcs::MemoryMgrImpl::_unlinkListNode(_ListNode* chunk) {
  if (chunk->size & FBIT) {
    if (chunk->getSize() + 2 * _cacheLineSize == _diffcast<_SysNode>(chunk, -(ptrdiff_t)_cacheLineSize)->getSize()) {
      _diffcast<_SysNode>(chunk, -(ptrdiff_t)_cacheLineSize)->touched = true;
    }
  }
  size_t binno = _getListIndex(chunk->size);
  assert((_listBinMap & (1 << binno)) != 0);
  chunk->linkOut();
  if (_listBins[binno].isLast())
    _listBinMap &= ~(1 << binno);
}

void jitcs::MemoryMgrImpl::_linkListNode(_ListNode* chunk) {
  size_t binno = _getListIndex(chunk->size);
  chunk->linkInBefore(&_listBins[binno]);
  _listBinMap |= (1 << binno);
}

void jitcs::MemoryMgrImpl::_unlinkNode(_BlockNode* chunk) {
  if (chunk->getSize() <= MAX_LIST_CHUNK_SIZE)
    _unlinkListNode(chunk->toList());
  else
    _unlinkTreeNode(chunk->toTree());
}

void jitcs::MemoryMgrImpl::_linkNode(_BlockNode* chunk) {
  if (chunk->getSize() <= MAX_LIST_CHUNK_SIZE)
    _linkListNode(chunk->toList());
  else
    _linkTreeNode(chunk->toTree());
}

void jitcs::MemoryMgrImpl::_replaceVictim(_BlockNode* chunk) {
  chunk->setCBitTrue();
  chunk->setFooterSize(0);
  if (_victim) {
    _fuseAndLinkNode(_victim);
  }
  _victim = chunk;
}
jitcs::MemoryMgrImpl::_BlockNode* jitcs::MemoryMgrImpl::_allocVictim(size_t sz) {
  assert(_victim && _victim->getSize() >= sz);
  assert((sz & (_cacheLineSize - 1)) == 0);
  size_t vsz = _victim->getSize();
  if (vsz <= sz + _cacheLineSize) {
    _BlockNode* r = _victim;
    r->setFooterSize(vsz);
    r->setCBitTrue();
    _victim = 0;
    return r;
  }
  _BlockNode* r = _victim;
  r->setSize(sz);
  _victim = _diffcast<_BlockNode>(r, sz);
  _victim->size = (vsz - sz) | CBIT;
  _victim->prevsize = sz;
  assert(_victim->getNext()->prevsize == 0);
  return r;  
}
jitcs::MemoryMgrImpl::_BlockNode* jitcs::MemoryMgrImpl::_allocTop(size_t sz) {
  assert(_top && _top->getSize() >= sz);
  assert((sz & (_cacheLineSize - 1)) == 0);
  size_t vsz = _top->getSize();
  if (vsz <= sz + _cacheLineSize) {
    _BlockNode* r = _top;
    r->setFooterSize(vsz);
    r->setCBitTrue();
    _top = 0;
    return r;
  }
  _BlockNode* r = _top;
  r->setSize(sz);
  _top = _diffcast<_BlockNode>(r, sz);
  _top->size = (vsz - sz) | CBIT;
  _top->prevsize = sz;
  assert(_top->getNext()->prevsize == 0);
  return r;  
}

bool jitcs::MemoryMgrImpl::check() {
  _SysNode* sys = _pageList.next;
  bool foundvictim = false;
  bool foundtop = false;
  while (sys != &_pageList) {
    u8* top = _diffcast<u8>(sys, sys->getSize());
    size_t lastsize = 0;
    bool lastinuse = true;
    bool first = true;
    _BlockNode* n = _diffcast<_BlockNode>(sys, _cacheLineSize);
    while (_cast<u8>(n) < top) {
      if (n == _top) foundtop = true;
      if (n == _victim) foundvictim = true;
      if (first && (n->prevsize != _cacheLineSize)) {
        return false;
      }
      if (!first && !(n->prevsize == 0 || n->prevsize == lastsize)) {
        return false;
      }
      if (((n->size & FBIT) != 0) != first) {
        return false;
      }
      if (!((n->getSize() & (_cacheLineSize-1)) == 0)) {
        return false;
      }
      if (!(lastinuse || n->isInUse())) {
        return false;
      }
      lastsize = n->getSize();
      lastinuse = n->isInUse();
      n = n->getNext();
      first = false;
    }
    if (!(_cast<u8>(n) == top)) {
      return false;
    }
    sys = sys->next;
  }
  if ((_victim && !foundvictim) || (_top && !foundtop)) {
    return false;
  }

  int i;
  for (i = 0; i < LIST_BIN_COUNT; ++i) {
    bool bininuse1 = (_listBinMap & (1 << i)) != 0;
    bool bininuse2 = !_listBins[i].isLast();
    if (bininuse1 != bininuse2) {
      return false;
    }
  }
  for (i = 0; i < TREE_BIN_COUNT; ++i) {
    bool bininuse1 = (_treeBinMap & (1 << i)) != 0;
    bool bininuse2 = _treeBins[i] != 0;
    if (bininuse1 != bininuse2) {
      return false;
    }
  }
  return true;
}
size_t jitcs::MemoryMgrImpl::getAllocatedSpace() {
  size_t sum = 0;
  _SysNode* sys = _pageList.next;
  while (sys != &_pageList) {
    u8* top = _diffcast<u8>(sys, sys->getSize());
    _BlockNode* n = _diffcast<_BlockNode>(sys, _cacheLineSize);
    while (_cast<u8>(n) < top) {
      if (n->isInUse()) sum += n->getSize();
      n = n->getNext();
    }
    sys = sys->next;
  }
  return sum;
}
size_t jitcs::MemoryMgrImpl::getFreeSpace() {
  size_t sum = 0;
  _SysNode* sys = _pageList.next;
  while (sys != &_pageList) {
    u8* top = _diffcast<u8>(sys, sys->getSize());
    _BlockNode* n = _diffcast<_BlockNode>(sys, _cacheLineSize);
    while (_cast<u8>(n) < top) {
      if (!n->isInUse()) sum += n->getSize();
      n = n->getNext();
    }
    sys = sys->next;
  }
  return sum;
}
size_t jitcs::MemoryMgrImpl::_getFreeListSize(_ListNode* sentinel) {
  size_t sum = 0;
  _ListNode* n = sentinel->next;
  while (n != sentinel) {
    sum += n->getSize();
    n = n->next;
  }
  return sum;
}
size_t jitcs::MemoryMgrImpl::_getFreeTreeSize(_TreeNode* sentinel) {
  size_t sum = 0;
  if (!sentinel) return sum;
  assert(sentinel->parent != 0);
  _TreeNode* n = sentinel->next;
  sum += sentinel->getSize();
  while (n != sentinel) {
    sum += n->getSize();
    n = n->next;
  }
  if (sentinel->children[0])
    sum += _getFreeTreeSize(sentinel->children[0]);
  if (sentinel->children[1])
    sum += _getFreeTreeSize(sentinel->children[1]);
  return sum;
}
size_t jitcs::MemoryMgrImpl::getReachableFreeSpace() {
  size_t sum = 0;
  int i;
  for (i = 0; i < LIST_BIN_COUNT; ++i) {
    sum += _getFreeListSize(&_listBins[i]);
  }
  for (i = 0; i < TREE_BIN_COUNT; ++i) {
    sum += _getFreeTreeSize(_treeBins[i]);
  }
  return sum;
}
size_t jitcs::MemoryMgrImpl::getTotalSpace() {
  size_t sum = 0;
  _SysNode* sys = _pageList.next;
  while (sys != &_pageList) {
    sum += sys->getSize();
    sys = sys->next;
  }
  return sum;
}
bool jitcs::MemoryMgrImpl::shrink() {
  bool freedpages = false;
  _SysNode* sys = _pageList.next;
  while (sys != &_pageList) {
    bool isempty = false;
    _BlockNode* first = _diffcast<_BlockNode>(sys, _cacheLineSize);
    if (!first->isInUse() && first->getSize() == sys->getSize() - 2 * _cacheLineSize)
      isempty = true;
    if (isempty && !sys->touched) {
      _SysNode* next = sys->next;
      _unlinkNode(first);
      sys->linkOut();
      _freeSystemPages(MemoryArea(_cast<u8>(sys), sys->getSize()));
      freedpages = true;
      sys = next;
    } else {
      sys->touched = !isempty;
      sys = sys->next;
    }
  }
  return freedpages;
}

jitcs::MemoryMgr::MemoryMgr() 
  : _mgr(new MemoryMgrImpl) {
}
jitcs::MemoryMgr::~MemoryMgr() {
}

jitcs::MemoryMgr::CodeAndData jitcs::MemoryMgr::allocate(size_t data, size_t code) {
  return _mgr->allocate(data, code);
}
void jitcs::MemoryMgr::deallocate(CodeAndData cad) {
  _mgr->deallocate(cad);
}
jitcs::MemoryMgr::CodeAndData jitcs::MemoryMgr::shortenAllocation
    (jitcs::MemoryMgr::CodeAndData mem, size_t data, size_t code) {
  return _mgr->shortenAllocation(mem, data, code);
}
bool jitcs::MemoryMgr::check() {
  return _mgr->check();
}
size_t jitcs::MemoryMgr::getTotalSpace() {
  return _mgr->getTotalSpace();
}
size_t jitcs::MemoryMgr::getAllocatedSpace() {
  return _mgr->getAllocatedSpace();
}
size_t jitcs::MemoryMgr::getFreeSpace() {
  return _mgr->getFreeSpace();
}
size_t jitcs::MemoryMgr::getReachableFreeSpace() {
  return _mgr->getReachableFreeSpace();
}
bool jitcs::MemoryMgr::shrink() {
  return _mgr->shrink();
}
void jitcs::MemoryMgr::_release() {
  assert(_refCnt == 0);
  delete this;
}
