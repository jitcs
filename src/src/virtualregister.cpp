//===-- src/src/virtualregister.cpp -----------------------------*- C++ -*-===//
// The actual definition of the VirtualRegister class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// While client code uses VirtualRegister a lot, reference are enough for it.
// The several analysis steps need a bit more information.
//===----------------------------------------------------------------------===//

#include "jitcs_int_virtualregister.h"
#include "jitcs_machine.h"
#include "jitcs_int_machine.h"

namespace jitcs {
  RegClassId GetRegClass(const VirtualRegister* r);
}

void jitcs::VirtualRegister::setupDynamic(IMachineInfo& mi, size_t vregno, RegClassId rc, u8 logSz_) {
  Ref<const IMachineDetails> mid = mi.details();
  id = static_cast<RegId>(R_HardwareLimit + vregno);
  res = static_cast<ResId>(mid->getResCount() + vregno);
  regclass = rc;
  resclass = mid->getResClassOfRegClass(rc);
  logSz = logSz_ ? logSz_ : mid->getLogSizeOfRegClass(rc);
}

jitcs::RegClassId jitcs::GetRegClass(const VirtualRegister* r) {
  return r ? r->regclass : RCL_None;
}
