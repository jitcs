//===-- src/src/machine.cpp -------------------------------------*- C++ -*-===//
// More target machine info.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// IMachineDetails contain all information and methods necessary for
// target dependent dataflow analysis or register allocation.
//===----------------------------------------------------------------------===//

#include "jitcs_machine.h"
#include "jitcs_int_machine.h"
#include "jitcs_function.h"
#include "jitcs_int_function_impl.h"

jitcs::IMachineInfo::IMachineInfo(const CPUInfo& c) 
  : cpu(c) {
}

void jitcs::IMachineInfo::_release() {
  assert(_refCnt == 0);
  delete this;
}

std::unique_ptr<jitcs::Function> jitcs::IMachineInfo::createFnc
    (RefCounter<TempAllocator> alloc, 
     std::shared_ptr<const CallingConvention> cc) {
  jitcs::FunctionImpl* f = new FunctionImpl(alloc, cc);
  return std::unique_ptr<jitcs::Function>(reinterpret_cast<jitcs::Function*>(f));
}

size_t jitcs::IMachineDetails::getInsIdInfo(InsId id) const {
  // right now, we didn't define any instruction which are not hardware
  return 0;
}

void jitcs::IMachineDetails::buildCtrlFlow
    (Ref<Instruction> ins, Ref<BasicBlockImpl> src) const{
  assert(0);
}
