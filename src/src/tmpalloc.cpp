//===-- src/src/tmpalloc.cpp ------------------------------------*- C++ -*-===//
// Never-free allocator classes.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// TempAllocator allocates data en bloc, and releases it all at once.
// No destructors are called on release.
// StreamAllocator speeds up allocating multiple objects with enhanced 
// alignment requirements.
//===----------------------------------------------------------------------===//

#include "jitcs_tmpalloc.h"

#include <stdio.h>

#ifdef JITCS_DEBUG
void dumpTmpAllocState(jitcs::TempAllocator& a,
                       std::list< std::vector<jitcs::u8> >& blocks,
                       std::list< std::vector<jitcs::u8> >::iterator curblock,
                       jitcs::u8* cur, size_t rem, size_t allspc, size_t refcnt) {
  printf("TempAllocator %p (refcnt %d) state:\n", &a, refcnt);
  printf("  blocks:");
  bool foundcurblock = false;
  for (auto i = blocks.begin(), e = blocks.end(); i != e; ++i) {
    if (i->size() > 0)
      printf(" (%p,0x%0x)", &(*i)[0], i->size());
    else
      printf(" (-,0)");
    if (i == curblock) {
      foundcurblock = true;
      printf("=cur");
    }
  }
  if (!foundcurblock && curblock != blocks.end()) {
    printf(" (cur is invalid)");
  }
  printf("\n");
  printf("  workdata: ptr %p, rem 0x%0x, alloc space 0x%0x\n", cur, rem, allspc);
}
#endif

jitcs::TempAllocator::TempAllocator() 
  : _cur(nullptr)
  , _rem(0)
  , _allocatedSpace(0)
  , _refCnt(0)
{}
jitcs::TempAllocator::~TempAllocator()
{}

jitcs::u8* jitcs::TempAllocator::_alloc(size_t sz, size_t al) {
  _JITCS_ALWAYS_CHECK_(((sz | al) & (al - 1)) == 0);
  if (sz <= _rem) {
    uptr padding = ((((uptr)_cur) + (al - 1)) & -al) - (uptr)_cur;
    if (padding + sz <= _rem) {
      u8* res = _cur + padding;
      _cur = _cur + (padding + sz);
      _rem -= padding + sz;
      return res;
    }
  }
  if (_cur != nullptr) {
    ++_curblock;
    if (_curblock != _blocks.end()) {
      _cur = &(*_curblock)[0];
      _rem = (*_curblock).size();
      return _alloc(sz, al);
    }
  }
  _curblock = _blocks.insert(_blocks.end(), std::vector<u8>());
  size_t newspace = (std::max(sz + al, _allocatedSpace + 1) + MINIMUM_BLOCK_SIZE - 1) & -MINIMUM_BLOCK_SIZE;
  (*_curblock).resize(newspace);
  _allocatedSpace += newspace;
  _cur = &(*_curblock)[0];
  _rem = (*_curblock).size();
  return _alloc(sz, al);
}
void jitcs::TempAllocator::_clean(bool keepbest) {
  BlockList::iterator i = _blocks.begin();
  size_t rem = 0;
  while (i != _blocks.end()) {
    if (keepbest && (*i).size() * 2 > _allocatedSpace) {
      ++i;
      rem = (*i).size();
    } else
      i = _blocks.erase(i);
  }
  _allocatedSpace = rem;
  if (_blocks.empty()) {
    _cur = nullptr;
    _rem = 0;
  } else {
    _curblock = _blocks.begin();
    _cur = &(*_curblock)[0];
    _rem = (*_curblock).size();
  }
}
