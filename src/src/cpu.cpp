//===-- src/src/cpu.cpu -----------------------------------------*- C++ -*-===//
// CPU feature description and detection.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// Sets up an architecture and relevant feature flags. Has the architecture
// A_Host, which will run a feature detection of the host running the 
// applicaton.
//===----------------------------------------------------------------------===//

#include "jitcs_base.h"
#include "jitcs_cpu.h"
//#include <string.h>
#include <stdlib.h>
#include <memory.h>

// ------------------------------------------
static inline bool _TestBit(int v, int bit) {
   return (v & (1 << bit)) != 0;
}

#ifdef JITCS_X86
typedef jitcs::u32 DWORD;

namespace {
  enum {
    OS_SUPPORT_SSE= 1,
    OS_SUPPORT_AVX= 2,
  };
  struct X86CPUinfo {
    DWORD maxbasic;
    DWORD features1;
    DWORD features2;
    DWORD features3;
    DWORD max7;
    DWORD maxextended;
    DWORD ext_features1;
    DWORD ext_features2;
    DWORD processor;
    char  proc_idstr[16];
    char  proc_namestr[48];
 //   DWORD proc_cache_l1[4];
 //   DWORD proc_cache_l2[4];
    unsigned char os_support_flags;
    DWORD clflush_logcores, physcores;
    //DWORD monitorlinesizemin, monitorlinesizemax, cacheinfo;
  };
  struct dw4 { DWORD w_eax, w_ecx, w_edx, w_ebx; };
}

// =====================================

#if _MSC_VER >= 1400
#include <intrin.h>
static dw4 _CPUID(int n, int m = 0)
{
   dw4 result;

//   int _data[4];
//   __cpuid(_data, n);
   __asm {
     mov eax, n
     mov ecx, m
     cpuid
     mov result.w_eax, eax
     mov result.w_ebx, ebx
     mov result.w_ecx, ecx
     mov result.w_edx, edx
   }
     
//   result.w_eax= _data[0];
//   result.w_ebx= _data[1];
//   result.w_ecx= _data[2];
//   result.w_edx= _data[3];
   return result;
}
static void _TestSSE() {
   __asm { orps xmm0,xmm0 }
}
static bool _TestAVX() {
  DWORD a, d;
  __asm {
    xor ecx, ecx
    xgetbv
    mov a, eax
    mov d, edx
  }
  return (a & 6) == 6;
}
#elif defined(_MSC_VER)
#error Currently only newer VC++ supported
static dw4 _CPUID(int n) {
   dw4 result;
   __asm {
      push ebx;
      mov eax,n;
      cpuid;
      mov result.w_eax, eax;
      mov result.w_ebx, ebx;
      mov result.w_ecx, ecx;
      mov result.w_edx, edx;
      pop ebx,
   }
   return result;
}
static void _TestSSE() {
   __asm { orps xmm0,xmm0: emit ...; emit ... }
}
static void _TestSSE2() {
   __asm { orpd xmm0,xmm0 }
}
#elif defined(__GNUC__)
#include <cpuid.h>
#include <immintrin.h>
static dw4 _CPUID(int n, int m = 0)
{
   dw4 result;
   __cpuid_count(n, m, result.w_eax, result.w_ebx, result.w_ecx, result.w_edx);
   return result;
}
static void _TestSSE() {
   __asm__ ( "orps %xmm0,%xmm0" );
}
static bool _TestAVX() {
  DWORD a, d;
  __asm__ ("xgetbv"
	   : "=a" (a), "=d" (d)
	   : "c" (0));
  return (a & 6) == 6;
}
#else
#error Unrecognized compiler
#endif

static int _S_DetectBaseX86 (X86CPUinfo& _cpuinfo) {
  memset(&_cpuinfo, 0, sizeof(_cpuinfo));

  try  {
    {
      dw4 cpuid0 = _CPUID(0);
      _cpuinfo.maxbasic = cpuid0.w_eax;
      DWORD* pstr = (DWORD*)_cpuinfo.proc_idstr;
      pstr[0] = cpuid0.w_ebx;
      pstr[1] = cpuid0.w_edx;
      pstr[2] = cpuid0.w_ecx;
    }

    if (_cpuinfo.maxbasic >= 1) {
      dw4 cpuid1 = _CPUID(1);
      // w_eax: (according to intel/amd)
      // [ 3- 0]: stepping
      // [ 7- 4]: model
      // [11- 8]: family
      // [13-12]: intel:processor type; amd:reserved
      // [19-16]: extended model 
      //          mode = bitconcat(extmodel, model)
      //          (intel: only when family is 0x6 or 0xf; amd: only if family is 0xf )
      // [27-20]: extended family
      //          family = family + extended family (only when family is 0xf)
      // w_ebx: (according to intel)
      // [ 7- 0]: brand index
      // [15- 8]: clflush line size (in 8 bytes)
      // [23-16]: max number of logical processors in this phys. package
      // [31-24]: initial apic id
      // w_ecx: (according to intel)
      // w_edx: (according to intel)
      _cpuinfo.processor = cpuid1.w_eax;
      _cpuinfo.clflush_logcores = cpuid1.w_ebx;
      _cpuinfo.features1 = cpuid1.w_edx;
      _cpuinfo.features2 = cpuid1.w_ecx;
    }
    if (_cpuinfo.maxbasic >= 2) { // amd reserved
      // dw4 cpuid2= _CPUID(2);
      // w_*: (according to intel) 
      // cache & tlb infos
    }
    if (_cpuinfo.maxbasic >= 3) { // amd reserved
      // dw4 cpuid3= _CPUID(3);
      // w_eax,w_ebx: (according to intel) 
      //    reserved
      // w_ecx: bits 0-31 of serial#, in pentium iii only
      // w_edx: bits 32-63 of serial#, in pentium iii only
      // valid only if PSN feature enabled
    }
    if (_cpuinfo.maxbasic >= 4) { // amd reserved
      // dw4 cpuid4= _CPUID2(4,X);
      // output depends on initial value of ecx(=X)
      // visible only when IA32_MISC_ENABLES.BOOT_NT4[bit 22] = 0
      // w_eax: (according to intel)
      // [ 4- 0]: cache type field
      //          {0:none;1:data;2:instruction;3:unified;>=4:reserved}
      // [ 7- 5]: cache level (starts @ 1)
      //    [ 8]: cache needs no SW-init
      //    [ 9]: cache fully associative
      //    [10]: WBINVD/INVD {0:ins act upon lower-level caches; 1:not guaranteed}
      //    [11]: inclusive {0:doesn't include lower-level caches; 1:does}
      // [13-12]: reserved
      // [25-14]:* max number of threads this cache in phys. package
      // [31-26]:* max number of cores in phys. package
      // w_ebx: (according to intel)
      // [11- 0]:* system coherency line size (?)
      // [21-12]:* physical line partitions
      // [31-22]:* ways of associativity
      // w_ecx: (according to intel)
      //    number of sets (?)
      // w_edx: (according to intel)
      //    reserved
      // *: +1 to get result.
    }
    if (_cpuinfo.maxbasic >= 5) { // amd: eax, ebx: the same
      //dw4 cpuid5= _CPUID(5);
      // w_eax: (according to intel)
      // [15- 0]: smallest monitor-line size in bytes
      // [31-16]: reserved
      // w_ebx: (according to intel)
      // [15- 0]: largest monitor-line size in bytes
      // [31-16]: reserved
      // w_ecx: (according to intel)
      //    ???
      // w_edx: (according to intel)
      //    ???
      // *: +1 to get result.
      //_cpuinfo.monitorlinesizemin = cpuid5.w_eax;
      //_cpuinfo.monitorlinesizemax = cpuid5.w_ebx;
    }
    if (_cpuinfo.maxbasic >= 6) {
      // turbo boost stuff
    }
    if (_cpuinfo.maxbasic >= 7) {
      dw4 cpuid7_0= _CPUID(7, 0);
      // w_eax: (according to intel)
      // [15- 0]: smallest monitor-line size in bytes
      // [31-16]: reserved
      // w_ebx: (according to intel)
      // [15- 0]: largest monitor-line size in bytes
      // [31-16]: reserved
      // w_ecx: (according to intel)
      //    ???
      // w_edx: (according to intel)
      //    ???
      // *: +1 to get result.
      _cpuinfo.max7 = cpuid7_0.w_eax;
      _cpuinfo.features3 = cpuid7_0.w_ebx;
    }
    if (_cpuinfo.maxbasic >= 9) {
      // direct cache access
    }
    if (_cpuinfo.maxbasic >= 10) {
      // architectural performance monitoring
    }
    if (_cpuinfo.maxbasic >= 11) {
      // extended topology
    }
    if (_cpuinfo.maxbasic >= 13) {
      // extended state enumeration, e.g. size for sse/avx storage area
    }
    DWORD high = 0x80000000;
    if (_cpuinfo.maxbasic > 0) {
      dw4 cpuidf0 = _CPUID(high);
      _cpuinfo.maxextended = cpuidf0.w_eax;
    }
    if ((_cpuinfo.maxextended & high) > 0) {
      if (_cpuinfo.maxextended >= high + 1) {
        dw4 cpuidf1 = _CPUID(high + 1);
        _cpuinfo.ext_features1 = cpuidf1.w_edx;
        _cpuinfo.ext_features2 = cpuidf1.w_ecx;
      }
      if (_cpuinfo.maxextended >= high + 4) {
        dw4 cpuidf2 = _CPUID(high + 2);
        dw4 cpuidf3 = _CPUID(high + 3);
        dw4 cpuidf4 = _CPUID(high + 4);
        DWORD* pstr = (DWORD*)_cpuinfo.proc_namestr;
        pstr[0] = cpuidf2.w_eax;
        pstr[1] = cpuidf2.w_ebx;
        pstr[2] = cpuidf2.w_ecx;
        pstr[3] = cpuidf2.w_edx;
        pstr[4] = cpuidf3.w_eax;
        pstr[5] = cpuidf3.w_ebx;
        pstr[6] = cpuidf3.w_ecx;
        pstr[7] = cpuidf3.w_edx;
        pstr[8] = cpuidf4.w_eax;
        pstr[9] = cpuidf4.w_ebx;
        pstr[10]= cpuidf4.w_ecx;
        pstr[11]= cpuidf4.w_edx;
      }
      if (_cpuinfo.maxextended >= high + 5) {
        // intel: reserved
        // amd: L1 cache info
        // proc_cache_l1_info: eax,ebx,ecx,edx (in that order)
      }
      if (_cpuinfo.maxextended >= high + 6) {
        //dw4 cpuidf6= _CPUID(high+6);
        // intel:
        // amd: L2 cache info
        // proc_cache_l2_info: eax,ebx,ecx,edx (in that order)
        //_cpuinfo.cacheinfo = cpuidf6.w_ecx;
      }
    }
  }
  catch (...) {
    memset(&_cpuinfo,0,sizeof(_cpuinfo));
    return -1;
  }
  if (_TestBit(_cpuinfo.features1, 25)) {
    try {
      _TestSSE();
      _cpuinfo.os_support_flags |= OS_SUPPORT_SSE;

      if (_TestBit(_cpuinfo.features2, 27) && _TestAVX())
        _cpuinfo.os_support_flags |= OS_SUPPORT_AVX;
    }
    catch (...) {}
  }
  return 1;
}
#endif

// =====================================

void jitcs::CPUInfo::_initializeFromHost() {
  static int _s_flagHasRun = 0;
  _setOfFeats.clear();
  _sizeOfPreferredAlignment= 16;
  _sizeOfCacheline= 64;
  _countOfPhysCores = 1;
  _countOfLogCores = 1;

#ifdef JITCS_X86
  static X86CPUinfo _s_x86cpuinfo;
  _arch = sizeof(void*) == 8 ? A_X86_64 : A_X86_32;  
  if (_s_flagHasRun == 0)
    _s_flagHasRun= _S_DetectBaseX86(_s_x86cpuinfo);
#else
  _s_flagHasRun = -1;
#endif

  if (_s_flagHasRun == -1)
    return; // no CPUID

#ifdef JITCS_X86
  _setFeat(F_X86RDTSC, _TestBit(_s_x86cpuinfo.features1, 4));
  _setFeat(F_X86CMPXCHG8B, _TestBit(_s_x86cpuinfo.features1, 8));
  _setFeat(F_X86CMOV, _TestBit(_s_x86cpuinfo.features1, 15));
  _setFeat(F_X86CLFLUSH, _TestBit(_s_x86cpuinfo.features1, 19));
  //_setFeat(F_MULTITHREADING, _TestBit(_s_x86cpuinfo.features1, 28));
  //_setFeat(F_MONITOR, _TestBit(_s_x86cpuinfo.features2, 3));
  _setFeat(F_X86CMPXCHG16B, _TestBit(_s_x86cpuinfo.features2, 13) && sizeof(void*) == 8);
  _setFeat(F_X86POPCNT, _TestBit(_s_x86cpuinfo.features2, 23));
  _setFeat(F_X86RDRAND, _TestBit(_s_x86cpuinfo.features2, 30)); // amd 9/2010: reserved
  _setFeat(F_X86BMI1, _TestBit(_s_x86cpuinfo.features3, 3));
  // amd 9/2010: not defined
  _setFeat(F_X86BMI2, _TestBit(_s_x86cpuinfo.features3, 8)); 
  // old intel confirms, newest avx2 says reserved
  _setFeat(F_X86RDTSCP, _TestBit(_s_x86cpuinfo.ext_features1, 27)); 
  _setFeat(F_X86LSAHF64, _TestBit(_s_x86cpuinfo.ext_features2, 0));
  // intel avx2 doc is not clear which register it uses
  _setFeat(F_X86LZCNT, _TestBit(_s_x86cpuinfo.ext_features2, 5)); 
  //_setFeat(F_LWP_AMD, _TestBit(_s_x86cpuinfo.ext_features2,15)); // amd only
  //_setFeat(F_TBM_AMD, _TestBit(_s_x86cpuinfo.ext_features2,21)); // amd only

  //_setFeat(F_MMX,  _TestBit(_s_x86cpuinfo.features1,23));
  // FXSAVE/FXRSTOR: _TestBit(_s_x86cpuinfo.features1,24));
  //_setFeat(F_MMXEXT, _TestBit(_s_x86cpuinfo.features1,25)
  //                   || _TestBit(_s_x86cpuinfo.ext_features1,22));
  // 3dnow is abandoned by amd
  if ((_s_x86cpuinfo.os_support_flags & OS_SUPPORT_SSE) != 0) {
    _setFeat(F_X86SSE,  _TestBit(_s_x86cpuinfo.features1, 25));
    _setFeat(F_X86SSE2, _TestBit(_s_x86cpuinfo.features1, 26));
    _setFeat(F_X86SSE3, _TestBit(_s_x86cpuinfo.features2, 0));
    _setFeat(F_X86PCLMULQDQ, _TestBit(_s_x86cpuinfo.features2, 1));
    _setFeat(F_X86SSSE3, _TestBit(_s_x86cpuinfo.features2, 9));
    _setFeat(F_X86SSE41, _TestBit(_s_x86cpuinfo.features2, 19));
    _setFeat(F_X86SSE42, _TestBit(_s_x86cpuinfo.features2, 20));
    _setFeat(F_X86AES, _TestBit(_s_x86cpuinfo.features2, 25));
    // XSAVE/...: _TestBit(_s_x86cpuinfo.features2,26)
    // OSXSAVE/...: _TestBit(_s_x86cpuinfo.features2,27)
    _setFeat(F_X86F16C, _TestBit(_s_x86cpuinfo.features2, 29));
    // amd only
    //_setFeat(F_XOP_AMD, _TestBit(_s_x86cpuinfo.ext_features2,11));
    //_setFeat(F_FMA4_AMD, _TestBit(_s_x86cpuinfo.ext_features2,16)); 
    //_setFeat(F_MISALIGNEDSSE, _TestBit(_s_x86cpuinfo.ext_features2,7)); 
  }
  if ((_s_x86cpuinfo.os_support_flags & OS_SUPPORT_AVX) != 0) {
    _setFeat(F_X86AVX, _TestBit(_s_x86cpuinfo.features2, 28));
    // amd 9/2010: not defined
    _setFeat(F_X86AVX2, _TestBit(_s_x86cpuinfo.features3, 5)); 
    _setFeat(F_X86FMA3, _TestBit(_s_x86cpuinfo.features2, 12));
  }

  if (((_s_x86cpuinfo.clflush_logcores >> 8) & 0xff) > 0) {
    _sizeOfCacheline = 8 * ((_s_x86cpuinfo.clflush_logcores >> 8) & 0xff);
  }
  if (_TestBit(_s_x86cpuinfo.features1, 28)
      && ((_s_x86cpuinfo.clflush_logcores >> 16) & 0xff) > 0) {
    _countOfLogCores = ((_s_x86cpuinfo.clflush_logcores >> 16) & 0xff);
  }

#endif
}
void jitcs::CPUInfo::_setFeat(FeatId f, bool b) {
  _JITCS_DBG_CHECK_(f >= 0 && f < F_COUNT);
  _setOfFeats.setBit(f, b);
}
void jitcs::CPUInfo::enableFeat(FeatId f) {
  _JITCS_DBG_CHECK_(f >= 0 && f < F_COUNT);
  _setOfFeats.setBit(f, true);
  switch (f) {
  case F_X86SSE2:
    if (!hasX86SSE()) enableFeat(F_X86SSE);
    break;
  case F_X86SSE3:
    if (!hasX86SSE2()) enableFeat(F_X86SSE2);
    break;
  case F_X86SSSE3:
    if (!hasX86SSE3()) enableFeat(F_X86SSE3);
    break;
  case F_X86SSE41:
    if (!hasX86SSSE3()) enableFeat(F_X86SSSE3);
    break;
  case F_X86SSE42:
    if (!hasX86SSE41()) enableFeat(F_X86SSE41);
    break;
  case F_X86AVX:
    if (!hasX86SSE42()) enableFeat(F_X86SSE42);
    break;
  case F_X86AVX2:
    if (!hasX86AVX()) enableFeat(F_X86AVX);
    break;
  case F_X86AES:
    if (!hasX86SSE()) enableFeat(F_X86SSE);
    break;
  case F_X86PCLMULQDQ:
    if (!hasX86SSE()) enableFeat(F_X86SSE);
    break;
  case F_X86FMA3:
    if (!hasX86AVX()) enableFeat(F_X86AVX);
    break;
  case F_X86F16C:
    if (!hasX86SSE()) enableFeat(F_X86SSE);
    break;
  default:
    break;
  }  
}
void jitcs::CPUInfo::disableFeat(FeatId f) {
  _JITCS_DBG_CHECK_(f >= 0 && f < F_COUNT);
  _setOfFeats.setBit(f, false);
  switch (f) {
  case F_X86SSE:
    if (hasX86SSE2()) disableFeat(F_X86SSE2);
    if (hasX86AES()) disableFeat(F_X86AES);
    if (hasX86PCLMULQDQ()) disableFeat(F_X86PCLMULQDQ);
    if (hasX86F16C()) disableFeat(F_X86F16C);
    break;
  case F_X86SSE2:
    if (hasX86SSE3()) disableFeat(F_X86SSE3);
    break;
  case F_X86SSE3:
    if (hasX86SSSE3()) disableFeat(F_X86SSSE3);
    break;
  case F_X86SSSE3:
    if (hasX86SSE41()) disableFeat(F_X86SSE41);
    break;
  case F_X86SSE41:
    if (hasX86SSE42()) disableFeat(F_X86SSE42);
    break;
  case F_X86SSE42:
    if (hasX86AVX()) disableFeat(F_X86AVX);
    break;
  case F_X86AVX:
    if (hasX86AVX2()) disableFeat(F_X86AVX2);
    if (hasX86FMA3()) disableFeat(F_X86FMA3);
    break;
  default:
    break;
  }  
}
void jitcs::CPUInfo::setPreferredAlignment(uint al) {
  _sizeOfPreferredAlignment = al;
}
void jitcs::CPUInfo::setCachelineSize(uint cs) {
  _sizeOfCacheline = cs;
}
void jitcs::CPUInfo::setCoreCount(uint phys, uint log) {
  _countOfPhysCores = phys;
  _countOfLogCores = log;
}

jitcs::CPUInfo::CPUInfo(ArchitectureId a) 
  : _arch(a)
  , _sizeOfPreferredAlignment(16)
  , _sizeOfCacheline(32)
  , _countOfPhysCores(1)
  , _countOfLogCores(1) {
  if (_arch == A_Host)
    _initializeFromHost();
}
jitcs::CPUInfo jitcs::CPUInfo::_hostCPUInfo(A_Host);
