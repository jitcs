//===-- src/jitcs_int_function_impl.h ---------------------------*- C++ -*-===//
// Function implementation class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_FUNCTION_IMPL_H_
#define _JITCS_INT_FUNCTION_IMPL_H_

#include "jitcs_adt_range.h"
#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"
#include "jitcs_int_adt_tmpvector.h"
#include "jitcs_memmgr.h"
#include "jitcs_machine.h"
#include "jitcs_function.h"

namespace jitcs {
class BasicBlockImpl;
class CallingConvention;
class IMachineDetails;
class MachineDumper;
class TempAllocator;
struct VirtualRegister;
//struct XFragmentList;
//class BBlockClusterMap;

class FunctionImpl : public Function {
public:
  typedef TmpVector<BasicBlockImpl*, 16> BasicBlockList;
  typedef TmpVector<VirtualRegister*, 16> VirtualRegisterList;
  typedef TmpVector<const VirtualRegister*, 16> ConstVirtualRegisterList;

  typedef BasicBlockList::iterator bb_iterator;
  typedef BasicBlockList::const_iterator const_bb_iterator;
  typedef Range<bb_iterator> bb_range;
  typedef ConstRange<const_bb_iterator> const_bb_range;

  typedef VirtualRegisterList::iterator vreg_iterator;
  typedef VirtualRegisterList::const_iterator const_vreg_iterator;
  typedef Range<vreg_iterator> vreg_range;
  typedef ConstRange<const_vreg_iterator> const_vreg_range;

  struct FrameRegisterInfo {
    RefOrNull<const VirtualRegister> reg;
    int ofs;
  };
  /*[[override]]*/ virtual FunctionImpl& impl() { return *this; }
  /*[[override]]*/ virtual const FunctionImpl& impl() const { return *this; }
  
public:
  FunctionImpl(RefCounter<TempAllocator>& ins, const std::shared_ptr<const CallingConvention>& cc);
  ~FunctionImpl();

  size_t bbs_size() const { return _bblocks.size(); }
  bb_iterator bbs_begin() { return _bblocks.begin(); }
  bb_iterator bbs_end() { return _bblocks.end(); }
  const_bb_iterator bbs_begin() const { return bbs_cbegin(); }
  const_bb_iterator bbs_end() const { return bbs_cend(); }
  const_bb_iterator bbs_cbegin() const { return _bblocks.begin(); }
  const_bb_iterator bbs_cend() const { return _bblocks.end(); }

  bb_range bbs_range() { return bb_range(bbs_begin(), bbs_end()); }
  const_bb_range bbs_range() const { return bbs_crange(); }
  const_bb_range bbs_crange() const { return const_bb_range(bbs_cbegin(), bbs_cend()); }

  size_t vregs_size() const { return _vregs.size(); }
  vreg_iterator vregs_begin() { return _vregs.begin(); }
  vreg_iterator vregs_end() { return _vregs.end(); }
  const_vreg_iterator vregs_begin() const { return vregs_cbegin(); }
  const_vreg_iterator vregs_end() const { return vregs_cend(); }
  const_vreg_iterator vregs_cbegin() const { return _vregs.begin(); }
  const_vreg_iterator vregs_cend() const { return _vregs.end(); }

  vreg_range vregs_range() { return vreg_range(vregs_begin(), vregs_end()); }
  const_vreg_range vregs_range() const { return vregs_crange(); }
  const_vreg_range vregs_crange() const { return const_vreg_range(vregs_cbegin(), vregs_cend()); }


public:
  //void check();
  //void clean();
  void dump(MachineDumper&) const;
  std::shared_ptr<const CallingConvention> getCallingConvention() const { return _cc; }

  Ref<const IMachineDetails> getMachineDetails() { return _mi->details(); }

  Ref<const VirtualRegister> getArgumentRegister(uint n, RegClassId);
  Ref<const VirtualRegister> getResultRegister(uint n, RegClassId);
  Ref<const VirtualRegister> createRegister(RegClassId, int logSz = 0);

  //void allocateFragmentClusters(BBlockClusterMap const&);
  //void allocateVRegMemRefs();
  //void runPassPreDefUse();
  //void runPassDefUseAnalysis();
  //void runPassRegAlloc1P();
  //VMMemory::CodeDataArea runPassCodeGen(VMMemory&);
  
  //bool needsPossiblyFramePointer() const;
  //bool needsDefinitelyFramePointer() const;
  
public:
  TempAllocator& getAllocator() const { return *_allocator._ptr; }


  Ref<BasicBlockImpl> getStartBlock();
  Ref<BasicBlockImpl> createBasicBlock();

  MemoryMgr::CodeAndData generate(RefCounter<MemoryMgr>, Function::Strategy);

  FrameRegisterInfo getFrameRegisterInfo(size_t i) { 
    assert(i < MMODE_FPCount);
    return _frameRegisterInfo[i];
  }

  //VRegRef getFrameReg(uint fr) const { assert(fr < _frameInfoCnt); return _frameInfo[fr].reg; }
  //int getFrameOffset(uint fr) const { assert(fr < _frameInfoCnt); return _frameInfo[fr].ofs; }
  //void setFrame(uint fr, VRegRef vr, int o) {
  //  assert(fr < _frameInfoCnt);
   // _frameInfo[fr].reg = vr;
   // _frameInfo[fr].ofs = o;
  //}

//  u8* getEstimatedOrFinalAddress(BBId);
  //bool isValidVReg(RegId id) const { return id.id >= R_HardwareLimit && id.id - R_HardwareLimit < _vregs.size(); }
//  RegId allocVReg(uint regclass, uint valuesize);
//  RegId addVReg(u32 ofs);

  //InsPtr createIns(BBlock* bb, u32 sz);
  //InsRef createFreeIns(u32 sz);
  //core::CopyInsPtr createCopyIns(BBlock* bb, size_t cnt);
  //InsRef createFreeCopyIns(size_t cnt);
//  core::KillInsPtr createKillIns(BBlock* bb, u32 cnt);

  //void fixCtrlFlow();

public:
//  u8* ofs2ptr(u32 ofs) const { assert(ofs > 0); return _allocator.getPtr(ofs); }
//  VReg* getVReg(RegId id) { assert(isValidVReg(id)); return _vregs[id.id-R_HardwareLimit]; }
//  VReg const * getVReg(RegId id) const { assert(isValidVReg(id)); return _vregs[id.id-R_HardwareLimit]; }
  //VReg * createVReg(RegClassId regclass, int sz = 0);
  //VReg const* arg(uint n) const { assert(n >= 0 && n < _cc.getParamCount()); return _vregs[_cc.getResultCount()+n]; }
  //VReg const* result(uint n) const { assert(n >= 0 && n < _cc.getResultCount()); return _vregs[n]; }

  //VReg *getVRegRW(ResId r) { return _vregs[r.id - evm::platform::GetResCount()]; }
  //VReg *getVRegRW(RegId r) { return _vregs[r.id - R_HardwareLimit]; }
  //CallConv const& getCallConv() const { return _cc; }

  //VReg const * allocCompatibleTemporaryVReg(VRegRef id);
  //void deallocTemporaryVReg(VRegRef id);
  //u32 getRegMask(VRegRef r) const { return r.toVReg()->mask; }
  
public:
  //u8* allocBlock(size_t sz) { return _allocator.alloc(sz); }
//  u32 alloc(size_t sz) { return _allocator.alloc(sz); }
//  void align(size_t sz) { return _allocator.align(sz); }

protected:
  //void _setCode0(u8* x) { _zero = x; }

private:
  //void _tryToAllocateXFragmentCluster(XFragmentList const&);
  void _prepareArgumentAndResultRegisterSpace();
  
private:
  RefCounter<TempAllocator> _allocator;
  std::shared_ptr<const CallingConvention> _cc;
  RefCounter<IMachineInfo> _mi;
  BasicBlockList _bblocks;
  ConstVirtualRegisterList _parregs;
  VirtualRegisterList _vregs;
  //std::vector< std::vector<VReg const*> > _tempvregs;
  FrameRegisterInfo _frameRegisterInfo[MMODE_FPCount];
  //uint _frameInfoCnt;
};

} // End jitcs namespace

#endif
// _JITCS_INT_FUNCTION_IMPL_H_
