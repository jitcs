//===-- src/jitcs_int_adt_tmpvector.h ---------------------------*- C++ -*-===//
// A vector like container object, which uses a TempAllocator for all
// allocations.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_ADT_TMPVECTOR_H_
#define _JITCS_INT_ADT_TMPVECTOR_H_

#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include "jitcs_tmpalloc.h"

namespace jitcs {

template <typename T, unsigned N>
struct TmpVector : public Slice<T> {
  typedef T ItemType;
  typedef Slice<T> super;
  ItemType _short_data[N];
  size_t _capacity;
  TempAllocator& _alloc;
  
  TmpVector(TempAllocator& a) : super(_short_data, 0), _capacity(N), _alloc(a) {}
  
  TmpVector() = delete;
  TmpVector(const TmpVector&) = delete;
  TmpVector& operator =(const TmpVector&) = delete;
  
  void push_back(ItemType i) {
    if (this->size() >= _capacity) _resize(1);
    this->_ptr[this->_size++] = i;
  }
  void append(Slice<ItemType> items) {
    if (this->size() + items.size() > _capacity) _resize(items.size());
    std::copy(items.begin(), items.end(), this->ptr() + this->size());
    this->_size += items.size();
  }
  bool isLocalData() const { return this->ptr() == _short_data; }

  void initSizeAndClear(size_t sz, const ItemType& filler) {
    if (sz > _capacity) _resize(sz);
    this->_size = sz;
    std::fill(this->ptr(), this->ptr() + sz, filler);
  }

private:
  void _resize(size_t g) {
    Slice<ItemType> newdata 
      = _alloc.allocTypedArray<ItemType>((_capacity | g) * 2 + 2);
    std::copy(this->ptr(), this->ptr() + this->size(), newdata.ptr());
    this->_ptr = newdata.ptr();
    this->_capacity = newdata.size();
  }
};

} // end of namespace jitcs

#endif
// _JITCS_INT_ADT_TMPVECTOR_H_
