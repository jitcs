//===-- src/jitcs_int_machine.h ---------------------------------*- C++ -*-===//
// More target machine info.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// IMachineDetails contain all information and methods necessary for
// target dependent dataflow analysis or register allocation.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_MACHINE_H_
#define _JITCS_INT_MACHINE_H_

#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include "jitcs_cpu.h"
#include "jitcs_machine.h"
#include "jitcs_ids.h"
#include <memory>

namespace jitcs {
struct Instruction;
struct VirtualRegister;
//class BBlock;
//struct CCInfo;
class BasicBlockImpl;
class CallingConvention;
class CodeGenerator;
class Function;
class IDumper;
class TempAllocator;
class SetupBlockResourceInfo;

class IMachineDetails {
protected:
  IMachineDetails() = default;
  virtual ~IMachineDetails() = default;
private:
  IMachineDetails(const IMachineDetails&) = delete;
  IMachineDetails& operator =(const IMachineDetails&) = delete;
public:
  virtual Ref<const VirtualRegister> stackFrameRegister() const = 0;

  virtual ResId getResOfReg(RegId r) const = 0;
  virtual RegClassId getRegClassOfReg(RegId r) const = 0;
  virtual ResClassId getResClassOfReg(RegId r) const = 0;
  //
  virtual RegClassId getRegClassCount() const = 0;
  virtual ResClassId getResClassOfRegClass(RegClassId rc) const = 0;
  virtual u32 getLogSizeOfRegClass(RegClassId rc) const = 0;
  virtual u32 getDontAllocOfRegClass(RegClassId rc) const = 0;
  //
  virtual ResId getResCount() const = 0;
  virtual ResClassId getResClassOfRes(ResId r) const = 0;
  virtual RegId getRegIdForRes(RegClassId rc, ResId res) const = 0;
  //
  virtual ResClassId getResClassCount() const = 0;
  virtual u32 getResIndexOfResClass(ResClassId r) const = 0;

  //
  virtual size_t getInsIdInfo(InsId) const;

  virtual void buildCtrlFlow(Ref<Instruction> ins, Ref<BasicBlockImpl> src) const;

  virtual void extractDefUse(Slice<const Instruction*> insns, 
                             Ref<const BasicBlockImpl> bb,
                             SetupBlockResourceInfo& dfh) const = 0;

  virtual size_t estimateCodeSize(Slice<const Instruction*> insns, 
                                  Ref<const BasicBlockImpl> bb,
                                  CodeGenerator& cg) const = 0;
  virtual Slice<u8> generateCode(Slice<u8> space, 
                                 Slice<const Instruction*> insns, 
                                 Ref<const BasicBlockImpl> bb,
                                 CodeGenerator& cg) const = 0;


  //u32 numRes;
  //u8 numRegClass, numResClass;

  //virtual bool hasCtrlFlowIns(Slice<Instruction*>) = 0;
  //virtual bool isRetIns(Ref<Instruction>) = 0;
  //virtual void updateCtrlFlow(Ref<Instruction>, BBlock* src, Function& b);

  //inline u32 GetInsNextSize(InsPtr ip, Function& b) {
  //inline void AnalyzePreDefUse(InsPtr ip, PreDefUseData& ud, u32 idx) {
  //inline void AnalyzeDefUse(InsPtr ip, DefUseData& ud, u32& idx, NextData* next) {

  //inline void RegAlloc1P(InsPtr ip, RegAlloc1PData& ra, Function& b, NextData* next) {
  //inline void ReserveRegisters(BitmapRef res, bool requireSuperAlignment) {
  //inline void CreatePrologueAndEpilogue(PrologueEpilogueData & ped) {
  //inline void CreateMove(RegAlloc1PData &ra, VRegRef dest, VRegRef src) {
  //inline void CreateMove(RegAlloc1PData &ra, VRegRef dest, MemRef src) {
  //inline void CreateMove(RegAlloc1PData &ra, MemRef dest, VRegRef src) {
  //inline VRegRef GetRRefForRes(RegClassId rc, ResId res) {
  //inline ResId evm::DefUseData::reg2resV(RegId r) const { return evm::platform::GetResOfReg(r); }
  //inline bool IsHWReg(RegId r) { return r.id < R_HardwareLimit; }
  //inline bool  IsHWRes(ResId r) { return r.id < evm::platform::GetResCount(); }

  //void DumpMem(evm::IDumper& o, evm::MemPtr mp, bool vsib);
};

} // end namespace jitcs

#endif 
// _JITCS_INT_MACHINE_H_
