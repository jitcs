//===-- src/jitcs_int_virtualregister.h -------------------------*- C++ -*-===//
// The actual definition of the VirtualRegister class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// While client code uses VirtualRegister a lot, reference are enough for it.
// The several analysis steps need a bit more information.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_VIRTUALREGISTER_H_
#define _JITCS_INT_VIRTUALREGISTER_H_

#include "jitcs_adt_ref.h"
#include "jitcs_adt_slice.h"
#include "jitcs_base.h"
#include "jitcs_ids.h"
#include "jitcs_spillslotinfo.h"

namespace jitcs {
class MachineDumper;
class IMachineInfo;

struct VirtualRegister {
  enum StaticType { VR_Static };
  // static data: set at creation time
  RegId id;
  ResId res;
  RegClassId regclass;
  ResClassId resclass;
  u8 logSz;
  //the following two fields might be useful in emulation:
  //by passing in the address of the context structure, the address of each
  //emulated register is used directly as spill slot, avoiding many load/store instructions
  //and enabling reg->mem folding which is only supported for spill slots (because
  //general reg->mem folding requires alias analysis, which is not necessary for spill slots)
  //disabled for the moment:
  //SpillSlotInfo spillplacement;
  //Slice<VirtualRegister*> aliases;
  bool isHWReg() const { return id < R_HardwareLimit; }
  
  void setupDynamic(IMachineInfo&, size_t vregno, RegClassId rc, u8 logSz = 0);

  VirtualRegister() = default;
  VirtualRegister(const VirtualRegister&) = default;
  VirtualRegister& operator =(const VirtualRegister&) = default;

  VirtualRegister(StaticType,
                  RegId hwr,  ResId r, 
                  RegClassId rc, ResClassId rescl, 
                  u8 logSz_)
    : id(hwr), res(r), regclass(rc), resclass(rescl), logSz(logSz_)
  {}
};
} // end of jitcs namespace

#endif
// _JITCS_INT_VIRTUALREGISTER_H_
