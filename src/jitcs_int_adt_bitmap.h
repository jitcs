//===-- src/jitcs_int_adt_bitmap.h ------------------------------*- C++ -*-===//
// BitSlice class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// A BitSlice is a view to an array of words, and allows checking and setting
// single bits. A BitSlice does NOT own the data it points to.
// A DynBitSlice provides the data array for the BitSlice. It will use a
// fixed size array, or allocate the space from the heap.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_ADT_BITMAP_H_
#define _JITCS_INT_ADT_BITMAP_H_

#include "jitcs_base.h"
#include "jitcs_adt_ref.h"
#include "jitcs_int_bitfuncs.h"
#include "jitcs_int_power2funcs.h"

#include <stdio.h>

namespace jitcs {
class IDumper;
void DumpBitmap(IDumper& o, const size_t*, size_t);

template <unsigned FLAGS>
struct _Bits {
  typedef const size_t CType;
  typedef size_t NCType;
  static const bool IsConstRef = (FLAGS & REF_Const) != 0;
  typedef typename std::conditional<IsConstRef, CType, NCType>::type WordType;
  typedef WordType* PtrType;
  typedef WordType& RefType;

  typedef WordType* iterator;
  typedef WordType const* const_iterator;

  static_assert(sizeof(WordType) == 4 || sizeof(WordType) == 8, "surprising size_t size");
  enum {
    E_BitsPerWord = sizeof(WordType) * 8, // 32 or 64
    E_Log2OfBitsPerWord = Log2CT<E_BitsPerWord>::Val, // 5 or 6
    E_MaskOfBitsPerWord = (1 << E_Log2OfBitsPerWord) - 1, // 0x1f or 0x3f
  };

  struct BitEnumerator {
    size_t const *ptr;
    size_t nbits;
    size_t curpos0, curbit;

    BitEnumerator(const WordType* p, size_t n)
      : ptr(p)
      , curpos0(0)
      , curbit(0)
      , nbits(n) {
      if (!isEmpty()) {
        _advance();
      }
    }
    BitEnumerator(const BitEnumerator&) = default;
    BitEnumerator& operator =(const BitEnumerator&) = default;
    
    bool isEmpty() const { return curpos0 >= nbits; }
    size_t pos() const { return curpos0 + curbit; }
    void next() { ++curbit; _advance(); }
    
    bool operator !() const { return isEmpty(); }
    size_t operator *() const { assert(!isEmpty()); return pos(); }
    void operator ++() { assert(!isEmpty()); next(); }

  private:
    void _advance() {
      _JITCS_DBG_CHECK_(!isEmpty());
      while (curbit >= E_BitsPerWord || (*ptr >> curbit) == 0) {
        curpos0 += E_BitsPerWord;
        if (isEmpty()) return;
        ++ptr;
        curbit = 0;
      }
      curbit = t0cntnz(*ptr, curbit);
    }
  };


  _Bits() : _ptr(nullptr), _nbits(0) {}
  _Bits(const _Bits&) = default;
  _Bits& operator = (const _Bits&) = default;

  template <unsigned FLAGS2>
  _Bits(_Bits<FLAGS2> o) {
    static_assert((~FLAGS & FLAGS2) == 0, "incompatible _Bits flags");
    //static_assert(std::is_base_of<T, T2>::value, "incompatible _Bits types");
    _ptr = o._ptr;
    _nbits = o._nbits;
  }
  _Bits(PtrType o, size_t n) {
    _ptr = o;
    _nbits = n;
  }

  template <unsigned FLAGS2>
  _Bits& operator =(_Bits<FLAGS2> o) {
    static_assert((~FLAGS & FLAGS2) == 0, "incompatible _Bits flags");
    _ptr = o._ptr;
    _nbits = o._nbits;
  }

  PtrType ptr() const { return _ptr; }
  size_t bitSize() const { return _nbits; }
  size_t wordSize() const { return DivRoundedUpByPowerOf2<E_BitsPerWord>(_nbits); }

  bool isEmpty() const { return _nbits == 0; }
  bool empty() const { return isEmpty(); }

  bool isValidIndex(size_t r) const { return r >= 0 && r < bitSize(); }
  
  iterator wordBegin() { return ptr(); }
  iterator wordEnd() { return ptr() + wordSize(); }
  const_iterator wordBegin() const { return wordCBegin(); }
  const_iterator wordEnd() const { return wordCEnd(); }
  const_iterator wordCBegin() const { return ptr(); }
  const_iterator wordCEnd() const { return ptr() + wordSize(); }

  BitEnumerator enumBits() const { return BitEnumerator(_ptr, _nbits); }

  size_t getWordNoForIndex(size_t r) const { return r >> E_Log2OfBitsPerWord; }
  size_t getBitNoForIndex(size_t r) const { return r & E_MaskOfBitsPerWord; }
  RefType getWordForIndex(size_t r) const { return _ptr[getWordNoForIndex(r)]; }
  size_t getBitMaskForIndex(size_t r) const { return 1 << getBitNoForIndex(r); }
  size_t getLastWordBitNo() const { return bitSize() & E_MaskOfBitsPerWord; }
  size_t getLastWordShift() const { return E_BitsPerWord - getLastWordBitNo(); }
  bool hasIncompleteLastWord() const { return getLastWordBitNo() != 0; }

  bool test(size_t r) const {
    _checkIndex(r);
    return (getWordForIndex(r) & getBitMaskForIndex(r)) != 0;
  }
  bool operator [](size_t i) const { return test(i); }

  size_t countSetBits() const {
    size_t n = wordSize();
    size_t sum = 0;
    if (hasIncompleteLastWord()) {
      sum += popcnt(_ptr[n - 1] << getLastWordShift());
      --n;
    }
    for (size_t i = 0; i < n; ++i) {
      sum += popcnt(_ptr[i]);
    }
    return sum;
  }
  bool isAllZeroes() const {
    size_t n = wordSize();
    if (hasIncompleteLastWord()) {
      if ((_ptr[n - 1] << getLastWordShift()) != 0) return false;
    }
    for (size_t i = 0; i < n; ++i) {
      if (_ptr[i] != 0) return false;
    }
    return true;
  }
  bool isAllOnes() const {
    size_t n = wordSize();
    if (hasIncompleteLastWord()) {
      if (((~_ptr[n - 1]) << getLastWordShift()) != 0) return false;
    }
    for (size_t i = 0; i < n; ++i) {
      if ((~_ptr[i]) != 0) return false;
    }
    return true;
  }
  template <unsigned FLAGS2>
  bool isSubsetOf(const _Bits<FLAGS2>& p) const {
    _checkNotLongerThan(p);
    size_t n = wordSize();
    if (hasIncompleteLastWord()) {
      if ((_ptr[n - 1] & ~p._ptr[n - 1]) << getLastWordShift()) return false;
      --n;
    }
    for (size_t i = 0; i < n; ++i) {
      if (_ptr[i] & ~p._ptr[i]) return false;
    }
    return true;
  }
  template <unsigned FLAGS2>
  bool equals(const _Bits<FLAGS2>& p) const {
    _checkNotLongerThan(p);
    //if (bitSize() != p.bitSize()) return false;
    size_t n = wordSize();
    if (hasIncompleteLastWord()) {
      if ((_ptr[n - 1] ^ p._ptr[n - 1]) << getLastWordShift()) return false;
      --n;
    }
    for (size_t i = 0; i < n; ++i) {
      if (_ptr[i] != p._ptr[i]) return false;
    }
    return true;
  }
  
  void mark(size_t r) {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    _checkIndex(r); 
    RefType rw = getWordForIndex(r); 
    rw |= getBitMaskForIndex(r);
  }
  void clear(size_t r) {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    _checkIndex(r); 
    RefType rw = getWordForIndex(r); 
    rw &= ~getBitMaskForIndex(r);
  }
  bool testAndMark(size_t r)  {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    _checkIndex(r); 
    RefType rw = getWordForIndex(r); 
    WordType m = getBitMaskForIndex(r);
    bool res = (rw & m) != 0; 
    rw |= m;
    return res;
  }
  bool testAndClear(size_t r)  {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    _checkIndex(r); 
    RefType rw = getWordForIndex(r); 
    WordType m = getBitMaskForIndex(r);
    bool res = (rw & m) != 0; 
    rw &= ~m;
    return res;
  }
  bool testAndComplement(size_t r)  {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    _checkIndex(r); 
    RefType rw = getWordForIndex(r); 
    WordType m = getBitMaskForIndex(r);
    bool res = (rw & m) != 0; 
    rw ^= m;
    return res;
  }

  void clearAll() {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    for (size_t i = 0, n = wordSize(); i < n; ++i) {
      _ptr[i] = 0;
    }
  }
  void setAll() {
    guarantee<(FLAGS & REF_Const) == 0>
      ("mutation disabled for immutable bitmap views");
    for (size_t i = 0, n = wordSize(); i < n; ++i) {
      _ptr[i] = ~0;
    }
  }
  template <unsigned FLAGS2>
  void copyFrom(const _Bits<FLAGS2>& p) {
    _checkNotLongerThan(p);
    for (size_t i = 0, n = wordSize(); i < n; ++i) {
      _ptr[i] = p._ptr[i];
    }
  }
  template <unsigned FLAGS2>
  void intersectWith(const _Bits<FLAGS2>& p) {
    _checkNotLongerThan(p);
    for (size_t i = 0, n = wordSize(); i < n; ++i) {
      _ptr[i] &= p._ptr[i];
    }
  }
  template <unsigned FLAGS2>
  void uniteWith(const _Bits<FLAGS2>& p) {
    _checkNotLongerThan(p);
    for (size_t i = 0, n = wordSize(); i < n; ++i) {
      _ptr[i] |= p._ptr[i];
    }
  }
  template <unsigned FLAGS2>
  void setFrom(const _Bits<FLAGS2>& p) {
    return uniteWith(p);
  }
  template <unsigned FLAGS2>
  void clearFrom(const _Bits<FLAGS2>& p) {
    _checkNotLongerThan(p);
    for (size_t i = 0, n = wordSize(); i < n; ++i) {
      _ptr[i] &= ~p._ptr[i];
    }
  }

  void dump(IDumper& o) const { DumpBitmap(o, _ptr, _nbits); }

private:
  void _checkIndex(size_t r) const { _JITCS_DBG_CHECK_(isValidIndex(r)); }
  template <unsigned FLAGS2>
  void _checkNotLongerThan(const _Bits<FLAGS2>& bmp) const { 
    _JITCS_DBG_CHECK_(bitSize() <= bmp.bitSize());
  }
  
public:
  WordType *_ptr;
  size_t _nbits;
};

typedef _Bits<REF_Any> BitSlice;
typedef _Bits<REF_Const> ConstBitSlice;

template <size_t N>
struct DynBitSlice : BitSlice {
  size_t _short_data[RoundUpToPowerOf2CT<N, E_BitsPerWord>::Val];

  DynBitSlice(size_t nbits, bool clear) {
    size_t n = DivRoundedUpByPowerOf2< E_BitsPerWord>(nbits);
    _ptr = n <= RoundUpToPowerOf2CT<N,  E_BitsPerWord>::Val ? _short_data : new size_t[n];
    _nbits = nbits;
    if (clear) clearAll();
  }
  ~DynBitSlice() {
    if (isUsingHeap()) delete[] _ptr;
  }
  bool isUsingHeap() const { return _ptr != _short_data; }
};


} // end of jitcs namespace

#endif
// _JITCS_INT_ADT_BITMAP_H_
