//===-- src/jitcs_int_adt_heap.h --------------------------------*- C++ -*-===//
// A binary heap object with an unlimited size.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// While currently unused, it might be used in the cross-basicblock
// register allocation phase.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_ADT_HEAP_H_
#define _JITCS_INT_ADT_HEAP_H_

#include "jitcs_base.h"
#include "jitcs_adt_ref.h"
#include <vector>
#include <stdio.h>

namespace jitcs {
// SmallTopHeap can store pairs of (IX, V), where IX is between 0 and N - 1,
// and V is the values to be ordered by. In this case, the largest V is on top.

template <typename T>
struct DefaultTopHeapTrait {
  typedef std::vector<T> ContainerType;
  typedef T ItemType;
  typedef typeof(T::value) ValueType;
  static size_t GetHeapPos(T t) { return t.heappos; }
  static ValueType GetValue(T t) { return t.value; }
  static void SetHeapPos(T& t, size_t pos) { t.heappos = pos; }
  static void PushBack(ContainerType& c, T t) { c.push_back(t); }
  static T PopBack(ContainerType& c) { T t = c.back(); c.pop_back(); return t; }
};

template <typename T, typename TRAITS = DefaultTopHeapTrait<T>>
struct TopHeap {
  typedef typename TRAITS::ContainerType ContainerType;
  typedef typename TRAITS::ItemType ItemType;
  typedef typename TRAITS::ValueType ValueType;
  ContainerType data;

  TopHeap() {}
  
  bool isEmpty() const { return data.size() == 0; }

  bool check() const {
    // check that hpos is the reverse of heap
    for (size_t pos = 0; pos < data.size(); ++pos) {
      if (TRAITS::GetHeapPos(data[pos]) != pos) return false;
    }
    // check that parents are never smaller than their children
    for (size_t pos = data.size(); pos > 1;) {
      --pos;
      size_t ppos = (pos - 1) >> 1;
      if (TRAITS::GetValue(data[ppos]) < TRAITS::GetValue(data[pos]))
        return false;
    }
    return true;
  }
  
  ItemType peekTop() const { 
    _JITCS_DBG_CHECK_(!isEmpty());
    return data[0];
  }
  ValueType peekTopValue() const { 
    _JITCS_DBG_CHECK_(!isEmpty());
    return TRAITS::GetValue(data[0]);
  }
  void push(ItemType i) {
    _JITCS_DBG_CHECK_(TRAITS::GetHeapPos(i) >= data.size() 
                      || data[TRAITS::GetHeapPos(i)] != i);
    // add hole at end of storage space
    TRAITS::PushBack(data, i);
    unsigned pos = _moveHoleUp(data.size() - 1, TRAITS::GetValue(i));
    data[pos] = i;
    TRAITS::SetHeapPos(data[pos], pos);
  }
  ItemType dropTop() {
    _JITCS_DBG_CHECK_(!isEmpty());
    ItemType i = TRAITS::PopBack(data);
    if (!isEmpty()) {
      unsigned pos = _moveHoleDown(0, TRAITS::GetValue(i));
      data[pos] = i;
      TRAITS::SetHeapPos(data[pos], pos);
    }
    return i;
  }
  ItemType drop(ItemType j) {
    _JITCS_DBG_CHECK_(TRAITS::GetHeapPos(j) < data.size()
                      && data[TRAITS::GetHeapPos(j)] == j);
    ItemType i = TRAITS::PopBack(data);
    if (!isEmpty()) {
      ValueType val = TRAITS::GetValue(i);
      size_t pos = TRAITS::GetHeapPos(j);
      if (pos > 0 && TRAITS::GetValue(data[(pos - 1) >> 1]) < val) {
        pos = _moveHoleUp(pos, val);
      } else {
        pos = _moveHoleDown(pos, val);
      }
      data[pos] = i;
      TRAITS::SetHeapPos(data[pos], pos);
    }
    return j;
  }
private:
  unsigned _moveHoleUp(unsigned pos, ValueType val) {
    // move hole up while the new value is larger than its parent
    while (pos > 0) {
      unsigned ppos = (pos - 1) >> 1;
      if (TRAITS::GetValue(data[ppos]) >= val) return pos;
      // swap parent with hole.
      data[pos] = data[ppos];
      TRAITS::SetHeapPos(data[pos], pos);
      pos = ppos;
    }
    return pos;
  }
  unsigned _moveHoleDown(unsigned pos, ValueType val) {
    // move hole up while the new value is larger than its parent
    size_t cpos = pos * 2 + 1, sz = data.size();
    while (cpos < sz) {
      if (cpos + 1 >= sz) {
        // only the "left" child. this also means, it's a leaf.
        if (TRAITS::GetValue(data[cpos]) > val) {
          data[pos] = data[cpos];
          TRAITS::SetHeapPos(data[pos], pos);
          pos = cpos;
        }
        return pos;
      }
      // both children available
      // choose the child with the bigger value. if both children's values
      // a below val, we are done.
      ValueType vpos = TRAITS::GetValue(data[cpos]), 
                vpos1 = TRAITS::GetValue(data[cpos + 1]);
      if (vpos1 > val) {
        if (vpos1 >= vpos) {
          // choose right child if it is greater than the left child
          ++cpos;
          vpos = vpos1;
        }
      } else {
        if (vpos <= val) return pos;
      }
      // swap child with hole.
      data[pos] = data[cpos];
      TRAITS::SetHeapPos(data[pos], pos);
      pos = cpos;
      cpos = pos * 2 + 1;
    }
    return pos;
  }
};

} // end of jitcs namespace

#endif
// _JITCS_INT_ADT_HEAP_H_
