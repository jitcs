//===-- src/jitcs_int_bblock_impl.h -----------------------------*- C++ -*-===//
// BasicBlock implementation class.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// BasicBlockImpl objects are allocated from a TempAllocator, and their
// destructor is never called.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_BBLOCK_IMPL_H_
#define _JITCS_INT_BBLOCK_IMPL_H_

#include "jitcs_adt_range.h"
#include "jitcs_adt_slice.h"
#include "jitcs_int_adt_tmpvector.h"
#include "jitcs_base.h"
#include "jitcs_bblock.h"
#include "jitcs_instruction.h"

namespace jitcs {
class FunctionImpl;
class MachineDumper;
class TempAllocator;

class BasicBlockImpl : public BasicBlock {
public:
  // TODO: use structures that use TempAllocator
  typedef TmpVector<BasicBlockImpl*, 2> BasicBlockList;
  typedef TmpVector<Instruction*, 8> InstructionList;

  typedef BasicBlockList::iterator bb_iterator;
  typedef BasicBlockList::const_iterator const_bb_iterator;
  typedef InstructionList::iterator ins_iterator;
  typedef InstructionList::const_iterator const_ins_iterator;
  typedef Range<bb_iterator> bb_range;
  typedef Range<ins_iterator> ins_range;
  typedef ConstRange<const_bb_iterator> const_bb_range;
  typedef ConstRange<const_ins_iterator> const_ins_range;

public:
  static Ref<BasicBlockImpl> From(Ref<BasicBlock> s) {
    return reinterpret_cast<BasicBlockImpl*>(s._ptr);
  }
  static Ref<const BasicBlockImpl> From(Ref<const BasicBlock> s) {
    return reinterpret_cast<const BasicBlockImpl*>(s._ptr);
  }
  /*[[override]] virtual */ BasicBlockImpl& impl() { return *this; }
  /*[[override]] virtual */ const BasicBlockImpl& impl() const { return *this; }

public:
  BasicBlockImpl(FunctionImpl& f, BBId id);
  ~BasicBlockImpl() = default;
  BasicBlockImpl(const BasicBlockImpl &) = delete;
  void operator=(const BasicBlockImpl &) = delete;
  
public:
  /*[[override]] virtual */ BBId id() const { return _bbId; }
  RefOrNull<BasicBlockImpl> getFallThruFrom() const { return _bbFallThruFrom; }
  RefOrNull<BasicBlockImpl> getFallThruTo() const { return _bbFallThruTo; }

  size_t instr_size() const { return _listOfIns.size(); }
  Slice<Instruction*> insns() { return _listOfIns; }
  Slice<const Instruction*> cinsns() const { 
    return Slice<const Instruction*>
           (const_cast<const Instruction**>(_listOfIns.ptr()), 
            _listOfIns.size());
  }
  
  bb_iterator pred_begin() { return _predecessors.begin(); }
  bb_iterator pred_end() { return _predecessors.end(); }
  const_bb_iterator pred_begin() const { return pred_cbegin(); }
  const_bb_iterator pred_end() const { return pred_cend(); }
  const_bb_iterator pred_cbegin() const { return _predecessors.begin(); }
  const_bb_iterator pred_cend() const { return _predecessors.end(); }

  bb_iterator succ_begin() { return _successors.begin(); }
  bb_iterator succ_end() { return _successors.end(); }
  const_bb_iterator succ_begin() const { return succ_cbegin(); }
  const_bb_iterator succ_end() const { return succ_cend(); }
  const_bb_iterator succ_cbegin() const { return _successors.begin(); }
  const_bb_iterator succ_cend() const { return _successors.end(); }

  ins_iterator instr_begin() { return _listOfIns.begin(); }
  ins_iterator instr_end() { return _listOfIns.end(); }
  const_ins_iterator instr_begin() const { return instr_cbegin(); }
  const_ins_iterator instr_end() const { return instr_cend(); }
  const_ins_iterator instr_cbegin() const { return _listOfIns.begin(); }
  const_ins_iterator instr_cend() const { return _listOfIns.end(); }

  bb_range pred_range() { return bb_range(pred_begin(), pred_end()); }
  const_bb_range pred_range() const { return pred_crange(); }
  const_bb_range pred_crange() const { return const_bb_range(pred_cbegin(), pred_cend()); }

  bb_range succ_range() { return bb_range(succ_begin(), succ_end()); }
  const_bb_range succ_range() const { return succ_crange(); }
  const_bb_range succ_crange() const { return const_bb_range(succ_cbegin(), succ_cend()); }

  ins_range instr_range() { return ins_range(instr_begin(), instr_end()); }
  const_ins_range instr_range() const { return instr_crange(); }
  const_ins_range instr_crange() const { return const_ins_range(instr_cbegin(), instr_cend()); }

//  std::vector<BBlock*> const* getPredecessors() const { return &_predecessors; }
//  std::vector<BBlock*> const* getSuccessors() const { return &_successors; }
//  std::vector<InsRef> const* getInstructions() const { return &_listOfIns; }

public:
  // methods for use when constructing basic blocks
  // append: add a block of instructions to the basic block. the stream is broken down
  // into individual instructions here. also, it must be ascertained, that the data pointed to by
  // p is alive until the compilation of the function is complete. if in doubt, use append_copy
  // instead.
  // append_copy: same as append, but copies data to the storage of a TempAllocator first.
  /*[[override]] virtual */ size_t append(Slice<iptr>);
  /*[[override]] virtual */ size_t append_copy(Slice<const iptr>);

  // methods for fixing up the control flow graph
  static void BuildEdge(Ref<BasicBlockImpl> fromBB, 
                        Ref<BasicBlockImpl> toBB, bool isFallthru);
  static void SetFallthru(Ref<BasicBlockImpl> fromBB, 
                          Ref<BasicBlockImpl> toBB);
  
  void dump(MachineDumper&) const;

private:
  FunctionImpl& _fnc;
  TempAllocator& _alloc;
  InstructionList _listOfIns;
  BasicBlockList _successors, _predecessors;
  BBId _bbId;
  RefOrNull<BasicBlockImpl> _bbFallThruFrom, _bbFallThruTo;
  bool _hasCFIns;
};
//inline bool evm::NextData::isRegDead(BBlock* bb, ResId res) const {
//  return isRegDead() || (isRegDeadOrLiveOut() && !bb->getLiveOut().test(res.id));
//}


} // End jitcs namespace

#endif
// _JITCS_INT_BBLOCK_IMPL_H_
