//===-- src/jitcs_int_callingconvention_impl.h ------------------*- C++ -*-===//
// Implementation class to handle calling convention object setup.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_CALLINGCONVENTION_IMPL_H_
#define _JITCS_INT_CALLINGCONVENTION_IMPL_H_

#include "jitcs_callingconvention.h"
#include <vector>
#include <memory>

namespace jitcs {
class CallingConventionImpl : public CallingConvention {
public:
  struct IOParamInfo {
    RefOrNull<const VirtualRegister> reg;
    SpillSlotInfo save;
  };

public:
  CallingConventionImpl(const RefCounter<IMachineInfo>& m);
  virtual ~CallingConventionImpl() = default;

public:
  virtual RefCounter<IMachineInfo> getMachineInfo() const { return _machine; }
  virtual size_t getResultCount() const { return _results.size(); }
  virtual size_t getParamCount() const { return _params.size(); }
  virtual size_t getParamAreaSize() const { return _paramArea; }
  virtual SpillSlotInfo getResultSpillSlotInfo(size_t r) const { return _results[r].save; }
  virtual RefOrNull<const VirtualRegister> getResultRegister(size_t r) const { return _results[r].reg; }
  virtual SpillSlotInfo getParamSpillSlotInfo(size_t p) const { return _params[p].save; }
  virtual RefOrNull<const VirtualRegister> getParamRegister(size_t p) const { return _params[p].reg; }
  virtual size_t getRequiredStackAlignment() const { return _alignment; }
  virtual size_t getRequiredStackAlignmentOffset() const { return _alignmentOffset; }
  virtual CallerCalleeStrategyId getStackCleaningStrategy() const { return _stackCleaning; }

  virtual FTSubType getSubType() const { return _subtype; }
  virtual FTTypeId getResultType(size_t r) const { return _outtypes[r]; }
  virtual FTTypeId getParamType(size_t p) const { return _intypes[p]; }

  virtual u32 getScratchResources(ResClassId) const;
  virtual u32 getReservedResources(ResClassId) const;

public:
  virtual void setTypeInfo(FTSubType,Slice<FTTypeId> res,Slice<FTTypeId> par);
  virtual void addResult(const VirtualRegister*, SpillSlotInfo);
  virtual void addParam(const VirtualRegister*, SpillSlotInfo);
  virtual void setStackAlignment(size_t align, size_t ofs);
  virtual void setParamAreaSize(size_t);
  virtual void setCleanStack(CallerCalleeStrategyId);
  virtual void setResClassCount(size_t resNo);
  virtual void setScratchResources(ResClassId, u32);
  virtual void setReservedResources(ResClassId, u32);
  
private:
  RefCounter<IMachineInfo> _machine;
  std::vector<IOParamInfo> _results, _params;
  //ConstBitmapRef _callerSave, _calleeSave;
  //std::vector< std::vector<u32> > _callerSaveList, _calleeSaveList;
  //u8* _resourceData;
  size_t _alignment, _alignmentOffset, _paramArea;
  CallerCalleeStrategyId _stackCleaning;
  FTSubType _subtype;
  std::vector<FTTypeId> _outtypes, _intypes;
  std::vector<u32> _scratchResources;
  std::vector<u32> _reservedResources;
};

} // end of namespace jitcs

#endif
// _JITCS_INT_CALLINGCONVENTION_IMPL_H_
