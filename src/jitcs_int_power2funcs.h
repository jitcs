//===-- src/jitcs_int_power2funcs.h -----------------------------*- C++ -*-===//
// Some helper functions to handle powers of 2 at compile or run time.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_POWER2FUNCS_H_
#define _JITCS_INT_POWER2FUNCS_H_

#include "jitcs_base.h"

namespace jitcs {

// compile time functions
template <size_t t>
struct IsPowerOf2OrZeroCT { static const bool Val = (t & (t - 1)) == 0; };
template <size_t t>
struct IsPowerOf2CT { static const bool Val = t != 0 && IsPowerOf2OrZeroCT<t>::Val; };

template <size_t t>
struct Log2CT { static const size_t Val = t > 1 ? (Log2CT<(t>>1)>::Val + 1) : 0; };
template <>
struct Log2CT<0> { static const size_t Val = 0; };

template <size_t t, size_t u>
struct RoundUpToPowerOf2CT { 
  static_assert(IsPowerOf2CT<u>::Val, "expected power of 2"); 
  static const size_t Val = (t + u - 1) & (~u + 1); 
};

// runtime functions
inline bool IsPowerOf2OrZero(size_t t) { return (t & (t - 1)) == 0; }
inline bool IsPowerOf2(size_t t) { return t != 0 && IsPowerOf2OrZero(t); }

template <size_t u>
inline size_t RoundUpToPowerOf2(size_t t) { 
  static_assert(IsPowerOf2CT<u>::Val, "expected power of 2"); 
  return (t + u - 1) & (~u + 1); 
}
template <size_t u>
inline size_t DivRoundedUpByPowerOf2(size_t t) { 
  static_assert(IsPowerOf2CT<u>::Val, "expected power of 2"); 
  return (t + u - 1) / u; 
}

inline size_t RoundUpToPowerOf2(size_t t, size_t u) { 
  assert(IsPowerOf2(u)); 
  return (t + u - 1) & (~u + 1);
}

} // end of namespace jitcs
#endif
// _JITCS_INT_POWER2FUNCS_H_
