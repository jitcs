//===-- src/jitcs_int_adt_smallheap.h ---------------------------*- C++ -*-===//
// A binary heap object with a limited size.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// While currently unused, it might be used in the register allocator.
// The choice of which register to spill seems to fit this ADT perfectly.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_ADT_SMALLHEAP_H_
#define _JITCS_INT_ADT_SMALLHEAP_H_

#include "jitcs_base.h"
#include "jitcs_adt_ref.h"

#include <stdio.h>

namespace jitcs {
// SmallTopHeap can store pairs of (IX, V), where IX is between 0 and N - 1,
// and V is the values to be ordered by. In this case, the largest V is on top.

// TODO: add RELIABLE way of checking if an item is on the heap or not
// the current way is WAY too fishy
template <typename VAL, unsigned N, bool ORDERVAL>
struct SmallTopHeap {
  static const unsigned NUM = N;
  typedef typename std::conditional<N <= 256, u8, 
                   typename std::conditional<N <= 65536, u16, 
                            u32>::type>::type IxType;
  // heap ordered storage area: POS -> IX
  IxType heap[N];
  // reverse lookup: IX -> POS
  IxType hpos[N];
  // values to be ordered by: 
  // this is: IX -> VAL if !ORDERVAL, POS -> VAL if ORDERVAL
  VAL values[N];
  unsigned size;

  SmallTopHeap() : size(0) {
  }
  
  bool isEmpty() const { return size == 0; }
  bool isFull() const { return size == N; }

  bool check() const {
    // check that hpos is the reverse of heap
    for (u32 pos = 0; pos < size; ++pos) {
      if (hpos[heap[pos]] != pos) return false;
    }
    // check that parents are never smaller than their children
    for (u32 pos = size; pos > 1;) {
      --pos;
      unsigned ppos = (pos - 1) >> 1;
      if (_val(ppos) < _val(pos)) return false;
    }
    return true;
  }
  
  IxType peekTop() const { 
    _JITCS_DBG_CHECK_(!isEmpty());
    return heap[0];
  }
  VAL peekTopValue() const { 
    _JITCS_DBG_CHECK_(!isEmpty());
    return _val(0);
  }
  void push(unsigned ix, VAL val) {
    _JITCS_DBG_CHECK_(ix < N);
    _JITCS_DBG_CHECK_(!isFull());
    // add hole at end of storage space
    unsigned pos = _moveHoleUp(size++, val);
    values[ORDERVAL ? pos : ix] = val;
    heap[pos] = ix;
    hpos[ix] = pos;
  }
  void dropTop() {
    _JITCS_DBG_CHECK_(!isEmpty());
    if (!--size) return;
    VAL val = _val(size);
    unsigned pos = _moveHoleDown(0, val);
    if (ORDERVAL) values[pos] = val;
    heap[pos] = heap[size];
    hpos[heap[pos]] = pos;
  }
  void drop(unsigned ix) {
    _JITCS_DBG_CHECK_(ix < N);
    _JITCS_DBG_CHECK_(heap[hpos[ix]] == ix);
    if (!--size) return;
    VAL val = _val(size);
    u32 pos = hpos[ix];
    if (pos > 0 && _val((pos - 1) >> 1) < val) {
      pos = _moveHoleUp(pos, val);
    } else {
      pos = _moveHoleDown(pos, val);
    }
    if (ORDERVAL) values[pos] = val;
    heap[pos] = heap[size];
    hpos[heap[pos]] = pos;
  }
private:
  const VAL& _val(unsigned pos) const {
    return (ORDERVAL ? values[pos] : values[heap[pos]]);
  }
  unsigned _moveHoleUp(unsigned pos, VAL val) {
    // move hole up while the new value is larger than its parent
    while (pos > 0) {
      unsigned ppos = (pos - 1) >> 1;
      if (_val(ppos) >= val) return pos;
      // swap parent with hole.
      if (ORDERVAL) values[pos] = values[ppos];
      heap[pos] = heap[ppos];
      hpos[heap[pos]] = pos;
      pos = ppos;
    }
    return pos;
  }
  unsigned _moveHoleDown(unsigned pos, VAL val) {
    // move hole up while the new value is larger than its parent
    unsigned cpos = pos * 2 + 1;
    while (cpos < size) {
      if (cpos + 1 >= size) {
        // only the "left" child. this also means, it's a leaf.
        if (_val(cpos) > val) {
          if (ORDERVAL) values[pos] = values[cpos];
          heap[pos] = heap[cpos];
          hpos[heap[pos]] = pos;
          pos = cpos;
        }
        return pos;
      }
      // both children available
      // choose the child with the bigger value. if both children's values
      // a below val, we are done.
      VAL vpos = _val(cpos), vpos1 = _val(cpos + 1);
      if (vpos1 > val) {
        if (vpos1 >= vpos) {
          // choose right child if it is greater than the left child
          ++cpos;
          vpos = vpos1;
        }
      } else {
        if (vpos <= val) return pos;
      }
      // swap child with hole.
      if (ORDERVAL) values[pos] = vpos;
      heap[pos] = heap[cpos];
      hpos[heap[pos]] = pos;
      pos = cpos;
      cpos = pos * 2 + 1;
    }
    return pos;
  }
};


} // end of jitcs namespace

#endif
// _JITCS_INT_ADT_SMALLHEAP_H_
