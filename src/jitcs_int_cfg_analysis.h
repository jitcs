//===-- src/jitcs_int_cfg_analysis.h ----------------------------*- C++ -*-===//
// Control flow graph analysis.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//
// The point of CFG analysis is twofold: it will generate an order of basic
// blocks for code generation, trying to keep any "fallthru" hint encountered.
// Secondly, it can, on request, try to determine to loop depth for each and
// every basic block. The used algorithm is probably slow for large sets
// of basic blocks with many loops.
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_CFGANALYSIS_H_
#define _JITCS_INT_CFGANALYSIS_H_

#include "jitcs_base.h"
#include "jitcs_int_adt_bitmap.h"
#include "jitcs_adt_range.h"
#include "jitcs_adt_slice.h"
#include "jitcs_int_bblock_impl.h"

namespace jitcs {
class FunctionImpl;
class IDumper;

class CFGAnalysis {
public:
  enum AMode {
    SIMPLE,
    DEPTH_ANALYSIS
  };
public:
  CFGAnalysis();
  CFGAnalysis(const CFGAnalysis &) = delete;
  CFGAnalysis& operator=(const CFGAnalysis &) = delete;
  
public:
  void init(FunctionImpl& f, AMode);
  void clear();
  void dump(IDumper&) const;
  
  bool checkIntegrity() const;

public:
  //
  size_t getMaxBBlockDepth() const { return _maxDepth; }
  size_t getBlockCount() const { return _order.size(); }
  Ref<BasicBlockImpl> getOrderedBlock(size_t pos) const { return _order[pos]; }
  Ref<const BasicBlockImpl> cgetOrderedBlock(size_t pos) const { return _order[pos]; }
  size_t getDepth(Ref<BasicBlockImpl> bb) const { return _bblocks[bb->id()].depth; }
//  size_t getOrder(BBlock* bb) const { return _bblocks[bb->id().id].order; }

private:
  struct BBlockInfo {
    BasicBlockImpl* bb;
    size_t depth;
  };
private:
  size_t _calcDepth(size_t depth, size_t headbb, 
                    BitSlice handled, BitSlice candidates, 
                    Slice<size_t> worklist, BitSlice inworklist);

private:
  Slice<BasicBlockImpl*> _order;
  Slice<BBlockInfo> _bblocks;
  size_t _maxDepth;
};

} // end of namespace jitcs

#endif
// _JITCS_INT_CFGANALYSIS_H_
