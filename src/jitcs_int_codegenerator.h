//===-- src/jitcs_int_codegenerator.h ---------------------------*- C++ -*-===//
// Code generation helper.
//
// Copyright (C) 2013-2014 Dirk Steinke. 
// See copyright and license notice in COPYRIGHT or include/jitcs.h
//===----------------------------------------------------------------------===//

#ifndef _JITCS_INT_CODEGENERATOR_H_
#define _JITCS_INT_CODEGENERATOR_H_

#include "jitcs_base.h"
#include "jitcs_adt_ref.h"
#include "jitcs_adt_refcounter.h"
#include "jitcs_adt_slice.h"
#include "jitcs_int_bblock_impl.h"
#include "jitcs_ids.h"
#include "jitcs_tmpalloc.h"
#include "jitcs_memmgr.h"

namespace jitcs {
class FunctionImpl;
class CFGAnalysis;

class CodeGenerator {
public:
  enum RelocType {
    REL_PCREL32_TAIL = 32,
    REL_PCREL16_TAIL = 16,
    REL_PCREL8_TAIL = 8,
  };
  struct FrameRegisterInfo {
    RegId reg;
    int ofs;
  };

public:
  CodeGenerator();
  CodeGenerator(const CodeGenerator &) = delete;
  void operator=(const CodeGenerator &) = delete;
  
  MemoryMgr::CodeAndData generate(FunctionImpl&, CFGAnalysis const&, 
                                  MemoryMgr& vm, Slice<u8> data);

public:
  // methods for use when generating code
  bool hasEstimatedPos(Ref<const BasicBlockImpl> bb) const { 
    return _testFlags(bb, BBLOC_ESTIMATED); 
  }
  bool hasFinalAddress(Ref<const BasicBlockImpl> bb) const { 
    return _testFlags(bb, BBLOC_FINAL);
  }
  bool hasEstimatedOrFinalPos(Ref<const BasicBlockImpl> bb) const { 
    return _testFlags(bb, BBLOC_ESTIMATED | BBLOC_FINAL);
  }
  size_t getEstimatedOrFinalPos(Ref<const BasicBlockImpl> bb) const { 
    return _getPos(bb);
  }
  Ref<u8> getEstimatedOrFinalAddress(Ref<const BasicBlockImpl> bb) const { 
    return _codespace._ptr + _getPos(bb); 
  }
  void addRelocation(Ref<const BasicBlockImpl> bb, Ref<u8> x, RelocType rel);

  FrameRegisterInfo getFrameRegisterInfo(size_t i) { 
    assert(i < MMODE_FPCount);
    return _frameRegisterInfo[i];
  }

private:
  enum Layout {
    BBLOC_ESTIMATED = 1, // position of this basic block is estimated and cannot be further away
    BBLOC_FINAL = 2,     // position of this basic block is precisely defined
    BBLOC_SHIFT = 2,     // lowest two bits are flags
  };
  struct RelocInfo {
    RefOrNull<RelocInfo> next;
    RefOrNull<u8> tgt;
    RelocType type;
  };
  struct BBlockInfo {
    RefOrNull<RelocInfo> relocChain;
    size_t posAndFlags;
  };

private:
  void _setEstimatedPosAndClearChain(Ref<const BasicBlockImpl> bb, size_t x);
  void _setFinalPos(Ref<const BasicBlockImpl> bb, size_t x, Ref<u8> y);

  bool _testFlags(Ref<const BasicBlockImpl> bb, u8 f) const { return (_bblocks[bb->id()].posAndFlags & f) != 0; }
  size_t _getPos(Ref<const BasicBlockImpl> bb) const { return _bblocks[bb->id()].posAndFlags >> BBLOC_SHIFT; }

private:
  Slice<BBlockInfo> _bblocks;
  StreamAllocator<RelocInfo>* _relocalloc;
  Slice<u8> _codespace;
  FrameRegisterInfo _frameRegisterInfo[MMODE_FPCount];
};

} // end of namespace jitcs

#endif
// _JITCS_INT_CODEGENERATOR_H_
