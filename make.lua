generated_source_files = {
  ["src/data/x86_inslist.@(ltxt -> dat)"] = { 
    tools = "tools/x86_inslist2data.lua",
    params = {"$(SRC)", "$(DST)"}, 
  },
  ["src/data/x86_insalias.@(ltxt -> dat)"] = { 
    tools = "tools/x86_insalias2data.lua",
    params = {"$(SRC)", "$(DST)"}, 
  },
--[[  ["src/data/x86_reglist.@(ltxt -> dat)"] = { 
    tools = "tools/x86_reglist2data.lua",
    params = {"$(SRC)", "$(DST)"}, 
  },
--]]
  ["include/jitcs_x86_xx_cons.lh -> include/jitcs_x86_$(N)_cons.h"] = { 
    tools = "tools/template2header.lua",
    params = {"$(SRC)", "N=$(N)", "$(DST)" }, 
    N = {"32", "64"},
    add_deps = {"src/data/x86_inslist.dat", "src/data/x86_insalias.dat"},
  },
  ["include/jitcs_x86_xx_insids.lh -> include/jitcs_x86_$(N)_insids.h"] = {
    tools = "tools/template2header.lua",
    params = {"$(SRC)", "N=$(N)", "$(DST)" },
    N = {"32", "64"},
    add_deps = {"src/data/x86_inslist.dat", "src/data/x86_insalias.dat"},
  },
  ["include/jitcs_x86_xx_regs.lh -> include/jitcs_x86_$(N)_regs.h"] = {
    tools = "tools/template2header.lua",
    params = {"include/jitcs_x86_xx_regs.h", "N=$(N)", "$(DST)" },
    N = {"32", "64"},
    add_deps = {"src/data/x86_reglist.dat"},
  },
}
includefiles = {
  "include/jitcs.h",
  "include/jitcs_adt_bitstore.h",
  "include/jitcs_adt_ref.h",
  "include/jitcs_base.h",
  "include/jitcs_bblock.h",
  "include/jitcs_callconv.h",
  "include/jitcs_cpu.h",
  "include/jitcs_function.h",
  "include/jitcs_ids.h",
  "include/jitcs_insref.h",
  "include/jitcs_memmgr.h",
  "include/jitcs_memref.h",
  "include/jitcs_tmpalloc.h",
  "include/jitcs_vregref.h",
  "include/jitcs_x86_32.h",
  "include/jitcs_x86_64.h",
  "include/jitcs_x86_common_insids.h",
  "include/jitcs_x86_32_cons.h",
  "include/jitcs_x86_64_cons.h",
  "include/jitcs_x86_32_insids.h",
  "include/jitcs_x86_64_insids.h",
  "include/jitcs_x86_32_regs.h",
  "include/jitcs_x86_64_regs.h",
}
sourcefiles = {
  "src/cpu.cpp",
  "src/memmgr.cpp",
}
testfiles = {
}
