CC = g++.bat
CFLAGS = -I ./include
CFLAGS_DBG32 = -D_DEBUG
CFLAGS_REL32 = -D_NDEBUG -O3
CFLAGS_DBG64 = -D_DEBUG -m64
CFLAGS_REL64 = -D_NDEBUG -O3 -m64
LUA = luax51.bat
NASM = nasm.bat
RM = del
SEP = $(subst A,\,A)
SRC_SRC = cpu.cpp dumper.cpp tmpalloc.cpp memmgr.cpp \
    bblock.cpp bblock_impl.cpp function.cpp  function_impl.cpp \
    callingconvention.cpp callingconvention_impl.cpp \
		host.cpp machine.cpp virtualregister.cpp spillslotinfo.cpp \
		cfg_analysis.cpp codegenerator.cpp dfg_analysis.cpp adt_bitmap.cpp
SRC_X86 = x86_32_callingconvention.cpp x86_64_callingconvention.cpp \
		x86_32_machine.cpp x86_64_machine.cpp x86_32_regs.cpp x86_64_regs.cpp \
		x86_32_insinfo.cpp x86_64_insinfo.cpp x86_32_dumper.cpp x86_64_dumper.cpp \
		x86_32_codegen.cpp x86_64_codegen.cpp x86_32_ins2reg.cpp x86_64_ins2reg.cpp
SRC_UNITTEST = test_unittest.cpp test_main.cpp test_fnctypeinfo.cpp \
		test_adt_range.cpp test_adt_slice.cpp test_callingconvention.cpp \
		test_simplefunction.cpp test_adt_bitmap.cpp test_adt_tmpvector.cpp \
    test_adt_heap.cpp test_adt_smallheap.cpp \
		test_bitfuncs.cpp test_power2funcs.cpp test_cfg_analysis.cpp \
		test_simplecodegen.cpp test_asm.cpp \
		test_asm_x86_32_ins.cpp test_asm_x86_32_reg.cpp test_asm_x86_32_ext.cpp \
		test_asm_x86_64_ins.cpp test_asm_x86_64_reg.cpp test_asm_x86_64_ext.cpp \
		test_allinstructions.cpp test_dfg_analysis.cpp
X86_INS = x86_arith.ins x86_mov_set.ins x86_other.ins x86_other_vex.ins \
		x86_simd_arith.ins x86_simd_blend.ins x86_simd_cvt.ins \
		x86_simd_insext.ins x86_simd_mov.ins x86_simd_shuffle.ins\
		x86_simd_arith_vex.ins x86_simd_blend_vex.ins x86_simd_cvt_vex.ins \
		x86_simd_insext_vex.ins x86_simd_mov_vex.ins x86_simd_shuffle_vex.ins \
		x86_cf.ins x86_addrmode.ins
PREFIXES_LIBSRC = dbg32_ rel32_ dbg64_ rel64_
PREFIXES_TESTSRC = test32_ test64_ testd32_ testd64_
OBJDIR = _objs
LIBDIR = _lib
BINDIR = _bin
BINDATADIR = _bin/data
TEST_NAMES = $(patsubst %.cpp,%.o,$(SRC_SRC))
LIBOBJ_NAMES = $(addprefix src_,$(patsubst %.cpp,%.o,$(SRC_SRC))) \
               $(addprefix x86_,$(patsubst %.cpp,%.o,$(SRC_X86)))
LIBOBJS_DEBUG32 = $(addprefix $(OBJDIR)/dbg32_,$(LIBOBJ_NAMES))
LIBOBJS_RELEASE32 = $(addprefix $(OBJDIR)/rel32_,$(LIBOBJ_NAMES))
LIBOBJS_DEBUG64 = $(addprefix $(OBJDIR)/dbg64_,$(LIBOBJ_NAMES))
LIBOBJS_RELEASE64 = $(addprefix $(OBJDIR)/rel64_,$(LIBOBJ_NAMES))
UNITTESTOBJS_DEBUG32 = $(addprefix $(OBJDIR)/testd32_,$(patsubst test_%.cpp,%.o,$(SRC_UNITTEST)))
UNITTESTOBJS_DEBUG64 = $(addprefix $(OBJDIR)/testd64_,$(patsubst test_%.cpp,%.o,$(SRC_UNITTEST)))
UNITTESTOBJS_RELEASE32 = $(addprefix $(OBJDIR)/test32_,$(patsubst test_%.cpp,%.o,$(SRC_UNITTEST)))
UNITTESTOBJS_RELEASE64 = $(addprefix $(OBJDIR)/test64_,$(patsubst test_%.cpp,%.o,$(SRC_UNITTEST)))
OBJPREFIX_SRC = $(addprefix $(OBJDIR)/,$(addsuffix src_,$(PREFIXES_LIBSRC)))
OBJPREFIX_X86 = $(addprefix $(OBJDIR)/,$(addsuffix x86_,$(PREFIXES_LIBSRC)))
OBJPREFIX_TEST = $(addprefix $(OBJDIR)/,$(PREFIXES_TESTSRC))
AS32_FILES = $(addprefix $(BINDATADIR)/,$(patsubst x86_%.ins,x86_32_%.as,$(X86_INS)))
AS64_FILES = $(addprefix $(BINDATADIR)/,$(patsubst x86_%.ins,x86_64_%.as,$(X86_INS)))

#all : debug release tests
all : lua debug tests

clean:
	$(RM) $(subst /,$(SEP),_objs/*)
	$(RM) $(subst /,$(SEP),_lib/*)
	$(RM) $(subst /,$(SEP),_bin/*)
info:
	rem info
debug : $(LIBDIR)/jitcs_debug32.a $(LIBDIR)/jitcs_debug64.a
	
release : $(LIBDIR)/jitcs_release32.a $(LIBDIR)/jitcs_release64.a
tests : $(BINDIR)/test_cpuinfo32.exe $(BINDIR)/test_cpuinfo64.exe \
	$(AS32_FILES) $(AS64_FILES) \
	$(BINDIR)/unittestsd32.exe $(BINDIR)/unittestsd64.exe \
	$(BINDIR)/unittests32.exe $(BINDIR)/unittests64.exe

lua : include/jitcs_x86_32_cons.h include/jitcs_x86_64_cons.h \
	include/jitcs_x86_32_insids.h include/jitcs_x86_64_insids.h \
	include/jitcs_x86_32_regs.h include/jitcs_x86_64_regs.h \
	include/jitcs_x86_32_machine.h include/jitcs_x86_64_machine.h \
	src/x86/jitcs_int_x86_32_machine.h src/x86/jitcs_int_x86_64_machine.h \
	src/x86/jitcs_int_x86_32_dumper.h src/x86/jitcs_int_x86_64_dumper.h \
	src/x86/jitcs_int_x86_32_regs.h src/x86/jitcs_int_x86_64_regs.h \
	src/x86/src/x86_32_callingconvention.cpp src/x86/src/x86_64_callingconvention.cpp \
	src/x86/src/x86_32_codegen.cpp src/x86/src/x86_64_codegen.cpp \
	src/x86/src/x86_32_dumper.cpp src/x86/src/x86_64_dumper.cpp \
	src/x86/src/x86_32_ins2reg.cpp src/x86/src/x86_64_ins2reg.cpp \
	src/x86/src/x86_32_insinfo.cpp src/x86/src/x86_64_insinfo.cpp \
	src/x86/src/x86_32_machine.cpp src/x86/src/x86_64_machine.cpp \
	src/x86/src/x86_32_regs.cpp src/x86/src/x86_64_regs.cpp

include/jitcs_x86_32_cons.h : include/jitcs_x86_xx_cons.lh \
				src/data/x86_inslist.dat src/data/x86_insalias.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
include/jitcs_x86_64_cons.h : include/jitcs_x86_xx_cons.lh \
				src/data/x86_inslist.dat src/data/x86_insalias.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
include/jitcs_x86_32_insids.h : include/jitcs_x86_xx_insids.lh \
				src/data/x86_inslist.dat src/data/x86_insalias.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
include/jitcs_x86_64_insids.h : include/jitcs_x86_xx_insids.lh \
				src/data/x86_inslist.dat src/data/x86_insalias.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
include/jitcs_x86_32_regs.h : include/jitcs_x86_xx_regs.lh src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
include/jitcs_x86_64_regs.h : include/jitcs_x86_xx_regs.lh src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
include/jitcs_x86_32_machine.h : include/jitcs_x86_xx_machine.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
include/jitcs_x86_64_machine.h : include/jitcs_x86_xx_machine.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/jitcs_int_x86_32_machine.h : src/x86/jitcs_int_x86_xx_machine.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/jitcs_int_x86_64_machine.h : src/x86/jitcs_int_x86_xx_machine.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/jitcs_int_x86_32_regs.h : src/x86/jitcs_int_x86_xx_regs.lh src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/jitcs_int_x86_64_regs.h : src/x86/jitcs_int_x86_xx_regs.lh src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/jitcs_int_x86_32_dumper.h : src/x86/jitcs_int_x86_xx_dumper.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/jitcs_int_x86_64_dumper.h : src/x86/jitcs_int_x86_xx_dumper.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_callingconvention.cpp : src/x86/src/x86_xx_callingconvention.lcpp \
				src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_callingconvention.cpp : src/x86/src/x86_xx_callingconvention.lcpp \
				src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_machine.cpp : src/x86/src/x86_xx_machine.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_machine.cpp : src/x86/src/x86_xx_machine.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_regs.cpp : src/x86/src/x86_xx_regs.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_regs.cpp : src/x86/src/x86_xx_regs.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_insinfo.cpp : src/x86/src/x86_xx_insinfo.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua src/data/x86_inslist.dat
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_insinfo.cpp : src/x86/src/x86_xx_insinfo.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua src/data/x86_inslist.dat
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_dumper.cpp : src/x86/src/x86_xx_dumper.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua src/data/x86_inslist.dat
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_dumper.cpp : src/x86/src/x86_xx_dumper.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua src/data/x86_inslist.dat
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_codegen.cpp : src/x86/src/x86_xx_codegen.lcpp src/data/x86_inslist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_codegen.cpp : src/x86/src/x86_xx_codegen.lcpp src/data/x86_inslist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
src/x86/src/x86_32_ins2reg.cpp : src/x86/src/x86_xx_ins2reg.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua src/data/x86_inslist.dat
	$(LUA) tools/template2source.lua $< 32 $@
src/x86/src/x86_64_ins2reg.cpp : src/x86/src/x86_xx_ins2reg.lcpp src/data/x86_reglist.dat \
				tools/template2source.lua src/data/x86_inslist.dat
	$(LUA) tools/template2source.lua $< 64 $@

tests/test_asm_x86_32.h : tests/test_asm_x86_xx.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
tests/test_asm_x86_64.h : tests/test_asm_x86_xx.lh \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
tests/test_asm_x86_32_reg.cpp : tests/test_asm_x86_xx_reg.lcpp \
				src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
tests/test_asm_x86_64_reg.cpp : tests/test_asm_x86_xx_reg.lcpp \
				src/data/x86_reglist.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
tests/test_asm_x86_32_ins.cpp : tests/test_asm_x86_xx_ins.lcpp \
				src/data/x86_inslist.dat src/data/x86_reglist.dat \
				src/data/x86_insalias.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
tests/test_asm_x86_64_ins.cpp : tests/test_asm_x86_xx_ins.lcpp \
				src/data/x86_inslist.dat src/data/x86_reglist.dat \
				src/data/x86_insalias.dat \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@
tests/test_asm_x86_32_ext.cpp : tests/test_asm_x86_xx_ext.lcpp \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 32 $@
tests/test_asm_x86_64_ext.cpp : tests/test_asm_x86_xx_ext.lcpp \
				tools/template2source.lua
	$(LUA) tools/template2source.lua $< 64 $@


src/data/x86_inslist.dat : src/data/x86_inslist.ltxt tools/x86_inslist2data.lua
	$(LUA) tools/x86_inslist2data.lua $< $@
src/data/x86_insalias.dat : src/data/x86_insalias.ltxt tools/x86_insalias2data.lua
	$(LUA) tools/x86_insalias2data.lua $< $@

$(LIBDIR)/jitcs_debug32.a : $(LIBOBJS_DEBUG32) | $(LIBDIR)
	ar r $@ $?
$(LIBDIR)/jitcs_release32.a : $(LIBOBJS_RELEASE32) | $(LIBDIR)
	ar r $@ $?
$(LIBDIR)/jitcs_debug64.a : | $(LIBDIR)
	rem --
$(LIBDIR)/jitcs_release64.a : | $(LIBDIR)
	rem --

$(BINDIR)/test_cpuinfo32.exe : tests/cpuinfo.cpp $(LIBDIR)/jitcs_release32.a \
                             include/jitcs_cpu.h \
                            | $(BINDIR)
	$(CC) -o $@ $(CFLAGS) $(CFLAGS_REL32) $< $(LIBDIR)/jitcs_release32.a
$(BINDIR)/test_cpuinfo64.exe : | $(BINDIR)
	rem --
$(BINDIR)/unittests32.exe : $(UNITTESTOBJS_RELEASE32) $(LIBDIR)/jitcs_release32.a \
                            | $(BINDIR) $(BINDATADIR)
	$(CC) -o $@ $(CFLAGS) $(CFLAGS_REL32) $(UNITTESTOBJS_RELEASE32) $(LIBDIR)/jitcs_release32.a
$(BINDIR)/unittestsd32.exe : $(UNITTESTOBJS_DEBUG32) $(LIBDIR)/jitcs_debug32.a \
                            | $(BINDIR) $(BINDATADIR)
	$(CC) -o $@ $(CFLAGS) $(CFLAGS_DBG32) $(UNITTESTOBJS_DEBUG32) $(LIBDIR)/jitcs_debug32.a
$(BINDIR)/unittests64.exe : | $(BINDIR) $(BINDATADIR)
	rem --
$(BINDIR)/unittestsd64.exe : | $(BINDIR) $(BINDATADIR)
	rem --

$(OBJDIR)/dbg32_src_%.dep : src/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_DBG32) $<
$(OBJDIR)/rel32_src_%.dep : src/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_REL32) $<
$(OBJDIR)/dbg64_src_%.dep : src/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_DBG64) $<
$(OBJDIR)/rel64_src_%.dep : src/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_REL64) $<
$(OBJDIR)/dbg32_x86_%.dep : src/x86/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_DBG32) $<
$(OBJDIR)/rel32_x86_%.dep : src/x86/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_REL32) $<
$(OBJDIR)/dbg64_x86_%.dep : src/x86/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_DBG64) $<
$(OBJDIR)/rel64_x86_%.dep : src/x86/src/%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./src $(CFLAGS) $(CFLAGS_REL64) $<
$(OBJDIR)/test32_%.dep : tests/test_%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./tests -I ./src $(CFLAGS) $(CFLAGS_REL32) $<
$(OBJDIR)/testd32_%.dep : tests/test_%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./tests -I ./src $(CFLAGS) $(CFLAGS_DBG32) $<
$(OBJDIR)/test64_%.dep : tests/test_%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./tests -I ./src $(CFLAGS) $(CFLAGS_REL64) $<
$(OBJDIR)/testd64_%.dep : tests/test_%.cpp
	$(CC) -o $@ -MM -MT $(patsubst %.dep,%.o,$@) -MT $@ -I ./tests -I ./src $(CFLAGS) $(CFLAGS_DBG64) $<

ifneq ($(MAKECMDGOALS),clean)
     include $(LIBOBJS_DEBUG32:.o=.dep)
     include $(LIBOBJS_RELEASE32:.o=.dep)
     include $(UNITTESTOBJS_DEBUG32:.o=.dep)
     include $(UNITTESTOBJS_RELEASE32:.o=.dep)
endif

$(OBJDIR)/dbg32_src_%.o : src/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_DBG32) $<
$(OBJDIR)/rel32_src_%.o : src/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_REL32) $<
$(OBJDIR)/dbg64_src_%.o : src/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_DBG64) $<
$(OBJDIR)/rel64_src_%.o : src/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_REL64) $<
$(OBJDIR)/dbg32_x86_%.o : src/x86/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_DBG32) $<
$(OBJDIR)/rel32_x86_%.o : src/x86/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_REL32) $<
$(OBJDIR)/dbg64_x86_%.o : src/x86/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_DBG64) $<
$(OBJDIR)/rel64_x86_%.o : src/x86/src/%.cpp
	$(CC) -o $@ -c -I ./src $(CFLAGS) $(CFLAGS_REL64) $<
$(OBJDIR)/test32_%.o : tests/test_%.cpp
	$(CC) -o $@ -c -I ./tests -I ./src $(CFLAGS) $(CFLAGS_REL32) $<
$(OBJDIR)/testd32_%.o : tests/test_%.cpp
	$(CC) -o $@ -c -I ./tests -I ./src $(CFLAGS) $(CFLAGS_DBG32) $<
$(OBJDIR)/test64_%.o : tests/test_%.cpp
	$(CC) -o $@ -c -I ./tests -I ./src $(CFLAGS) $(CFLAGS_REL64) $<
$(OBJDIR)/testd64_%.o : tests/test_%.cpp
	$(CC) -o $@ -c -I ./tests -I ./src $(CFLAGS) $(CFLAGS_DBG64) $<
  
$(BINDATADIR)/x86_32_%.as : tests/x86/x86_%.ins tools/x86_instool.lua
	$(LUA) tools/x86_instool.lua ins2nasm32 $< $(OBJDIR)/n32_$*.nasm
	$(NASM) -o $(OBJDIR)/n32_$*.bin -l $(OBJDIR)/n32_$*.lst $(OBJDIR)/n32_$*.nasm
	$(LUA) tools/x86_instool.lua nasm2as32 $< $(OBJDIR)/n32_$*.lst $@
$(BINDATADIR)/x86_64_%.as : tests/x86/x86_%.ins tools/x86_instool.lua
	$(LUA) tools/x86_instool.lua ins2nasm64 $< $(OBJDIR)/n64_$*.nasm
	$(NASM) -o $(OBJDIR)/n64_$*.bin -l $(OBJDIR)/n64_$*.lst $(OBJDIR)/n64_$*.nasm
	$(LUA) tools/x86_instool.lua nasm2as64 $< $(OBJDIR)/n64_$*.lst $@

$(LIBOBJS_DEBUG32) $(LIBOBJS_RELEASE32) \
	$(LIBOBJS_DEBUG64) $(LIBOBJS_RELEASE64) \
	$(UNITTESTOBJS_RELEASE32) : | $(OBJDIR)
$(BINDATADIR): | $(BINDIR)
$(OBJDIR) $(LIBDIR) $(BINDIR) $(BINDATADIR):
	mkdir $(subst /,$(SEP),$@)
