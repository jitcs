--[[
  Instruction List To Data tool for X86:
  Copyright (C) 2013-2014 Dirk Steinke. 
  See copyright and license notice in COPYRIGHT or include/jitcs.h

  The tool reads src/data/x86_inslist.ltxt and transforms it into 
  src/data/x86_inslist.dat.
]]

local _type, _ipairs, _pairs, _tonum, _tostr, _assert, _error = type, ipairs, pairs, tonumber, tostring, assert, error
local _loadstr = loadstring
local _match, _gmatch, _sub, _lower, _upper = string.match, string.gmatch, string.sub, string.lower, string.upper
local _fmt, _gsub = string.format, string.gsub
local _sort = table.sort
local _open = io.open
local _ceil, _pow = math.ceil, math.pow

require"dump"

local _trim = function(s) return _match(s, "^%s*(.-)%s*$") or s end
function _dump(x, unquote)
  if not unquote and _type(x) == "string" then
    return _fmt("%q", x)
  elseif _type(x) ~= "table" then
    return tostring(x)
  else
    local r = "{"
    local n = #x
    for i = 1,n do r = r .. (i > 1 and ", " or "") .. _dump(x[i], unquote) end
    local pending = n > 0 and ";"
    for k,v in _pairs(x) do
      if _type(k) == "number" and k == _ceil(k) and k >= 1 and k <=n then
        --
      elseif _type(k) == "string" and _match(k,"^%a[%w_]*$") then
        if pending then r = r .. pending; pending = nil end
        r = r .. k .. " = " .. _dump(v, unquote)
        pending = ","
      else
        if pending then r = r .. pending; pending = nil end
        r = r .. "[" .. _dump(k, unquote) .. "] = " .. _dump(v, unquote)
        pending = ","
      end
    end
    r = r .. "}"
    return r
  end
end
--------------
local function readfile(n) 
  local f = _open(n) or _open("tools/"..n)
  local ctnt = f:read("*a")
  f:close()
  return ctnt
end

print("PAR",...)

local testname, outname = ...
if (testname == nil or testname == "") then 
  testname = "..\\src-lib\\x86\\src\\_x86_inslist.ltxt"
end
if outname == nil or outname == "" then
  outname = _match(testname, "^(.-)%.[^.\\/]*$") .. ".dat"
end

local emblua = _loadstr(readfile("emblua.lua"))
local inslist = emblua(testname,{type="string"})
--local inslist = readfile("..\\evm\\x86\\src\\_x86_inslist.txt")
-----------------------------------------------------------------
--
-----------------------------------------------------------------

local _cacheMatchWords = {}
local function matchWords(w, n)
  local v = _cacheMatchWords[n]
  if not v then
    v = "^%s*"
    for i = 1, n do v = v .. "(%w+)%s*" end
    v = v .. "$"
    _cacheMatchWords[n] = v
  end
  return _match(w, v)
end
local _cacheMatchID = {}
local function matchID(w, n)
  local v = _cacheMatchID[n]
  if not v then
    v = _gsub(n, "$ID", "%%a[%%w_]*")
    v = _gsub(v, "$NUM", "%%d+")
    v = _gsub(v, "$OP", "[^,]-")
    v = _gsub(v, " ", "%%s*")
    v = "^%s*" .. v .. "%s*$"
    _cacheMatchID[n] = v
  end
  return _match(w, v)
end


-----------------------------------------------------------------
--
-----------------------------------------------------------------


-----------------------------------------------------------------
--
-----------------------------------------------------------------

local cache, cachecount = {}, {}
local function unify(section, t, pred)
  local subcache = cache[section]
  if not subcache then subcache = {}; cache[section] = subcache end
  local sig = (pred or _dump)(t, true)
  local data = subcache[sig] 
  if not data then 
    local id = (cachecount[section] or 0) + 1
    data = t; data.id = id; data.sig = sig; subcache[sig] = data; cachecount[section] = id end
  return data
end
local function _opassert(cond, op, msg, ...)
  if cond then return end
  assert(false, _fmt("op %s: "..msg, op.name, ...))
end
--------------------------------------
local _isetset= {
  def = {},
  cmov = {},
  mmx = {},
  mmxext = {"mmx"},
  sse = {},
  sse2 = {"sse", "mmxext"},
  sse3 = {"sse2"},
  ssse3 = {"sse3"},
  sse41 = {"ssse3"},
  sse42 = {"sse41"},
  avx = {"sse42"},
  avx2 = {"avx"},
  fma3 = {"avx"},
  aes = {"sse2"},
  clmul = {"sse2"},
  hle = {},
  rtm = {},
  bmi1 = {"avx"},
  bmi2 = {"avx"},
  movbe = {},
  lzcnt = {},
  popcnt = {},
  rdrand = {},
  clflush = {},
  rdtsc = {},
  rdtscp = {},
  f16c = {"avx"},
  x32only = {},
  x64 = {"sse2","cmov"},
  x64only = {"x64"},
  avx512f = {"avx2","f16c","fma3"}}
local function _simplifyISA(t, v)
  --if not _isetset[v] then return end
  for k,vv in _ipairs(_isetset[v]) do
    t[vv] = true
    _simplifyISA(t, vv)
  end
end
  
local function _parseISA(op, data)
  _opassert(not op.isa, op, "double ISA declaration")
  local iset = {}
  for i in _gmatch(data,"[%w_]+") do
    _opassert(_isetset[i], op, "invalid isa %s", i)
    if i == "x64only" then i = "x64" end
    iset[i] = true
    _simplifyISA(iset, i)
  end
  local dep = {}
  for k,v in _pairs(iset) do
    _simplifyISA(dep, k)
  end
  local iset2 = {}
  for k,v in _pairs(iset) do if not dep[k] then iset2[#iset2 + 1] = k end end
  _sort(iset2)
  op.isa = unify("isa", iset2)
  if iset.x64 then op.isa.x64 = true end
  if iset.x32only then op.isa.x32only = true end
end
--------------------------------------
local function list2set( list )
  local set= {}
  for k,v in _ipairs(list) do set[v]= k end
  return set
end
local _typeset= list2set{"GR8","GR16","GR32","GR64","VR128","VR256","VR512","KR",
                   "i8","i16","i32","i64",
                   "i8*","i16*","i32*","i64*","i128*","i256*","i512*",
                   "BB"}
local _fixedregs = {
  al = "rax", ah = "rax", ax = "rax", eax = "rax", rax = "rax",
  cl = "rcx", ecx = "rcx", rcx = "rcx",
  dx = "rdx",  edx = "rdx", rdx = "rdx",
  ebx = "rbx", rbx = "rbx",
  edi = "rdi", rdi = "rdi",
  esp = "rsp", 
  xmm0 = "xmm0",
}
local function _parseOps(op, data)
  _opassert(not op.ops, op, "double operands declaration")
  local varlist, varset = {}, {}
  for p in _gmatch(data, "[^,]+") do
    p = _trim(p)
    if p ~= "" then
      local tp, fixedreg, vname, mode = matchID(p, "($ID%*?) { ($ID) } ($ID)/($ID)" )
      if not tp then
        tp,vname,mode= matchID(p, "($ID%*?) ($ID)/($ID)")
        fixedreg= nil
      end
      fixedreg = fixedreg and _lower(fixedreg)
      _opassert(tp and vname, op, "invalid operand \"%s\"", p)
      _opassert(_typeset[tp], op, "invalid type %s", tp)
      _opassert(not varset[vname], op, "double declaration of operand %s", vname)
      _opassert(mode == "i" or mode == "o" or mode == "io",
                op, "operand %s uses invalid mode %s", vname, mode)
      varset[vname]= true
      if (tp == "GR8" or tp == "GR16") and mode == "o" then mode = "io" end
      local skip = false
      if fixedreg then
        _opassert(_fixedregs[fixedreg], op, "unsupported fixed reg %s", fixedreg)
        fixedreg = _fixedregs[fixedreg]
        for i,v in _ipairs(varlist) do
          if v.fixed == fixedreg then
            local mi = _match(v.mode, "i") or _match(mode, "i")
            local mo = _match(v.mode, "o") or _match(mode, "o")
            v.mode = (mi and "i" or "")..(mo and "o" or "")
            skip = true
            break
          end
        end
      end
      if not skip then
        varlist[ #varlist+1 ]= {name=vname,type=tp,fixed=fixedreg,mode=mode}
      end
    end
  end
  op.operands = varlist
end
--------------------------------------
local _flagset= {
  rflags = {flagin = true}, rflag_c = {flagin = true}, rflag_o = {flagin = true}, 
  rflag_s = {flagin = true}, rflag_z = {flagin = true}, rflag_p = {flagin = true},
  wflags = {flagout = true}, 
  wflag_c = {flagin = true, flagout = true}, wflag_o = {flagin = true, flagout = true}, 
  subflags = {flagin = true},
  wmem = {}, rmem = {}, rwmem = {}, usemem = {}, amem = {}, umem = {},
  cf_jmp = {}, cf_call = {}, cf_ret = {}, cf_fallthru = {},
  vsib = {},
  }

local function _parseFlags(op, data)
  _opassert(not op.flags, op, "double flags declaration")
  local fset = {}
  for i in _gmatch(data,"[%w_]+") do
    _opassert(_flagset[i], op, "invalid flag %s", i)
    fset[i] = true
  end
  if fset.rwmem then fset.wmem = true; fset.rmem = true; fset.rwmem = false end
  local fset2 = {}
  for k,v in _pairs(fset) do fset2[#fset2 + 1] = k end
  _sort(fset2)
  op.flags = unify("flags", fset2)
end
--------------------------------------
local _prefset= {
  ["66"] = {},
  ["f2"] = {},
  ["f3"] = {},
  ["66f2"] = {}, -- only for crc32_16
  }
local function _parsePref(op, data)
  _opassert(not op.pref, op, "double prefix declaration")
  if data == "" then op.pref = ""; return end
  local p = _match(data, "^([%w_]+)$")
  _opassert(p and _prefset[p], op, "invalid prefix %s", _tostr(p))
  op.pref = p
end
--------------------------------------
local _extopcodeset= {
  ["0f"] = {},
  ["0f38"] = {},
  ["0f3a"] = {},
  }
local function _parseExtOpcode(op, data)
  _opassert(not op.extopcode, op, "double extended opcode declaration")
  if data == "" then op.extopcode = ""; return end
  local p = _match(data, "^([%w_]+)$")
  _opassert(p and _extopcodeset[p], op, "invalid extended opcode %s", _tostr(p))
  op.extopcode = p
end
--------------------------------------
local function _parseCoding(op, data)
  _opassert(not op.code, op, "double coding declaration")
  local result= {}
  for c in _gmatch(data, "%S+") do
    local cnt = {}
    _gsub(c, "[%(%)]", function (m) cnt[m] = (cnt[m] or 0) + 1 end)
    _opassert(cnt["("] == cnt[")"], op, "unbalanced parenthesis in coding")
    
    c = _gsub(c, "!par%((%w+)%)", function (p)
                _opassert(not op.parm, op, "double !par declaration")
                _opassert(_tonum(p), op, "invalid !par value")
                op.parm = _tonum(p)
                return "$parm"
              end)
    c = _gsub(c, "!sub%((%w+)%)", function (p)
                _opassert(not op.sub, op, "double !sub declaration")
                _opassert(_tonum(p), op, "invalid !sub value")
                op.sub = _tonum(p)
                return "$sub"
              end)
    c = _gsub(c, "!parsub%((%w+)%)", function (p)
                _opassert(not op.par, op, "double !par declaration")
                _opassert(not op.sub, op, "double !sub declaration")
                _opassert(_tonum(p), op, "invalid !parsub value")
                op.parm = _tonum(p)
                op.sub = op.parm % 32
                op.parm = op.parm - op.sub
                return "$parm+$sub"
              end)
    result[ #result+1 ]= c
  end
  op.code = result
end
--------------------------------------
local function _parseLEAREX(op, data)
  _opassert(not op.rex, op, "double rex/vex declaration in op "..op.name)
  local w, reg, base, index = matchWords(data,4)
  _opassert(reg, op, "invalid rexlea parameters")
  _opassert(w == "0" or w == "1", op, "invalid rex w field")
  op.rex = {mode = "lea", reg = reg, rmb = base, rmi = index, r64 = w}
end
local function _parseREX(op, data)
  _opassert(not op.rex, op, "double rex/vex declaration in op "..op.name)
  local rm, w, rmr, rmm = matchWords(data,4)
  _opassert(rm, op, "invalid rex parameters")
  _opassert(rm == "rr" or rm == "rm", op, "invalid rex rm field")
  _opassert(w == "0" or w == "1", op, "invalid rex w field")
  op.rex = {mode = rm, reg = rmr, rm = rmm, r64 = w}
end
local function _parseVEX(op, data)
  _opassert(not op.rex, op, "double rex/vex declaration")
  local rm, w, src1, rmr, rmm, l = matchWords(data,6)
  _opassert(rm, op, "invalid rex parameters")
  _opassert(rm == "rr" or rm == "rm", op, "invalid vex rm field")
  _opassert(w == "0" or w == "1", op, "invalid vex w field")
  _opassert(l == "0" or l == "1", op, "invalid vex l field")
  op.rex = {vex = true, mode = rm, reg = rmr, rm = rmm, r64 = w, l = l, src1 = src1}
end
local function _parseEVEX(op, data)
  _opassert(not op.rex, op, "double rex/vex declaration in op "..op.name)
  local rm, w, src1, rmr, rmm, l, k, z, b = matchWords(data,9)
  _opassert(rm, op, "invalid rex parameters")
  _opassert(rm == "rr" or rm == "rm", op, "invalid evex rm field")
  _opassert(w == "0" or w == "1", op, "invalid evex w field")
  _opassert(l == "0" or l == "1" or l == "2" or l == "3", op, "invalid evex l field")
  _opassert(z == "0" or z == "1", op, "invalid evex z field")
  _opassert(b == "0" or b == "1", op, "invalid evex b field")
  op.rex = {evex = true, mode = rm, reg = rmr, rm = rmm, r64 = w, l = l, src1 = src1,
            k = k, z = z, b = b}
end
local function _parseFold(op, data)
end
local function _parseCommute(op, data)
end

--------------------------------------

local function _getValidVarSet( op )
--print( op.name )
  local vars={ parm="i8", sub="i8" }
  for _,v in _ipairs(op.operands) do vars[v.name]= v.type end
  return vars
end

local function _ptrtype(s)
  return s and _sub(s,-1,-1)=="*"
end
local function _immtype(s)
  return s and _sub(s,1,1)=="i" and not _ptrtype(s)
end
local function _regtype(s)
  return s and not _ptrtype(s) and not _immtype(s)
end

local function _checkOp( op )
  local vars= _getValidVarSet(op)

  local memops = 0
  for _,v in _ipairs(op.operands) do 
    if _ptrtype(v.type) then
      _opassert(v.mode == "i", op, "memory operands are input only")
      _opassert(memops == 0, op, "only one memory operand allowed")
      memops = memops + 1
    end
  end
  
  for _,v in _ipairs(op.code) do
    for vv in _gmatch(v, "%$(%a%w*)") do
      _opassert(vars[vv], op, "invalid variable %s", vv)
    end

    local p1,p2,p3,p4,p5= matchID(v,"!RRMLEA%( ($OP) , ($OP) , ($OP) , ($OP) , ($OP) %)")
    if p1 then
      _opassert(op.rex.mode=="lea", op, "RRM?-rex mode mismatch" )
      _opassert(not op.rex.vex and not op.rex.evex, op, "RRMLEA and vex don't mix" )
      _opassert(_sub(p1,1,1)=="$" and vars[_sub(p1,2)], op, ": RRMLEA-reg error" )
      _opassert(_sub(p1,2)==op.rex.reg, op, "RRMLEA-rex reg mismatch" )
      _opassert(_sub(p2,1,1)=="$" and vars[_sub(p2,2)], op, "RRMLEA-rm base error" )
      _opassert(_sub(p2,2)==op.rex.rmb, op, "RRMLEA-rex rm base mismatch" )
      _opassert(_sub(p3,1,1)=="$" and vars[_sub(p3,2)], op, "RRMLEA-rm index error" )
      _opassert(_sub(p3,2)==op.rex.rmi, op, "RRMLEA-rex rm index mismatch" )
    end

    p1,p2,p3= matchID(v, "!RRMMVSIB([RM])%( ($OP) , ($OP) , ($OP) %)")
    if p1 then
      _opassert(op.rex.mode=="rm", op, "RRMMVSIB-rex mode mismatch" )
      _opassert(op.rex.vex or op.rex.evex, op, "RRMMVSIB requires vex" )
      _opassert(_sub(p1,1,1)=="$" and vars[_sub(p1,2)], op, ": RRMMVSIB-reg error" )
      _opassert(_sub(p1,2)==op.rex.reg, op, "RRMMVSIB-rex reg mismatch" )
      _opassert(_sub(p2,1,1)=="$" and vars[_sub(p2,2)], op, "RRMMVSIB-rm base error" )
      _opassert(_sub(p2,2)==op.rex.rm, op, "RRMMVSIB-rex rm base mismatch" )
      _opassert(_sub(p3,1,1)=="$" and vars[_sub(p3,2)], op, "RRMMVSIB-vsib index error" )
    end
    local x
    x,p1,p2= matchID(v, "!RRM([RM])%( ($OP) , ($OP) %)")
    if x then
      local rm = "r".._lower(x)
      _opassert(op.rex.mode == rm, op, "RRM%s-rex mode mismatch", x)
      if _tonum(p1) then
        _opassert( op.rex.reg==p1, op, "RRM%s-rex reg(#) mismatch", x)
      elseif p1=="$parm" then
        _opassert( op.parm>=0 and op.parm<8 and op.rex.reg=="0", op, "RRM%s-rex reg(parm) mismatch", x)
      elseif p1=="$sub" then
        local sub= op.sub or 0
        _opassert( sub>=0 and sub<8 and op.rex.reg=="0", op, "RRM%s-rex reg(parm) mismatch", x)
      else
        _opassert(_sub(p1,1,1)=="$" and vars[_sub(p1,2)], op, "RRM%s-reg error", x)
        _opassert(_sub(p1,2)==op.rex.reg, op, "RRM%s-rex reg mismatch", x)
      end
      if _tonum(p2) then
        _opassert((op.rex.rm or "0") == "0", op, "RRM%s-rex rm mismatch", x)
      else
        _opassert(_sub(p2,1,1)=="$" and vars[_sub(p2,2)], op, "RRM%s-rm error", x)
        _opassert(_sub(p2,2)==op.rex.rm, op, "RRM%s-rex rm mismatch", x)
      end
    end
  end
  _opassert( op.rex.reg=="0" or _regtype(vars[op.rex.reg]), op, "rex-reg error" )
  if op.rex.mode=="rr" then
    _opassert( op.rex.rm=="0" or _regtype(vars[op.rex.rm]), op, "rex-rmr error" )
  elseif op.rex.mode=="rm" then
    _opassert( op.rex.mode=="rm" and _ptrtype(vars[op.rex.rm]), op, "rex-rmm error" )
  end
end

local function _createLayout(op)
  local layout, renametab, pnum = {}, {}, nil
  local indices = {}
  --print(op.name, dump(op.rex))
  local function renameVar(v)
    local res = renametab[v]
    if not res then 
      pnum = (pnum or 0) + 1
      res = {"p".._tostr(pnum), pnum}
      renametab[v] = res
    end
    return res[1]
  end
  local function add2layout(v, defuse)
    if v.fixed then return end
    local n = renameVar(v.name)
    if indices[v.name] then return end
    local mode, lp3
    if _ptrtype(v.type) then
      mode = "MemId"
    elseif _immtype(v.type) then
      mode, lp3 = "Imm", 0
    elseif v.type == "BB" then
      mode = "BBId"
    else
      mode, lp3 = "RegId", nil
      if v.type == "GR8" then lp3 = "GR8" end
      --if v.type == "GR16" or v.type == "GR32" or v.type == "GR64" or v.type == "GR" then lp3 = "GR" end
      --if v.type == "VR128" or v.type == "VR256" or v.type == "VR512" then lp3 = "VR" end
    end
    layout[#layout + 1] = {mode, defuse, n, lp3}; 
    indices[v.name] = #layout 
    if v.type == "i64" then
      layout[#layout + 1] = {"Imm", "use", n, 32}
    end
  end
   
   -- make sure, memory operands are the first parameter
   -- this preferred order should simplify the code generator a bit
  for _,v in _ipairs(op.operands) do 
    if _ptrtype(v.type) then add2layout(v, "use") end
  end
  local orderedlayout = {}
  if op.rex.rm and not _tonum(op.rex.rm) then orderedlayout[#orderedlayout+1] = op.rex.rm end
  if op.rex.reg and not _tonum(op.rex.reg) then orderedlayout[#orderedlayout+1] = op.rex.reg end
  if op.rex.src1 and not _tonum(op.rex.src1) then orderedlayout[#orderedlayout+1] = op.rex.src1 end
  if op.rex.msk and not _tonum(op.rex.msk) then orderedlayout[#orderedlayout+1] = op.rex.msk end
  local t = {i = "use", o = "def", io = "use/def"}
  for _1,v1 in _ipairs(orderedlayout) do
    for _,v in _ipairs(op.operands) do 
      if v.name == v1 then
        add2layout(v, t[v.mode])
      end
    end
  end

  for _,v in _ipairs(op.operands) do
    add2layout(v, t[v.mode])
  end
  for _,v in _ipairs(op.operands) do
    if v.fixed then
      local n = renameVar(v.name)
    end
  end
  op.layout = unify("layout", layout)
  op.rename2layout = renametab
end

local function _renameFromLayout(op)
  op.orgnames = {}
  for i,v in _ipairs(op.operands) do 
    op.orgnames[i] = v.name
    v.name = op.rename2layout[v.name][1]
  end
  for _,v in _ipairs{"reg", "rm", "src1", "msk", "rmb", "rmi"} do
    if op.rex[v] and op.rename2layout[op.rex[v]] then
      op.rex[v] = op.rename2layout[op.rex[v]][1]
    end
  end
  for i,v in _ipairs(op.code) do
    op.code[i] = _gsub(v, "%$(%a%w*)", function (vv)
                         if op.rename2layout[vv] then return "$"..op.rename2layout[vv][1] end
                       end)
  end

  op.operands = unify("operands", op.operands)
  op.orgnames = unify("orgnames", op.orgnames)
  op.rex = unify("rex", op.rex)
  op.code = unify("code", op.code)
  op.rename2layout = nil
end
local function _createImpRegs(op)
  local regs = {}
  local t = {i = "use", o = "def", io = "use/def"}
  for _,v in _ipairs(op.operands) do
    if not _ptrtype(v.type) and not _immtype(v.type) and not (v.type == "BB") then
      if v.fixed then
        regs[#regs + 1] = {v.fixed, t[v.mode]}
      end
    end
  end   
  -- flag comes last
  local flagin, flagout = false, false
  for _,v in _ipairs(op.flags) do
    flagin = flagin or _flagset[v].flagin
    flagout = flagout or _flagset[v].flagout
  end
  if flagin or flagout then
    regs[#regs + 1] = {"FLAGS", flagout and (flagin and "use/def" or "def") or "use"}
  end
  op.impregs = unify("impregs", regs)
end
function _getPrefixSig(op)
  local r, pending = ""
  if op.rex.vex then
    r = "vex"
    if op.pref ~= "" then r = r .. "-" .. op.pref end
    if op.extopcode ~= "" then r = r .. "-" .. op.extopcode end
    r = r .. "-"
    if tonumber(op.rex.l) > 0 then r = r .. (128 * _pow(2,tonumber(op.rex.l))) .. "." end
    if tonumber(op.rex.r64) > 0 then r = r .. "W" end
    if not tonumber(op.rex.reg) then r = r .. "R".._gsub(op.rex.reg,"p","") end
    if not tonumber(op.rex.rm) then
      r = r .. (op.rex.mode == "rr" and "B" or "M").._gsub(op.rex.rm,"p","") 
    end
    if not tonumber(op.rex.src1) then r = r .. "S".._gsub(op.rex.src1,"p","") end
  else
    if op.pref ~= "" then r = r .. op.pref; pending = "-" end
    if op.rex.mode == "lea" then
      if pending then r = r .. pending; pending = nil end; 
      r = r .. "lea"
      if tonumber(op.rex.r64) > 0 then r = r .. "W" end
      if not tonumber(op.rex.reg) then r = r .. "R".._gsub(op.rex.reg,"p","") end
      if not tonumber(op.rex.rmb) then r = r .. "B".._gsub(op.rex.rmb,"p","") end
      if not tonumber(op.rex.rmi) then r = r .. "I".._gsub(op.rex.rmi,"p","") end
      pending = "-"
    elseif tonumber(op.rex.r64) or not tonumber(op.rex.reg) or not tonumber(op.rex.rm) then
      if pending then r = r .. pending; pending = nil end; 
      r = r .. "rex"
      if tonumber(op.rex.r64) > 0 then r = r .. "W" end
      if not tonumber(op.rex.reg) then r = r .. "R".._gsub(op.rex.reg,"p","") end
      if not tonumber(op.rex.rm) then
        r = r .. (op.rex.mode == "rr" and "B" or "M").._gsub(op.rex.rm,"p","") 
      end
      pending = "-"
    end
    if op.extopcode ~= "" then 
      if pending then r = r .. pending; pending = nil end; 
      r = r .. op.extopcode
    end
    if r == "" then r = "none" end
  end
  return r
end
function _getPrefixClass(op)
  local pcl = {pref = op.pref, rex = op.rex, extopcode = op.extopcode}
  pcl = unify("prefixclass", pcl, _getPrefixSig)
--  print(dump(pcl))
  op.prefixclass = pcl
  op.pref = nil
  op.rex = nil
  op.extopcode = nil
end

function _getOpClassSig(op)
  return op.isa.sig .. "|" .. op.layout.sig .. "|" .. op.impregs.sig
         .. "|" .. op.prefixclass.sig .. "|" .. op.code.sig
         .. "|" .. _fmt("PAR%2X",op.parm or 0)
         .. "|" .. op.flags.sig
end
function _getOpClass(op)
  local cl = {isa = op.isa, layout = op.layout, impregs = op.impregs,
              prefixclass = op.prefixclass, code = op.code, parm = op.parm, flags = op.flags}
  cl = unify("opclass", cl, _getOpClassSig)
--  print(dump(pcl))
  op.opclass = cl
  op.isa = nil
  op.layout = nil
  op.impregs = nil
  op.prefixclass = nil
  op.code = nil
  op.parm = nil
  op.flags = nil
end

--------------------------------------
--KORW_KKK
--  {isa avx512f; ops KR dst/o, KR src1/i, KR src/i; 
--   pref ; %(VEXDS({"avx",256,"rr"},"S")); extopcode 0f; coding !parsub(0x45) !RRMR($dst,$src)}
--------------------------------------

local _parsePart = {
  isa = _parseISA,
  ops = _parseOps,
  flags = _parseFlags,
  pref = _parsePref,
  extopcode = _parseExtOpcode,
  coding = _parseCoding,
  rex = _parseREX,
  learex = _parseLEAREX,
  rexlea = _parseLEAREX,
  vex = _parseVEX,
  evex = _parseEVEX,
  fold = _parseFold,
  commute = _parseCommute,
}

local function parseLine(name, defs)
  local op= { name = name }
  for part,info in _gmatch(defs,"%s*(%w+)%s+([^;]*);?") do
    info = _match(info, "^%s*(.-)%s*$")
    _assert(_parsePart[part], "invalid option "..part.." in opcode "..name)
    _parsePart[part](op, info)
  end
  if not op.isa then op.isa = unify("isa", {}) end
  if not op.operands then op.operands = {} end
  if not op.flags then op.flags = unify("flags", {}) end
  if not op.pref then op.pref = "" end
  if not op.rex then op.rex = {mode = "rr", reg = "0", rm = "0", r64 = "0"} end
  if not op.extopcode then op.extopcode = "" end
  if not op.code then op.code = {} end
  if op.pref == "66f2" then _assert(not op.rex.vex and not op.rex.evex) end
  --print(1, dump(op))
  _checkOp(op)
  _createLayout(op)
  _renameFromLayout(op)
  _createImpRegs(op)
  _getPrefixClass(op)
  _getOpClass(op)
  return op
end

-----------------------------------------------------------------
--
-----------------------------------------------------------------
--local mode,file= ...

local ops,opset= {},{}
local opclasses = {}

-----------------------------------------------------------------
-- output functions
-----------------------------------------------------------------
inslist= _gsub(inslist, "(//[^\r\n]*)([\r\n])","%2")
inslist= _gsub(inslist, "(/%*.-%*/)","" )

local prevopname
for opname, def in _gmatch(inslist, "%s*([%w_]+)%s*(%b{})") do
  def = _sub(_gsub(def,"[\r\n]"," "),2,-2)
  _assert( not opset[opname],"double definition of "..opname )
  local ok, op = pcall(parseLine,opname, def)
  if (ok) then
    prevopname = opname
    if not op then print("error in parseLine: ", opname, prevopname) end
    ops[ #ops+1 ]= op
    opset[op.name]= op
    --print(_dump(op))
  else
    print(op)
  end
end
for k,v in _pairs(cache.opclass) do
  opclasses[#opclasses + 1] = v
end
-- before avx2: 120 prefix (prefix + rex + extopcode/vex) classes, 60 coding classes
-- total: isa 39, rex 37, code 73, defuse 42, flags 61, layout 96, operands 321, orgnames 40
-- bits: isa 6, rex 6, code 7, defuse 6, flags 7, layout 7 -> sum 39 bits
-- class: to check if op is available): isa + 1 bit per opcodesubmask checking
--        prefix encoding: pref + rex + extopcode
--        main encoding: code
--        data flow: defuse + ???
--   register class is setup at construction time (of the vregs), and as such, not 
--   relevant for def use. instruction constructors do the type checking for us.
--   allocation and data flow is done for virtual and fixed regs.so we have fixed registers to worry about, 

--local 

local function opclassid(op)
  if op.flags.rmem then r = r .. "R" end
  if op.flags.wmem then r = r .. "W" end
  if op.flags.usemem then r = r .. "U" end
  return r
--  if cl.subs[op.sub or 0] then
--      print("class "..(op.class or op.name).." contains the sub "..(op.sub or 0).." multiple times")
end

local function _compareISA(a, b)
  if a == b then return false end
  if (a.x64 or false) ~= (b.x64 or false) then return b.x64 end
  if (a.x32only or false) ~= (b.x32only or false) then return b.x32only end
  if a.sig ~= b.sig then return a.sig < b.sig end
  _assert("comparing ISA failed")
end
_sort(opclasses,function(a,b)
                 if a.isa ~= b.isa then return _compareISA(a.isa, b.isa) end
                 return a.sig < b.sig
               end)
for i,v in _ipairs(opclasses) do v.id = i end

_sort(ops,function(a,b)
                 if a.opclass.isa ~= b.opclass.isa then return _compareISA(a.opclass.isa, b.opclass.isa) end
                 return a.name < b.name
               end)

-----------------------------------------
-- selector
-----------------------------------------
local type2type={
  GR8="T_I8", GR16= "T_I16", GR32= "T_I32", GR64= "T_I64",
  VRI64="T_V1I64", VRI128="T_V2I64", VRI256="T_V4I64", VRI128Z="T_V2I64", VRI512="T_V8I64",
  VR4F32="T_V4F32", VR2F64= "T_V2F64", VR8F32="T_V8F32", VR4F64= "T_V4F64", 
  VR4F32Z="T_V4F32", VR2F64Z= "T_V2F64", VR16F32="T_V16F32", VR8F64 = "T_V8F64",
  BB="T_BBLOCK",
  i8="T_I8", i16= "T_I16", i32= "T_I32", i64= "T_I64",
}
local ptr2type={
  ["i8*"]= "T_I8", ["i16*"]= "T_I16", ["i32*"]= "T_I32", ["i64*"]= "T_I64",
  ["VI16*"]= "T_I16", ["VI32*"]= "T_I32", ["VI64*"]= "T_V1I64", 
  ["VI128*"]= "T_V2I64", ["VI256*"]= "T_V4I64", ["VI512*"]= "T_V8I64",
  ["V2F32*"]= "T_V2F32", ["V4F32*"]= "T_V4F32", ["V2F64*"]= "T_V2F64", 
  ["V8F32*"]= "T_V8F32", ["V4F64*"]= "T_V4F64",
  ["V16F32*"]= "T_V16F32", ["V8F64*"]= "T_V8F64",
  ["f32*"]= "T_F32", ["f64*"]= "T_F64", ["V1F64*"]= "T_F64",
}
--local data = {ops=ops,type2type=type2type,ptr2type=ptr2type,opclasses=opclasses}
local data = {ops=ops,opclasses=opclasses}
-- lib for creating/loading/storing/changing memory blocks

---------------------------------------------
local function _scandump(x, m)
  if _type(x) == "table" then
    if not m[x] then m[#m + 1] = x; m[x] = {0, "l["..#m.."]"};  end
    m[x][1] = m[x][1] + 1
    if (m[x][1] > 1) then return m end
    for k,v in _pairs(x) do
      _scandump(k, m)
      _scandump(v, m)
    end
  end
  return m
end
local function _write_value(f,x,m)
  if _type(x)=="string" then
    f:write(_fmt("%q",x))
    return
  end
  if _type(x)=="table" then
    f:write(m[x] and m[x][2] or "$ERROR")
    return
  end
  f:write(tostring(x))
end
local function _write_index(f,x,m)
  if type(x)=="string" and _match(x, "^[%a_][%w_]*$") then
    f:write(".")
    f:write(x)
    return
  end
  f:write("[")
  _write_value(f,x,m)
  f:write("]")
end
local function writedump(f,x)
  if _type(x) ~= "table" then
    f:write("return ")
    _write_value(f,x)
    f:write("\n")
    return
  end
  local m = _scandump(x, {})
  local i = 0
  -- create refs for all tables
  f:write("local l = {}; for i = 1, "..#m.." do l[i] = {} end\n")
  -- fill fields
  for k,v in _ipairs(m) do
    f:write("do local k = "..m[v][2].."\n")
    for kk,vv in _pairs(v) do
--      f:write(m[v][2])
      f:write("k")
      _write_index(f,kk,m)
      f:write(" = ")
      _write_value(f,vv,m)
      f:write("\n")
    end
    f:write("end\n")
  end
  f:write("return "..m[x][2].."\n")
end
do
  local f = io.open(outname, "w")
  writedump(f, data)
  f:close()
end
