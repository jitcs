--[[
  Template To Source tool:
  Copyright (C) 2013-2014 Dirk Steinke. 
  See copyright and license notice in COPYRIGHT or include/jitcs.h

  This file turns a source template file (*.lh, *.lcpp) into the desired
  source file (*.h, *.cpp). It takes a parameter N, the bitwidth of the
  target architecture. This is mostly for x86, where the templates are shared
  between 32- and 64-bit versions.
]]

local _setfenv, _ipairs, _loadstr = setfenv, ipairs, loadstring
local _open = io.open
local _match, _gmatch, _fmt = string.match, string.gmatch, string.format
local _gsub, _sub = string.gsub, string.sub
local _exec = os.execute

local function readfile(n) 
  local f = _open(n) or _open("tools/"..n)
  local ctnt = f:read("*a")
  f:close()
  return ctnt
end

local srcname, N, dstname = ...
if N == "32" or N == "64" then
else
  N = "32"
end
if (srcname == nil or srcname == "") then 
  srcname = "..\\include\\vmjit_x86_xx_insids.lh"
end
if dstname == nil or dstname == "" then
  dstname = _match(_gsub(srcname, "xx", N), "^(.-)%.[^.\\/]*$") .. ".h"
end

local emblua = _loadstr(readfile("emblua.lua"))
local oldtext = readfile(srcname)
oldtext = _gsub(oldtext, "%$%$%$", N)
local newtext = emblua({type = "string", value = oldtext, file = srcname},{type="string"})

local f = _open(dstname, "w")
f:write(newtext)
f:close()
