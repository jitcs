--[[
  Register List To Data tool for X86:
  Copyright (C) 2013-2014 Dirk Steinke. 
  See copyright and license notice in COPYRIGHT or include/jitcs.h

  This tool is NOT used right now.

  The tool reads src/data/x86_reglist.ltxt and transforms it into 
  src/data/x86_reglist.dat
]]

local _setfenv, _ipairs, _loadstr = setfenv, ipairs, loadstring
local _open = io.open
local _match, _gmatch, _fmt = string.match, string.gmatch, string.format
local _gsub, _sub = string.gsub, string.sub
local _exec = os.execute

--local function readfile(n) 
--  local f = _open(n)
--  local ctnt = f:read("*a")
--  f:close()
--  return ctnt
--end

--local testname, outname = ...
--if (testname == nil or testname == "") then 
--  testname = "..\\evm\\x86\\src\\_x86_insalias.ltxt"
--end
--if outname == nil or outname == "" then
--  outname = _match(testname, "^(.-)%.[^.\\/]*$") .. ".dat"
--end

--local emblua = _loadstr(readfile("emblua.lua"))
--local aliaslist = emblua(testname,{type="string"})

--f= ...
--
--f= f:gsub( "(//[^\r\n]*)([\r\n])","%2" )
--f= f:gsub( "(/%*.-%*/)","" )
--
--local mode, lines, data = "", {}, {}
--for l in f:lines() do
--  l= l:trim()
--  if (l~="") then
--    local m = l:match("^:(%w+)$")
--    if m then
--      mode = m
--    else
--      local ll = lines[mode] or {}
--      ll[#ll + 1] = l
--      lines[mode] = ll
--    end
--  end
--end

-----------------------------------------
local barrier = {}
barrier.start = function() return {false,false} end
barrier.stop = function(b)
  if b[1] or b[2] then
    print("  #endif")
  end
end
barrier.continue2 = function(b,newx32,newx64)
  if (b[1] and not newx64) or (b[2] and not newx32) then
    print("  #endif")
    b = {false,false}
  end
  if not b[1] and newx64 then
    print("  #ifdef EVM_64")
    return {true,false}
  end
  if not b[2] and newx32 then
    print("  #ifndef EVM_64")
    return {false,true}
  end
  return b
end
barrier.continue = function(b,op)
  return barrier.continue2(b,op.x32only,op.x64)
end
-----------------------------------------
-- selector
-----------------------------------------

local data = {}
data.classes = {
  {name = "FLAGSR", resclass = "FLAGRSC", subres = "0x01", regbase = 1},
  {name = "GR8L", partial = "true", subres = "0x01", resclass = "GRRSC", restrictres = "0-3", regbase = 32},
  {name = "GR8H", partial = "true", subres = "0x02", resclass = "GRRSC", restrictres = "0-3", regbase = 36},
  {name = "GR16", partial = "true", subres = "0x03", resclass = "GRRSC", regbase = 64},
  {name = "GR32", subres = "0x07", resclass = "GRRSC", regbase = 16},
  {name = "VR128", subres = "0x01", resclass = "VRRSC", regbase = 96, restrictres_64 = "0-31"},
  {name = "VR256", subres = "0x03", resclass = "VRRSC", regbase = 128, restrictres_64 = "0-31"},
  {name = "VR512", subres = "0x07", resclass = "VRRSC", regbase = 160, restrictres_64 = "0-31"},
  {name = "GR8X", partial = "true", subres = "0x01", x64 = true, resclass = "GRRSC", restrictres = "4-15", regbase = 48},
  {name = "GR64", subres = "0x07", x64 = true, resclass = "GRRSC", regbase = 80},
}
data.classaliases = {
  {name = "GR", dest = "GR32", x32 = true},
  {name = "GR", dest = "GR64", x64 = true},
}
data.resources = {
  {name = "GRRSC", cnt = 8, resbits = 4, x32 = true},
  {name = "VRRSC", cnt = 8, resbits = 4, x32 = true},
  {name = "GRRSC", cnt = 16, resbits = 4, x64 = true},
  {name = "VRRSC", cnt = 32, resbits = 4, x64 = true},
  {name = "FLAGRSC", cnt = 1, resbits = 1},
}
data.registers = {
  {name = "FLAGS", class = "FLAGSR", id = 1, refclass = "FLAGS", resindex = 0},
}
local r = data.registers
for k,v in pairs({ A = 0, C = 1, D = 2, B = 3}) do
  r[#r+1] = {name = k.."L", id = 32+v, class = "GR8L", refclass = "GR8", resindex = v}
  r[#r+1] = {name = k.."H", id = 32+4+v, class = "GR8H", refclass = "GR8", resindex = v}
  r[#r+1] = {name = k.."X", id = 64+v, class = "GR16", refclass = "GR16", resindex = v}
  r[#r+1] = {name = "E"..k.."X", id = 16+v, class = "GR32", refclass = "GR32", resindex = v}
  r[#r+1] = {name = "R"..k.."X", id = 80+v, class = "GR64", refclass = "GR64", resindex = v, x64 = true}
end
for k,v in pairs({ SP = 4, BP = 5, SI = 6, DI = 7}) do
  r[#r+1] = {name = k.."L", id = 48+v, class = "GR8X", refclass = "GR8", resindex = v, x64 = true}
  r[#r+1] = {name = k, id = 64+v, class = "GR16", refclass = "GR16", resindex = v}
  r[#r+1] = {name = "E"..k, id = 16+v, class = "GR32", refclass = "GR32", resindex = v}
  r[#r+1] = {name = "R"..k, id = 80+v, class = "GR64", refclass = "GR64", resindex = v, x64 = true}
end
for v = 0,7 do
  r[#r+1] = {name = "XMM"..v, namemask = "XMM%d", id = 96+v, class = "VR128", refclass = "VR128", resindex = v}
  r[#r+1] = {name = "YMM"..v, namemask = "YMM%d", id = 128+v, class = "VR256", refclass = "VR256", resindex = v}
  r[#r+1] = {name = "ZMM"..v, namemask = "ZMM%d", id = 160+v, class = "VR512", refclass = "VR512", resindex = v}
end
for v = 8,31 do
  if v < 16 then
    r[#r+1] = {name = "R"..v.."L", namemask = "R%dL", id = 48+v, class = "GR8X", refclass = "GR8", resindex = v, x64 = true}
    r[#r+1] = {name = "R"..v.."W", namemask = "R%dW", id = 64+v, class = "GR16", refclass = "GR16", resindex = v, x64 = true}
    r[#r+1] = {name = "R"..v.."D", namemask = "R%dD", id = 16+v, class = "GR32", refclass = "GR32", resindex = v, x64 = true}
    r[#r+1] = {name = "R"..v, namemask = "R%d", id = 80+v, class = "GR64", refclass = "GR64", resindex = v, x64 = true}
  end
  r[#r+1] = {name = "XMM"..v, namemask = "XMM%d", id = 96+v, class = "VR128", refclass = "VR128", resindex = v, x64 = true}
  r[#r+1] = {name = "YMM"..v, namemask = "YMM%d", id = 128+v, class = "VR256", refclass = "VR256", resindex = v, x64 = true}
  r[#r+1] = {name = "ZMM"..v, namemask = "ZMM%d", id = 160+v, class = "VR512", refclass = "VR512", resindex = v, x64 = true}
end
table.sort(r,function(a,b)
                 if a.x64 ~= b.x64 then return b.x64 end
                 if a.x32 ~= b.x32 then return b.x32 end
                 return a.id<b.id
               end)

data.registeraliases = {
}
r = data.registeraliases
for k,v in pairs({ A = 0, C = 1, D = 2, B = 3}) do
  r[#r+1] = {name = "R"..v.."L", dest = k.."L"}
  r[#r+1] = {name = "R"..v.."H", dest = k.."H"}
  r[#r+1] = {name = "R"..v.."W", dest = k.."X"}
  r[#r+1] = {name = "R"..v.."D", dest = "E"..k.."X"}
  r[#r+1] = {name = "R"..v, dest = "R"..k.."X", x64 = true}
  r[#r+1] = {name = "R"..v, dest = "E"..k.."X", x32 = true}
  r[#r+1] = {name = "R"..k.."X", dest = "E"..k.."X", x32 = true}
end
for k,v in pairs({ SP = 4, BP = 5, SI = 6, DI = 7}) do
  r[#r+1] = {name = "R"..v.."L", dest = k.."L", x64 = true}
  r[#r+1] = {name = "R"..v.."W", dest = k}
  r[#r+1] = {name = "R"..v.."D", dest = "E"..k}
  r[#r+1] = {name = "R"..v, dest = "R"..k, x64 = true}
  r[#r+1] = {name = "R"..v, dest = "E"..k, x32 = true}
  r[#r+1] = {name = "R"..k, dest = "E"..k, x32 = true}
end
table.sort(r,function(a,b)
                 if a.x64 ~= b.x64 then return b.x64 end
                 if a.x32 ~= b.x32 then return b.x32 end
                 return a.name<b.name
               end)
data.refclasses = {
  {name = "GR8", classes = {"GR8L", "GR8H", "GR8X"}, dontalloc="4-7", defsz = 1},
  {name = "GR16", classes = {"GR16", "GR32", "GR64"}, superrefclasses = {"GR32", "GR64"}, defsz = 2},
  {name = "GR32", classes = {"GR32", "GR64"}, superrefclasses = {"GR64"}, defsz = 4},
  {name = "VR128", classes = {"VR128", "VR256","VR512"}, superrefclasses = {"VR256","VR512"}, defsz = 16},
  {name = "VR256", classes = {"VR256","VR512"}, superrefclasses = {"VR512"}, defsz = 32},
  {name = "VR512", classes = {"VR512"}, defsz = 64},
  {name = "GR64", x64 = true, classes = {"GR64"}, defsz = 8},
  {name = "FLAGS", classes = {"FLAGSR"}, defsz = 0},
}
data.refclassaliases = {
  {name = "GR", dest = "GR32", x32 = true},
  {name = "GR", dest = "GR64", x64 = true},
}
data.barrier = barrier

return data
