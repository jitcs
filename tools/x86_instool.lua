--[[
  Instruction Tool for X86:
  Copyright (C) 2013-2014 Dirk Steinke. 
  See copyright and license notice in COPYRIGHT or include/jitcs.h

  The tool reads instruction files from tests/x86/*.ins, and transforms them
  into _bin/data/*.as files for use in the unit test program.
]]

local _setfenv, _ipairs = setfenv, ipairs
local _open = io.open
local _match, _gmatch, _fmt = string.match, string.gmatch, string.format
local _gsub, _sub = string.gsub, string.sub
local _exec = os.execute

local function readfile(n) 
  local f = _open(n)
  local ctnt = f:read("*a")
  f:close()
  return ctnt
end

local validModes = {ins2nasm32 = true, ins2nasm64 = true, nasm2as32 = true, nasm2as64 = true}

local MODE, insfile, p1, p2 = ...
MODE = string.lower(MODE or "")
assert(validModes[MODE], "mode missing")
assert(insfile, "insfile missing")
local nasmfile, nasmlstfile, asfile
if MODE == "ins2nasm32" or MODE == "nasm2as32" then
  N = 32
else
  N = 64
end
if MODE == "ins2nasm32" or MODE == "ins2nasm64" then
  ALG = "ins2nasm"
  nasmfile = p1
  assert(nasmfile, ".nasm file not given")
else
  ALG = "nasm2as"
  nasmlstfile, asfile = p1, p2
  assert(nasmlstfile and asfile, ".lst and .as file not given")
end

local prog, progcode = ""
local progstats = { scopes = {} }
local errors = false
local lineno = 1
for l in _gmatch(readfile(insfile), "([^\r\n]*)[\r\n]") do
--print(l)
  if _match(l, "^|") then
    if (_match(l, "^|%s*[Ff][Oo][Rr]%s*(%w+)%s*=%s*%[([^]]-)%]%s*[Dd][Oo]%s*$"))
    then
      local var, list = _match(l, "^|%s*[Ff][Oo][Rr]%s*(%w+)%s*=%s*%[([^]]-)%]%s*[Dd][Oo]%s*$")
      local list2, list3 = {}, nil
      for e in _gmatch(list, "([^,]+)") do
        local e2 = _match(e, "^%s*(%S+)%s*$")
        if e2 then 
          list2[#list2 + 1] = e2; 
          list3 = (list3 and (list3..", ") or "").._fmt("%q", e2) 
        end
      end
      if #list2 > 0 then 
        local sn = #progstats.scopes
        progstats.scopes[sn+1] = {type = "for", start = lineno, 
                                var = var, list = list2}
         
        prog = prog .. "for k"..sn..",v"..sn.." in ipairs({"..list3.."}) do\n"
        prog = prog .. "  ENV."..var.." = v"..sn.."\n"
      else
        errors = true
        print(lineno .. ": illegal list in for statement")
      end
      

    elseif (_match(l,"^|%s*[Ee][Nn][Dd][Ff][Oo][Rr]%s*$")) then
      local sn = #progstats.scopes
      if (sn > 0 and progstats.scopes[sn].type == "for") then
        progstats.scopes[sn] = nil
        prog = prog .. "end\n"
      else
        errors = true
        print(lineno .. ": endfor without for scope")
      end
    elseif (_match(l,"^|%s*[Ll][Ee][Tt]%s*(%w+)%s*=%s*(.-)%s*$"))
    then
      local var, val = _match(l, "^|%s*[Ll][Ee][Tt]%s*(%w+)%s*=%s*(.-)%s*$")
      prog = prog .. _fmt("ENV.%s = ENV:handleExpr(%q, %q, %d)\n", var, val, l, lineno)
    else
      errors = true
      print(lineno .. ": illegal commandline "..l)
    end
  elseif _match(l,"^%s*$") then
    -- ignore empty lines
  else
    prog = prog .. _fmt("ENV:handleLine(%q, %d)",l, lineno) .. "\n"
  end
  lineno = lineno + 1
end
if (#progstats.scopes > 0) then
  errors = true
  print(lineno .. ": not all scopes have been closed")
end
if errors then return end
progcode = loadstring(prog)
if not progcode then
  print("cannot compile program")
  return
end
local ENV = {}
function ENV.handleLine(self, line, lineno)
  local function replacer(expr)
    local repfnc = loadstring("return "..expr)
    if not repfnc then
      errors = true
      print(lineno .. ": illegal expression "..expr)
      return "ERROR"
    else
      _setfenv(repfnc, self)
      return repfnc() or ""
    end
  end
  local function filter(expr)
    local repfnc = loadstring("return "..expr)
    if not repfnc then
      errors = true
      print(lineno .. ": illegal expression "..expr)
      return "??"
    else
      _setfenv(repfnc, self)
      return repfnc() and "" or "??"
    end
  end
  line = _gsub(line, "$(%b())", replacer)
  
  if _match(line, "^(%d+):") then
    local m = _match(line, "^(%d+):")
    if m ~= "32" and m ~= "64" then
      errors = true
      print(lineno .. ": illegal architecture "..m)
      return
    else
      if self.arch ~= m then return end
      line = _sub(line, #m + 2)
    end
  end
  line = _gsub(line, "%?(%b())", filter)
  if (_sub(line,1,2) == "??") then return end
  if self.tgt == "nasm" then
    line = _gsub(line, "%b{}", "")
    line = _gsub(line, "[<>]", "")
  end
  if self.tgt == "jitcs" then
    line = _gsub(line, "%b<>", "")
    line = _gsub(line, "[{}]", "")
  end
  
  self:write(line)
end
function ENV.handleExpr(self, line, lineno)
  local function replacer(expr)
    local repfnc = loadstring("return "..expr)
    if not repfnc then
      errors = true
      print(lineno .. ": illegal expression "..expr)
      return "ERROR"
    else
      _setfenv(repfnc, self)
      return repfnc() or ""
    end
  end
  line = _gsub(line, "$(%b())", replacer)
  return line
end
function ENV.write(self, line)
  if self.tgt == "jitcs" and self.lst and _sub(line,1,1) ~= ":" and not _match(line,"^%s*$") then
    self.lstpos = self.lstpos + 1
    line = line .. "    ==> " .. (self.lst[self.lstpos] or "")
  end
  if self.file then
    self.file:write(line)
    self.file:write("\n")
  else
    print(line)
  end
end
function ENV.X86(Y)
  return tonumber(Y) == 64 and "64:" or ""
end
function ENV.MSZ(Y)
  if Y == "DEF" then return "" end
  return Y
end
function ENV.MVSZ(M)
  if M == "S" then return "32" end
  if M == "D" then return "64" end
  return ""
end
function ENV.MVHSZ(Y, D)
  if tonumber(Y) == 128 or tonumber(Y) == 256 or tonumber(Y) == 512 then 
    return tostring(tonumber(Y) / (D and tonumber(D) or 2))
  end
  print("VSZ error", Y)
  return "ERROR"
end
function ENV.RSZ(Y)
  if Y == "DEF" then return "R" end
  if tonumber(Y) == 8 then return "B" end
  if tonumber(Y) == 16 then return "H" end
  if tonumber(Y) == 32 then return "W" end
  if tonumber(Y) == 64 then return "D" end
  print("RSZ error", Y)
  return "ERROR"
end
function ENV.VSZ(Y)
  if tonumber(Y) == 128 then return "X" end
  if tonumber(Y) == 256 then return "Y" end
  if tonumber(Y) == 512 then return "Z" end
  print("VSZ error", Y)
  return "ERROR"
end
function ENV.VHSZ(Y)
  if tonumber(Y) == 128 then return "X" end
  if tonumber(Y) == 256 then return "X" end
  if tonumber(Y) == 512 then return "Y" end
  print("VHSZ error", Y)
  return "ERROR"
end
function ENV.VOSZ(Y,N)
  if tonumber(Y) == tonumber(N) then return "" end
  return tostring(N)
end
function ENV.RSZ2(Y)
--  if Y == "DEF" then return "ERROR" end
  if tonumber(Y) == 8 then return "B" end
  if tonumber(Y) == 16 then return "W" end
  if tonumber(Y) == 32 then return "D" end
  if tonumber(Y) == 64 then return "Q" end
  return "ERROR"
end
function ENV.NASMSZ(Y)
  if Y == "DEF" then Y = ENV.arch end
  if tonumber(Y) == 8 then return "byte" end
  if tonumber(Y) == 16 then return "word" end
  if tonumber(Y) == 32 then return "dword" end
  if tonumber(Y) == 64 then return "qword" end
  if tonumber(Y) == 128 then return "oword" end
  if tonumber(Y) == 256 then return "yword" end
  return "ERROR"
end
function ENV.MAX(A,B)
  if A == "DEF" then A = ENV.arch end
  if B == "DEF" then B = ENV.arch end
  return tonumber(A) > tonumber(B) and A or B
end
ENV.RNAME_Table_R2R = {ECX = "RCX", EAX = "RAX", EBX = "RBX", EDX = "RDX",
                       YMM0 = "XMM0", YMM1 = "XMM1", YMM2 = "XMM2", YMM3 = "XMM3"}
ENV.RNAME_Table_R2N = {
  RAX = {["8"] = "AL", ["16"] = "AX", ["32"] = "EAX", ["64"]= "RAX"},
  RBX = {["8"] = "BL", ["16"] = "BX", ["32"] = "EBX", ["64"]= "RBX"},
  RCX = {["8"] = "CL", ["16"] = "CX", ["32"] = "ECX", ["64"]= "RCX"},
  RDX = {["8"] = "DL", ["16"] = "DX", ["32"] = "EDX", ["64"]= "RDX"},
}
function ENV.RCONST(I, Y)
  if Y == "DEF" then Y = ENV.arch end
  return math.mod(I, math.pow(2,math.min(tonumber(Y),32)))
end
function ENV.RNAME(R, SZ)
  if SZ == "DEF" then SZ = ENV.arch end
  R = ENV.RNAME_Table_R2R[R] or R
  return ENV.RNAME_Table_R2N[R][tostring(SZ)] or "ERROR"
end
function ENV.RNAMEDEF(R, SZ)
  if SZ == "DEF" then SZ = 64 end
  R = ENV.RNAME_Table_R2R[R] or R
  return ENV.RNAME_Table_R2N[R][tostring(SZ)] or "ERROR"
end
ENV.VNAME_Table_R2N = {
  XMM0 = {["64"] = "XMM0", ["128"] = "XMM0", ["256"] = "YMM0", ["512"] = "ZMM0"},
  XMM1 = {["64"] = "XMM1", ["128"] = "XMM1", ["256"] = "YMM1", ["512"] = "ZMM1"},
  XMM2 = {["64"] = "XMM2", ["128"] = "XMM2", ["256"] = "YMM2", ["512"] = "ZMM2"},
  XMM3 = {["64"] = "XMM3", ["128"] = "XMM3", ["256"] = "YMM3", ["512"] = "ZMM3"},
  XMM4 = {["64"] = "XMM4", ["128"] = "XMM4", ["256"] = "YMM4", ["512"] = "ZMM4"},
}
function ENV.VNAME(R, SZ)
  if SZ == "DEF" then SZ = ENV.arch end
  R = ENV.RNAME_Table_R2R[R] or R
  return ENV.VNAME_Table_R2N[R][tostring(SZ)] or "ERROR"
end
function ENV.VHNAME(R, SZ)
  if SZ == "DEF" then SZ = ENV.arch end
  R = ENV.RNAME_Table_R2R[R] or R
  return ENV.VNAME_Table_R2N[R][tostring(tonumber(SZ)/2)] or "ERROR"
end

local function readnasmlist(v)
  local result = {}
  for l in _gmatch(readfile(v), "([^\r\n]*)[\r\n]") do
    local line, addr, opcodes, cmd = _match(l,
       "^([ %d][ %d][ %d][ %d][ %d][ %d])"
       .. " ([ %x][ %x][ %x][ %x][ %x][ %x][ %x][ %x])"
       .. " (%x*)"
       .. "%s*(.-)%s*$")
    if opcodes and #opcodes > 0 then result[#result + 1] = opcodes end
  end
  return result
end

ENV.arch = tostring(N)
ENV.ipairs = _ipairs
ENV.ENV = ENV
_setfenv(progcode, ENV)
if ALG == "ins2nasm" then
  ENV.file = _open(nasmfile, "w")
  ENV.tgt = "nasm"
  ENV:write(" bits " .. ENV.arch)
  if N == 32 then
    ENV:write("%define RAX EAX")
    ENV:write("%define RBX EBX")
    ENV:write("%define RCX ECX")
    ENV:write("%define RDX EDX")
  end
  progcode()
  ENV:write("  ret")
  ENV.file:close()
elseif ALG == "nasm2as" then
  local lst = readnasmlist(nasmlstfile)

  ENV.file = _open(asfile, "w")
  ENV.tgt = "jitcs"
  ENV:write("<== test")
  ENV:write("@fun: void -> void")
  ENV:write("@strategy: direct")
  ENV:write(":entry")
  ENV.lst = lst
  ENV.lstpos = 0
  progcode()
  ENV:write("  ret |")
  if ENV.lstpos ~= #ENV.lst then
    print("length difference between nasm and jitcs versions ("..N.."bit)")
  end
  ENV.lst = nil
  ENV.file:close()
end
