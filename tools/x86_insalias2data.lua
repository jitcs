--[[
  Instruction Alias To Data tool for X86:
  Copyright (C) 2013-2014 Dirk Steinke. 
  See copyright and license notice in COPYRIGHT or include/jitcs.h

  The tool reads src/data/x86_insalias.ltxt and transforms it into 
  src/data/x86_insalias.dat.
]]

local _setfenv, _ipairs, _loadstr = setfenv, ipairs, loadstring
local _open = io.open
local _match, _gmatch, _fmt = string.match, string.gmatch, string.format
local _gsub, _sub = string.gsub, string.sub
local _exec = os.execute

local function readfile(n) 
  local f = _open(n) or _open("tools/"..n)
  local ctnt = f:read("*a")
  f:close()
  return ctnt
end

local testname, outname = ...
if (testname == nil or testname == "") then 
  testname = "..\\src-lib\\x86\\src\\_x86_insalias.ltxt"
end
if outname == nil or outname == "" then
  outname = _match(testname, "^(.-)%.[^.\\/]*$") .. ".dat"
end

local emblua = _loadstr(readfile("emblua.lua"))
local aliaslist = emblua(testname,{type="string"})

local function rep_r(c) 
  if c == "R" then return "R" end
  if c == "M" then return "M" end
  if c == "m" then return "M" end
end
local function rep_w(c) 
  if c == "R" then return "W" end
  if c == "M" then return "M32" end
  if c == "m" then return "M" end
end
local function rep_d(c) 
  if c == "R" then return "D" end
  if c == "M" then return "M64" end
  if c == "m" then return "M" end
end
local aliases, aliases32, aliases64 = {}, {}, {}
for l in _gmatch(aliaslist, "([^\r\n]*)[\r\n]") do
  if _match(l, "^%s*//") then
    --
  else
    if _match(l, "%$([RMm])") then
      l = _gsub(l, "%$([RMm])", rep_r)
          .. " -> " .. _gsub(l, "%$([RMm])", rep_w)
          .. ", " .. _gsub(l, "%$([RMm])", rep_d)
    end
    local l,r32,r64 = l:match("^%s*(%S+)%s*%->%s*(%S+)%s*,%s*(%S+)%s*$")
    if l and r32 and r64 then 
      if r32 == r64 and r32 ~= "INVALID" then 
        aliases[#aliases+1] = {l,r32}; 
      else
        if r32 ~= "INVALID" then aliases32[#aliases32+1] = {l,r32}; end
        if r64 ~= "INVALID" then aliases64[#aliases64+1] = {l,r64}; end
      end 
    end
  end
end
local f = _open(outname, "w")
f:write("return {\n")
f:write("aliases = {\n")
for k,v in _ipairs(aliases) do
  f:write(_fmt("  {%q, %q},\n", v[1], v[2]))
end
f:write("}, aliases32 = {\n")
for k,v in _ipairs(aliases32) do
  f:write(_fmt("  {%q, %q},\n", v[1], v[2]))
end
f:write("}, aliases64 = {\n")
for k,v in _ipairs(aliases64) do
  f:write(_fmt("  {%q, %q},\n", v[1], v[2]))
end
f:write("}}\n")
f:close()
