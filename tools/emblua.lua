--[[
  Embedded Lua tool:
  Copyright (C) 2013-2014 Dirk Steinke. 
  See copyright and license notice in COPYRIGHT or include/jitcs.h

  This tool allows the execution of embedded lua code in a source file
  template to generate source code tables, functions or classes from 
  data descriptions. See *.lh, *.lcpp and *.ltxt files to see how the
  embedding looks like.
  This tool is not used directly in the build chain, but as part of the other
  tools in this directory.
]]
----------------------------------------------------
local _setfenv, _ipairs, _pairs, _select, _tostr = setfenv, ipairs, pairs, select, tostring
local _loadstr, _pcall, _type = loadstring, pcall, type
local _open, _write, _err, _rerouteoutput = io.open, io.write, io.stderr, io.output
local _flush, _filelines = io.flush, io.lines
local _match, _gmatch, _fmt, _upper = string.match, string.gmatch, string.format, string.upper
local _gsub, _sub, _byte = string.gsub, string.sub, string.byte
local _exec, _getenv = os.execute, os.getenv
local _cat = table.concat

local _filesep = _match(_getenv("OS"),"Windows") and "\\" or "/"

local function _existfile(n)
  local f = _open(n)
  if not f then return false end
  f:close()
  return true
end

local function _readfile(n) 
  local f = _open(n)
  local ctnt = f:read("*a")
  f:close()
  return ctnt
end
local function _writefile(n, c) 
  local f = _open(n, "w")
  f:write(c)
  f:close()
end

local function _strlines(self)
  local me, ix = self[1], self[2]
  local n = #me
  if ix > n then return nil end
  local ix2 = ix
  local c = _byte(me, ix2)
  while c ~= 13 and c ~= 10 and ix2 <= n do
     ix2 = ix2 + 1
     c = _byte(me, ix2)
  end
  local ix3 = ix2
  while ix3 <= n and _byte(me, ix3) == 13 do ix3 = ix3 + 1 end
  if ix3 <= n and _byte(me, ix3) == 10 then ix3 = ix3 + 1 end
  self[2] = ix3
  return _sub(me, ix, ix2 - 1)
end
local function strlines(str) return _strlines,{str,1} end

local function stream_valid_input(s)
  if type(s) == "string" then return _existfile(s) end
  if type(s) == "table" and s.type == "file" then return _existfile(s.value) end
  if type(s) == "table" and s.type == "string" then return true end
  return false
end
local function _stream_filename(s)
  if type(s) == "string" then return s or "" end
  if type(s) == "table" and s.type == "file" then return s.value or "" end
  if type(s) == "table" and s.type == "string" then return s.file or "" end
  return ""
end

local function stream_lines(s)
  if type(s) == "string" then return _filelines(s) end
  if type(s) == "table" and s.type == "file" then return _filelines(s.value) end
  if type(s) == "table" and s.type == "string" then return strlines(s.value) end
  return nil, nil
end
local function _nil_output_stream()
  return { 
    close = function(self) end,
    write = function(self, c) end,
  }
end
local function _file_output_stream(s)
  return { 
    _f = _open(s, "w"), 
    close = function(self) self._f:close() end,
    write = function(self, c) self._f:write(c) end,
  }
end
local function _string_output_stream(t)
  return { 
    _ref = t,
    _t = {},
    _tt = {},
    _flush = function(self)
      if #self._tt == 0 then return end
      local t = self._t;
      t[#t + 1] = _cat(self._tt, "")
      self._tt = {}
    end,
    close = function(self) self:_flush(); self._ref.value = _cat(self._t, "") end,
    write = function(self, c) 
      local tt = self._tt; 
      tt[#tt + 1] = c; 
      if #self._tt >= 32 then self:_flush() end
    end,
  }
end
local function stream_output(s)
  if type(s) == "string" then return _file_output_stream(s) end
  if type(s) == "table" and s.type == "file" then return _file_output_stream(s.value) end
  if type(s) == "table" and s.type == "string" then return _string_output_stream(s) end
end

local myprint,myprintf= print,printf
local print_outstream = nil
function _printwrite(s)
  if print_outstream then
    print_outstream:write(s)
    return
  end
  _write(s)
end
function print( ... )
  local n= _select( "#",... )
  for i=1,n do 
    _printwrite( _tostr(_select(i,...)) ) 
    if i<n then _printwrite( "\t" ) end
  end
  _printwrite( "\n" )
end
function printf( ... )
--print( ... )
  _printwrite( _fmt( ... ) )
  _printwrite( "\n" )
end

function readfile( s )
  return _readfile(FILEDIR.._gsub(s, "[\\/]", _filesep))   
end
function runfile( s,... )
  local d= readfile( s )
  local code= _loadstr( d,s )
  return code(...)
end

local function run(script, srcfile, params)
  local fun,msg= _loadstr(script)
  local doneok
  if not fun then
     _err:write( msg,"\n" )
  else
     doneok,msg= _pcall( fun, srcfile, params )
     if not doneok then
        _err:write( msg,"\n" )
     else
        return true
     end
  end
  return false
end

local function varprefix(vardelim)
  return _gsub(vardelim,".","%%%0")
end
local function varpattern(vardelim)
  return varprefix(vardelim).."(%b())"
end
local function preidpattern()
  return "([%w_]+)"
end
local function idpattern()
  return "([%a_][%w_]*)"
end
local function varpattern2(vardelim)
  return varprefix(vardelim)..idpattern()
end

local function getidents(s,vardelim)
  local result={}
  local nonempty= false
  local ignore = {["not"]=true,["or"]=true,["and"]=true,["false"]=true,["true"]=true,["nil"]=true}
  for id in _gmatch(s, varpattern2(vardelim)) do
    if not ignore[id] then
      result[id]= true
      nonempty = true
    end
  end
  for b in _gmatch(s, varpattern(vardelim)) do
    local bb= _sub(b, 2,-2)
    for id in _gmatch(bb, preidpattern()) do
      if _match(id, "^"..idpattern().."$") and not ignore[id] then
        result[id]= true
        nonempty = true
      end
    end
  end
  return nonempty and result
end
local function identsparam(idents)
  local result= ""
  for k,v in _pairs(idents) do 
    result= result..k.."="..k..","
  end
  return "{"..result.."}"
end
local function generateXYZPrint(vardelim)
  return _fmt( [[
local _gsub, _sub, _loadstr, _setfenv, _tostr, _err = string.gsub, string.sub
local _loadstr, _setfenv, _tostr, _err = loadstring, setfenv, tostring, io.stderr
function _xyz_print( line,vars )
  line= _gsub(line,"%s",vars)
  line= _gsub(line,"(%s)",
          function(pattern,x)
            local fx,gx= _loadstr("return ".._sub(x,2,-2))
            if not fx then
              _err:write( "error in pattern "..pattern.." : "..gx.."\n" )
            end
            _setfenv(fx,vars)
            local xnew= fx()
            return _tostr(xnew)
          end)
  print( line )
end
]],varpattern2(vardelim),varpattern(vardelim))
end

local function main(SELF, IN, OUT, SEP, PARAMS)
  if _type(IN) == "string" then IN = {type = "file", value = IN} end
  if _type(OUT) == "string" then OUT = {type = "file", value = OUT} end
  if not stream_valid_input(IN) then
    printf( [[
usage: %s IN [OUT=IN.out] [SEP=|] [PARAMS=]
convert lua interface file IN into C source code file OUT
lua script lines start with separator SEP, defaults to |
PARAMS may denote a file containing further data 
]],SELF )
    return
  end
  FILEPOS= _stream_filename(IN)
  if not (_type(OUT) == "table" and OUT.type == "string") then 
    local OUTN = _stream_filename(OUT)
    if OUTN == "" or _sub(OUTN, 1, 1) == "." then
      if FILEPOS == "" then
        OUT = {type = "string"}
      else
        if OUTN == "" then OUTN = ".out" end
        OUT = {type = "file", value = _match(FILEPOS, "^(.-)%.[^.\\/]*$") .. OUTN}
      end
    end
  end
  assert(FILEPOS == "" or FILEPOS ~= _stream_filename(OUT))
  local prefix= SEP or "|"
  FILEDIR= ""
  if _match(FILEPOS, "[/\\]" ) then 
    FILEDIR= _match(FILEPOS, [[^(.-[/\])[^/\]*$]] )
  end

  local prefixlist = {}
  for line in stream_lines(IN) do
    local handled= false
    local x= _match(line, "^#/.-/emblua(.-)$")
    if (x) then
      --title line with settings
      for var,val in _gmatch(x, idpattern().."=(%S+)") do
        if _upper(var) == "LUAPREFIX" then
          prefixlist[#prefixlist + 1] = val
        else
          _err:write( "unknown setup variable "..var.." in line 1" )
        end
      end
      handled= true
    end
    break
  end
  if #prefixlist == 0 then prefixlist = {prefix} end

  local inputfile = IN
  
  for k,prefix in _ipairs(prefixlist) do
printf("running pass %d of %d, prefix %s",k,#prefixlist,prefix)
    local paramsfile = PARAMS
    outputfile = #prefixlist == k and OUT or {type="string"}
    local first= true
    local vardelim= "$"
    local comment= "//"
    local currentscript= ""
    local needvarreplfun= false
    local printing = false
  
    for line in stream_lines(inputfile) do
      local handled= false
      if first then
        local x= _match(line, "^#/.-/emblua(.-)$")
        if (x) then
          handled= true
        end
        first= false
      end
      if not handled and not printing then
        local pfx, var, val= _match(line, "^%s*(%S+)%s*%-%-%s*"..idpattern().."%s*=%s*(%S+)%s*$" )
        if pfx and pfx == prefix then
          if _upper(var) == "VARDELIM" then
            vardelim= val
          elseif _upper(var)=="CMTDELIM" then
            comment= val
          elseif _upper(var)=="DUMPSCRIPT" then
            dumpscript=true
          end
          handled = true
        end
      end

      local firstword, remainder= _match(line, "^%s*(%S+)(.*)$")
      if not handled and firstword==prefix then
         -- scriptline
         currentscript= currentscript..remainder.."\n"
         handled= true
         printing = true
      end
      if not handled then
         local idents= getidents(line,vardelim)
         local quoteline = _fmt("%q",line)
         local newline
         if idents then
            needvarreplfun= true
            local parms= identsparam(idents)
            newline= "_xyz_print("..quoteline..","..parms..")"
         else
            newline= "print("..quoteline..")"
         end
         currentscript= currentscript..newline.."\n"
         printing = true
      end
    end
    if needvarreplfun then
      currentscript= generateXYZPrint(vardelim).."local SRCFILE,PARAMS=...;\n"..currentscript
    end
--print( "RUN" )

    local params = {}
    if paramsfile and paramsfile ~= "" then
      local paramsfile = readfile(paramsfile)
      if paramsfile then
        local paramsdatafnc = _loadstr("return "..paramsfile)
        if paramsdatafnc then
          local paramsdata = paramsdatafnc()
          if _type(paramsdata) == "table" then
            params = paramsdata
          end
        end
      end
    end

    if dumpscript and _stream_filename(outputfile) ~= "" then
      _writefile(_stream_filename(outputfile)..".script",currentscript )
    end

    print_outstream = stream_output(outputfile)
    if k == #prefixlist then
      if _stream_filename(IN) ~= "" then
        printf(comment.." DO NOT EDIT. this file was autogenerated from %s",_stream_filename(IN))
      else
        print(comment.." DO NOT EDIT. this file was autogenerated")
      end
    end
--print( currentscript )
    run(currentscript, paramsfile, params)
--print( "DONE" )
    print_outstream:close()
    print_outstream = nil
    inputfile = outputfile
  end
  return OUT.value
end

return main(arg[0], ...)
